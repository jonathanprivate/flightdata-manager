using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace FlightDataManager.Properties
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
	[DebuggerNonUserCode]
	[CompilerGenerated]
	public class Resources
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public static ResourceManager ResourceManager
		{
			get
			{
				if (resourceMan == null)
				{
					resourceMan = new ResourceManager("FlightDataManager.Properties.Resources", typeof(Resources).Assembly);
				}
				return resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public static CultureInfo Culture
		{
			get
			{
				return resourceCulture;
			}
			set
			{
				resourceCulture = value;
			}
		}

		public static byte[] Anafi_Feet_Template_for_Garmin_Virb => (byte[])ResourceManager.GetObject("Anafi_Feet_Template_for_Garmin_Virb", resourceCulture);

		public static byte[] Anafi_Meter_Template_for_Garmin_Virb => (byte[])ResourceManager.GetObject("Anafi_Meter_Template_for_Garmin_Virb", resourceCulture);

		public static byte[] Bebop_Feet_Template_for_Garmin_Virb => (byte[])ResourceManager.GetObject("Bebop_Feet_Template_for_Garmin_Virb", resourceCulture);

		public static byte[] Bebop_Meter_Template_for_Garmin_Virb => (byte[])ResourceManager.GetObject("Bebop_Meter_Template_for_Garmin_Virb", resourceCulture);

		public static byte[] Disco_Feet_Template_for_Garmin_Virb => (byte[])ResourceManager.GetObject("Disco_Feet_Template_for_Garmin_Virb", resourceCulture);

		public static byte[] Disco_Meter_Template_for_Garmin_Virb => (byte[])ResourceManager.GetObject("Disco_Meter_Template_for_Garmin_Virb", resourceCulture);

		public static Icon Flight_Track_Icon => (Icon)ResourceManager.GetObject("Flight_Track_Icon", resourceCulture);

		public static byte[] Gauges_for_Garmin_Virb => (byte[])ResourceManager.GetObject("Gauges_for_Garmin_Virb", resourceCulture);

		internal Resources()
		{
		}
	}
}
