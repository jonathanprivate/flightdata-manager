using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace FlightDataManager.Properties
{
	[CompilerGenerated]
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
	internal sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());

		public static Settings Default => defaultInstance;

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		public string DefaultPath
		{
			get
			{
				return (string)this["DefaultPath"];
			}
			set
			{
				this["DefaultPath"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		public int DefaultRadioButtonSelection
		{
			get
			{
				return (int)this["DefaultRadioButtonSelection"];
			}
			set
			{
				this["DefaultRadioButtonSelection"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		public string DroneAcademyUsername
		{
			get
			{
				return (string)this["DroneAcademyUsername"];
			}
			set
			{
				this["DroneAcademyUsername"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		public string DroneAcademyPassword
		{
			get
			{
				return (string)this["DroneAcademyPassword"];
			}
			set
			{
				this["DroneAcademyPassword"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		public bool DroneAcademyDontRememberUser
		{
			get
			{
				return (bool)this["DroneAcademyDontRememberUser"];
			}
			set
			{
				this["DroneAcademyDontRememberUser"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		public double gmapLat
		{
			get
			{
				return (double)this["gmapLat"];
			}
			set
			{
				this["gmapLat"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		public double gmapLon
		{
			get
			{
				return (double)this["gmapLon"];
			}
			set
			{
				this["gmapLon"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		public double gmapZoom
		{
			get
			{
				return (double)this["gmapZoom"];
			}
			set
			{
				this["gmapZoom"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("True")]
		public bool IgnoreNegativeAltitude
		{
			get
			{
				return (bool)this["IgnoreNegativeAltitude"];
			}
			set
			{
				this["IgnoreNegativeAltitude"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		public int RadioButtonPublicOrOwnFlight
		{
			get
			{
				return (int)this["RadioButtonPublicOrOwnFlight"];
			}
			set
			{
				this["RadioButtonPublicOrOwnFlight"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		public int RadioButtonShowFlightWithMedia
		{
			get
			{
				return (int)this["RadioButtonShowFlightWithMedia"];
			}
			set
			{
				this["RadioButtonShowFlightWithMedia"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		public int OffsetGpsVideoSync
		{
			get
			{
				return (int)this["OffsetGpsVideoSync"];
			}
			set
			{
				this["OffsetGpsVideoSync"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		public bool WindowMainStateMaximized
		{
			get
			{
				return (bool)this["WindowMainStateMaximized"];
			}
			set
			{
				this["WindowMainStateMaximized"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("50")]
		public int WindowMainPosX
		{
			get
			{
				return (int)this["WindowMainPosX"];
			}
			set
			{
				this["WindowMainPosX"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		public int WindowMainPosY
		{
			get
			{
				return (int)this["WindowMainPosY"];
			}
			set
			{
				this["WindowMainPosY"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("1150")]
		public int WindowMainSizeX
		{
			get
			{
				return (int)this["WindowMainSizeX"];
			}
			set
			{
				this["WindowMainSizeX"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("734")]
		public int WindowMainSizeY
		{
			get
			{
				return (int)this["WindowMainSizeY"];
			}
			set
			{
				this["WindowMainSizeY"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		public bool WindowSubStateMaximized
		{
			get
			{
				return (bool)this["WindowSubStateMaximized"];
			}
			set
			{
				this["WindowSubStateMaximized"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("30")]
		public int WindowSubPosX
		{
			get
			{
				return (int)this["WindowSubPosX"];
			}
			set
			{
				this["WindowSubPosX"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		public int WindowSubPosY
		{
			get
			{
				return (int)this["WindowSubPosY"];
			}
			set
			{
				this["WindowSubPosY"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("1230")]
		public int WindowSubSizeX
		{
			get
			{
				return (int)this["WindowSubSizeX"];
			}
			set
			{
				this["WindowSubSizeX"] = value;
			}
		}

		[UserScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("734")]
		public int WindowSubSizeY
		{
			get
			{
				return (int)this["WindowSubSizeY"];
			}
			set
			{
				this["WindowSubSizeY"] = value;
			}
		}
	}
}
