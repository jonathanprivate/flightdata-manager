using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class CheckVersionForm : Form
	{
		private string currentToolVersion = DataExchangerClass.currentToolVersion;

		private string versionFromWebStringCompiled = "";

		private IContainer components;

		private RichTextBox richTextBox1;

		private Button buttonRemindMeLater;

		private Button buttonOpenWebsite;

		private Label label1;

		private Label label2;

		private Label label3;

		private Label label4;

		private Button buttonDownload;

		public CheckVersionForm()
		{
			InitializeComponent();
		}

		private void CheckVersionForm_Load(object sender, EventArgs e)
		{
			base.Visible = false;
			try
			{
				string text = new WebClient().DownloadString("https://sites.google.com/site/pud2gpxkmlcsv/Version2.txt");
				if (!text.StartsWith("Version:"))
				{
					throw new Exception("Version not found");
				}
				string text2 = text.Substring(8, 5);
				int[] array = new int[3]
				{
					Convert.ToInt32(text2.Split('.')[0]),
					Convert.ToInt32(text2.Split('.')[1]),
					Convert.ToInt32(text2.Split('.')[2])
				};
				int num = array[0] * 1000000 + array[1] * 1000 + array[2];
				versionFromWebStringCompiled = array[0] + "." + array[1] + "." + array[2];
				buttonDownload.Text = "Download Version " + versionFromWebStringCompiled;
				int[] array2 = new int[3]
				{
					Convert.ToInt32(currentToolVersion.Split('.')[0]),
					Convert.ToInt32(currentToolVersion.Split('.')[1]),
					Convert.ToInt32(currentToolVersion.Split('.')[2])
				};
				int num2 = array2[0] * 1000000 + array2[1] * 1000 + array2[2];
				string[] array3 = text.Split(new string[1]
				{
					"Version:"
				}, StringSplitOptions.None);
				label2.Text = "Currently installed version on PC: " + currentToolVersion;
				label3.Text = "Available version from website: " + text2;
				for (int i = 1; i < array3.Length; i++)
				{
					string text3 = array3[i].Substring(0, 5);
					int[] array4 = new int[3]
					{
						Convert.ToInt32(text3.Split('.')[0]),
						Convert.ToInt32(text3.Split('.')[1]),
						Convert.ToInt32(text3.Split('.')[2])
					};
					if (array4[0] * 1000000 + array4[1] * 1000 + array4[2] > num2)
					{
						richTextBox1.AppendText("Version: " + array3[i]);
					}
				}
				richTextBox1.SelectionStart = 0;
				richTextBox1.ScrollToCaret();
				if (num2 < num)
				{
					base.Visible = true;
					buttonDownload.Focus();
				}
				else
				{
					Close();
				}
			}
			catch (Exception)
			{
				Close();
			}
		}

		private void buttonOpenWebsite_Click(object sender, EventArgs e)
		{
			Process.Start("https://sites.google.com/site/pud2gpxkmlcsv/#Version_history2");
		}

		private void buttonRemindMeLater_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void buttonDownload_Click(object sender, EventArgs e)
		{
			Process.Start("https://sites.google.com/site/pud2gpxkmlcsv/FlightData_Manager_v" + versionFromWebStringCompiled.Replace(".", "_") + ".zip");
		}

		private void CheckVersionForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				Close();
			}
		}

		private void buttonDownload_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				Close();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			richTextBox1 = new System.Windows.Forms.RichTextBox();
			buttonRemindMeLater = new System.Windows.Forms.Button();
			buttonOpenWebsite = new System.Windows.Forms.Button();
			label1 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			label4 = new System.Windows.Forms.Label();
			buttonDownload = new System.Windows.Forms.Button();
			SuspendLayout();
			richTextBox1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			richTextBox1.Location = new System.Drawing.Point(12, 115);
			richTextBox1.Name = "richTextBox1";
			richTextBox1.Size = new System.Drawing.Size(523, 172);
			richTextBox1.TabIndex = 0;
			richTextBox1.TabStop = false;
			richTextBox1.Text = "";
			buttonRemindMeLater.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			buttonRemindMeLater.Location = new System.Drawing.Point(297, 293);
			buttonRemindMeLater.Name = "buttonRemindMeLater";
			buttonRemindMeLater.Size = new System.Drawing.Size(103, 23);
			buttonRemindMeLater.TabIndex = 2;
			buttonRemindMeLater.Text = "Remind me later";
			buttonRemindMeLater.UseVisualStyleBackColor = true;
			buttonRemindMeLater.Click += new System.EventHandler(buttonRemindMeLater_Click);
			buttonOpenWebsite.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			buttonOpenWebsite.Location = new System.Drawing.Point(12, 293);
			buttonOpenWebsite.Name = "buttonOpenWebsite";
			buttonOpenWebsite.Size = new System.Drawing.Size(105, 23);
			buttonOpenWebsite.TabIndex = 3;
			buttonOpenWebsite.Text = "Open Website";
			buttonOpenWebsite.UseVisualStyleBackColor = true;
			buttonOpenWebsite.Click += new System.EventHandler(buttonOpenWebsite_Click);
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(12, 21);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(310, 13);
			label1.TabIndex = 4;
			label1.Text = "A newer version of FlightData Manager is available for download";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(12, 46);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(35, 13);
			label2.TabIndex = 5;
			label2.Text = "label2";
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(12, 63);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(35, 13);
			label3.TabIndex = 6;
			label3.Text = "label3";
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(12, 99);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(310, 13);
			label4.TabIndex = 7;
			label4.Text = "New features and bugfixes available since your installed version:";
			buttonDownload.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			buttonDownload.Location = new System.Drawing.Point(134, 293);
			buttonDownload.Name = "buttonDownload";
			buttonDownload.Size = new System.Drawing.Size(143, 23);
			buttonDownload.TabIndex = 1;
			buttonDownload.Text = "Download Version x.x.x";
			buttonDownload.UseVisualStyleBackColor = true;
			buttonDownload.Click += new System.EventHandler(buttonDownload_Click);
			buttonDownload.KeyDown += new System.Windows.Forms.KeyEventHandler(buttonDownload_KeyDown);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(547, 323);
			base.Controls.Add(buttonDownload);
			base.Controls.Add(label4);
			base.Controls.Add(label3);
			base.Controls.Add(label2);
			base.Controls.Add(label1);
			base.Controls.Add(buttonOpenWebsite);
			base.Controls.Add(buttonRemindMeLater);
			base.Controls.Add(richTextBox1);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			MinimumSize = new System.Drawing.Size(440, 310);
			base.Name = "CheckVersionForm";
			Text = "A newer version of FlightData Manager is available, open website for download now?";
			base.Load += new System.EventHandler(CheckVersionForm_Load);
			base.KeyDown += new System.Windows.Forms.KeyEventHandler(CheckVersionForm_KeyDown);
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
