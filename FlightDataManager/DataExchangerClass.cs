using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;

namespace FlightDataManager
{
	internal static class DataExchangerClass
	{
		public static List<double> chartValueList1
		{
			get;
			set;
		}

		public static List<double> chartValueList2
		{
			get;
			set;
		}

		public static List<double> chartValueList3
		{
			get;
			set;
		}

		public static List<double> chartValueList4
		{
			get;
			set;
		}

		public static List<double> chartValueList5
		{
			get;
			set;
		}

		public static Point startPointOfForm
		{
			get;
			set;
		}

		public static string[] droneAcademyFlightData
		{
			get;
			set;
		}

		public static string droneAcademyFlightDuration
		{
			get;
			set;
		}

		public static int lengthOfFileInTotalSec
		{
			get;
			set;
		}

		public static bool reportStatistics
		{
			get;
			set;
		}

		public static DateTime droneAcademyFlightStartDateTime
		{
			get;
			set;
		}

		public static RegionInfo regionInfo
		{
			get;
			set;
		}

		public static string currentToolVersion
		{
			get;
			set;
		}

		public static string flightID
		{
			get;
			set;
		}

		public static string droneModel
		{
			get;
			set;
		}

		public static string listSep
		{
			get;
			set;
		}

		public static List<string> videoLinksList
		{
			get;
			set;
		}

		public static List<string> pictureLinksList
		{
			get;
			set;
		}

		public static List<string> mediaIdList
		{
			get;
			set;
		}

		public static bool myOwnFlight
		{
			get;
			set;
		}

		public static List<string> mediaIdToBeUnlinkedList
		{
			get;
			set;
		}

		public static bool flightDataFieldNrOfSvsPresent
		{
			get;
			set;
		}

		public static bool flightDataFieldPitotSpeedPresent
		{
			get;
			set;
		}

		public static int flightDataFieldIndexTime
		{
			get;
			set;
		}

		public static int flightDataFieldIndexBattery
		{
			get;
			set;
		}

		public static int flightDataFieldIndexFlyingState
		{
			get;
			set;
		}

		public static int flightDataFieldIndexAlertState
		{
			get;
			set;
		}

		public static int flightDataFieldIndexWifiSignal
		{
			get;
			set;
		}

		public static int flightDataFieldIndexProductGpsLat
		{
			get;
			set;
		}

		public static int flightDataFieldIndexProductGpsLon
		{
			get;
			set;
		}

		public static int flightDataFieldIndexProductGpsSvNr
		{
			get;
			set;
		}

		public static int flightDataFieldIndexSpeedVx
		{
			get;
			set;
		}

		public static int flightDataFieldIndexSpeedVy
		{
			get;
			set;
		}

		public static int flightDataFieldIndexSpeedVz
		{
			get;
			set;
		}

		public static int flightDataFieldIndexAnglePhi
		{
			get;
			set;
		}

		public static int flightDataFieldIndexAngleTheta
		{
			get;
			set;
		}

		public static int flightDataFieldIndexAnglePsi
		{
			get;
			set;
		}

		public static int flightDataFieldIndexAltitude
		{
			get;
			set;
		}

		public static int flightDataFieldIndexControllerGpsLat
		{
			get;
			set;
		}

		public static int flightDataFieldIndexControllerGpsLon
		{
			get;
			set;
		}

		public static int flightDataFieldIndexProductGpsAvailable
		{
			get;
			set;
		}

		public static int flightDataFieldIndexProductGpsPosError
		{
			get;
			set;
		}

		public static int flightDataFieldIndexPitotSpeed
		{
			get;
			set;
		}

		public static int flightDataFieldIndexFlipType
		{
			get;
			set;
		}
	}
}
