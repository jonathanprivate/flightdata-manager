using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class Donate : Form
	{
		private IContainer components;

		private Button buttonClose;

		private RichTextBox richTextBox1;

		private Button button1;

		public Donate()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void button1_Click_1(object sender, EventArgs e)
		{
			Process.Start("ftp://192.168.42.1/internal_000/Bebop_Drone/academy/");
		}

		private void FormGetPudFiles_Load(object sender, EventArgs e)
		{
			base.MaximizeBox = false;
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Location = new Point(220, 100);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.Donate));
			buttonClose = new System.Windows.Forms.Button();
			richTextBox1 = new System.Windows.Forms.RichTextBox();
			button1 = new System.Windows.Forms.Button();
			SuspendLayout();
			buttonClose.Location = new System.Drawing.Point(315, 222);
			buttonClose.Name = "buttonClose";
			buttonClose.Size = new System.Drawing.Size(75, 23);
			buttonClose.TabIndex = 0;
			buttonClose.Text = "Close";
			buttonClose.UseVisualStyleBackColor = true;
			buttonClose.Click += new System.EventHandler(button1_Click);
			richTextBox1.BackColor = System.Drawing.SystemColors.Control;
			richTextBox1.Location = new System.Drawing.Point(12, 12);
			richTextBox1.Name = "richTextBox1";
			richTextBox1.ReadOnly = true;
			richTextBox1.Size = new System.Drawing.Size(679, 154);
			richTextBox1.TabIndex = 11;
			richTextBox1.Text = resources.GetString("richTextBox1.Text");
			button1.Location = new System.Drawing.Point(128, 184);
			button1.Name = "button1";
			button1.Size = new System.Drawing.Size(446, 23);
			button1.TabIndex = 12;
			button1.Text = "Open ftp://192.168.42.1/internal_000/Bebop_Drone/academy/ in Internet Browser";
			button1.UseVisualStyleBackColor = true;
			button1.Click += new System.EventHandler(button1_Click_1);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(705, 258);
			base.Controls.Add(button1);
			base.Controls.Add(richTextBox1);
			base.Controls.Add(buttonClose);
			base.MinimizeBox = false;
			base.Name = "FormGetPudFiles";
			Text = "Get Pud Files from Bebop";
			base.Load += new System.EventHandler(FormGetPudFiles_Load);
			ResumeLayout(false);
		}
	}
}
