using Dynastream.Fit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FlightDataManager
{
	internal static class FitFileGenerator
	{
		public static void GenerateFitFiles(string fileNameWithoutExtension, List<double> fitValueLat, List<double> fitValueLon, List<int> fitValueBattery, List<int> fitValueNrOfSvs, List<int> fitValueWifiSignal, List<float> fitValueSpeed, List<float> fitValueAirSpeed, List<double> fitValueAltitude, List<double> fitValueAltitudeRelative, List<int> fitValueDistanceToDrone, List<int> fitValueDistanceFlown, List<List<int>> fitValueAccelerationXyzMilliSecTimeStamp, List<List<float>> fitValueAccelerationXyzPitch, List<List<float>> fitValueAccelerationXyzRoll, List<List<float>> fitValueAccelerationXyzYaw, System.DateTime startDateTime, string LiquidVolumeUnitInVirbSettings, string speedUnitInVirbSettings)
		{
			EncodeFile(fileNameWithoutExtension + "_Meters.fit", fitValueLat, fitValueLon, fitValueBattery, fitValueNrOfSvs, fitValueWifiSignal, fitValueSpeed, fitValueAirSpeed, fitValueAltitude, fitValueAltitudeRelative, fitValueDistanceToDrone, fitValueDistanceFlown, fitValueAccelerationXyzMilliSecTimeStamp, fitValueAccelerationXyzPitch, fitValueAccelerationXyzRoll, fitValueAccelerationXyzYaw, startDateTime, useMeters: true, LiquidVolumeUnitInVirbSettings, speedUnitInVirbSettings);
			EncodeFile(fileNameWithoutExtension + "_Feet.fit", fitValueLat, fitValueLon, fitValueBattery, fitValueNrOfSvs, fitValueWifiSignal, fitValueSpeed, fitValueAirSpeed, fitValueAltitude, fitValueAltitudeRelative, fitValueDistanceToDrone, fitValueDistanceFlown, fitValueAccelerationXyzMilliSecTimeStamp, fitValueAccelerationXyzPitch, fitValueAccelerationXyzRoll, fitValueAccelerationXyzYaw, startDateTime, useMeters: false, LiquidVolumeUnitInVirbSettings, speedUnitInVirbSettings);
		}

		public static void EncodeFile(string fileName, List<double> fitValueLat, List<double> fitValueLon, List<int> fitValueBattery, List<int> fitValueNrOfSvs, List<int> fitValueWifiSignal, List<float> fitValueSpeed, List<float> fitValueAirSpeed, List<double> fitValueAltitude, List<double> fitValueAltitudeRelative, List<int> fitValueDistanceToDrone, List<int> fitValueDistanceFlown, List<List<int>> fitValueAccelerationXyzMilliSecTimeStamp, List<List<float>> fitValueAccelerationXyzPitch, List<List<float>> fitValueAccelerationXyzRoll, List<List<float>> fitValueAccelerationXyzYaw, System.DateTime startDateTime, bool useMeters, string LiquidVolumeUnitInVirbSettings, string speedUnitInVirbSettings)
		{
			System.DateTime dateTime = new System.DateTime(1989, 12, 31, 0, 0, 0, DateTimeKind.Utc);
			System.DateTime dateTime2 = new System.DateTime(1989, 12, 31, 0, 0, 0, DateTimeKind.Utc);
			FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
			Encode encode = new Encode();
			encode.Open(fileStream);
			FileIdMesg fileIdMesg = new FileIdMesg();
			fileIdMesg.SetType(Dynastream.Fit.File.Activity);
			fileIdMesg.SetManufacturer(15);
			fileIdMesg.SetProduct((ushort)1001);
			fileIdMesg.SetSerialNumber(54321u);
			fileIdMesg.SetTimeCreated(new Dynastream.Fit.DateTime(dateTime));
			fileIdMesg.SetNumber(0);
			encode.Write(fileIdMesg);
			FileCreatorMesg fileCreatorMesg = new FileCreatorMesg();
			fileCreatorMesg.SetHardwareVersion((byte)1);
			fileCreatorMesg.SetSoftwareVersion((ushort)1);
			encode.Write(fileCreatorMesg);
			EventMesg eventMesg = new EventMesg();
			eventMesg.SetTimestamp(new Dynastream.Fit.DateTime(dateTime));
			eventMesg.SetTimerTrigger(0);
			eventMesg.SetEvent(Event.Timer);
			eventMesg.SetEventType(EventType.Start);
			encode.Write(eventMesg);
			TimeSpan utcOffset = System.TimeZone.CurrentTimeZone.GetUtcOffset(System.DateTime.Now);
			startDateTime -= utcOffset;
			TimestampCorrelationMesg timestampCorrelationMesg = new TimestampCorrelationMesg();
			uint value = (uint)(startDateTime - dateTime).TotalSeconds;
			timestampCorrelationMesg.SetTimestamp(new Dynastream.Fit.DateTime(startDateTime));
			timestampCorrelationMesg.SetSystemTimestamp(new Dynastream.Fit.DateTime(dateTime2));
			timestampCorrelationMesg.SetLocalTimestamp(value);
			encode.Write(timestampCorrelationMesg);
			RecordMesg recordMesg = new RecordMesg();
			AviationAttitudeMesg aviationAttitudeMesg = new AviationAttitudeMesg();
			for (int i = 0; i < fitValueLat.Count(); i++)
			{
				int value2 = (int)(fitValueLat[i] * (Math.Pow(2.0, 31.0) / 180.0));
				int value3 = (int)(fitValueLon[i] * (Math.Pow(2.0, 31.0) / 180.0));
				recordMesg.SetTimestamp(new Dynastream.Fit.DateTime(dateTime2));
				recordMesg.SetActivityType(ActivityType.Cycling);
				recordMesg.SetPositionLat(value2);
				recordMesg.SetPositionLong(value3);
				recordMesg.SetCadence(Convert.ToByte(fitValueBattery[i]));
				if (useMeters)
				{
					recordMesg.SetStanceTime(Convert.ToSingle((double)fitValueSpeed[i] * 3.6));
					if (fitValueDistanceFlown[i] < 65535)
					{
						recordMesg.SetPower(Convert.ToUInt16(fitValueDistanceFlown[i]));
					}
					else
					{
						recordMesg.SetPower(Convert.ToUInt16(65535));
					}
					recordMesg.SetStanceTimePercent((float)fitValueAltitude[i]);
					recordMesg.SetAltitude((float)fitValueAltitude[i]);
				}
				else
				{
					recordMesg.SetStanceTime(Convert.ToSingle((double)fitValueSpeed[i] * 2.237));
					if ((double)fitValueDistanceFlown[i] * 3.2808 < 65535.0)
					{
						recordMesg.SetPower(Convert.ToUInt16((double)fitValueDistanceFlown[i] * 3.2808));
					}
					else
					{
						recordMesg.SetPower(ushort.MaxValue);
					}
					recordMesg.SetStanceTimePercent((float)fitValueAltitude[i] * 3.2808f);
					recordMesg.SetAltitude((float)fitValueAltitude[i] * 3.2808f);
				}
				if (fitValueNrOfSvs[i] != 0)
				{
					if (fitValueNrOfSvs[i] < 26)
					{
						recordMesg.SetVerticalOscillation((int)Convert.ToByte(fitValueNrOfSvs[i] * 10));
					}
					else
					{
						recordMesg.SetVerticalOscillation((int)Convert.ToByte(250));
					}
				}
				if (fitValueWifiSignal[i] != 0)
				{
					recordMesg.SetHeartRate(Convert.ToByte(fitValueWifiSignal[i]));
				}
				encode.Write(recordMesg);
				ObdiiDataMesg obdiiDataMesg = new ObdiiDataMesg();
				obdiiDataMesg.SetTimestamp(new Dynastream.Fit.DateTime(dateTime2));
				obdiiDataMesg.SetSystemTime(0, Convert.ToUInt32(i * 1000));
				obdiiDataMesg.SetSystemTime(1, Convert.ToUInt32(i * 1000));
				obdiiDataMesg.SetPid((byte)12);
				if (useMeters)
				{
					obdiiDataMesg.SetRawData(0, (byte)(fitValueDistanceToDrone[i] * 4 / 256));
					obdiiDataMesg.SetRawData(1, (byte)(fitValueDistanceToDrone[i] * 4 % 256));
				}
				else
				{
					obdiiDataMesg.SetRawData(0, (byte)((double)fitValueDistanceToDrone[i] * 3.2808 * 4.0 / 256.0));
					obdiiDataMesg.SetRawData(1, (byte)((double)fitValueDistanceToDrone[i] * 3.2808 * 4.0 % 256.0));
				}
				encode.Write(obdiiDataMesg);
				ObdiiDataMesg obdiiDataMesg2 = new ObdiiDataMesg();
				obdiiDataMesg2.SetTimestamp(new Dynastream.Fit.DateTime(dateTime2));
				obdiiDataMesg2.SetSystemTime(0, Convert.ToUInt32(i * 1000));
				obdiiDataMesg2.SetSystemTime(1, Convert.ToUInt32(i * 1000));
				obdiiDataMesg2.SetPid((byte)94);
				double num = fitValueAltitudeRelative[i] * 20.0;
				if (LiquidVolumeUnitInVirbSettings == "ukgallons")
				{
					num *= 4.54609188;
				}
				else if (LiquidVolumeUnitInVirbSettings == "usgallons")
				{
					num *= 3.78541178;
				}
				if (!useMeters)
				{
					num *= 3.2808;
				}
				if (num > 65535.0)
				{
					num = 65534.0;
				}
				obdiiDataMesg2.SetRawData(0, (byte)(num / 256.0));
				obdiiDataMesg2.SetRawData(1, (byte)(num % 256.0));
				encode.Write(obdiiDataMesg2);
				ObdiiDataMesg obdiiDataMesg3 = new ObdiiDataMesg();
				obdiiDataMesg3.SetTimestamp(new Dynastream.Fit.DateTime(dateTime2));
				obdiiDataMesg3.SetSystemTime(0, Convert.ToUInt32(i * 1000));
				obdiiDataMesg3.SetPid((byte)13);
				if (useMeters)
				{
					if (speedUnitInVirbSettings == "kmh")
					{
						obdiiDataMesg3.SetRawData(0, (byte)((double)fitValueAirSpeed[i] * 3.6));
					}
					else if (speedUnitInVirbSettings == "mph")
					{
						obdiiDataMesg3.SetRawData(0, (byte)((double)fitValueAirSpeed[i] * 3.6 * 1.609344));
					}
					else
					{
						obdiiDataMesg3.SetRawData(0, (byte)((double)fitValueAirSpeed[i] * 3.6 * 1.852));
					}
				}
				else if (speedUnitInVirbSettings == "kmh")
				{
					obdiiDataMesg3.SetRawData(0, (byte)((double)fitValueAirSpeed[i] * 2.2369));
				}
				else if (speedUnitInVirbSettings == "mph")
				{
					obdiiDataMesg3.SetRawData(0, (byte)((double)fitValueAirSpeed[i] * 2.2369 * 1.609344));
				}
				else
				{
					obdiiDataMesg3.SetRawData(0, (byte)((double)fitValueAirSpeed[i] * 2.2369 * 1.852));
				}
				encode.Write(obdiiDataMesg3);
				if (fitValueAccelerationXyzMilliSecTimeStamp.Count() > i)
				{
					aviationAttitudeMesg.SetTimestamp(new Dynastream.Fit.DateTime(dateTime2));
					for (int j = 0; j < fitValueAccelerationXyzMilliSecTimeStamp[i].Count(); j++)
					{
						aviationAttitudeMesg.SetSystemTime(j, (uint)fitValueAccelerationXyzMilliSecTimeStamp[i][j]);
						aviationAttitudeMesg.SetPitch(j, fitValueAccelerationXyzPitch[i][j]);
						aviationAttitudeMesg.SetRoll(j, fitValueAccelerationXyzRoll[i][j]);
						aviationAttitudeMesg.SetTrack(j, fitValueAccelerationXyzYaw[i][j]);
					}
					encode.Write(aviationAttitudeMesg);
				}
				dateTime2 = dateTime2.AddSeconds(1.0);
			}
			eventMesg = new EventMesg();
			eventMesg.SetTimestamp(new Dynastream.Fit.DateTime(dateTime));
			eventMesg.SetTimerTrigger(0);
			eventMesg.SetEvent(Event.Timer);
			eventMesg.SetEventType(EventType.StopAll);
			encode.Write(eventMesg);
			LapMesg lapMesg = new LapMesg();
			lapMesg.SetTimestamp(new Dynastream.Fit.DateTime(dateTime));
			lapMesg.SetTotalElapsedTime(fitValueLat.Count());
			lapMesg.SetMessageIndex(0);
			lapMesg.SetLapTrigger(LapTrigger.SessionEnd);
			encode.Write(lapMesg);
			eventMesg = new EventMesg();
			eventMesg.SetTimestamp(new Dynastream.Fit.DateTime(dateTime));
			eventMesg.SetTimerTrigger(0);
			eventMesg.SetEvent(Event.Session);
			eventMesg.SetEventType(EventType.StopDisableAll);
			encode.Write(eventMesg);
			SessionMesg sessionMesg = new SessionMesg();
			sessionMesg.SetTimestamp(new Dynastream.Fit.DateTime(dateTime));
			sessionMesg.SetTotalElapsedTime(fitValueLat.Count());
			sessionMesg.SetMessageIndex(0);
			sessionMesg.SetSport(Sport.Cycling);
			encode.Write(sessionMesg);
			ActivityMesg activityMesg = new ActivityMesg();
			activityMesg.SetTimestamp(new Dynastream.Fit.DateTime(dateTime));
			activityMesg.SetTotalTimerTime(fitValueLat.Count());
			activityMesg.SetNumSessions((ushort)1);
			activityMesg.SetEvent(Event.Workout);
			activityMesg.SetEventType(EventType.StopAll);
			activityMesg.SetType(Activity.Manual);
			encode.Write(activityMesg);
			encode.Close();
			fileStream.Close();
		}
	}
}
