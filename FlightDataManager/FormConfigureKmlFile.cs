using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class FormConfigureKmlFile : Form
	{
		private List<int> altitudeLimits = new List<int>();

		private List<string> colors = new List<string>();

		private int activeColorNumber;

		private IContainer components;

		private Button buttonSave;

		private Button buttonCancel;

		private NumericUpDown numericUpDownNumberOfAltitudeColors;

		private Label label1;

		private TextBox textBoxColor15;

		private RichTextBox richTextBox1;

		private NumericUpDown numericUpDown14;

		private TextBox textBoxColor14;

		private NumericUpDown numericUpDown13;

		private TextBox textBoxColor13;

		private TextBox textBoxColor11;

		private NumericUpDown numericUpDown11;

		private TextBox textBoxColor12;

		private NumericUpDown numericUpDown12;

		private TextBox textBoxColor7;

		private NumericUpDown numericUpDown7;

		private TextBox textBoxColor8;

		private NumericUpDown numericUpDown8;

		private TextBox textBoxColor9;

		private NumericUpDown numericUpDown9;

		private TextBox textBoxColor10;

		private NumericUpDown numericUpDown10;

		private TextBox textBoxColor1;

		private NumericUpDown numericUpDown1;

		private TextBox textBoxColor2;

		private NumericUpDown numericUpDown2;

		private TextBox textBoxColor3;

		private NumericUpDown numericUpDown3;

		private TextBox textBoxColor4;

		private NumericUpDown numericUpDown4;

		private TextBox textBoxColor5;

		private NumericUpDown numericUpDown5;

		private TextBox textBoxColor6;

		private NumericUpDown numericUpDown6;

		private Label label2;

		private Label label3;

		private Label label4;

		private Label label5;

		private Label label6;

		private Label label7;

		private Label label8;

		private Label label9;

		private Label label10;

		private Label label11;

		private Label label12;

		private Label label13;

		private Label label14;

		private Label label15;

		private Label labelKmlColor15;

		private Label labelKmlColor14;

		private Label labelKmlColor13;

		private Label labelKmlColor12;

		private Label labelKmlColor11;

		private Label labelKmlColor10;

		private Label labelKmlColor9;

		private Label labelKmlColor8;

		private Label labelKmlColor7;

		private Label labelKmlColor6;

		private Label labelKmlColor5;

		private Label labelKmlColor4;

		private Label labelKmlColor3;

		private Label labelKmlColor2;

		private Label labelKmlColor1;

		private Button buttonSetDefault;

		private Button buttonMoreColors;

		private Label label31;

		private Label label32;

		private Label label33;

		private NumericUpDown numericUpDownBlue;

		private NumericUpDown numericUpDownGreen;

		private NumericUpDown numericUpDownRed;

		private TextBox textBoxCurrentColor;

		private ColorDialog myColorDialog;

		private Label label16;

		private Label label17;

		private TextBox textBoxQuickColorPicker1;

		private TextBox textBoxQuickColorPicker2;

		private TextBox textBoxQuickColorPicker4;

		private TextBox textBoxQuickColorPicker3;

		private TextBox textBoxQuickColorPicker8;

		private TextBox textBoxQuickColorPicker7;

		private TextBox textBoxQuickColorPicker6;

		private TextBox textBoxQuickColorPicker5;

		private TextBox textBoxQuickColorPicker16;

		private TextBox textBoxQuickColorPicker15;

		private TextBox textBoxQuickColorPicker14;

		private TextBox textBoxQuickColorPicker13;

		private TextBox textBoxQuickColorPicker12;

		private TextBox textBoxQuickColorPicker11;

		private TextBox textBoxQuickColorPicker10;

		private TextBox textBoxQuickColorPicker9;

		private TextBox textBoxQuickColorPicker32;

		private TextBox textBoxQuickColorPicker31;

		private TextBox textBoxQuickColorPicker30;

		private TextBox textBoxQuickColorPicker29;

		private TextBox textBoxQuickColorPicker28;

		private TextBox textBoxQuickColorPicker27;

		private TextBox textBoxQuickColorPicker26;

		private TextBox textBoxQuickColorPicker25;

		private TextBox textBoxQuickColorPicker24;

		private TextBox textBoxQuickColorPicker23;

		private TextBox textBoxQuickColorPicker22;

		private TextBox textBoxQuickColorPicker21;

		private TextBox textBoxQuickColorPicker20;

		private TextBox textBoxQuickColorPicker19;

		private TextBox textBoxQuickColorPicker18;

		private TextBox textBoxQuickColorPicker17;

		private TextBox textBoxQuickColorPicker48;

		private TextBox textBoxQuickColorPicker47;

		private TextBox textBoxQuickColorPicker46;

		private TextBox textBoxQuickColorPicker45;

		private TextBox textBoxQuickColorPicker44;

		private TextBox textBoxQuickColorPicker43;

		private TextBox textBoxQuickColorPicker42;

		private TextBox textBoxQuickColorPicker41;

		private TextBox textBoxQuickColorPicker40;

		private TextBox textBoxQuickColorPicker39;

		private TextBox textBoxQuickColorPicker38;

		private TextBox textBoxQuickColorPicker37;

		private TextBox textBoxQuickColorPicker36;

		private TextBox textBoxQuickColorPicker35;

		private TextBox textBoxQuickColorPicker34;

		private TextBox textBoxQuickColorPicker33;

		private TextBox textBoxColorIndicator15;

		private TextBox textBoxColorIndicator14;

		private TextBox textBoxColorIndicator12;

		private TextBox textBoxColorIndicator13;

		private TextBox textBoxColorIndicator8;

		private TextBox textBoxColorIndicator9;

		private TextBox textBoxColorIndicator10;

		private TextBox textBoxColorIndicator11;

		private TextBox textBoxColorIndicator1;

		private TextBox textBoxColorIndicator2;

		private TextBox textBoxColorIndicator3;

		private TextBox textBoxColorIndicator4;

		private TextBox textBoxColorIndicator5;

		private TextBox textBoxColorIndicator6;

		private TextBox textBoxColorIndicator7;

		public FormConfigureKmlFile()
		{
			InitializeComponent();
		}

		private void FormConfigureKmlFile_Load(object sender, EventArgs e)
		{
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Location = new Point(200, 20);
			BuildQuickColorPicker();
			buttonMoreColors.Enabled = false;
			numericUpDownRed.Enabled = false;
			numericUpDownBlue.Enabled = false;
			numericUpDownGreen.Enabled = false;
			AddEventHandlers();
			updateAllColorIndicatorFromTextBoxColorTxt();
			if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("Kml config menu loaded");
			}
			string text = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\"), "AltitudeColorsAndLimitsForKmlFile.txt");
			if (File.Exists(text))
			{
				string text2 = "";
				StreamReader streamReader = new StreamReader(text);
				while ((text2 = streamReader.ReadLine()) != null)
				{
					try
					{
						string[] array = text2.Split(';');
						if (array.Count() >= 2)
						{
							altitudeLimits.Add(Convert.ToInt32(array[0]));
							colors.Add(array[1]);
							if (colors[colors.Count - 1].Length != 8)
							{
								MessageBox.Show(this, "Error in file: " + text + "\n\nColor does not contain 8 characters");
								return;
							}
						}
					}
					catch (Exception ex)
					{
						MessageBox.Show(this, "Error in file: " + text + "\n\n" + ex.Message);
						return;
					}
				}
				streamReader.Close();
				if (colors.Count < 2)
				{
					MessageBox.Show(this, "Too few lines in file: " + text);
					return;
				}
				for (int i = 0; i < colors.Count; i++)
				{
					if (i == 0)
					{
						textBoxColor1.Text = colors[i];
					}
					if (i == 1)
					{
						textBoxColor2.Text = colors[i];
						numericUpDown1.Value = altitudeLimits[i - 1];
					}
					if (i == 2)
					{
						textBoxColor3.Text = colors[i];
						numericUpDown2.Value = altitudeLimits[i - 1];
					}
					if (i == 3)
					{
						textBoxColor4.Text = colors[i];
						numericUpDown3.Value = altitudeLimits[i - 1];
					}
					if (i == 4)
					{
						textBoxColor5.Text = colors[i];
						numericUpDown4.Value = altitudeLimits[i - 1];
					}
					if (i == 5)
					{
						textBoxColor6.Text = colors[i];
						numericUpDown5.Value = altitudeLimits[i - 1];
					}
					if (i == 6)
					{
						textBoxColor7.Text = colors[i];
						numericUpDown6.Value = altitudeLimits[i - 1];
					}
					if (i == 7)
					{
						textBoxColor8.Text = colors[i];
						numericUpDown7.Value = altitudeLimits[i - 1];
					}
					if (i == 8)
					{
						textBoxColor9.Text = colors[i];
						numericUpDown8.Value = altitudeLimits[i - 1];
					}
					if (i == 9)
					{
						textBoxColor10.Text = colors[i];
						numericUpDown9.Value = altitudeLimits[i - 1];
					}
					if (i == 10)
					{
						textBoxColor11.Text = colors[i];
						numericUpDown10.Value = altitudeLimits[i - 1];
					}
					if (i == 11)
					{
						textBoxColor12.Text = colors[i];
						numericUpDown11.Value = altitudeLimits[i - 1];
					}
					if (i == 12)
					{
						textBoxColor13.Text = colors[i];
						numericUpDown12.Value = altitudeLimits[i - 1];
					}
					if (i == 13)
					{
						textBoxColor14.Text = colors[i];
						numericUpDown13.Value = altitudeLimits[i - 1];
					}
					if (i == 14)
					{
						textBoxColor15.Text = colors[i];
						numericUpDown14.Value = altitudeLimits[i - 1];
					}
				}
				numericUpDownNumberOfAltitudeColors.Value = colors.Count;
				numericUpDownNumberOfAltitudeColors_ValueChanged(null, null);
			}
			else
			{
				numericUpDownNumberOfAltitudeColors_ValueChanged(null, null);
			}
		}

		private void AddEventHandlers()
		{
			for (int i = 0; i < 15; i++)
			{
				Control control = base.Controls.Find("textBoxColor" + (i + 1), searchAllChildren: true)[0];
				Control obj = base.Controls.Find("textBoxColorIndicator" + (i + 1), searchAllChildren: true)[0];
				control.TextChanged += textBoxColor_TextChanged;
				control.MouseClick += textBoxColor_MouseClick;
				obj.MouseClick += textBoxColor_MouseClick;
			}
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			bool flag = false;
			for (int i = 1; (decimal)i < numericUpDownNumberOfAltitudeColors.Value - 1m; i++)
			{
				NumericUpDown numericUpDown = (NumericUpDown)base.Controls.Find("numericUpDown" + i, searchAllChildren: true)[0];
				NumericUpDown obj = (NumericUpDown)base.Controls.Find("numericUpDown" + (i + 1), searchAllChildren: true)[0];
				int num = (int)numericUpDown.Value;
				int num2 = (int)obj.Value;
				if (num >= num2)
				{
					MessageBox.Show(this, "There is a problem with the altitude limits, they are expected to be increasing in value for each color");
					flag = true;
				}
			}
			for (int j = 0; (decimal)j < numericUpDownNumberOfAltitudeColors.Value; j++)
			{
				if (base.Controls.Find("textBoxColor" + (j + 1), searchAllChildren: true)[0].Text.Count() != 8)
				{
					MessageBox.Show(this, "Color nr " + (j + 1) + " does not contain 8 characters as expected\n\nCorrect the error and try saving it again\n\nYou can restore the default values by clicking the 'Set default values' button");
					flag = true;
				}
			}
			if (!flag)
			{
				string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
				if (!Directory.Exists(text))
				{
					Directory.CreateDirectory(text);
				}
				using (StreamWriter streamWriter = new StreamWriter(Path.Combine(text, "AltitudeColorsAndLimitsForKmlFile.txt"), append: false))
				{
					for (int k = 0; (decimal)k < numericUpDownNumberOfAltitudeColors.Value; k++)
					{
						if (k == 0)
						{
							streamWriter.WriteLine(numericUpDown1.Value + ";" + textBoxColor1.Text);
						}
						if (k == 1)
						{
							streamWriter.WriteLine(numericUpDown2.Value + ";" + textBoxColor2.Text);
						}
						if (k == 2)
						{
							streamWriter.WriteLine(numericUpDown3.Value + ";" + textBoxColor3.Text);
						}
						if (k == 3)
						{
							streamWriter.WriteLine(numericUpDown4.Value + ";" + textBoxColor4.Text);
						}
						if (k == 4)
						{
							streamWriter.WriteLine(numericUpDown5.Value + ";" + textBoxColor5.Text);
						}
						if (k == 5)
						{
							streamWriter.WriteLine(numericUpDown6.Value + ";" + textBoxColor6.Text);
						}
						if (k == 6)
						{
							streamWriter.WriteLine(numericUpDown7.Value + ";" + textBoxColor7.Text);
						}
						if (k == 7)
						{
							streamWriter.WriteLine(numericUpDown8.Value + ";" + textBoxColor8.Text);
						}
						if (k == 8)
						{
							streamWriter.WriteLine(numericUpDown9.Value + ";" + textBoxColor9.Text);
						}
						if (k == 9)
						{
							streamWriter.WriteLine(numericUpDown10.Value + ";" + textBoxColor10.Text);
						}
						if (k == 10)
						{
							streamWriter.WriteLine(numericUpDown11.Value + ";" + textBoxColor11.Text);
						}
						if (k == 11)
						{
							streamWriter.WriteLine(numericUpDown12.Value + ";" + textBoxColor12.Text);
						}
						if (k == 12)
						{
							streamWriter.WriteLine(numericUpDown13.Value + ";" + textBoxColor13.Text);
						}
						if (k == 13)
						{
							streamWriter.WriteLine(numericUpDown14.Value + ";" + textBoxColor14.Text);
						}
						if (k == 14)
						{
							streamWriter.WriteLine(160 + ";" + textBoxColor15.Text);
						}
					}
				}
				MessageBox.Show(this, "Settings saved");
				if (DataExchangerClass.reportStatistics)
				{
					GoogleTracker.trackEvent("Kml colors changed");
				}
				Close();
			}
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void numericUpDownNumberOfAltitudeColors_ValueChanged(object sender, EventArgs e)
		{
			activeColorNumber = 0;
			buttonMoreColors.Enabled = false;
			numericUpDownRed.Enabled = false;
			numericUpDownBlue.Enabled = false;
			numericUpDownGreen.Enabled = false;
			textBoxCurrentColor.Text = "Select a color to change to the left";
			for (int i = 1; i <= 15; i++)
			{
				Control obj = base.Controls.Find("labelKmlColor" + i, searchAllChildren: true)[0];
				obj.Text = "Kml Color";
				obj.Font = new Font(labelKmlColor3.Font, FontStyle.Regular);
			}
			textBoxCurrentColor.BackColor = SystemColors.Control;
			for (int j = 1; (decimal)j <= numericUpDownNumberOfAltitudeColors.Value; j++)
			{
				if (j < 15)
				{
					base.Controls.Find("numericUpDown" + j, searchAllChildren: true)[0].Enabled = true;
					base.Controls.Find("textBoxColor" + (j + 1), searchAllChildren: true)[0].Enabled = true;
					base.Controls.Find("textBoxColorIndicator" + (j + 1), searchAllChildren: true)[0].Enabled = true;
				}
			}
			for (int k = (int)numericUpDownNumberOfAltitudeColors.Value + 1; k <= 15; k++)
			{
				base.Controls.Find("numericUpDown" + (k - 1), searchAllChildren: true)[0].Enabled = false;
				base.Controls.Find("textBoxColor" + k, searchAllChildren: true)[0].Enabled = false;
				base.Controls.Find("textBoxColorIndicator" + k, searchAllChildren: true)[0].Enabled = false;
			}
			updateAllColorIndicatorFromTextBoxColorTxt();
		}

		private void buttonSetDefault_Click(object sender, EventArgs e)
		{
			numericUpDownNumberOfAltitudeColors.Value = 6m;
			numericUpDownNumberOfAltitudeColors_ValueChanged(null, null);
			numericUpDown1.Value = 20m;
			numericUpDown2.Value = 40m;
			numericUpDown3.Value = 60m;
			numericUpDown4.Value = 90m;
			numericUpDown5.Value = 120m;
			numericUpDown6.Value = 123m;
			numericUpDown7.Value = 126m;
			numericUpDown8.Value = 129m;
			numericUpDown9.Value = 132m;
			numericUpDown10.Value = 135m;
			numericUpDown11.Value = 138m;
			numericUpDown12.Value = 141m;
			numericUpDown13.Value = 144m;
			numericUpDown14.Value = 147m;
			textBoxColor1.Text = "FF00FF00";
			textBoxColor2.Text = "FFFFFF00";
			textBoxColor3.Text = "FFFF0000";
			textBoxColor4.Text = "FF00FFFF";
			textBoxColor5.Text = "FF8080FF";
			textBoxColor6.Text = "FF0000FF";
			textBoxColor7.Text = "FF0000F0";
			textBoxColor8.Text = "FF0000F0";
			textBoxColor9.Text = "FF0000F0";
			textBoxColor10.Text = "FF0000F0";
			textBoxColor11.Text = "FF0000F0";
			textBoxColor12.Text = "FF0000F0";
			textBoxColor13.Text = "FF0000F0";
			textBoxColor14.Text = "FF0000F0";
			textBoxColor15.Text = "FF0000F0";
		}

		private void textBoxColor_TextChanged(object sender, EventArgs e)
		{
			string name = ((TextBox)sender).Name;
			int textBoxColorNumber = Convert.ToInt32(name.Substring(12, name.Length - 12));
			updateColorIndicatorFromTextBoxColorTxt(textBoxColorNumber);
		}

		private void updateAllColorIndicatorFromTextBoxColorTxt()
		{
			for (int i = 1; i <= 15; i++)
			{
				updateColorIndicatorFromTextBoxColorTxt(i);
			}
		}

		private void updateColorIndicatorFromTextBoxColorTxt(int textBoxColorNumber)
		{
			Control control = base.Controls.Find("textBoxColor" + textBoxColorNumber, searchAllChildren: true)[0];
			Control control2 = base.Controls.Find("textBoxColorIndicator" + textBoxColorNumber, searchAllChildren: true)[0];
			if ((decimal)textBoxColorNumber > numericUpDownNumberOfAltitudeColors.Value)
			{
				control2.BackColor = SystemColors.Control;
			}
			else
			{
				try
				{
					int blue = int.Parse(control.Text.Substring(2, 2), NumberStyles.HexNumber);
					int green = int.Parse(control.Text.Substring(4, 2), NumberStyles.HexNumber);
					int red = int.Parse(control.Text.Substring(6, 2), NumberStyles.HexNumber);
					control2.BackColor = Color.FromArgb(red, green, blue);
					control2.Text = "";
				}
				catch
				{
					control2.BackColor = SystemColors.Control;
					control2.Text = "!!!!!!!!!!!!!!";
				}
			}
		}

		private void textBoxColor_MouseClick(object sender, EventArgs e)
		{
			activeColorNumber = 0;
			textBoxCurrentColor.Text = "";
			buttonMoreColors.Enabled = true;
			numericUpDownRed.Enabled = true;
			numericUpDownBlue.Enabled = true;
			numericUpDownGreen.Enabled = true;
			string name = ((TextBox)sender).Name;
			int num = 0;
			if (name.StartsWith("textBoxColorIndicator"))
			{
				num = Convert.ToInt32(name.Substring(21, name.Length - 21));
				buttonMoreColors.Select();
				Control control = base.Controls.Find(name, searchAllChildren: true)[0];
				textBoxCurrentColor.BackColor = control.BackColor;
				numericUpDownRed.Value = textBoxCurrentColor.BackColor.R;
				textBoxCurrentColor.BackColor = control.BackColor;
				numericUpDownGreen.Value = textBoxCurrentColor.BackColor.G;
				textBoxCurrentColor.BackColor = control.BackColor;
				numericUpDownBlue.Value = textBoxCurrentColor.BackColor.B;
				textBoxCurrentColor.BackColor = control.BackColor;
			}
			else
			{
				num = Convert.ToInt32(name.Substring(12, name.Length - 12));
				Control control2 = base.Controls.Find(name.Insert(12, "Indicator"), searchAllChildren: true)[0];
				textBoxCurrentColor.BackColor = control2.BackColor;
				numericUpDownRed.Value = textBoxCurrentColor.BackColor.R;
				textBoxCurrentColor.BackColor = control2.BackColor;
				numericUpDownGreen.Value = textBoxCurrentColor.BackColor.G;
				textBoxCurrentColor.BackColor = control2.BackColor;
				numericUpDownBlue.Value = textBoxCurrentColor.BackColor.B;
				textBoxCurrentColor.BackColor = control2.BackColor;
			}
			activeColorNumber = num;
			for (int i = 1; (decimal)i <= numericUpDownNumberOfAltitudeColors.Value; i++)
			{
				Control obj = base.Controls.Find("labelKmlColor" + i, searchAllChildren: true)[0];
				obj.Text = "Kml Color";
				obj.Font = new Font(labelKmlColor3.Font, FontStyle.Regular);
			}
			Control obj2 = base.Controls.Find("labelKmlColor" + num, searchAllChildren: true)[0];
			obj2.Text = "* Kml Color *";
			obj2.Font = new Font(labelKmlColor3.Font, FontStyle.Bold);
		}

		private void buttonMoreColors_Click(object sender, EventArgs e)
		{
			myColorDialog.FullOpen = true;
			if (myColorDialog.ShowDialog() == DialogResult.OK)
			{
				numericUpDownRed.Value = myColorDialog.Color.R;
				numericUpDownGreen.Value = myColorDialog.Color.G;
				numericUpDownBlue.Value = myColorDialog.Color.B;
				Refresh();
				base.Controls.Find("textBoxColor" + activeColorNumber, searchAllChildren: true)[0].Text = "FF" + Convert.ToInt32(numericUpDownBlue.Value).ToString("X2") + Convert.ToInt32(numericUpDownGreen.Value).ToString("X2") + Convert.ToInt32(numericUpDownRed.Value).ToString("X2");
			}
		}

		private void numericUpDownRGB_ValueChanged(object sender, EventArgs e)
		{
			if (activeColorNumber != 0)
			{
				base.Controls.Find("textBoxColorIndicator" + activeColorNumber, searchAllChildren: true)[0].BackColor = Color.FromArgb((int)numericUpDownRed.Value, (int)numericUpDownGreen.Value, (int)numericUpDownBlue.Value);
			}
			textBoxCurrentColor.BackColor = Color.FromArgb((int)numericUpDownRed.Value, (int)numericUpDownGreen.Value, (int)numericUpDownBlue.Value);
		}

		private void numericUpDown_MouseUp(object sender, MouseEventArgs e)
		{
			base.Controls.Find("textBoxColor" + activeColorNumber, searchAllChildren: true)[0].Text = "FF" + Convert.ToInt32(numericUpDownBlue.Value).ToString("X2") + Convert.ToInt32(numericUpDownGreen.Value).ToString("X2") + Convert.ToInt32(numericUpDownRed.Value).ToString("X2");
		}

		private void BuildQuickColorPicker()
		{
			List<int[]> list = new List<int[]>();
			list.Add(new int[3]
			{
				255,
				128,
				128
			});
			list.Add(new int[3]
			{
				255,
				255,
				128
			});
			list.Add(new int[3]
			{
				128,
				255,
				128
			});
			list.Add(new int[3]
			{
				0,
				255,
				128
			});
			list.Add(new int[3]
			{
				128,
				255,
				255
			});
			list.Add(new int[3]
			{
				0,
				128,
				255
			});
			list.Add(new int[3]
			{
				255,
				128,
				192
			});
			list.Add(new int[3]
			{
				255,
				128,
				255
			});
			list.Add(new int[3]
			{
				255,
				0,
				0
			});
			list.Add(new int[3]
			{
				255,
				255,
				0
			});
			list.Add(new int[3]
			{
				128,
				255,
				0
			});
			list.Add(new int[3]
			{
				0,
				255,
				64
			});
			list.Add(new int[3]
			{
				0,
				255,
				255
			});
			list.Add(new int[3]
			{
				0,
				128,
				192
			});
			list.Add(new int[3]
			{
				128,
				128,
				192
			});
			list.Add(new int[3]
			{
				255,
				0,
				255
			});
			list.Add(new int[3]
			{
				128,
				64,
				64
			});
			list.Add(new int[3]
			{
				255,
				128,
				64
			});
			list.Add(new int[3]
			{
				0,
				255,
				0
			});
			list.Add(new int[3]
			{
				0,
				128,
				128
			});
			list.Add(new int[3]
			{
				0,
				64,
				128
			});
			list.Add(new int[3]
			{
				128,
				128,
				255
			});
			list.Add(new int[3]
			{
				128,
				0,
				64
			});
			list.Add(new int[3]
			{
				255,
				0,
				128
			});
			list.Add(new int[3]
			{
				128,
				0,
				0
			});
			list.Add(new int[3]
			{
				255,
				128,
				0
			});
			list.Add(new int[3]
			{
				0,
				128,
				0
			});
			list.Add(new int[3]
			{
				0,
				128,
				64
			});
			list.Add(new int[3]
			{
				0,
				0,
				255
			});
			list.Add(new int[3]
			{
				0,
				0,
				160
			});
			list.Add(new int[3]
			{
				128,
				0,
				128
			});
			list.Add(new int[3]
			{
				128,
				0,
				255
			});
			list.Add(new int[3]
			{
				64,
				0,
				0
			});
			list.Add(new int[3]
			{
				128,
				64,
				0
			});
			list.Add(new int[3]
			{
				0,
				64,
				0
			});
			list.Add(new int[3]
			{
				0,
				64,
				64
			});
			list.Add(new int[3]
			{
				0,
				0,
				128
			});
			list.Add(new int[3]
			{
				0,
				0,
				64
			});
			list.Add(new int[3]
			{
				64,
				0,
				64
			});
			list.Add(new int[3]
			{
				64,
				0,
				128
			});
			list.Add(new int[3]);
			list.Add(new int[3]
			{
				128,
				128,
				0
			});
			list.Add(new int[3]
			{
				128,
				128,
				64
			});
			list.Add(new int[3]
			{
				128,
				128,
				128
			});
			list.Add(new int[3]
			{
				64,
				128,
				128
			});
			list.Add(new int[3]
			{
				192,
				192,
				192
			});
			list.Add(new int[3]
			{
				64,
				0,
				64
			});
			list.Add(new int[3]
			{
				255,
				255,
				255
			});
			for (int i = 1; i <= 48; i++)
			{
				TextBox obj = (TextBox)base.Controls.Find("textBoxQuickColorPicker" + i, searchAllChildren: true)[0];
				obj.ReadOnly = true;
				obj.Multiline = true;
				obj.BackColor = Color.FromArgb(list[i - 1][0], list[i - 1][1], list[i - 1][2]);
				obj.MouseClick += textBoxQuickColorPicker_MouseClick;
			}
		}

		private void textBoxQuickColorPicker_MouseClick(object sender, EventArgs e)
		{
			buttonCancel.Select();
			if (activeColorNumber != 0)
			{
				string name = ((TextBox)sender).Name;
				Control control = base.Controls.Find(name, searchAllChildren: true)[0];
				textBoxCurrentColor.BackColor = control.BackColor;
				numericUpDownRed.Value = control.BackColor.R;
				numericUpDownGreen.Value = control.BackColor.G;
				numericUpDownBlue.Value = control.BackColor.B;
				Refresh();
				base.Controls.Find("textBoxColor" + activeColorNumber, searchAllChildren: true)[0].Text = "FF" + Convert.ToInt32(numericUpDownBlue.Value).ToString("X2") + Convert.ToInt32(numericUpDownGreen.Value).ToString("X2") + Convert.ToInt32(numericUpDownRed.Value).ToString("X2");
			}
			else
			{
				MessageBox.Show(this, "You need to select a color you want to change to the left before clicking on a color");
			}
		}

		private void textBoxCurrentColor_MouseClick(object sender, MouseEventArgs e)
		{
			buttonCancel.Select();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.FormConfigureKmlFile));
			buttonSave = new System.Windows.Forms.Button();
			buttonCancel = new System.Windows.Forms.Button();
			numericUpDownNumberOfAltitudeColors = new System.Windows.Forms.NumericUpDown();
			label1 = new System.Windows.Forms.Label();
			textBoxColor15 = new System.Windows.Forms.TextBox();
			richTextBox1 = new System.Windows.Forms.RichTextBox();
			numericUpDown14 = new System.Windows.Forms.NumericUpDown();
			textBoxColor14 = new System.Windows.Forms.TextBox();
			numericUpDown13 = new System.Windows.Forms.NumericUpDown();
			textBoxColor13 = new System.Windows.Forms.TextBox();
			textBoxColor11 = new System.Windows.Forms.TextBox();
			numericUpDown11 = new System.Windows.Forms.NumericUpDown();
			textBoxColor12 = new System.Windows.Forms.TextBox();
			numericUpDown12 = new System.Windows.Forms.NumericUpDown();
			textBoxColor7 = new System.Windows.Forms.TextBox();
			numericUpDown7 = new System.Windows.Forms.NumericUpDown();
			textBoxColor8 = new System.Windows.Forms.TextBox();
			numericUpDown8 = new System.Windows.Forms.NumericUpDown();
			textBoxColor9 = new System.Windows.Forms.TextBox();
			numericUpDown9 = new System.Windows.Forms.NumericUpDown();
			textBoxColor10 = new System.Windows.Forms.TextBox();
			numericUpDown10 = new System.Windows.Forms.NumericUpDown();
			textBoxColor1 = new System.Windows.Forms.TextBox();
			numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			textBoxColor2 = new System.Windows.Forms.TextBox();
			numericUpDown2 = new System.Windows.Forms.NumericUpDown();
			textBoxColor3 = new System.Windows.Forms.TextBox();
			numericUpDown3 = new System.Windows.Forms.NumericUpDown();
			textBoxColor4 = new System.Windows.Forms.TextBox();
			numericUpDown4 = new System.Windows.Forms.NumericUpDown();
			textBoxColor5 = new System.Windows.Forms.TextBox();
			numericUpDown5 = new System.Windows.Forms.NumericUpDown();
			textBoxColor6 = new System.Windows.Forms.TextBox();
			numericUpDown6 = new System.Windows.Forms.NumericUpDown();
			label2 = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			label4 = new System.Windows.Forms.Label();
			label5 = new System.Windows.Forms.Label();
			label6 = new System.Windows.Forms.Label();
			label7 = new System.Windows.Forms.Label();
			label8 = new System.Windows.Forms.Label();
			label9 = new System.Windows.Forms.Label();
			label10 = new System.Windows.Forms.Label();
			label11 = new System.Windows.Forms.Label();
			label12 = new System.Windows.Forms.Label();
			label13 = new System.Windows.Forms.Label();
			label14 = new System.Windows.Forms.Label();
			label15 = new System.Windows.Forms.Label();
			labelKmlColor15 = new System.Windows.Forms.Label();
			labelKmlColor14 = new System.Windows.Forms.Label();
			labelKmlColor13 = new System.Windows.Forms.Label();
			labelKmlColor12 = new System.Windows.Forms.Label();
			labelKmlColor11 = new System.Windows.Forms.Label();
			labelKmlColor10 = new System.Windows.Forms.Label();
			labelKmlColor9 = new System.Windows.Forms.Label();
			labelKmlColor8 = new System.Windows.Forms.Label();
			labelKmlColor7 = new System.Windows.Forms.Label();
			labelKmlColor6 = new System.Windows.Forms.Label();
			labelKmlColor5 = new System.Windows.Forms.Label();
			labelKmlColor4 = new System.Windows.Forms.Label();
			labelKmlColor3 = new System.Windows.Forms.Label();
			labelKmlColor2 = new System.Windows.Forms.Label();
			labelKmlColor1 = new System.Windows.Forms.Label();
			buttonSetDefault = new System.Windows.Forms.Button();
			buttonMoreColors = new System.Windows.Forms.Button();
			label31 = new System.Windows.Forms.Label();
			label32 = new System.Windows.Forms.Label();
			label33 = new System.Windows.Forms.Label();
			numericUpDownBlue = new System.Windows.Forms.NumericUpDown();
			numericUpDownGreen = new System.Windows.Forms.NumericUpDown();
			numericUpDownRed = new System.Windows.Forms.NumericUpDown();
			textBoxCurrentColor = new System.Windows.Forms.TextBox();
			myColorDialog = new System.Windows.Forms.ColorDialog();
			label16 = new System.Windows.Forms.Label();
			label17 = new System.Windows.Forms.Label();
			textBoxQuickColorPicker1 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker2 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker4 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker3 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker8 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker7 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker6 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker5 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker16 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker15 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker14 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker13 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker12 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker11 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker10 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker9 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker32 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker31 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker30 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker29 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker28 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker27 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker26 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker25 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker24 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker23 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker22 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker21 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker20 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker19 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker18 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker17 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker48 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker47 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker46 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker45 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker44 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker43 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker42 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker41 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker40 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker39 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker38 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker37 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker36 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker35 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker34 = new System.Windows.Forms.TextBox();
			textBoxQuickColorPicker33 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator15 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator14 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator12 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator13 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator8 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator9 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator10 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator11 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator1 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator2 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator3 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator4 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator5 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator6 = new System.Windows.Forms.TextBox();
			textBoxColorIndicator7 = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)numericUpDownNumberOfAltitudeColors).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown14).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown13).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown11).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown12).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown7).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown8).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown9).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown10).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown1).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown2).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown3).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown4).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown5).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown6).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDownBlue).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDownGreen).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDownRed).BeginInit();
			SuspendLayout();
			buttonSave.Location = new System.Drawing.Point(359, 627);
			buttonSave.Name = "buttonSave";
			buttonSave.Size = new System.Drawing.Size(110, 23);
			buttonSave.TabIndex = 0;
			buttonSave.Text = "Save and Close";
			buttonSave.UseVisualStyleBackColor = true;
			buttonSave.Click += new System.EventHandler(buttonSave_Click);
			buttonCancel.Location = new System.Drawing.Point(481, 627);
			buttonCancel.Name = "buttonCancel";
			buttonCancel.Size = new System.Drawing.Size(75, 23);
			buttonCancel.TabIndex = 1;
			buttonCancel.Text = "Cancel";
			buttonCancel.UseVisualStyleBackColor = true;
			buttonCancel.Click += new System.EventHandler(buttonCancel_Click);
			numericUpDownNumberOfAltitudeColors.Location = new System.Drawing.Point(413, 188);
			numericUpDownNumberOfAltitudeColors.Maximum = new decimal(new int[4]
			{
				15,
				0,
				0,
				0
			});
			numericUpDownNumberOfAltitudeColors.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDownNumberOfAltitudeColors.Name = "numericUpDownNumberOfAltitudeColors";
			numericUpDownNumberOfAltitudeColors.Size = new System.Drawing.Size(38, 20);
			numericUpDownNumberOfAltitudeColors.TabIndex = 2;
			numericUpDownNumberOfAltitudeColors.Value = new decimal(new int[4]
			{
				6,
				0,
				0,
				0
			});
			numericUpDownNumberOfAltitudeColors.ValueChanged += new System.EventHandler(numericUpDownNumberOfAltitudeColors_ValueChanged);
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(283, 190);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(124, 13);
			label1.TabIndex = 3;
			label1.Text = "Number of altitude colors";
			textBoxColor15.Location = new System.Drawing.Point(75, 14);
			textBoxColor15.MaxLength = 8;
			textBoxColor15.Name = "textBoxColor15";
			textBoxColor15.Size = new System.Drawing.Size(68, 20);
			textBoxColor15.TabIndex = 4;
			textBoxColor15.Text = "FF00FF00";
			textBoxColor15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			richTextBox1.Location = new System.Drawing.Point(228, 12);
			richTextBox1.Name = "richTextBox1";
			richTextBox1.Size = new System.Drawing.Size(328, 141);
			richTextBox1.TabIndex = 10;
			richTextBox1.Text = resources.GetString("richTextBox1.Text");
			numericUpDown14.Location = new System.Drawing.Point(105, 36);
			numericUpDown14.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown14.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown14.Name = "numericUpDown14";
			numericUpDown14.Size = new System.Drawing.Size(38, 20);
			numericUpDown14.TabIndex = 11;
			numericUpDown14.Value = new decimal(new int[4]
			{
				147,
				0,
				0,
				0
			});
			textBoxColor14.Location = new System.Drawing.Point(75, 58);
			textBoxColor14.MaxLength = 8;
			textBoxColor14.Name = "textBoxColor14";
			textBoxColor14.Size = new System.Drawing.Size(68, 20);
			textBoxColor14.TabIndex = 12;
			textBoxColor14.Text = "FF00FF00";
			textBoxColor14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown13.Location = new System.Drawing.Point(105, 80);
			numericUpDown13.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown13.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown13.Name = "numericUpDown13";
			numericUpDown13.Size = new System.Drawing.Size(38, 20);
			numericUpDown13.TabIndex = 13;
			numericUpDown13.Value = new decimal(new int[4]
			{
				144,
				0,
				0,
				0
			});
			textBoxColor13.Location = new System.Drawing.Point(75, 102);
			textBoxColor13.MaxLength = 8;
			textBoxColor13.Name = "textBoxColor13";
			textBoxColor13.Size = new System.Drawing.Size(68, 20);
			textBoxColor13.TabIndex = 14;
			textBoxColor13.Text = "FF00FF00";
			textBoxColor13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			textBoxColor11.Location = new System.Drawing.Point(75, 190);
			textBoxColor11.MaxLength = 8;
			textBoxColor11.Name = "textBoxColor11";
			textBoxColor11.Size = new System.Drawing.Size(68, 20);
			textBoxColor11.TabIndex = 18;
			textBoxColor11.Text = "FF00FF00";
			textBoxColor11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown11.Location = new System.Drawing.Point(105, 168);
			numericUpDown11.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown11.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown11.Name = "numericUpDown11";
			numericUpDown11.Size = new System.Drawing.Size(38, 20);
			numericUpDown11.TabIndex = 17;
			numericUpDown11.Value = new decimal(new int[4]
			{
				138,
				0,
				0,
				0
			});
			textBoxColor12.Location = new System.Drawing.Point(75, 146);
			textBoxColor12.MaxLength = 8;
			textBoxColor12.Name = "textBoxColor12";
			textBoxColor12.Size = new System.Drawing.Size(68, 20);
			textBoxColor12.TabIndex = 16;
			textBoxColor12.Text = "FF00FF00";
			textBoxColor12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown12.Location = new System.Drawing.Point(105, 124);
			numericUpDown12.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown12.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown12.Name = "numericUpDown12";
			numericUpDown12.Size = new System.Drawing.Size(38, 20);
			numericUpDown12.TabIndex = 15;
			numericUpDown12.Value = new decimal(new int[4]
			{
				141,
				0,
				0,
				0
			});
			textBoxColor7.Location = new System.Drawing.Point(75, 366);
			textBoxColor7.MaxLength = 8;
			textBoxColor7.Name = "textBoxColor7";
			textBoxColor7.Size = new System.Drawing.Size(68, 20);
			textBoxColor7.TabIndex = 26;
			textBoxColor7.Text = "FF00FF00";
			textBoxColor7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown7.Location = new System.Drawing.Point(105, 344);
			numericUpDown7.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown7.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown7.Name = "numericUpDown7";
			numericUpDown7.Size = new System.Drawing.Size(38, 20);
			numericUpDown7.TabIndex = 25;
			numericUpDown7.Value = new decimal(new int[4]
			{
				126,
				0,
				0,
				0
			});
			textBoxColor8.Location = new System.Drawing.Point(75, 322);
			textBoxColor8.MaxLength = 8;
			textBoxColor8.Name = "textBoxColor8";
			textBoxColor8.Size = new System.Drawing.Size(68, 20);
			textBoxColor8.TabIndex = 24;
			textBoxColor8.Text = "FF00FF00";
			textBoxColor8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown8.Location = new System.Drawing.Point(105, 300);
			numericUpDown8.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown8.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown8.Name = "numericUpDown8";
			numericUpDown8.Size = new System.Drawing.Size(38, 20);
			numericUpDown8.TabIndex = 23;
			numericUpDown8.Value = new decimal(new int[4]
			{
				129,
				0,
				0,
				0
			});
			textBoxColor9.Location = new System.Drawing.Point(75, 278);
			textBoxColor9.MaxLength = 8;
			textBoxColor9.Name = "textBoxColor9";
			textBoxColor9.Size = new System.Drawing.Size(68, 20);
			textBoxColor9.TabIndex = 22;
			textBoxColor9.Text = "FF00FF00";
			textBoxColor9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown9.Location = new System.Drawing.Point(105, 256);
			numericUpDown9.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown9.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown9.Name = "numericUpDown9";
			numericUpDown9.Size = new System.Drawing.Size(38, 20);
			numericUpDown9.TabIndex = 21;
			numericUpDown9.Value = new decimal(new int[4]
			{
				132,
				0,
				0,
				0
			});
			textBoxColor10.Location = new System.Drawing.Point(75, 234);
			textBoxColor10.MaxLength = 8;
			textBoxColor10.Name = "textBoxColor10";
			textBoxColor10.Size = new System.Drawing.Size(68, 20);
			textBoxColor10.TabIndex = 20;
			textBoxColor10.Text = "FF00FF00";
			textBoxColor10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown10.Location = new System.Drawing.Point(105, 212);
			numericUpDown10.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown10.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown10.Name = "numericUpDown10";
			numericUpDown10.Size = new System.Drawing.Size(38, 20);
			numericUpDown10.TabIndex = 19;
			numericUpDown10.Value = new decimal(new int[4]
			{
				135,
				0,
				0,
				0
			});
			textBoxColor1.Location = new System.Drawing.Point(75, 630);
			textBoxColor1.MaxLength = 8;
			textBoxColor1.Name = "textBoxColor1";
			textBoxColor1.Size = new System.Drawing.Size(68, 20);
			textBoxColor1.TabIndex = 38;
			textBoxColor1.Text = "FF00FF00";
			textBoxColor1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown1.Location = new System.Drawing.Point(105, 608);
			numericUpDown1.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown1.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown1.Name = "numericUpDown1";
			numericUpDown1.Size = new System.Drawing.Size(38, 20);
			numericUpDown1.TabIndex = 37;
			numericUpDown1.Value = new decimal(new int[4]
			{
				20,
				0,
				0,
				0
			});
			textBoxColor2.Location = new System.Drawing.Point(75, 586);
			textBoxColor2.MaxLength = 8;
			textBoxColor2.Name = "textBoxColor2";
			textBoxColor2.Size = new System.Drawing.Size(68, 20);
			textBoxColor2.TabIndex = 36;
			textBoxColor2.Text = "FFFFFF00";
			textBoxColor2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown2.Location = new System.Drawing.Point(105, 564);
			numericUpDown2.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown2.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown2.Name = "numericUpDown2";
			numericUpDown2.Size = new System.Drawing.Size(38, 20);
			numericUpDown2.TabIndex = 35;
			numericUpDown2.Value = new decimal(new int[4]
			{
				40,
				0,
				0,
				0
			});
			textBoxColor3.Location = new System.Drawing.Point(75, 542);
			textBoxColor3.MaxLength = 8;
			textBoxColor3.Name = "textBoxColor3";
			textBoxColor3.Size = new System.Drawing.Size(68, 20);
			textBoxColor3.TabIndex = 34;
			textBoxColor3.Text = "FFFF0000";
			textBoxColor3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown3.Location = new System.Drawing.Point(105, 520);
			numericUpDown3.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown3.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown3.Name = "numericUpDown3";
			numericUpDown3.Size = new System.Drawing.Size(38, 20);
			numericUpDown3.TabIndex = 33;
			numericUpDown3.Value = new decimal(new int[4]
			{
				60,
				0,
				0,
				0
			});
			textBoxColor4.Location = new System.Drawing.Point(75, 498);
			textBoxColor4.MaxLength = 8;
			textBoxColor4.Name = "textBoxColor4";
			textBoxColor4.Size = new System.Drawing.Size(68, 20);
			textBoxColor4.TabIndex = 32;
			textBoxColor4.Text = "FF00FFFF";
			textBoxColor4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown4.Location = new System.Drawing.Point(105, 476);
			numericUpDown4.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown4.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown4.Name = "numericUpDown4";
			numericUpDown4.Size = new System.Drawing.Size(38, 20);
			numericUpDown4.TabIndex = 31;
			numericUpDown4.Value = new decimal(new int[4]
			{
				90,
				0,
				0,
				0
			});
			textBoxColor5.Location = new System.Drawing.Point(75, 454);
			textBoxColor5.MaxLength = 8;
			textBoxColor5.Name = "textBoxColor5";
			textBoxColor5.Size = new System.Drawing.Size(68, 20);
			textBoxColor5.TabIndex = 30;
			textBoxColor5.Text = "FF8080FF";
			textBoxColor5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown5.Location = new System.Drawing.Point(105, 432);
			numericUpDown5.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown5.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown5.Name = "numericUpDown5";
			numericUpDown5.Size = new System.Drawing.Size(38, 20);
			numericUpDown5.TabIndex = 29;
			numericUpDown5.Value = new decimal(new int[4]
			{
				120,
				0,
				0,
				0
			});
			textBoxColor6.Location = new System.Drawing.Point(75, 410);
			textBoxColor6.MaxLength = 8;
			textBoxColor6.Name = "textBoxColor6";
			textBoxColor6.Size = new System.Drawing.Size(68, 20);
			textBoxColor6.TabIndex = 28;
			textBoxColor6.Text = "FF0000FF";
			textBoxColor6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			numericUpDown6.Location = new System.Drawing.Point(105, 388);
			numericUpDown6.Maximum = new decimal(new int[4]
			{
				160,
				0,
				0,
				0
			});
			numericUpDown6.Minimum = new decimal(new int[4]
			{
				2,
				0,
				0,
				0
			});
			numericUpDown6.Name = "numericUpDown6";
			numericUpDown6.Size = new System.Drawing.Size(38, 20);
			numericUpDown6.TabIndex = 27;
			numericUpDown6.Value = new decimal(new int[4]
			{
				123,
				0,
				0,
				0
			});
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(149, 38);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(62, 13);
			label2.TabIndex = 39;
			label2.Text = "Altitude limit";
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(149, 82);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(62, 13);
			label3.TabIndex = 39;
			label3.Text = "Altitude limit";
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(149, 126);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(62, 13);
			label4.TabIndex = 39;
			label4.Text = "Altitude limit";
			label5.AutoSize = true;
			label5.Location = new System.Drawing.Point(149, 170);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(62, 13);
			label5.TabIndex = 39;
			label5.Text = "Altitude limit";
			label6.AutoSize = true;
			label6.Location = new System.Drawing.Point(149, 214);
			label6.Name = "label6";
			label6.Size = new System.Drawing.Size(62, 13);
			label6.TabIndex = 39;
			label6.Text = "Altitude limit";
			label7.AutoSize = true;
			label7.Location = new System.Drawing.Point(149, 258);
			label7.Name = "label7";
			label7.Size = new System.Drawing.Size(62, 13);
			label7.TabIndex = 39;
			label7.Text = "Altitude limit";
			label8.AutoSize = true;
			label8.Location = new System.Drawing.Point(149, 302);
			label8.Name = "label8";
			label8.Size = new System.Drawing.Size(62, 13);
			label8.TabIndex = 39;
			label8.Text = "Altitude limit";
			label9.AutoSize = true;
			label9.Location = new System.Drawing.Point(149, 346);
			label9.Name = "label9";
			label9.Size = new System.Drawing.Size(62, 13);
			label9.TabIndex = 39;
			label9.Text = "Altitude limit";
			label10.AutoSize = true;
			label10.Location = new System.Drawing.Point(149, 390);
			label10.Name = "label10";
			label10.Size = new System.Drawing.Size(62, 13);
			label10.TabIndex = 39;
			label10.Text = "Altitude limit";
			label11.AutoSize = true;
			label11.Location = new System.Drawing.Point(149, 434);
			label11.Name = "label11";
			label11.Size = new System.Drawing.Size(62, 13);
			label11.TabIndex = 39;
			label11.Text = "Altitude limit";
			label12.AutoSize = true;
			label12.Location = new System.Drawing.Point(149, 527);
			label12.Name = "label12";
			label12.Size = new System.Drawing.Size(62, 13);
			label12.TabIndex = 39;
			label12.Text = "Altitude limit";
			label13.AutoSize = true;
			label13.Location = new System.Drawing.Point(149, 478);
			label13.Name = "label13";
			label13.Size = new System.Drawing.Size(62, 13);
			label13.TabIndex = 39;
			label13.Text = "Altitude limit";
			label14.AutoSize = true;
			label14.Location = new System.Drawing.Point(149, 610);
			label14.Name = "label14";
			label14.Size = new System.Drawing.Size(62, 13);
			label14.TabIndex = 39;
			label14.Text = "Altitude limit";
			label15.AutoSize = true;
			label15.Location = new System.Drawing.Point(149, 566);
			label15.Name = "label15";
			label15.Size = new System.Drawing.Size(62, 13);
			label15.TabIndex = 39;
			label15.Text = "Altitude limit";
			labelKmlColor15.AutoSize = true;
			labelKmlColor15.Location = new System.Drawing.Point(149, 17);
			labelKmlColor15.Name = "labelKmlColor15";
			labelKmlColor15.Size = new System.Drawing.Size(51, 13);
			labelKmlColor15.TabIndex = 40;
			labelKmlColor15.Text = "Kml Color";
			labelKmlColor14.AutoSize = true;
			labelKmlColor14.Location = new System.Drawing.Point(149, 61);
			labelKmlColor14.Name = "labelKmlColor14";
			labelKmlColor14.Size = new System.Drawing.Size(51, 13);
			labelKmlColor14.TabIndex = 40;
			labelKmlColor14.Text = "Kml Color";
			labelKmlColor13.AutoSize = true;
			labelKmlColor13.Location = new System.Drawing.Point(149, 105);
			labelKmlColor13.Name = "labelKmlColor13";
			labelKmlColor13.Size = new System.Drawing.Size(51, 13);
			labelKmlColor13.TabIndex = 40;
			labelKmlColor13.Text = "Kml Color";
			labelKmlColor12.AutoSize = true;
			labelKmlColor12.Location = new System.Drawing.Point(149, 149);
			labelKmlColor12.Name = "labelKmlColor12";
			labelKmlColor12.Size = new System.Drawing.Size(51, 13);
			labelKmlColor12.TabIndex = 40;
			labelKmlColor12.Text = "Kml Color";
			labelKmlColor11.AutoSize = true;
			labelKmlColor11.Location = new System.Drawing.Point(149, 193);
			labelKmlColor11.Name = "labelKmlColor11";
			labelKmlColor11.Size = new System.Drawing.Size(51, 13);
			labelKmlColor11.TabIndex = 40;
			labelKmlColor11.Text = "Kml Color";
			labelKmlColor10.AutoSize = true;
			labelKmlColor10.Location = new System.Drawing.Point(149, 237);
			labelKmlColor10.Name = "labelKmlColor10";
			labelKmlColor10.Size = new System.Drawing.Size(51, 13);
			labelKmlColor10.TabIndex = 40;
			labelKmlColor10.Text = "Kml Color";
			labelKmlColor9.AutoSize = true;
			labelKmlColor9.Location = new System.Drawing.Point(149, 281);
			labelKmlColor9.Name = "labelKmlColor9";
			labelKmlColor9.Size = new System.Drawing.Size(51, 13);
			labelKmlColor9.TabIndex = 40;
			labelKmlColor9.Text = "Kml Color";
			labelKmlColor8.AutoSize = true;
			labelKmlColor8.Location = new System.Drawing.Point(149, 325);
			labelKmlColor8.Name = "labelKmlColor8";
			labelKmlColor8.Size = new System.Drawing.Size(51, 13);
			labelKmlColor8.TabIndex = 40;
			labelKmlColor8.Text = "Kml Color";
			labelKmlColor7.AutoSize = true;
			labelKmlColor7.Location = new System.Drawing.Point(149, 369);
			labelKmlColor7.Name = "labelKmlColor7";
			labelKmlColor7.Size = new System.Drawing.Size(51, 13);
			labelKmlColor7.TabIndex = 40;
			labelKmlColor7.Text = "Kml Color";
			labelKmlColor6.AutoSize = true;
			labelKmlColor6.Location = new System.Drawing.Point(149, 413);
			labelKmlColor6.Name = "labelKmlColor6";
			labelKmlColor6.Size = new System.Drawing.Size(51, 13);
			labelKmlColor6.TabIndex = 40;
			labelKmlColor6.Text = "Kml Color";
			labelKmlColor5.AutoSize = true;
			labelKmlColor5.Location = new System.Drawing.Point(149, 457);
			labelKmlColor5.Name = "labelKmlColor5";
			labelKmlColor5.Size = new System.Drawing.Size(51, 13);
			labelKmlColor5.TabIndex = 40;
			labelKmlColor5.Text = "Kml Color";
			labelKmlColor4.AutoSize = true;
			labelKmlColor4.Location = new System.Drawing.Point(149, 501);
			labelKmlColor4.Name = "labelKmlColor4";
			labelKmlColor4.Size = new System.Drawing.Size(51, 13);
			labelKmlColor4.TabIndex = 40;
			labelKmlColor4.Text = "Kml Color";
			labelKmlColor3.AutoSize = true;
			labelKmlColor3.Location = new System.Drawing.Point(149, 545);
			labelKmlColor3.Name = "labelKmlColor3";
			labelKmlColor3.Size = new System.Drawing.Size(51, 13);
			labelKmlColor3.TabIndex = 40;
			labelKmlColor3.Text = "Kml Color";
			labelKmlColor2.AutoSize = true;
			labelKmlColor2.Location = new System.Drawing.Point(149, 589);
			labelKmlColor2.Name = "labelKmlColor2";
			labelKmlColor2.Size = new System.Drawing.Size(51, 13);
			labelKmlColor2.TabIndex = 40;
			labelKmlColor2.Text = "Kml Color";
			labelKmlColor1.AutoSize = true;
			labelKmlColor1.Location = new System.Drawing.Point(149, 633);
			labelKmlColor1.Name = "labelKmlColor1";
			labelKmlColor1.Size = new System.Drawing.Size(51, 13);
			labelKmlColor1.TabIndex = 40;
			labelKmlColor1.Text = "Kml Color";
			buttonSetDefault.Location = new System.Drawing.Point(245, 627);
			buttonSetDefault.Name = "buttonSetDefault";
			buttonSetDefault.Size = new System.Drawing.Size(104, 23);
			buttonSetDefault.TabIndex = 41;
			buttonSetDefault.Text = "Set default values";
			buttonSetDefault.UseVisualStyleBackColor = true;
			buttonSetDefault.Click += new System.EventHandler(buttonSetDefault_Click);
			buttonMoreColors.Location = new System.Drawing.Point(264, 556);
			buttonMoreColors.Name = "buttonMoreColors";
			buttonMoreColors.Size = new System.Drawing.Size(225, 23);
			buttonMoreColors.TabIndex = 49;
			buttonMoreColors.Text = "Select from advanced color selection";
			buttonMoreColors.UseVisualStyleBackColor = true;
			buttonMoreColors.Click += new System.EventHandler(buttonMoreColors_Click);
			label31.AutoSize = true;
			label31.Location = new System.Drawing.Point(424, 498);
			label31.Name = "label31";
			label31.Size = new System.Drawing.Size(28, 13);
			label31.TabIndex = 48;
			label31.Text = "Blue";
			label32.AutoSize = true;
			label32.Location = new System.Drawing.Point(357, 498);
			label32.Name = "label32";
			label32.Size = new System.Drawing.Size(36, 13);
			label32.TabIndex = 47;
			label32.Text = "Green";
			label33.AutoSize = true;
			label33.Location = new System.Drawing.Point(297, 498);
			label33.Name = "label33";
			label33.Size = new System.Drawing.Size(27, 13);
			label33.TabIndex = 46;
			label33.Text = "Red";
			numericUpDownBlue.Location = new System.Drawing.Point(416, 514);
			numericUpDownBlue.Maximum = new decimal(new int[4]
			{
				255,
				0,
				0,
				0
			});
			numericUpDownBlue.Name = "numericUpDownBlue";
			numericUpDownBlue.Size = new System.Drawing.Size(45, 20);
			numericUpDownBlue.TabIndex = 45;
			numericUpDownBlue.ValueChanged += new System.EventHandler(numericUpDownRGB_ValueChanged);
			numericUpDownBlue.MouseUp += new System.Windows.Forms.MouseEventHandler(numericUpDown_MouseUp);
			numericUpDownGreen.Location = new System.Drawing.Point(351, 514);
			numericUpDownGreen.Maximum = new decimal(new int[4]
			{
				255,
				0,
				0,
				0
			});
			numericUpDownGreen.Name = "numericUpDownGreen";
			numericUpDownGreen.Size = new System.Drawing.Size(45, 20);
			numericUpDownGreen.TabIndex = 44;
			numericUpDownGreen.Value = new decimal(new int[4]
			{
				255,
				0,
				0,
				0
			});
			numericUpDownGreen.ValueChanged += new System.EventHandler(numericUpDownRGB_ValueChanged);
			numericUpDownGreen.MouseUp += new System.Windows.Forms.MouseEventHandler(numericUpDown_MouseUp);
			numericUpDownRed.Location = new System.Drawing.Point(288, 514);
			numericUpDownRed.Maximum = new decimal(new int[4]
			{
				255,
				0,
				0,
				0
			});
			numericUpDownRed.Name = "numericUpDownRed";
			numericUpDownRed.Size = new System.Drawing.Size(45, 20);
			numericUpDownRed.TabIndex = 43;
			numericUpDownRed.ValueChanged += new System.EventHandler(numericUpDownRGB_ValueChanged);
			numericUpDownRed.MouseUp += new System.Windows.Forms.MouseEventHandler(numericUpDown_MouseUp);
			textBoxCurrentColor.Location = new System.Drawing.Point(264, 441);
			textBoxCurrentColor.Multiline = true;
			textBoxCurrentColor.Name = "textBoxCurrentColor";
			textBoxCurrentColor.ReadOnly = true;
			textBoxCurrentColor.Size = new System.Drawing.Size(225, 52);
			textBoxCurrentColor.TabIndex = 42;
			textBoxCurrentColor.Text = "Select a color to change to the left";
			textBoxCurrentColor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			textBoxCurrentColor.MouseClick += new System.Windows.Forms.MouseEventHandler(textBoxCurrentColor_MouseClick);
			label16.AutoSize = true;
			label16.Location = new System.Drawing.Point(329, 427);
			label16.Name = "label16";
			label16.Size = new System.Drawing.Size(78, 13);
			label16.TabIndex = 50;
			label16.Text = "Selected color:";
			label17.AutoSize = true;
			label17.Location = new System.Drawing.Point(324, 232);
			label17.Name = "label17";
			label17.Size = new System.Drawing.Size(104, 13);
			label17.TabIndex = 51;
			label17.Text = "Quick select a color:";
			textBoxQuickColorPicker1.Location = new System.Drawing.Point(267, 249);
			textBoxQuickColorPicker1.Name = "textBoxQuickColorPicker1";
			textBoxQuickColorPicker1.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker1.TabIndex = 52;
			textBoxQuickColorPicker2.Location = new System.Drawing.Point(295, 249);
			textBoxQuickColorPicker2.Name = "textBoxQuickColorPicker2";
			textBoxQuickColorPicker2.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker2.TabIndex = 53;
			textBoxQuickColorPicker4.Location = new System.Drawing.Point(351, 249);
			textBoxQuickColorPicker4.Name = "textBoxQuickColorPicker4";
			textBoxQuickColorPicker4.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker4.TabIndex = 56;
			textBoxQuickColorPicker3.Location = new System.Drawing.Point(323, 249);
			textBoxQuickColorPicker3.Name = "textBoxQuickColorPicker3";
			textBoxQuickColorPicker3.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker3.TabIndex = 55;
			textBoxQuickColorPicker8.Location = new System.Drawing.Point(463, 249);
			textBoxQuickColorPicker8.Name = "textBoxQuickColorPicker8";
			textBoxQuickColorPicker8.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker8.TabIndex = 60;
			textBoxQuickColorPicker7.Location = new System.Drawing.Point(435, 249);
			textBoxQuickColorPicker7.Name = "textBoxQuickColorPicker7";
			textBoxQuickColorPicker7.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker7.TabIndex = 59;
			textBoxQuickColorPicker6.Location = new System.Drawing.Point(407, 249);
			textBoxQuickColorPicker6.Name = "textBoxQuickColorPicker6";
			textBoxQuickColorPicker6.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker6.TabIndex = 58;
			textBoxQuickColorPicker5.Location = new System.Drawing.Point(379, 249);
			textBoxQuickColorPicker5.Name = "textBoxQuickColorPicker5";
			textBoxQuickColorPicker5.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker5.TabIndex = 57;
			textBoxQuickColorPicker16.Location = new System.Drawing.Point(463, 275);
			textBoxQuickColorPicker16.Name = "textBoxQuickColorPicker16";
			textBoxQuickColorPicker16.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker16.TabIndex = 68;
			textBoxQuickColorPicker15.Location = new System.Drawing.Point(435, 275);
			textBoxQuickColorPicker15.Name = "textBoxQuickColorPicker15";
			textBoxQuickColorPicker15.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker15.TabIndex = 67;
			textBoxQuickColorPicker14.Location = new System.Drawing.Point(407, 275);
			textBoxQuickColorPicker14.Name = "textBoxQuickColorPicker14";
			textBoxQuickColorPicker14.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker14.TabIndex = 66;
			textBoxQuickColorPicker13.Location = new System.Drawing.Point(379, 275);
			textBoxQuickColorPicker13.Name = "textBoxQuickColorPicker13";
			textBoxQuickColorPicker13.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker13.TabIndex = 65;
			textBoxQuickColorPicker12.Location = new System.Drawing.Point(351, 275);
			textBoxQuickColorPicker12.Name = "textBoxQuickColorPicker12";
			textBoxQuickColorPicker12.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker12.TabIndex = 64;
			textBoxQuickColorPicker11.Location = new System.Drawing.Point(323, 275);
			textBoxQuickColorPicker11.Name = "textBoxQuickColorPicker11";
			textBoxQuickColorPicker11.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker11.TabIndex = 63;
			textBoxQuickColorPicker10.Location = new System.Drawing.Point(295, 275);
			textBoxQuickColorPicker10.Name = "textBoxQuickColorPicker10";
			textBoxQuickColorPicker10.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker10.TabIndex = 62;
			textBoxQuickColorPicker9.Location = new System.Drawing.Point(267, 275);
			textBoxQuickColorPicker9.Name = "textBoxQuickColorPicker9";
			textBoxQuickColorPicker9.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker9.TabIndex = 61;
			textBoxQuickColorPicker32.Location = new System.Drawing.Point(463, 327);
			textBoxQuickColorPicker32.Name = "textBoxQuickColorPicker32";
			textBoxQuickColorPicker32.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker32.TabIndex = 84;
			textBoxQuickColorPicker31.Location = new System.Drawing.Point(435, 327);
			textBoxQuickColorPicker31.Name = "textBoxQuickColorPicker31";
			textBoxQuickColorPicker31.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker31.TabIndex = 83;
			textBoxQuickColorPicker30.Location = new System.Drawing.Point(407, 327);
			textBoxQuickColorPicker30.Name = "textBoxQuickColorPicker30";
			textBoxQuickColorPicker30.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker30.TabIndex = 82;
			textBoxQuickColorPicker29.Location = new System.Drawing.Point(379, 327);
			textBoxQuickColorPicker29.Name = "textBoxQuickColorPicker29";
			textBoxQuickColorPicker29.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker29.TabIndex = 81;
			textBoxQuickColorPicker28.Location = new System.Drawing.Point(351, 327);
			textBoxQuickColorPicker28.Name = "textBoxQuickColorPicker28";
			textBoxQuickColorPicker28.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker28.TabIndex = 80;
			textBoxQuickColorPicker27.Location = new System.Drawing.Point(323, 327);
			textBoxQuickColorPicker27.Name = "textBoxQuickColorPicker27";
			textBoxQuickColorPicker27.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker27.TabIndex = 79;
			textBoxQuickColorPicker26.Location = new System.Drawing.Point(295, 327);
			textBoxQuickColorPicker26.Name = "textBoxQuickColorPicker26";
			textBoxQuickColorPicker26.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker26.TabIndex = 78;
			textBoxQuickColorPicker25.Location = new System.Drawing.Point(267, 327);
			textBoxQuickColorPicker25.Name = "textBoxQuickColorPicker25";
			textBoxQuickColorPicker25.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker25.TabIndex = 77;
			textBoxQuickColorPicker24.Location = new System.Drawing.Point(463, 301);
			textBoxQuickColorPicker24.Name = "textBoxQuickColorPicker24";
			textBoxQuickColorPicker24.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker24.TabIndex = 76;
			textBoxQuickColorPicker23.Location = new System.Drawing.Point(435, 301);
			textBoxQuickColorPicker23.Name = "textBoxQuickColorPicker23";
			textBoxQuickColorPicker23.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker23.TabIndex = 75;
			textBoxQuickColorPicker22.Location = new System.Drawing.Point(407, 301);
			textBoxQuickColorPicker22.Name = "textBoxQuickColorPicker22";
			textBoxQuickColorPicker22.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker22.TabIndex = 74;
			textBoxQuickColorPicker21.Location = new System.Drawing.Point(379, 301);
			textBoxQuickColorPicker21.Name = "textBoxQuickColorPicker21";
			textBoxQuickColorPicker21.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker21.TabIndex = 73;
			textBoxQuickColorPicker20.Location = new System.Drawing.Point(351, 301);
			textBoxQuickColorPicker20.Name = "textBoxQuickColorPicker20";
			textBoxQuickColorPicker20.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker20.TabIndex = 72;
			textBoxQuickColorPicker19.Location = new System.Drawing.Point(323, 301);
			textBoxQuickColorPicker19.Name = "textBoxQuickColorPicker19";
			textBoxQuickColorPicker19.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker19.TabIndex = 71;
			textBoxQuickColorPicker18.Location = new System.Drawing.Point(295, 301);
			textBoxQuickColorPicker18.Name = "textBoxQuickColorPicker18";
			textBoxQuickColorPicker18.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker18.TabIndex = 70;
			textBoxQuickColorPicker17.Location = new System.Drawing.Point(267, 301);
			textBoxQuickColorPicker17.Name = "textBoxQuickColorPicker17";
			textBoxQuickColorPicker17.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker17.TabIndex = 69;
			textBoxQuickColorPicker48.Location = new System.Drawing.Point(463, 379);
			textBoxQuickColorPicker48.Name = "textBoxQuickColorPicker48";
			textBoxQuickColorPicker48.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker48.TabIndex = 100;
			textBoxQuickColorPicker47.Location = new System.Drawing.Point(435, 379);
			textBoxQuickColorPicker47.Name = "textBoxQuickColorPicker47";
			textBoxQuickColorPicker47.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker47.TabIndex = 99;
			textBoxQuickColorPicker46.Location = new System.Drawing.Point(407, 379);
			textBoxQuickColorPicker46.Name = "textBoxQuickColorPicker46";
			textBoxQuickColorPicker46.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker46.TabIndex = 98;
			textBoxQuickColorPicker45.Location = new System.Drawing.Point(379, 379);
			textBoxQuickColorPicker45.Name = "textBoxQuickColorPicker45";
			textBoxQuickColorPicker45.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker45.TabIndex = 97;
			textBoxQuickColorPicker44.Location = new System.Drawing.Point(351, 379);
			textBoxQuickColorPicker44.Name = "textBoxQuickColorPicker44";
			textBoxQuickColorPicker44.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker44.TabIndex = 96;
			textBoxQuickColorPicker43.Location = new System.Drawing.Point(323, 379);
			textBoxQuickColorPicker43.Name = "textBoxQuickColorPicker43";
			textBoxQuickColorPicker43.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker43.TabIndex = 95;
			textBoxQuickColorPicker42.Location = new System.Drawing.Point(295, 379);
			textBoxQuickColorPicker42.Name = "textBoxQuickColorPicker42";
			textBoxQuickColorPicker42.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker42.TabIndex = 94;
			textBoxQuickColorPicker41.Location = new System.Drawing.Point(267, 379);
			textBoxQuickColorPicker41.Name = "textBoxQuickColorPicker41";
			textBoxQuickColorPicker41.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker41.TabIndex = 93;
			textBoxQuickColorPicker40.Location = new System.Drawing.Point(463, 353);
			textBoxQuickColorPicker40.Name = "textBoxQuickColorPicker40";
			textBoxQuickColorPicker40.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker40.TabIndex = 92;
			textBoxQuickColorPicker39.Location = new System.Drawing.Point(435, 353);
			textBoxQuickColorPicker39.Name = "textBoxQuickColorPicker39";
			textBoxQuickColorPicker39.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker39.TabIndex = 91;
			textBoxQuickColorPicker38.Location = new System.Drawing.Point(407, 353);
			textBoxQuickColorPicker38.Name = "textBoxQuickColorPicker38";
			textBoxQuickColorPicker38.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker38.TabIndex = 90;
			textBoxQuickColorPicker37.Location = new System.Drawing.Point(379, 353);
			textBoxQuickColorPicker37.Name = "textBoxQuickColorPicker37";
			textBoxQuickColorPicker37.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker37.TabIndex = 89;
			textBoxQuickColorPicker36.Location = new System.Drawing.Point(351, 353);
			textBoxQuickColorPicker36.Name = "textBoxQuickColorPicker36";
			textBoxQuickColorPicker36.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker36.TabIndex = 88;
			textBoxQuickColorPicker35.Location = new System.Drawing.Point(323, 353);
			textBoxQuickColorPicker35.Name = "textBoxQuickColorPicker35";
			textBoxQuickColorPicker35.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker35.TabIndex = 87;
			textBoxQuickColorPicker34.Location = new System.Drawing.Point(295, 353);
			textBoxQuickColorPicker34.Name = "textBoxQuickColorPicker34";
			textBoxQuickColorPicker34.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker34.TabIndex = 86;
			textBoxQuickColorPicker33.Location = new System.Drawing.Point(267, 353);
			textBoxQuickColorPicker33.Name = "textBoxQuickColorPicker33";
			textBoxQuickColorPicker33.Size = new System.Drawing.Size(22, 20);
			textBoxQuickColorPicker33.TabIndex = 85;
			textBoxColorIndicator15.Location = new System.Drawing.Point(20, 14);
			textBoxColorIndicator15.Name = "textBoxColorIndicator15";
			textBoxColorIndicator15.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator15.TabIndex = 101;
			textBoxColorIndicator14.Location = new System.Drawing.Point(20, 58);
			textBoxColorIndicator14.Name = "textBoxColorIndicator14";
			textBoxColorIndicator14.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator14.TabIndex = 102;
			textBoxColorIndicator12.Location = new System.Drawing.Point(20, 146);
			textBoxColorIndicator12.Name = "textBoxColorIndicator12";
			textBoxColorIndicator12.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator12.TabIndex = 104;
			textBoxColorIndicator13.Location = new System.Drawing.Point(20, 102);
			textBoxColorIndicator13.Name = "textBoxColorIndicator13";
			textBoxColorIndicator13.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator13.TabIndex = 103;
			textBoxColorIndicator8.Location = new System.Drawing.Point(20, 322);
			textBoxColorIndicator8.Name = "textBoxColorIndicator8";
			textBoxColorIndicator8.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator8.TabIndex = 108;
			textBoxColorIndicator9.Location = new System.Drawing.Point(20, 278);
			textBoxColorIndicator9.Name = "textBoxColorIndicator9";
			textBoxColorIndicator9.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator9.TabIndex = 107;
			textBoxColorIndicator10.Location = new System.Drawing.Point(20, 234);
			textBoxColorIndicator10.Name = "textBoxColorIndicator10";
			textBoxColorIndicator10.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator10.TabIndex = 106;
			textBoxColorIndicator11.Location = new System.Drawing.Point(20, 190);
			textBoxColorIndicator11.Name = "textBoxColorIndicator11";
			textBoxColorIndicator11.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator11.TabIndex = 105;
			textBoxColorIndicator1.Location = new System.Drawing.Point(20, 630);
			textBoxColorIndicator1.Name = "textBoxColorIndicator1";
			textBoxColorIndicator1.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator1.TabIndex = 115;
			textBoxColorIndicator2.Location = new System.Drawing.Point(20, 586);
			textBoxColorIndicator2.Name = "textBoxColorIndicator2";
			textBoxColorIndicator2.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator2.TabIndex = 114;
			textBoxColorIndicator3.Location = new System.Drawing.Point(20, 542);
			textBoxColorIndicator3.Name = "textBoxColorIndicator3";
			textBoxColorIndicator3.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator3.TabIndex = 113;
			textBoxColorIndicator4.Location = new System.Drawing.Point(20, 498);
			textBoxColorIndicator4.Name = "textBoxColorIndicator4";
			textBoxColorIndicator4.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator4.TabIndex = 112;
			textBoxColorIndicator5.Location = new System.Drawing.Point(20, 454);
			textBoxColorIndicator5.Name = "textBoxColorIndicator5";
			textBoxColorIndicator5.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator5.TabIndex = 111;
			textBoxColorIndicator6.Location = new System.Drawing.Point(20, 410);
			textBoxColorIndicator6.Name = "textBoxColorIndicator6";
			textBoxColorIndicator6.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator6.TabIndex = 110;
			textBoxColorIndicator7.Location = new System.Drawing.Point(20, 366);
			textBoxColorIndicator7.Name = "textBoxColorIndicator7";
			textBoxColorIndicator7.Size = new System.Drawing.Size(50, 20);
			textBoxColorIndicator7.TabIndex = 109;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(568, 656);
			base.Controls.Add(textBoxColorIndicator1);
			base.Controls.Add(textBoxColorIndicator2);
			base.Controls.Add(textBoxColorIndicator3);
			base.Controls.Add(textBoxColorIndicator4);
			base.Controls.Add(textBoxColorIndicator5);
			base.Controls.Add(textBoxColorIndicator6);
			base.Controls.Add(textBoxColorIndicator7);
			base.Controls.Add(textBoxColorIndicator8);
			base.Controls.Add(textBoxColorIndicator9);
			base.Controls.Add(textBoxColorIndicator10);
			base.Controls.Add(textBoxColorIndicator11);
			base.Controls.Add(textBoxColorIndicator12);
			base.Controls.Add(textBoxColorIndicator13);
			base.Controls.Add(textBoxColorIndicator14);
			base.Controls.Add(textBoxColorIndicator15);
			base.Controls.Add(textBoxQuickColorPicker48);
			base.Controls.Add(textBoxQuickColorPicker47);
			base.Controls.Add(textBoxQuickColorPicker46);
			base.Controls.Add(textBoxQuickColorPicker45);
			base.Controls.Add(textBoxQuickColorPicker44);
			base.Controls.Add(textBoxQuickColorPicker43);
			base.Controls.Add(textBoxQuickColorPicker42);
			base.Controls.Add(textBoxQuickColorPicker41);
			base.Controls.Add(textBoxQuickColorPicker40);
			base.Controls.Add(textBoxQuickColorPicker39);
			base.Controls.Add(textBoxQuickColorPicker38);
			base.Controls.Add(textBoxQuickColorPicker37);
			base.Controls.Add(textBoxQuickColorPicker36);
			base.Controls.Add(textBoxQuickColorPicker35);
			base.Controls.Add(textBoxQuickColorPicker34);
			base.Controls.Add(textBoxQuickColorPicker33);
			base.Controls.Add(textBoxQuickColorPicker32);
			base.Controls.Add(textBoxQuickColorPicker31);
			base.Controls.Add(textBoxQuickColorPicker30);
			base.Controls.Add(textBoxQuickColorPicker29);
			base.Controls.Add(textBoxQuickColorPicker28);
			base.Controls.Add(textBoxQuickColorPicker27);
			base.Controls.Add(textBoxQuickColorPicker26);
			base.Controls.Add(textBoxQuickColorPicker25);
			base.Controls.Add(textBoxQuickColorPicker24);
			base.Controls.Add(textBoxQuickColorPicker23);
			base.Controls.Add(textBoxQuickColorPicker22);
			base.Controls.Add(textBoxQuickColorPicker21);
			base.Controls.Add(textBoxQuickColorPicker20);
			base.Controls.Add(textBoxQuickColorPicker19);
			base.Controls.Add(textBoxQuickColorPicker18);
			base.Controls.Add(textBoxQuickColorPicker17);
			base.Controls.Add(textBoxQuickColorPicker16);
			base.Controls.Add(textBoxQuickColorPicker15);
			base.Controls.Add(textBoxQuickColorPicker14);
			base.Controls.Add(textBoxQuickColorPicker13);
			base.Controls.Add(textBoxQuickColorPicker12);
			base.Controls.Add(textBoxQuickColorPicker11);
			base.Controls.Add(textBoxQuickColorPicker10);
			base.Controls.Add(textBoxQuickColorPicker9);
			base.Controls.Add(textBoxQuickColorPicker8);
			base.Controls.Add(textBoxQuickColorPicker7);
			base.Controls.Add(textBoxQuickColorPicker6);
			base.Controls.Add(textBoxQuickColorPicker5);
			base.Controls.Add(textBoxQuickColorPicker4);
			base.Controls.Add(textBoxQuickColorPicker3);
			base.Controls.Add(textBoxQuickColorPicker2);
			base.Controls.Add(textBoxQuickColorPicker1);
			base.Controls.Add(label17);
			base.Controls.Add(label16);
			base.Controls.Add(buttonMoreColors);
			base.Controls.Add(label31);
			base.Controls.Add(label32);
			base.Controls.Add(label33);
			base.Controls.Add(numericUpDownBlue);
			base.Controls.Add(numericUpDownGreen);
			base.Controls.Add(numericUpDownRed);
			base.Controls.Add(textBoxCurrentColor);
			base.Controls.Add(buttonSetDefault);
			base.Controls.Add(labelKmlColor1);
			base.Controls.Add(labelKmlColor2);
			base.Controls.Add(labelKmlColor3);
			base.Controls.Add(labelKmlColor4);
			base.Controls.Add(labelKmlColor5);
			base.Controls.Add(labelKmlColor6);
			base.Controls.Add(labelKmlColor7);
			base.Controls.Add(labelKmlColor8);
			base.Controls.Add(labelKmlColor9);
			base.Controls.Add(labelKmlColor10);
			base.Controls.Add(labelKmlColor11);
			base.Controls.Add(labelKmlColor12);
			base.Controls.Add(labelKmlColor13);
			base.Controls.Add(labelKmlColor14);
			base.Controls.Add(labelKmlColor15);
			base.Controls.Add(label15);
			base.Controls.Add(label14);
			base.Controls.Add(label13);
			base.Controls.Add(label12);
			base.Controls.Add(label11);
			base.Controls.Add(label10);
			base.Controls.Add(label9);
			base.Controls.Add(label8);
			base.Controls.Add(label7);
			base.Controls.Add(label6);
			base.Controls.Add(label5);
			base.Controls.Add(label4);
			base.Controls.Add(label3);
			base.Controls.Add(label2);
			base.Controls.Add(textBoxColor1);
			base.Controls.Add(numericUpDown1);
			base.Controls.Add(textBoxColor2);
			base.Controls.Add(numericUpDown2);
			base.Controls.Add(textBoxColor3);
			base.Controls.Add(numericUpDown3);
			base.Controls.Add(textBoxColor4);
			base.Controls.Add(numericUpDown4);
			base.Controls.Add(textBoxColor5);
			base.Controls.Add(numericUpDown5);
			base.Controls.Add(textBoxColor6);
			base.Controls.Add(numericUpDown6);
			base.Controls.Add(textBoxColor7);
			base.Controls.Add(numericUpDown7);
			base.Controls.Add(textBoxColor8);
			base.Controls.Add(numericUpDown8);
			base.Controls.Add(textBoxColor9);
			base.Controls.Add(numericUpDown9);
			base.Controls.Add(textBoxColor10);
			base.Controls.Add(numericUpDown10);
			base.Controls.Add(textBoxColor11);
			base.Controls.Add(numericUpDown11);
			base.Controls.Add(textBoxColor12);
			base.Controls.Add(numericUpDown12);
			base.Controls.Add(textBoxColor13);
			base.Controls.Add(numericUpDown13);
			base.Controls.Add(textBoxColor14);
			base.Controls.Add(numericUpDown14);
			base.Controls.Add(richTextBox1);
			base.Controls.Add(textBoxColor15);
			base.Controls.Add(label1);
			base.Controls.Add(numericUpDownNumberOfAltitudeColors);
			base.Controls.Add(buttonCancel);
			base.Controls.Add(buttonSave);
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.Name = "FormConfigureKmlFile";
			Text = "Configure colors and altitudes in Kml File";
			base.Load += new System.EventHandler(FormConfigureKmlFile_Load);
			((System.ComponentModel.ISupportInitialize)numericUpDownNumberOfAltitudeColors).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown14).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown13).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown11).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown12).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown7).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown8).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown9).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown10).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown1).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown2).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown3).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown4).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown5).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDown6).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDownBlue).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDownGreen).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDownRed).EndInit();
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
