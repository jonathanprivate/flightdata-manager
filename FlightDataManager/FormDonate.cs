using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class FormDonate : Form
	{
		private IContainer components;

		private Button buttonClose;

		private RichTextBox richTextBox1;

		private PictureBox pictureBox1;

		public FormDonate()
		{
			InitializeComponent();
		}

		private void buttonClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FormDonate_Load(object sender, EventArgs e)
		{
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Location = new Point(220, 100);
			if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("Donate menu opened");
			}
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			string text = "kfj4100@gmail.com";
			string text2 = "Donation";
			string text3 = "US";
			string text4 = "USD";
			Process.Start("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=" + text + "&lc=" + text3 + "&item_name=" + text2 + "&currency_code=" + text4 + "&bn=PP%2dDonationsBF");
			if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("Donate PayPal link clicked");
			}
		}

		private void pictureBox1_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(pictureBox1, "Click here to make a donation via PayPal");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.FormDonate));
			buttonClose = new System.Windows.Forms.Button();
			richTextBox1 = new System.Windows.Forms.RichTextBox();
			pictureBox1 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
			SuspendLayout();
			buttonClose.Location = new System.Drawing.Point(216, 287);
			buttonClose.Name = "buttonClose";
			buttonClose.Size = new System.Drawing.Size(75, 23);
			buttonClose.TabIndex = 0;
			buttonClose.Text = "Close";
			buttonClose.UseVisualStyleBackColor = true;
			buttonClose.Click += new System.EventHandler(buttonClose_Click);
			richTextBox1.BackColor = System.Drawing.SystemColors.Control;
			richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			richTextBox1.Location = new System.Drawing.Point(13, 20);
			richTextBox1.Name = "richTextBox1";
			richTextBox1.ReadOnly = true;
			richTextBox1.Size = new System.Drawing.Size(493, 187);
			richTextBox1.TabIndex = 12;
			richTextBox1.Text = resources.GetString("richTextBox1.Text");
			pictureBox1.Image = (System.Drawing.Image)resources.GetObject("pictureBox1.Image");
			pictureBox1.Location = new System.Drawing.Point(184, 190);
			pictureBox1.Name = "pictureBox1";
			pictureBox1.Size = new System.Drawing.Size(136, 77);
			pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			pictureBox1.TabIndex = 13;
			pictureBox1.TabStop = false;
			pictureBox1.Click += new System.EventHandler(pictureBox1_Click);
			pictureBox1.MouseHover += new System.EventHandler(pictureBox1_MouseHover);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(511, 322);
			base.Controls.Add(pictureBox1);
			base.Controls.Add(richTextBox1);
			base.Controls.Add(buttonClose);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "FormDonate";
			Text = "Donate to support further development of FlightData Manager";
			base.Load += new System.EventHandler(FormDonate_Load);
			((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
			ResumeLayout(false);
		}
	}
}
