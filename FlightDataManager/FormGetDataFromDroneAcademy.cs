using FlightDataManager.Properties;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using System.Xml.Linq;

namespace FlightDataManager
{
	public class FormGetDataFromDroneAcademy : Form
	{
		private struct FlightDataStruct
		{
			public double Latitude;

			public double Longitude;

			public string flightDurationString;

			public DateTime flightStartDateTime;

			public string flightID;

			public int totalRunTimeTotalSec;

			public int nrOfPicsInFlight;

			public int nrOfVideosInFlight;

			public List<string> videoLinksList;

			public List<string> pictureLinksList;

			public List<string> mediaIdList;

			public string flightUserName;

			public bool visible;

			public int grade;
		}

		private const int nrOfFlightsPrPage = 2000;

		private bool ErrorInDecodingFlightList;

		private static WebClient myWebClient = new WebClient();

		private static int counterAddressesDecoded = 0;

		private static int counterAddressesRetries = 0;

		private bool IgnoreDecodedAddressEvent;

		private const int addressFieldNumber = 8;

		private bool forceFormToClose;

		private bool forceEndlessWhileLoopToClose;

		private bool forceEndlessWhileLoop2ToClose;

		private ItemComparer sorter;

		private bool allFlightStatisticsPerformed;

		private int NumberOfFlightsMissingStats;

		private bool disableExport;

		private int OnMarkerClickEventCounter;

		private string filterLatLonString = "";

		private string listSep = "";

		private string SpecificUsersFlightsUserName = "";

		private GMapOverlay markersOverlay;

		private CultureInfo originalCulture;

		private System.Timers.Timer timerMapZoomedDraggedMovedOrResized = new System.Timers.Timer(100.0);

		private int zoomForDroneAcademy = 10;

		private string itemTagString = "";

		private static readonly object myLockObject = new object();

		private bool mapModePublicFlights;

		private bool deleteMarkers;

		private bool processingClusteringOfOwnFlights;

		private bool mapModeOwnFlights;

		private bool mapModeSpecificUsersFlights;

		private string flightIdClicked = "";

		private List<FlightDataStruct> flightDataList;

		private int listViewIndexClicked;

		private bool disableHorizontalScrollResetOfListView;

		private List<string> newAccountTypeCredentials = new List<string>();

		private IContainer components;

		private TextBox textBoxUserName;

		private Label labelUserName;

		private Label labelPassword;

		private TextBox textBoxPassword;

		private Button buttonGetListOfFlights;

		private Label labelForListView;

		private Label labelStatus;

		internal ListView listView1;

		private Button buttonDeleteFlight;

		private CheckBox checkBoxDontRememberUser;

		private Button buttonMissingFlights;

		private TextBox textBoxStatus;

		private Button buttonExport;

		private GMapControl gmap;

		private Button buttonSwitchToMap;

		private Button buttonZoomIn;

		private Button buttonZoomOut;

		private Button buttonClearCachedInfo;

		private Label labelDevelopedBy;

		private ContextMenuStrip contextMenuStripListView;

		private ToolStripMenuItem openFlightToolStripMenuItem;

		private ToolStripMenuItem deleteFlightToolStripMenuItem;

		private ToolStripMenuItem flightIDToolStripMenuItem;

		private ToolStripMenuItem commentForFlightToolStripMenuItem;

		private ToolStripMenuItem commentAddToolStripMenuItem;

		private ToolStripMenuItem commentEditToolStripMenuItem;

		private ToolStripMenuItem commentDeleteToolStripMenuItem1;

		private ToolStripMenuItem flightIDSubToolStripMenuItem1;

		private ToolStripMenuItem copyFlightIDToClipboardToolStripMenuItem;

		private Label labelMapZoomLevel;

		private Button buttonForgotPassword;

		private ToolStripMenuItem linkedVideoFileToolStripMenuItem;

		private ToolStripMenuItem linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem;

		private ToolStripMenuItem linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem;

		private ToolStripMenuItem linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem;

		public FormGetDataFromDroneAcademy()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Text = "Get flight data from My.Parrot - FlightData Manager - for Parrot Anafi, Parrot Bebop and Parrot Disco drones - Version " + DataExchangerClass.currentToolVersion;
			listSep = DataExchangerClass.listSep;
			SetListOrMapsVisible(showList: true);
			DataExchangerClass.pictureLinksList = new List<string>();
			DataExchangerClass.videoLinksList = new List<string>();
			DataExchangerClass.flightDataFieldNrOfSvsPresent = false;
			DataExchangerClass.flightDataFieldPitotSpeedPresent = false;
			originalCulture = Thread.CurrentThread.CurrentCulture;
			MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;
			disableExport = false;
			textBoxStatus.Visible = false;
			buttonExport.Enabled = false;
			listView1.ColumnClick += listView1_ColumnClick;
			InitializeListView();
			buttonDeleteFlight.Enabled = false;
			labelStatus.Text = "";
			base.MinimizeBox = false;
			if (Settings.Default.WindowSubStateMaximized)
			{
				base.WindowState = FormWindowState.Maximized;
			}
			else
			{
				base.Location = new Point(Settings.Default.WindowSubPosX, Settings.Default.WindowSubPosY);
				base.Width = Settings.Default.WindowSubSizeX;
				base.Height = Settings.Default.WindowSubSizeY;
			}
			DataExchangerClass.droneAcademyFlightData = null;
			DataExchangerClass.droneAcademyFlightDuration = "";
			ReadSettingsAndSetValues();
			if (textBoxUserName.Text.Length >= 2 && textBoxPassword.Text.Length >= 2)
			{
				Refresh();
				Thread.Sleep(50);
				Refresh();
				buttonGetListOfFlights.PerformClick();
			}
		}

		private void ReadSettingsAndSetValues()
		{
			textBoxUserName.Text = Settings.Default.DroneAcademyUsername;
			textBoxPassword.Text = Settings.Default.DroneAcademyPassword;
			if (Settings.Default.DroneAcademyDontRememberUser)
			{
				checkBoxDontRememberUser.Checked = false;
			}
			else
			{
				checkBoxDontRememberUser.Checked = true;
			}
		}

		private void SaveSettings()
		{
			if (!checkBoxDontRememberUser.Checked)
			{
				Settings.Default.DroneAcademyUsername = "";
				Settings.Default.DroneAcademyPassword = "";
				Settings.Default.DroneAcademyDontRememberUser = true;
			}
			else
			{
				Settings.Default.DroneAcademyUsername = textBoxUserName.Text;
				Settings.Default.DroneAcademyPassword = textBoxPassword.Text;
				Settings.Default.DroneAcademyDontRememberUser = false;
			}
			if (markersOverlay != null)
			{
				SaveMapLocationAndZoomLevel();
			}
			Settings.Default.Save();
		}

		private void InitializeListView()
		{
			ColumnHeader columnHeader = new ColumnHeader();
			columnHeader.Text = "Date Of Flight";
			columnHeader.Width = 100;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Time";
			columnHeader.Width = 65;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Flight Duration";
			columnHeader.Width = 100;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Max Altitude";
			columnHeader.Width = 90;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Max Speed";
			columnHeader.Width = 82;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Max Dist.";
			columnHeader.Width = 70;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Dist. flown";
			columnHeader.Width = 87;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Battery";
			columnHeader.Width = 62;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Estimated Address";
			columnHeader.Width = 280;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Controller";
			columnHeader.Width = 120;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Drone";
			columnHeader.Width = 65;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "SW ver";
			columnHeader.Width = 58;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "HW ver";
			columnHeader.Width = 58;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Crash";
			columnHeader.Width = 59;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Serial Nr";
			columnHeader.Width = 130;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "FlightId";
			columnHeader.Width = 71;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Linked video";
			columnHeader.Width = 120;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "Comments";
			columnHeader.Width = 260;
			listView1.Columns.Add(columnHeader);
			columnHeader = new ColumnHeader();
			columnHeader.Text = "";
			columnHeader.Width = 20;
			listView1.Columns.Add(columnHeader);
			for (int i = 0; i < listView1.Columns.Count; i++)
			{
				if (i != 8 && i != 16 && i != 17)
				{
					listView1.Columns[i].TextAlign = HorizontalAlignment.Center;
				}
			}
			listView1.View = View.Details;
			listView1.FullRowSelect = true;
			listView1.MultiSelect = true;
		}

		private void SetListOrMapsVisible(bool showList)
		{
			if (showList)
			{
				buttonZoomIn.Visible = false;
				buttonZoomOut.Visible = false;
				gmap.Visible = false;
				textBoxStatus.Visible = false;
				buttonExport.Enabled = false;
				labelMapZoomLevel.Visible = false;
				listView1.Visible = true;
				labelForListView.Visible = true;
				labelStatus.Visible = true;
				buttonClearCachedInfo.Visible = true;
				buttonDeleteFlight.Visible = true;
				buttonMissingFlights.Visible = true;
				buttonExport.Visible = true;
			}
			else
			{
				listView1.Visible = false;
				labelForListView.Visible = false;
				labelStatus.Visible = false;
				buttonClearCachedInfo.Visible = false;
				buttonDeleteFlight.Visible = false;
				buttonMissingFlights.Visible = false;
				buttonExport.Visible = false;
				buttonZoomIn.Visible = true;
				buttonZoomOut.Visible = true;
				gmap.Visible = true;
				textBoxStatus.Visible = false;
				buttonExport.Enabled = true;
				labelMapZoomLevel.Visible = true;
			}
			Thread.Sleep(10);
			Refresh();
			Thread.Sleep(10);
		}

		private void buttonGetListOfFlights_Click(object sender, EventArgs e)
		{
			SaveSettings();
			if (!disableHorizontalScrollResetOfListView)
			{
				listView1.Scrollable = false;
				listView1.Scrollable = true;
			}
			else
			{
				disableHorizontalScrollResetOfListView = false;
			}
			flightDataList = new List<FlightDataStruct>();
			MouseEventArgs obj = e as MouseEventArgs;
			KeyEventArgs keyEventArgs = e as KeyEventArgs;
			if (obj != null || keyEventArgs != null)
			{
				OnMarkerClickEventCounter = 0;
				mapModeOwnFlights = false;
				mapModePublicFlights = false;
				mapModeSpecificUsersFlights = false;
				filterLatLonString = "";
				SetListOrMapsVisible(showList: true);
				buttonSwitchToMap.Enabled = true;
			}
			if (mapModeSpecificUsersFlights && filterLatLonString == "")
			{
				SetListOrMapsVisible(showList: false);
			}
			if (sorter != null)
			{
				sorter.Order = SortOrder.None;
				sorter.Column = 100;
				sorter = null;
			}
			forceEndlessWhileLoopToClose = true;
			forceEndlessWhileLoop2ToClose = true;
			if (mapModePublicFlights)
			{
				try
				{
					if (((MouseEventArgs)e).Button == MouseButtons.Left)
					{
						mapModePublicFlights = false;
					}
				}
				catch
				{
				}
			}
			forceFormToClose = false;
			listView1.Items.Clear();
			allFlightStatisticsPerformed = false;
			buttonDeleteFlight.Enabled = false;
			buttonGetListOfFlights.Enabled = false;
			IgnoreDecodedAddressEvent = true;
			if (myWebClient != null)
			{
				myWebClient = null;
			}
			labelStatus.Text = "";
			counterAddressesRetries = 0;
			counterAddressesDecoded = 0;
			Application.DoEvents();
			listView1.Items.Clear();
			if (textBoxUserName.Text.Length < 2)
			{
				MessageBox.Show(this, "Username for Parrot Cloud too short");
			}
			if (textBoxPassword.Text.Length < 2)
			{
				MessageBox.Show(this, "Password for Parrot Cloud too short");
			}
			if (textBoxPassword.Text.Length <= 1 || textBoxUserName.Text.Length <= 1)
			{
				return;
			}
			Cursor = Cursors.WaitCursor;
			string text = "";
			if (!mapModePublicFlights)
			{
				text = ((!mapModeSpecificUsersFlights) ? ("http://academy.ardrone.com/api3/runs/?page=0&paginate_by=" + 2000) : ("http://academy.ardrone.com/api3/runs/?user=" + SpecificUsersFlightsUserName));
			}
			else
			{
				string text2 = "";
				text2 = "/?paginate_by=300&product_id=2318&product_id=2316&product_id=2305&product_id=2324&gps=true";
				text = "http://academy.ardrone.com/api3/cluster/" + itemTagString + "/" + zoomForDroneAcademy + text2;
			}
			RestClient restClient = new RestClient(text);
			RestRequest request = new RestRequest(Method.GET);
			if (!mapModeSpecificUsersFlights)
			{
				restClient.Authenticator = new HttpBasicAuthenticator(textBoxUserName.Text, textBoxPassword.Text);
			}
			IRestResponse restResponse = restClient.Execute(request);
			LogTxtToFileInTempFolder("gkc1.gkc", restResponse.Content.ToString());
			if (restResponse.StatusCode.ToString() != "OK")
			{
				Cursor = Cursors.Default;
				if (!restResponse.Content.Contains("Invalid username"))
				{
					MessageBox.Show(this, "Something failed when trying to get data from Parrot Cloud,\nCheck that you entered the correct username and password and is connected to the internet\n\nResponse from server:\n" + restResponse.Content);
					buttonGetListOfFlights.Enabled = true;
					return;
				}
				newAccountTypeCredentials = FindNewAccountTypeCredentials();
				if (newAccountTypeCredentials.Count < 2)
				{
					MessageBox.Show(this, "Something failed when trying to get data from Parrot Cloud,\nCheck that you entered the correct username and password and is connected to the internet");
					buttonGetListOfFlights.Enabled = true;
					return;
				}
			}
			if (newAccountTypeCredentials.Count > 1)
			{
				restClient = new RestClient(text);
				request = new RestRequest(Method.GET);
				restClient.Authenticator = new HttpBasicAuthenticator(newAccountTypeCredentials[0], newAccountTypeCredentials[1]);
				restResponse = restClient.Execute(request);
				LogTxtToFileInTempFolder("gkc1.gkc", restResponse.Content.ToString());
			}
			string text3 = restResponse.Content.ToString();
			if (!text3.Contains("\"user\":"))
			{
				Cursor = Cursors.Default;
				MessageBox.Show(this, "The downloaded list of flights from Parrot Cloud appears to be empty");
				buttonGetListOfFlights.Enabled = true;
				return;
			}
			string[] array = text3.Split(new string[1]
			{
				"]},{\"id\":"
			}, StringSplitOptions.None);
			for (int i = 1; i < array.Count(); i++)
			{
				array[i] = "},{\"id\":" + array[i];
			}
			restClient = null;
			request = null;
			double num = 500.0;
			double num2 = -500.0;
			double num3 = 500.0;
			double num4 = -500.0;
			if (filterLatLonString != "")
			{
				string[] array2 = filterLatLonString.Split('_');
				num = Convert.ToDouble(array2[0]);
				num2 = Convert.ToDouble(array2[1]);
				num3 = Convert.ToDouble(array2[3]);
				num4 = Convert.ToDouble(array2[2]);
			}
			if (mapModeSpecificUsersFlights && array.Count() == 1 && array[0].Length < 10)
			{
				MessageBox.Show(this, "No flights found for username: " + SpecificUsersFlightsUserName);
				mapModeSpecificUsersFlights = false;
				mapModeOwnFlights = false;
				SetListOrMapsVisible(showList: true);
				buttonGetListOfFlights_Click(new MouseEventArgs(MouseButtons.Left, 1, 0, 0, 0), new EventArgs());
				return;
			}
			string[] array3 = array;
			foreach (string text4 in array3)
			{
				FlightDataStruct item = default(FlightDataStruct);
				if (text4.Length < 20)
				{
					continue;
				}
				bool flag = false;
				ErrorInDecodingFlightList = false;
				if (GetValueOfField(text4, "\"visible\":", "Visibility", 0, useLastValue: true) == "true")
				{
					item.visible = true;
				}
				else
				{
					item.visible = false;
				}
				item.grade = Convert.ToInt32(GetValueOfField(text4, "\"grade\":", "Grade"));
				item.flightID = GetValueOfField(text4, "\"id\":", "FlightId");
				string valueOfField = GetValueOfField(text4, "\"serial_number\":", "Serial number");
				string valueOfField2 = GetValueOfField(text4, "\"crash\":", "Number of crashes");
				string valueOfField3 = GetValueOfField(text4, "\"product_id\":", "Product id");
				valueOfField3 = valueOfField3.Replace("2324", "Anafi");
				valueOfField3 = valueOfField3.Replace("2305", "Bebop1");
				valueOfField3 = valueOfField3.Replace("2316", "Bebop2");
				valueOfField3 = valueOfField3.Replace("2318", "Disco");
				valueOfField3 = valueOfField3.Replace("2310", "Jumping Race");
				valueOfField3 = valueOfField3.Replace("2320", "Swing");
				valueOfField3 = valueOfField3.Replace("2315", "Mambo");
				valueOfField3 = valueOfField3.Replace("2313", "Airborne Cargo");
				string valueOfField4 = GetValueOfField(text4, "\"controller_model\":", "Controller model");
				string text5 = item.flightUserName = GetValueOfField(text4, "\"user\":", "User Name");
				string text6 = GetValueOfField(text4, "\"software_version\":", "Sw Ver");
				if (text6 == "")
				{
					text6 = "N/A";
				}
				string text7 = GetValueOfField(text4, "\"hardware_version\":", "Hw Ver");
				if (text7 == "")
				{
					text7 = "N/A";
				}
				string text8 = "";
				string text9 = "";
				string valueOfField5 = GetValueOfField(text4, "\"date\":", "Date");
				if (!ErrorInDecodingFlightList)
				{
					int year = Convert.ToInt16(valueOfField5.Substring(0, 4));
					int month = Convert.ToInt16(valueOfField5.Substring(4, 2));
					int day = Convert.ToInt16(valueOfField5.Substring(6, 2));
					int hour = Convert.ToInt16(valueOfField5.Substring(8, 2));
					int minute = Convert.ToInt16(valueOfField5.Substring(10, 2));
					int second = Convert.ToInt16(valueOfField5.Substring(12, 2));
					item.flightStartDateTime = new DateTime(year, month, day, hour, minute, second);
					text8 = new DateTime(year, month, day, hour, minute, second).ToString("dd. MMM. yyyy");
					text9 = new DateTime(year, month, day, hour, minute, second).ToString("HH:mm:ss");
				}
				int num5 = 0;
				int num6 = 0;
				string valueOfField6 = GetValueOfField(text4, "\"total_run_time\":", "Total run time");
				if (!ErrorInDecodingFlightList)
				{
					int num7 = item.totalRunTimeTotalSec = Convert.ToInt32(valueOfField6) / 1000;
					num5 = num7 / 60;
					num6 = num7 % 60;
					item.flightDurationString = num5.ToString("00") + ":" + num6.ToString("00");
				}
				int nrOfVideosInFlight = 0;
				int nrOfPicsInFlight = 0;
				List<string> list = new List<string>();
				List<string> list2 = new List<string>();
				if (text4.Contains("captures\": [{\"url\""))
				{
					string[] array4 = text4.Substring(text4.IndexOf("captures")).Split(new string[1]
					{
						"\"url\""
					}, StringSplitOptions.None);
					for (int k = 1; k < array4.Count(); k++)
					{
						string text10 = array4[k].Substring(3);
						text10 = text10.Substring(0, text10.IndexOf("\""));
						list2.Add(text10);
						string text11 = array4[k].Substring(array4[k].IndexOf("\"id\":") + 6);
						text11 = text11.Substring(0, text11.IndexOf(","));
						list.Add("captures:" + text11);
					}
					nrOfPicsInFlight = list2.Count;
				}
				item.pictureLinksList = list2;
				List<string> list3 = new List<string>();
				if (text4.Contains("videos\":[{"))
				{
					string[] array5 = text4.Substring(0, text4.IndexOf("captures")).Split(new string[1]
					{
						"\"url\""
					}, StringSplitOptions.None);
					for (int l = 1; l < array5.Count(); l++)
					{
						string text12 = array5[l].Substring(2);
						text12 = text12.Substring(0, text12.IndexOf("\""));
						list3.Add(text12);
						string text13 = array5[l].Substring(array5[l].IndexOf("\"id\":") + 6);
						text13 = text13.Substring(0, text13.IndexOf(","));
						list.Add("videos:" + text13);
					}
					nrOfVideosInFlight = list3.Count;
				}
				item.videoLinksList = list3;
				item.mediaIdList = list;
				item.nrOfPicsInFlight = nrOfPicsInFlight;
				item.nrOfVideosInFlight = nrOfVideosInFlight;
				item.Latitude = Convert.ToDouble(GetValueOfField(text4, "\"gps_latitude\":", "Latitude"));
				item.Longitude = Convert.ToDouble(GetValueOfField(text4, "\"gps_longitude\":", "Longitude"));
				if (item.Latitude > num || item.Latitude < num2 || item.Longitude > num3 || item.Longitude < num4)
				{
					flag = true;
				}
				if (!ErrorInDecodingFlightList && !flag)
				{
					ListViewItem listViewItem = new ListViewItem(text8);
					listViewItem.SubItems.Add(text9);
					listViewItem.SubItems.Add(num5.ToString("00") + " min " + num6.ToString("00") + " sec");
					listViewItem.SubItems.Add("-");
					listViewItem.SubItems.Add("-");
					listViewItem.SubItems.Add("-");
					listViewItem.SubItems.Add("-");
					listViewItem.SubItems.Add("-");
					listViewItem.SubItems.Add("");
					listViewItem.SubItems.Add(valueOfField4);
					listViewItem.SubItems.Add(valueOfField3);
					listViewItem.SubItems.Add(text6);
					listViewItem.SubItems.Add(text7);
					listViewItem.SubItems.Add(valueOfField2);
					listViewItem.SubItems.Add(valueOfField);
					listViewItem.SubItems.Add(item.flightID);
					listViewItem.SubItems.Add("");
					listViewItem.SubItems.Add("");
					listViewItem.SubItems.Add("");
					listView1.Items.Add(listViewItem);
				}
				if (!flag)
				{
					flightDataList.Add(item);
				}
			}
			Application.DoEvents();
			Refresh();
			Cursor = Cursors.Default;
			Refresh();
			labelStatus.Text = "Total number of flights=" + listView1.Items.Count;
			Application.DoEvents();
			if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("Flight list downloaded");
				if (flightDataList[0].Latitude < 200.0 && flightDataList[0].Longitude < 200.0)
				{
					string text14 = encryptString(textBoxUserName.Text);
					GoogleTracker.trackEventPos("Flight list downloaded, position", text14 + "@" + flightDataList[0].Latitude * 55.12 * -1.0 + ";" + flightDataList[0].Longitude * 12.55);
				}
			}
			if (!mapModeOwnFlights && !mapModePublicFlights && !mapModeSpecificUsersFlights && filterLatLonString == "" && DataExchangerClass.reportStatistics && flightDataList[0].Latitude < 200.0 && flightDataList[0].Longitude < 200.0 && !CheckIfFlightInThisAreaAlreadyReported(flightDataList[0].Latitude, flightDataList[0].Longitude))
			{
				string text15 = encryptString(textBoxUserName.Text);
				GoogleTracker.trackEventPos("UserAndPosition", text15 + "@" + flightDataList[0].Latitude + ";" + flightDataList[0].Longitude);
				LogTxtToFileInTempFolder("gkc4.gkc", flightDataList[0].Latitude + ";" + flightDataList[0].Longitude, append: true);
			}
			NumberOfFlightsMissingStats = CountNumberOfFlightsMissingStats();
			if (NumberOfFlightsMissingStats > 2)
			{
				textBoxStatus.Text = "\r\nPlease wait while downloading and analyzing flights to find maximum values.\r\nOnce a flight is analyzed the result is saved on the harddrive,\r\nnext time the flight don't need to be downloaded and analyzed again,\r\nwhich will speed up the processing time.\r\n\r\nNumber of remaining flights to be downloaded and analyzed: " + NumberOfFlightsMissingStats + "\r\n\r\nIf you click on a flight this message will disappear but the download will still continue.\r\nYou can also doubleclick on a flight to see graph etc during this download.\r\nFor sorting of a specific parameter, you have to wait for download to complete";
				textBoxStatus.Visible = true;
			}
			FindFlightStatisticsForAllFlights();
			FindFlightCommentsForAllFlights();
			FindVideoFileLinksForAllFlights();
			int num8 = 0;
			foreach (FlightDataStruct flightData in flightDataList)
			{
				num8 += flightData.totalRunTimeTotalSec;
			}
			int num9 = num8 / 3600;
			int num10 = num8 / 60 % 60;
			int num11 = num8 % 60;
			labelStatus.Text = "Total number of flights=" + listView1.Items.Count + "  - -  Total flight time hh:mm:ss = " + num9.ToString("00") + ":" + num10.ToString("00") + ":" + num11.ToString("00");
			for (int m = 0; m < listView1.Items.Count; m++)
			{
				textBoxStatus.BringToFront();
				if (counterAddressesDecoded == listView1.Items.Count)
				{
					break;
				}
				if (flightDataList[counterAddressesDecoded].Latitude > 200.0 || flightDataList[counterAddressesDecoded].Longitude > 200.0)
				{
					listView1.Items[counterAddressesDecoded].SubItems[8].Text = "No lat and lon for this flight";
					counterAddressesDecoded++;
					continue;
				}
				string addressFromAddressesTxt = GetAddressFromAddressesTxt(flightDataList[counterAddressesDecoded].flightID);
				if (addressFromAddressesTxt == "")
				{
					IgnoreDecodedAddressEvent = false;
					myWebClient = new WebClient();
					myWebClient.Headers.Add("user-agent", "Some random text!");
					myWebClient.DownloadStringCompleted += wc_DownloadStringCompleted;
					myWebClient.DownloadStringAsync(new Uri("http://nominatim.openstreetmap.org/reverse.php?format=xml&lat=" + flightDataList[counterAddressesDecoded].Latitude + "&lon=" + flightDataList[counterAddressesDecoded].Longitude));
					break;
				}
				listView1.Items[counterAddressesDecoded].SubItems[8].Text = addressFromAddressesTxt;
				counterAddressesDecoded++;
			}
			textBoxStatus.Visible = false;
			allFlightStatisticsPerformed = true;
			buttonExport.Enabled = true;
		}

		private List<string> FindNewAccountTypeCredentials()
		{
			List<string> list = new List<string>();
			HttpClient httpClient = new HttpClient();
			FormUrlEncodedContent content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
			{
				new KeyValuePair<string, string>("login", textBoxUserName.Text),
				new KeyValuePair<string, string>("password", textBoxPassword.Text)
			});
			HttpResponseMessage result = httpClient.PostAsync("https://accounts.parrot.com/V3/logform/", content).Result;
			if (result.IsSuccessStatusCode)
			{
				string originalString = result.RequestMessage.RequestUri.OriginalString;
				int num = originalString.IndexOf("\"user\":\"", StringComparison.Ordinal);
				if (num < 5)
				{
					return list;
				}
				int length = originalString.IndexOf("\"", num + 9, StringComparison.Ordinal) - num - 8;
				string item = originalString.Substring(num + 8, length);
				list.Add(item);
				int num2 = originalString.IndexOf("\"pwd\":\"", StringComparison.Ordinal);
				if (num2 < 5)
				{
					return list;
				}
				int length2 = originalString.IndexOf("\"", num2 + 9, StringComparison.Ordinal) - num2 - 7;
				string item2 = originalString.Substring(num2 + 7, length2);
				list.Add(item2);
			}
			return list;
		}

		private string encryptString(string textDecrypted)
		{
			int[] array = new int[39]
			{
				3,
				5,
				7,
				3,
				6,
				9,
				4,
				1,
				6,
				2,
				8,
				4,
				6,
				3,
				8,
				5,
				8,
				2,
				2,
				5,
				1,
				9,
				6,
				8,
				9,
				3,
				6,
				1,
				4,
				7,
				6,
				4,
				8,
				4,
				7,
				1,
				4,
				9,
				5
			};
			int[] array2 = new int[39]
			{
				615,
				51,
				271,
				332,
				68,
				91,
				404,
				106,
				623,
				231,
				884,
				421,
				660,
				323,
				812,
				534,
				898,
				221,
				276,
				538,
				190,
				952,
				628,
				832,
				961,
				359,
				609,
				12,
				48,
				77,
				612,
				487,
				827,
				462,
				72,
				189,
				453,
				912,
				506
			};
			string text = "";
			for (int i = 0; i < textDecrypted.Length; i++)
			{
				string text2 = Convert.ToString(Convert.ToChar(textDecrypted.Substring(i, 1)) * array[i]);
				text = text + text2 + ";" + array2[i] + ";";
			}
			return text.Substring(0, text.Length - 1);
		}

		private string decryptString(string textEncrypted)
		{
			int[] array = new int[39]
			{
				3,
				5,
				7,
				3,
				6,
				9,
				4,
				1,
				6,
				2,
				8,
				4,
				6,
				3,
				8,
				5,
				8,
				2,
				2,
				5,
				1,
				9,
				6,
				8,
				9,
				3,
				6,
				1,
				4,
				7,
				6,
				4,
				8,
				4,
				7,
				1,
				4,
				9,
				5
			};
			_ = new int[39]
			{
				615,
				51,
				271,
				332,
				68,
				91,
				404,
				106,
				623,
				231,
				884,
				421,
				660,
				323,
				812,
				534,
				898,
				221,
				276,
				538,
				190,
				952,
				628,
				832,
				961,
				359,
				609,
				12,
				48,
				77,
				612,
				487,
				827,
				462,
				72,
				189,
				453,
				912,
				506
			};
			string text = "";
			string[] array2 = textEncrypted.Split(';');
			for (int i = 0; i + 1 < array2.Length; i += 2)
			{
				string str = Convert.ToString(Convert.ToChar(Convert.ToInt32(array2[i]) / array[i / 2]));
				text += str;
			}
			return text;
		}

		private bool CheckIfFlightInThisAreaAlreadyReported(double lat, double lon)
		{
			string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			if (Directory.Exists(text))
			{
				string path = Path.Combine(text, "gkc4.gkc");
				if (File.Exists(path))
				{
					string text2 = "";
					StreamReader streamReader = new StreamReader(path);
					while ((text2 = streamReader.ReadLine()) != null)
					{
						string[] array = text2.Split(';');
						double lat2 = Convert.ToDouble(array[0]);
						double lon2 = Convert.ToDouble(array[1]);
						if (distanceBetweenTwoGpsCoordinatesinMeters(lat, lon, lat2, lon2) < 100.0)
						{
							return true;
						}
					}
					streamReader.Close();
					return false;
				}
				return false;
			}
			return false;
		}

		private int CountNumberOfFlightsMissingStats()
		{
			int num = 0;
			for (int i = 0; i < flightDataList.Count(); i++)
			{
				if (GetFlightStatFromFlightStatTxt(flightDataList[i].flightID)[0] == "")
				{
					num++;
				}
			}
			return num;
		}

		private int Pythagoras(int a, int b)
		{
			return Convert.ToInt32(Math.Sqrt(a * a + b * b));
		}

		private float PythagorasSingle(float a, float b)
		{
			return Convert.ToSingle(Math.Sqrt(a * a + b * b));
		}

		private void FindFlightStatisticsForAllFlights()
		{
			// iterate through every single loaded flight inside the listview
			for (int i = 0; i < listView1.Items.Count && !forceFormToClose; i++)
			{
				string[] flightStatFromFlightStatTxt = GetFlightStatFromFlightStatTxt(flightDataList[i].flightID);
				if (flightStatFromFlightStatTxt[0] == "")
				{
					double num = 0.0;
					double lon = 0.0;
					int num2 = 0;
					double num3 = 0.0;
					int num4 = 0;
					int num5 = 100;
					int num6 = 0;
					double num7 = 0.0;
					double num8 = 500.0;
					double num9 = 500.0;
					int num10 = 0;
					if (flightDataList[i].totalRunTimeTotalSec > 5)
					{
						RestClient restClient = new RestClient("http://academy.ardrone.com/api3/runs/" + flightDataList[i].flightID + "/details/");
						RestRequest request = new RestRequest(Method.GET);
						if (newAccountTypeCredentials.Count > 1)
						{
							restClient.Authenticator = new HttpBasicAuthenticator(newAccountTypeCredentials[0], newAccountTypeCredentials[1]);
						}
						else
						{
							restClient.Authenticator = new HttpBasicAuthenticator(textBoxUserName.Text, textBoxPassword.Text);
						}
						IRestResponse restResponse = restClient.Execute(request);
						LogTxtToFileInTempFolder("gkc6.gkc", restResponse.Content.ToString());
						if (restResponse.StatusCode.ToString() == "OK")
						{
							double num11 = 0.0;
							string text = restResponse.Content.ToString();
							string valueOfField = GetValueOfField(text, "\"product_name\":", "Product Name");
							if (!valueOfField.Contains("Bebop") && !valueOfField.Contains("Disco") && !valueOfField.Contains("Anafi"))
							{
								LogTxtToFileInTempFolder("FlightStat.txt", flightDataList[i].flightID + ";0 m;0 m/s;0 m;0 m;0 - 0 %", append: true);
								continue;
							}
							string[] array = text.Split('[');
							restClient = null;
							request = null;
							if (array.Length < 6)
							{
								continue;
							}
							FindIndexOfDataFields(array);
							byte b = 0;
							text.Contains("product_gps_sv_number");
							if (text.Contains("pitot_speed"))
							{
								b = 1;
							}
							int num12 = 0;
							for (int j = 0; j < array.Length; j++)
							{
								string[] array2 = array[j].Split(',');
								if (array2.Count() < 19)
								{
									continue;
								}
								int result = 0;
								if (array2.Length <= DataExchangerClass.flightDataFieldIndexTime)
								{
									continue;
								}
								if (!int.TryParse(array2[DataExchangerClass.flightDataFieldIndexTime], out result))
								{
									continue;
								}
								if(result / 1000 <= num12)
								{
									continue;
								}
								num12 = result / 1000;
								int num13 = 0;
								int num14 = 0;
								if (array2[DataExchangerClass.flightDataFieldIndexAltitude].Contains("."))
								{
									num13 = Convert.ToInt32(Convert.ToDouble(array2[DataExchangerClass.flightDataFieldIndexAltitude]));
									num14 = (int)(Convert.ToDouble(array2[DataExchangerClass.flightDataFieldIndexAltitude]) * 1000.0);
								}
								else
								{
									num13 = Convert.ToInt32(array2[DataExchangerClass.flightDataFieldIndexAltitude]) / 1000;
									num14 = Convert.ToInt32(array2[DataExchangerClass.flightDataFieldIndexAltitude]);
								}
								if (num13 > num2)
								{
									num2 = num13;
								}
								float value = Convert.ToSingle(array2[DataExchangerClass.flightDataFieldIndexSpeedVx]);
								float value2 = Convert.ToSingle(array2[DataExchangerClass.flightDataFieldIndexSpeedVy]);
								float value3 = Convert.ToSingle(array2[DataExchangerClass.flightDataFieldIndexSpeedVz]);
								float a = PythagorasSingle(Math.Abs(value), Math.Abs(value2));
								float num15 = PythagorasSingle(a, Math.Abs(value3));
								if ((double)num15 > num3)
								{
									num3 = num15;
								}
								int num16 = 0;
								if (Convert.ToInt32(array2[DataExchangerClass.flightDataFieldIndexBattery]) < 500)
								{
									num16 = Convert.ToInt16(array2[DataExchangerClass.flightDataFieldIndexBattery]);
								}
								if (num16 > 1)
								{
									if (num16 < num5)
									{
										num5 = num16;
									}
									if (num16 > num4)
									{
										num4 = num16;
									}
								}
								double num17 = Convert.ToDouble(array2[DataExchangerClass.flightDataFieldIndexProductGpsLon]);
								double num18 = Convert.ToDouble(array2[DataExchangerClass.flightDataFieldIndexProductGpsLat]);
								if (num17 < 0.0001 && num17 > -0.001)
								{
									num17 = 500.0;
								}
								if (num18 < 0.0001 && num18 > -0.001)
								{
									num18 = 500.0;
								}
								if (num == 0.0)
								{
									if (num18 < 200.0)
									{
										num = num18;
										lon = num17;
									}
								}
								else if (num18 < 200.0)
								{
									int a2 = Convert.ToInt32(distanceBetweenTwoGpsCoordinatesinMeters(num18, num17, num, lon));
									int num19 = Pythagoras(a2, num13);
									if (num19 > num6)
									{
										num6 = num19;
									}
								}
								float num20 = 0f;
								float num21 = 0f;
								if (num8 < 200.0 && num9 < 200.0 && num18 < 200.0 && num17 < 200.0)
								{
									num20 = (float)distanceBetweenTwoGpsCoordinatesinMeters(num8, num9, num18, num17);
									num21 = Math.Abs(num10 - num14) / 1000;
									num10 = num14;
									double num22 = PythagorasSingle(num20, num21);
									num7 += num22;
									if (valueOfField.Contains("Disco") && num22 > num3 && b == 0 && num22 > 5.0 && num22 < num11 * 1.7)
									{
										num3 = num22;
									}
									num11 = num22;
								}
								num8 = num18;
								num9 = num17;
							}
						}
					}
					else
					{
						num5 = 0;
					}
					listView1.Items[i].SubItems[3].Text = num2 + " m";
					listView1.Items[i].SubItems[4].Text = num3.ToString("0.0") + " m/s";
					listView1.Items[i].SubItems[5].Text = num6.ToString("n0") + " m";
					listView1.Items[i].SubItems[6].Text = num7.ToString("n0") + " m";
					listView1.Items[i].SubItems[7].Text = num4.ToString("00") + " - " + num5.ToString("00") + " %";
					Application.DoEvents();
					LogTxtToFileInTempFolder("FlightStat.txt", flightDataList[i].flightID + ";" + num2 + " m;" + num3.ToString("0.0") + " m/s;" + num6 + " m;" + num7.ToString("0") + " m;" + num4.ToString("00") + " - " + num5.ToString("00") + " %", append: true);
					NumberOfFlightsMissingStats--;
					textBoxStatus.Text = "\r\nPlease wait while downloading and analyzing flights to find maximum values.\r\nOnce a flight is analyzed the result is saved on the harddrive,\r\nnext time the flight don't need to be downloaded and analyzed again,\r\nwhich will speed up the processing time.\r\n\r\nNumber of remaining flights to be downloaded and analyzed: " + NumberOfFlightsMissingStats + "\r\n\r\nIf you click on a flight this message will disappear but the download will still continue.\r\nYou can also doubleclick on a flight to see graph etc during this download.\r\nFor sorting of a specific parameter, you have to wait for download to complete";
				}
				else
				{
					listView1.Items[i].SubItems[3].Text = flightStatFromFlightStatTxt[0];
					listView1.Items[i].SubItems[4].Text = flightStatFromFlightStatTxt[1];
					listView1.Items[i].SubItems[5].Text = Convert.ToInt32(flightStatFromFlightStatTxt[2].Substring(0, flightStatFromFlightStatTxt[2].Length - 2)).ToString("n0") + " m";
					listView1.Items[i].SubItems[6].Text = Convert.ToInt32(flightStatFromFlightStatTxt[3].Substring(0, flightStatFromFlightStatTxt[3].Length - 2)).ToString("n0") + " m";
					listView1.Items[i].SubItems[7].Text = flightStatFromFlightStatTxt[4];
					Application.DoEvents();
				}
			}
			buttonGetListOfFlights.Enabled = true;
		}

		private void FindFlightCommentsForAllFlights()
		{
			for (int i = 0; i < listView1.Items.Count; i++)
			{
				if (forceFormToClose)
				{
					break;
				}
				string flightCommentFromFlightCommentsTxt = GetFlightCommentFromFlightCommentsTxt(flightDataList[i].flightID);
				listView1.Items[i].SubItems[17].Text = flightCommentFromFlightCommentsTxt;
			}
		}

		private void FindVideoFileLinksForAllFlights()
		{
			for (int i = 0; i < listView1.Items.Count; i++)
			{
				if (forceFormToClose)
				{
					break;
				}
				string videoLinkFileFromFlightVideoLinksTxt = GetVideoLinkFileFromFlightVideoLinksTxt(flightDataList[i].flightID);
				listView1.Items[i].SubItems[16].Text = videoLinkFileFromFlightVideoLinksTxt;
			}
		}

		private double distanceBetweenTwoGpsCoordinatesinMeters(double lat1, double lon1, double lat2, double lon2)
		{
			double deg = lon1 - lon2;
			double d = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(deg));
			d = Math.Acos(d);
			d = rad2deg(d);
			d = d * 60.0 * 1853.159616;
			if (double.IsNaN(d))
			{
				d = 0.0;
			}
			return d;
		}

		private static double deg2rad(double deg)
		{
			return deg * Math.PI / 180.0;
		}

		private static double rad2deg(double rad)
		{
			return rad / Math.PI * 180.0;
		}

		private void wc_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
		{
			if (IgnoreDecodedAddressEvent)
			{
				myWebClient = null;
			}
			else
			{
				try
				{
					if (e.Result.Contains("addressparts"))
					{
						string value = (from elm in XElement.Parse(e.Result).Descendants()
							where elm.Name == "result"
							select elm).FirstOrDefault().Value;
						value = DecodeFromUtf8(value);
						counterAddressesRetries = 0;
						listView1.Items[counterAddressesDecoded].SubItems[8].Text = value;
						LogTxtToFileInTempFolder("Addresses.txt", flightDataList[counterAddressesDecoded].flightID + "=" + value, append: true);
						Thread.Sleep(10);
						myWebClient = null;
						counterAddressesDecoded++;
						if (counterAddressesDecoded < listView1.Items.Count)
						{
							counterAddressesRetries = 0;
							int num = 0;
							while (true)
							{
								if (num >= listView1.Items.Count || counterAddressesDecoded == listView1.Items.Count)
								{
									return;
								}
								if (flightDataList[counterAddressesDecoded].Latitude > 200.0 || flightDataList[counterAddressesDecoded].Longitude > 200.0)
								{
									listView1.Items[counterAddressesDecoded].SubItems[8].Text = "No lat and lon for this flight";
									counterAddressesDecoded++;
								}
								else
								{
									string addressFromAddressesTxt = GetAddressFromAddressesTxt(flightDataList[counterAddressesDecoded].flightID);
									if (addressFromAddressesTxt == "")
									{
										break;
									}
									listView1.Items[counterAddressesDecoded].SubItems[8].Text = addressFromAddressesTxt;
									counterAddressesDecoded++;
								}
								num++;
							}
							myWebClient = new WebClient();
							myWebClient.Headers.Add("user-agent", "Some random text!");
							myWebClient.DownloadStringCompleted += wc_DownloadStringCompleted;
							myWebClient.DownloadStringAsync(new Uri("http://nominatim.openstreetmap.org/reverse.php?format=xml&lat=" + flightDataList[counterAddressesDecoded].Latitude + "&lon=" + flightDataList[counterAddressesDecoded].Longitude));
						}
					}
					else
					{
						myWebClient = null;
						Thread.Sleep(20);
						if (counterAddressesRetries < 5 && flightDataList[counterAddressesDecoded].Latitude < 200.0 && flightDataList[counterAddressesDecoded].Longitude < 200.0)
						{
							counterAddressesRetries++;
							myWebClient = new WebClient();
							myWebClient.DownloadStringCompleted += wc_DownloadStringCompleted;
							myWebClient.DownloadStringAsync(new Uri("http://nominatim.openstreetmap.org/reverse.php?format=xml&lat=" + flightDataList[counterAddressesDecoded].Latitude + "&lon=" + flightDataList[counterAddressesDecoded].Longitude));
						}
						else
						{
							counterAddressesRetries = 0;
							if (flightDataList[counterAddressesDecoded].Latitude > 200.0 && flightDataList[counterAddressesDecoded].Longitude > 200.0)
							{
								listView1.Items[counterAddressesDecoded].SubItems[8].Text = "No lat and lon for this flight";
							}
							else if (e.Result.Contains("OVER_QUERY_LIMIT"))
							{
								listView1.Items[counterAddressesDecoded].SubItems[8].Text = "Address unknown, 'Over query limit' resp from Google Maps";
							}
							else
							{
								listView1.Items[counterAddressesDecoded].SubItems[8].Text = "Address unknown, No resp from Google Maps";
							}
							counterAddressesDecoded++;
							if (counterAddressesDecoded < listView1.Items.Count)
							{
								while (counterAddressesDecoded < listView1.Items.Count)
								{
									if (flightDataList[counterAddressesDecoded].Latitude == 0.0 && flightDataList[counterAddressesDecoded].Longitude == 0.0)
									{
										listView1.Items[counterAddressesDecoded].SubItems[8].Text = "No lat and lon for this flight";
										counterAddressesDecoded++;
									}
								}
								myWebClient = new WebClient();
								myWebClient.Headers.Add("user-agent", "Some random text!");
								myWebClient.DownloadStringCompleted += wc_DownloadStringCompleted;
								myWebClient.DownloadStringAsync(new Uri("http://nominatim.openstreetmap.org/reverse.php?format=xml&lat=" + flightDataList[counterAddressesDecoded].Latitude + "&lon=" + flightDataList[counterAddressesDecoded].Longitude));
							}
						}
					}
				}
				catch (Exception)
				{
					myWebClient = null;
					Thread.Sleep(20);
					listView1.Items[counterAddressesDecoded].SubItems[8].Text = "Unexpected error when trying to decode address";
					counterAddressesDecoded++;
					if (counterAddressesDecoded < listView1.Items.Count)
					{
						while (counterAddressesDecoded < listView1.Items.Count && flightDataList[counterAddressesDecoded].Latitude == 0.0 && flightDataList[counterAddressesDecoded].Longitude == 0.0)
						{
							listView1.Items[counterAddressesDecoded].SubItems[8].Text = "No lat and lon for this flight";
							counterAddressesDecoded++;
						}
						myWebClient = new WebClient();
						myWebClient.Headers.Add("user-agent", "Some random text!");
						myWebClient.DownloadStringCompleted += wc_DownloadStringCompleted;
						myWebClient.DownloadStringAsync(new Uri("http://nominatim.openstreetmap.org/reverse.php?format=xml&lat=" + flightDataList[counterAddressesDecoded].Latitude + "&lon=" + flightDataList[counterAddressesDecoded].Longitude));
					}
				}
			}
		}

		private string GetAddressFromAddressesTxt(string flightId)
		{
			string result = "";
			string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			if (Directory.Exists(text))
			{
				string path = Path.Combine(text, "Addresses.txt");
				if (File.Exists(path))
				{
					string text2 = "";
					StreamReader streamReader = new StreamReader(path);
					while ((text2 = streamReader.ReadLine()) != null)
					{
						if (text2.StartsWith(flightId + "="))
						{
							result = text2.Split('=')[1];
						}
					}
					streamReader.Close();
				}
			}
			return result;
		}

		private string[] GetFlightStatFromFlightStatTxt(string flightId)
		{
			// read cache for given flight id
			string[] array = new string[5]
			{
				"",
				"",
				"",
				"",
				""
			};
			string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			if (Directory.Exists(text))
			{
				string path = Path.Combine(text, "FlightStat.txt");
				if (File.Exists(path))
				{
					string text2 = "";
					StreamReader streamReader = new StreamReader(path);
					while ((text2 = streamReader.ReadLine()) != null)
					{
						if (text2.StartsWith(flightId + ";"))
						{
							Array.Copy(text2.Split(';'), 1, array, 0, 5);
						}
					}
					streamReader.Close();
				}
			}
			return array;
		}

		private string GetFlightCommentFromFlightCommentsTxt(string flightId)
		{
			string result = "";
			string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			if (Directory.Exists(text))
			{
				string path = Path.Combine(text, "FlightComments.txt");
				if (File.Exists(path))
				{
					string text2 = "";
					StreamReader streamReader = new StreamReader(path);
					while ((text2 = streamReader.ReadLine()) != null)
					{
						if (text2.StartsWith(flightId + ";"))
						{
							result = text2.Substring(text2.IndexOf(";") + 1);
						}
					}
					streamReader.Close();
				}
			}
			return result;
		}

		private string GetVideoLinkFileFromFlightVideoLinksTxt(string flightId)
		{
			string result = "";
			string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			if (Directory.Exists(text))
			{
				string path = Path.Combine(text, "FlightVideoLinks.txt");
				if (File.Exists(path))
				{
					string text2 = "";
					StreamReader streamReader = new StreamReader(path);
					while ((text2 = streamReader.ReadLine()) != null)
					{
						if (text2.StartsWith(flightId + ";"))
						{
							result = text2.Substring(text2.IndexOf(";") + 1);
						}
					}
					streamReader.Close();
				}
			}
			return result;
		}

		private string GetValueOfField(string stringToSearch, string nameOfField, string nameOfFieldFriendly, int myStartIndex = 0, bool useLastValue = false)
		{
			string text = "";
			if (!ErrorInDecodingFlightList)
			{
				try
				{
					int length = nameOfField.Length;
					int num = 0;
					num = ((!useLastValue) ? stringToSearch.IndexOf(nameOfField, myStartIndex) : stringToSearch.LastIndexOf(nameOfField));
					if (num <= -1)
					{
						throw new Exception("Exception generated by GetValueOfField method due to value of posOfFieldNameStart is less than 1");
					}
					int num2 = 0;
					num2 = ((!nameOfField.Contains("controller_model")) ? stringToSearch.IndexOf(",", num + 1) : stringToSearch.IndexOf("\"", num + length + 1));
					if (num2 < 1)
					{
						num2 = stringToSearch.Length;
					}
					text = stringToSearch.Substring(num + length, num2 - num - length);
					text = text.Trim();
					text = text.Replace("\"", "");
					text = text.Replace("}", "");
					text = text.Replace("]", "");
					return text;
				}
				catch (Exception ex)
				{
					ErrorInDecodingFlightList = true;
					string text2 = "Failed to decode data from a flight from Parrot Cloud, this flight will be ignored \nPress OK to continue \n\n Error details: \nAn error occurred when trying to decode field: " + nameOfFieldFriendly + " \n" + ex.Message + "\n\nData from flight= \n" + stringToSearch;
					MessageBox.Show(this, text2);
					return text;
				}
			}
			return text;
		}

		private string DecodeFromUtf8(string inputString)
		{
			byte[] bytes = Encoding.Default.GetBytes(inputString);
			return Encoding.UTF8.GetString(bytes);
		}

		private void LogTxtToFileInTempFolder(string fileName, string txtToWrite, bool append = false)
		{
			try
			{
				string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
				if (!Directory.Exists(text))
				{
					Directory.CreateDirectory(text);
				}
				using (StreamWriter streamWriter = new StreamWriter(Path.Combine(text, fileName), append))
				{
					streamWriter.WriteLine(txtToWrite);
				}
			}
			catch (Exception)
			{
			}
		}

		private void FormGetDataFromDroneAcademy_FormClosing(object sender, FormClosingEventArgs e)
		{
			forceEndlessWhileLoopToClose = true;
			forceEndlessWhileLoop2ToClose = true;
			forceFormToClose = true;
			if (base.WindowState == FormWindowState.Maximized)
			{
				Settings.Default.WindowSubStateMaximized = true;
			}
			else
			{
				Settings.Default.WindowSubStateMaximized = false;
				Settings.Default.WindowSubPosX = base.Location.X;
				Settings.Default.WindowSubPosY = base.Location.Y;
				Settings.Default.WindowSubSizeX = base.Size.Width;
				Settings.Default.WindowSubSizeY = base.Size.Height;
			}
			SaveSettings();
		}

		private void listView1_MouseDoubleClick_1(object sender, MouseEventArgs e)
		{
			int num = 0;
			if (e != null)
			{
				ListViewHitTestInfo listViewHitTestInfo = listView1.HitTest(e.Location);
				num = listViewHitTestInfo.Item.SubItems.IndexOf(listViewHitTestInfo.SubItem);
			}
			int index = listView1.SelectedIndices[0];
			flightIdClicked = listView1.Items[index].SubItems[15].Text;
			int num2 = 0;
			for (int i = 0; i < flightDataList.Count; i++)
			{
				if (flightDataList[i].flightID == flightIdClicked)
				{
					num2 = i;
					break;
				}
			}
			switch (num)
			{
			case 17:
				commentEditToolStripMenuItem.PerformClick();
				return;
			case 16:
				if (listView1.Items[index].SubItems[16].Text != "")
				{
					Process.Start(listView1.Items[index].SubItems[16].Text);
				}
				else
				{
					linkAVideofileToThisFlight();
				}
				return;
			}
			if (flightDataList[num2].totalRunTimeTotalSec < 5)
			{
				MessageBox.Show(this, "A flight with a duration shorter than 5 seconds cannot be opened");
				return;
			}
			LoadFlight(flightIdClicked, num2);
			SetDataExchangerPicAndVideoLinks();
		}

		private void LoadFlight(string flightID, int indexClickedOriginalBeforeSorting = -1)
		{
			bool flag = false;
			if (indexClickedOriginalBeforeSorting == -1)
			{
				flag = true;
				if (DataExchangerClass.reportStatistics)
				{
					GoogleTracker.trackEvent("Flight loaded from specific flight ID");
				}
			}
			RestClient restClient = new RestClient("http://academy.ardrone.com/api3/runs/" + flightID + "/details/");
			RestRequest request = new RestRequest(Method.GET);
			if (newAccountTypeCredentials.Count > 1)
			{
				restClient.Authenticator = new HttpBasicAuthenticator(newAccountTypeCredentials[0], newAccountTypeCredentials[1]);
			}
			else
			{
				restClient.Authenticator = new HttpBasicAuthenticator(textBoxUserName.Text, textBoxPassword.Text);
			}
			IRestResponse restResponse = restClient.Execute(request);
			LogTxtToFileInTempFolder("gkc2.gkc", restResponse.Content.ToString());
			if (restResponse.StatusCode.ToString() != "OK")
			{
				if (flag)
				{
					MessageBox.Show(this, "Specified flight ID can't be found");
				}
				else
				{
					MessageBox.Show(this, "Something failed when trying to get data from Parrot Cloud,\nCheck that you entered the correct username and password and is connected to the internet");
				}
				buttonGetListOfFlights.Enabled = true;
				return;
			}
			string text = restResponse.Content.ToString();
			string valueOfField = GetValueOfField(text, "\"product_name\":", "Product Name");
			valueOfField = valueOfField.Replace("Bebop Drone", "Bebop1");
			valueOfField = valueOfField.Replace("Bebop 2", "Bebop2");
			if (!valueOfField.Contains("Bebop") && !valueOfField.Contains("Disco") && !valueOfField.Contains("Anafi"))
			{
				MessageBox.Show(this, "Selected flight is not from a Parrot Anafi, Parrot Bebop or a Parrot Disco and is not supported, Product name=" + valueOfField);
				return;
			}
			DataExchangerClass.droneModel = valueOfField;
			string valueOfField2 = GetValueOfField(text, "\"product_id\":", "product_id");
			if (valueOfField2 != "2305" && valueOfField2 != "2316" && valueOfField2 != "2318" && valueOfField2 != "2324")
			{
				MessageBox.Show(this, "Selected FlightData is not from a Parrot Anafi, Bebop1, Bebop2 drone or Disco, and is not supported");
				return;
			}
			string[] array = text.Split('[');
			restClient = null;
			request = null;
			string str = "";
			if (array.Length < 6)
			{
				MessageBox.Show(this, "There is a problem with the flight data downloaded");
				return;
			}
			if (flag)
			{
				FlightDataStruct item = default(FlightDataStruct);
				string valueOfField3 = GetValueOfField(text, "\"date\":", "Date");
				if (!ErrorInDecodingFlightList)
				{
					int year = Convert.ToInt16(valueOfField3.Substring(0, 4));
					int month = Convert.ToInt16(valueOfField3.Substring(5, 2));
					int day = Convert.ToInt16(valueOfField3.Substring(8, 2));
					int hour = Convert.ToInt16(valueOfField3.Substring(11, 2));
					int minute = Convert.ToInt16(valueOfField3.Substring(13, 2));
					int second = Convert.ToInt16(valueOfField3.Substring(15, 2));
					item.flightStartDateTime = new DateTime(year, month, day, hour, minute, second);
					string valueOfField4 = GetValueOfField(text, "\"total_run_time\":", "Total run time");
					if (!ErrorInDecodingFlightList)
					{
						int num = item.totalRunTimeTotalSec = Convert.ToInt32(valueOfField4) / 1000;
						int num2 = num / 60;
						item.flightDurationString = string.Concat(str2: (num % 60).ToString("00"), str0: num2.ToString("00"), str1: ":");
						str = GetValueOfField(text, "\"controller_model\":", "Controller model");
					}
				}
				flightDataList = new List<FlightDataStruct>();
				flightDataList.Add(item);
			}
			FindIndexOfDataFields(array);
			if (text.Contains("product_gps_sv_number"))
			{
				DataExchangerClass.flightDataFieldNrOfSvsPresent = true;
			}
			if (text.Contains("pitot_speed"))
			{
				DataExchangerClass.flightDataFieldPitotSpeedPresent = true;
			}
			array = (DataExchangerClass.droneAcademyFlightData = new List<string>(array).GetRange(3, array.Length - 3).ToArray());
			if (flag)
			{
				DataExchangerClass.droneAcademyFlightStartDateTime = flightDataList[0].flightStartDateTime;
				DataExchangerClass.droneAcademyFlightDuration = flightDataList[0].flightDurationString;
				DataExchangerClass.lengthOfFileInTotalSec = flightDataList[0].totalRunTimeTotalSec;
				DataExchangerClass.flightID = flightID;
				MessageBox.Show(this, "Specified flight found\r\n\r\nController = " + str);
			}
			else
			{
				DataExchangerClass.droneAcademyFlightStartDateTime = flightDataList[indexClickedOriginalBeforeSorting].flightStartDateTime;
				DataExchangerClass.droneAcademyFlightDuration = flightDataList[indexClickedOriginalBeforeSorting].flightDurationString;
				DataExchangerClass.lengthOfFileInTotalSec = flightDataList[indexClickedOriginalBeforeSorting].totalRunTimeTotalSec;
				DataExchangerClass.flightID = flightDataList[indexClickedOriginalBeforeSorting].flightID;
			}
			Close();
			forceFormToClose = true;
		}

		public static void FindIndexOfDataFields(string[] inputString)
		{
			for (int i = 0; i < inputString.Length; i++)
			{
				if (!inputString[i].Contains("product_gps_latitude"))
				{
					continue;
				}
				string[] array = inputString[i].Split(',');
				for (int j = 0; j < array.Count(); j++)
				{
					if (array[j].Contains("\"time\""))
					{
						DataExchangerClass.flightDataFieldIndexTime = j;
					}
					else if (array[j].Contains("battery_level"))
					{
						DataExchangerClass.flightDataFieldIndexBattery = j;
					}
					else if (array[j].Contains("flying_state"))
					{
						DataExchangerClass.flightDataFieldIndexFlyingState = j;
					}
					else if (array[j].Contains("alert_state"))
					{
						DataExchangerClass.flightDataFieldIndexAlertState = j;
					}
					else if (array[j].Contains("wifi_signal"))
					{
						DataExchangerClass.flightDataFieldIndexWifiSignal = j;
					}
					else if (array[j].Contains("product_gps_longitude"))
					{
						DataExchangerClass.flightDataFieldIndexProductGpsLon = j;
					}
					else if (array[j].Contains("product_gps_latitude"))
					{
						DataExchangerClass.flightDataFieldIndexProductGpsLat = j;
					}
					else if (array[j].Contains("product_gps_sv_number"))
					{
						DataExchangerClass.flightDataFieldIndexProductGpsSvNr = j;
					}
					else if (array[j].Contains("speed_vx"))
					{
						DataExchangerClass.flightDataFieldIndexSpeedVx = j;
					}
					else if (array[j].Contains("speed_vy"))
					{
						DataExchangerClass.flightDataFieldIndexSpeedVy = j;
					}
					else if (array[j].Contains("speed_vz"))
					{
						DataExchangerClass.flightDataFieldIndexSpeedVz = j;
					}
					else if (array[j].Contains("angle_phi"))
					{
						DataExchangerClass.flightDataFieldIndexAnglePhi = j;
					}
					else if (array[j].Contains("angle_theta"))
					{
						DataExchangerClass.flightDataFieldIndexAngleTheta = j;
					}
					else if (array[j].Contains("angle_psi"))
					{
						DataExchangerClass.flightDataFieldIndexAnglePsi = j;
					}
					else if (array[j].Contains("altitude"))
					{
						DataExchangerClass.flightDataFieldIndexAltitude = j;
					}
					else if (array[j].Contains("controller_gps_longitude"))
					{
						DataExchangerClass.flightDataFieldIndexControllerGpsLon = j;
					}
					else if (array[j].Contains("controller_gps_latitude"))
					{
						DataExchangerClass.flightDataFieldIndexControllerGpsLat = j;
					}
					else if (array[j].Contains("product_gps_position_error"))
					{
						DataExchangerClass.flightDataFieldIndexProductGpsPosError = j;
					}
					else if (array[j].Contains("product_gps_available"))
					{
						DataExchangerClass.flightDataFieldIndexProductGpsAvailable = j;
					}
					else if (array[j].Contains("pitot_speed"))
					{
						DataExchangerClass.flightDataFieldIndexPitotSpeed = j;
					}
					else if (array[j].Contains("flip_type"))
					{
						DataExchangerClass.flightDataFieldIndexFlipType = j;
					}
				}
			}
		}

		private void buttonDeleteFlight_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show(this, "Are you sure you want to delete the selected flight\nThe flight data can't be restored again", "Confirm deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
			{
				for (int num = listView1.SelectedIndices.Count - 1; num > -1; num--)
				{
					disableExport = true;
					int index = listView1.SelectedIndices[num];
					string text = listView1.Items[index].SubItems[15].Text;
					string filePathAndName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\FlightStat.txt");
					string filePathAndName2 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\Addresses.txt");
					DeleteLineFromTxtFile(filePathAndName, text + ";");
					DeleteLineFromTxtFile(filePathAndName2, text + "=");
					RestClient restClient = new RestClient("http://academy.ardrone.com/api3/runs/" + text + "/");
					RestRequest request = new RestRequest(Method.DELETE);
					restClient.Authenticator = new HttpBasicAuthenticator(textBoxUserName.Text, textBoxPassword.Text);
					restClient.Execute(request);
					request = null;
					listView1.Items[index].Remove();
				}
				MessageBox.Show(this, "Flight data deleted");
				labelStatus.Text = "Total number of flights=" + listView1.Items.Count;
			}
		}

		private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			textBoxStatus.Visible = false;
			if (allFlightStatisticsPerformed && !mapModePublicFlights)
			{
				buttonDeleteFlight.Enabled = true;
			}
		}

		private void DeleteLineFromTxtFile(string filePathAndName, string textToFindInStartOfLine)
		{
			try
			{
				List<string> source = File.ReadAllLines(filePathAndName).ToList();
				source = source.Where((string x) => x.IndexOf(textToFindInStartOfLine) < 0).ToList();
				File.WriteAllLines(filePathAndName, source.ToArray());
			}
			catch
			{
			}
		}

		private void buttonMissingFlights_Click(object sender, EventArgs e)
		{
			new FormMissingFlights().ShowDialog();
		}

		private void textBoxUserName_TextChanged(object sender, EventArgs e)
		{
			buttonGetListOfFlights.Enabled = true;
		}

		private void textBoxPassword_TextChanged(object sender, EventArgs e)
		{
			buttonGetListOfFlights.Enabled = true;
		}

		private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			if (!allFlightStatisticsPerformed)
			{
				return;
			}
			disableExport = true;
			if (sorter == null)
			{
				sorter = (listView1.ListViewItemSorter as ItemComparer);
				sorter = new ItemComparer(e.Column);
				listView1.ListViewItemSorter = sorter;
				sorter.Order = SortOrder.None;
			}
			else
			{
				sorter = (listView1.ListViewItemSorter as ItemComparer);
			}
			if (e.Column == sorter.Column)
			{
				if (sorter.Order == SortOrder.Descending)
				{
					sorter.Order = SortOrder.Ascending;
				}
				else
				{
					sorter.Order = SortOrder.Descending;
				}
			}
			else
			{
				sorter.Column = e.Column;
				sorter.Order = SortOrder.Descending;
			}
			listView1.Sort();
		}

		private void buttonExport_Click(object sender, EventArgs e)
		{
			if (mapModePublicFlights || mapModeOwnFlights)
			{
				MessageBox.Show(this, "Export is not possible after having opened the map to see public flights\n\nTo be able to export data again, press the button 'Get list of own flights from Parrot Cloud' and export again");
				return;
			}
			if (disableExport)
			{
				buttonGetListOfFlights.PerformClick();
			}
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = "Csv|*.csv";
			saveFileDialog.Title = "Select file to export flight data table to";
			saveFileDialog.ShowDialog();
			if (saveFileDialog.FileName != "")
			{
				if (saveFileDialog.FileName.ToLower().EndsWith(".csv"))
				{
					try
					{
						using (StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName, append: false, Encoding.UTF8))
						{
							streamWriter.WriteLine("DateTime" + listSep + "FlightTime (min:sec)" + listSep + "FlightTime (sec)" + listSep + "Max altitude (m)" + listSep + " Max speed (m/s)" + listSep + " Max dist (m)" + listSep + " Dist flown (m)" + listSep + "Battery max" + listSep + " Battery min" + listSep + " Battery total" + listSep + " Latitude" + listSep + " Longitude" + listSep + " Estimated address" + listSep + " Controller" + listSep + " Drone" + listSep + " Sw ver" + listSep + " Hw ver" + listSep + " Crash" + listSep + " Serial Nr" + listSep + " FlightID" + listSep + " Linked videofile" + listSep + " Comments");
							for (int i = 0; i < listView1.Items.Count; i++)
							{
								int num = Convert.ToInt16(listView1.Items[i].SubItems[7].Text.Split(' ')[0]);
								int num2 = Convert.ToInt16(listView1.Items[i].SubItems[7].Text.Split(' ')[2]);
								string text = listView1.Items[i].SubItems[8].Text;
								text = text.Replace(listSep, "-");
								string value = flightDataList[i].flightStartDateTime.ToString("yyyy-MM-dd HH:mm:ss") + listSep + listView1.Items[i].SubItems[2].Text + listSep + flightDataList[i].totalRunTimeTotalSec.ToString() + listSep + listView1.Items[i].SubItems[3].Text.Split(' ')[0] + listSep + Convert.ToDouble(listView1.Items[i].SubItems[4].Text.Split(' ')[0]).ToString(CultureInfo.InstalledUICulture) + listSep + listView1.Items[i].SubItems[5].Text.Split(' ')[0].Replace(",", "") + listSep + listView1.Items[i].SubItems[6].Text.Split(' ')[0].Replace(",", "") + listSep + num + listSep + num2 + listSep + (num - num2) + listSep + flightDataList[i].Latitude.ToString(CultureInfo.InstalledUICulture) + listSep + flightDataList[i].Longitude.ToString(CultureInfo.InstalledUICulture) + listSep + text + listSep + listView1.Items[i].SubItems[9].Text + listSep + listView1.Items[i].SubItems[10].Text + listSep + listView1.Items[i].SubItems[11].Text + listSep + listView1.Items[i].SubItems[12].Text + listSep + listView1.Items[i].SubItems[13].Text + listSep + listView1.Items[i].SubItems[14].Text + listSep + listView1.Items[i].SubItems[15].Text + listSep + listView1.Items[i].SubItems[16].Text + listSep + listView1.Items[i].SubItems[17].Text + listSep;
								streamWriter.WriteLine(value);
							}
						}
						if (MessageBox.Show(this, "Files exported successfully\n\nOpen csv file in Excel now?", "Files exported succesfully", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
						{
							Process.Start(saveFileDialog.FileName);
						}
					}
					catch (Exception ex)
					{
						MessageBox.Show(this, "Failed to export data to csv file\n\n" + ex.Message);
					}
				}
				else
				{
					MessageBox.Show(this, "Selected filename shall contain extension .csv");
				}
			}
		}

		private void buttonSwitchToMap_Click(object sender, EventArgs e)
		{
			mapModeSpecificUsersFlights = false;
			buttonSwitchToMap.Enabled = false;
			mapModeOwnFlights = true;
			OnMarkerClickEventCounter = 0;
			ShowOwnFlightsOnMap();
		}

		private void ShowPublicFlightsOnMap()
		{
			filterLatLonString = "";
			mapModeOwnFlights = false;
			buttonGetListOfFlights.Enabled = true;
			textBoxStatus.Visible = false;
			forceEndlessWhileLoopToClose = false;
			mapModePublicFlights = true;
			lock (myLockObject)
			{
				if (markersOverlay != null && markersOverlay.Markers != null)
				{
					markersOverlay.Markers.Clear();
				}
			}
			forceFormToClose = true;
			gmap_OnMapZoomedDraggedMovedOrResized();
			disableExport = true;
			buttonDeleteFlight.Enabled = false;
			buttonSwitchToMap.Enabled = false;
			SetListOrMapsVisible(showList: false);
			Application.DoEvents();
			Refresh();
			gmap.MapProvider = GoogleMapProvider.Instance;
			Singleton<GMaps>.Instance.Mode = AccessMode.ServerAndCache;
			gmap.MinZoom = 2;
			gmap.MaxZoom = 19;
			gmap.ShowCenter = false;
			gmap.DragButton = MouseButtons.Left;
			markersOverlay = new GMapOverlay("Markers");
			gmap.Overlays.Add(markersOverlay);
			if (Settings.Default.gmapLat.Equals(0.0))
			{
				gmap.SetPositionByKeywords(DataExchangerClass.regionInfo.EnglishName);
				gmap.Zoom = 6.0;
			}
			else
			{
				gmap.Position = new PointLatLng(Settings.Default.gmapLat, Settings.Default.gmapLon);
				gmap.Zoom = Settings.Default.gmapZoom;
			}
			if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("Public flights shown on maps");
			}
			while (true)
			{
				Thread.Sleep(50);
				Application.DoEvents();
				if (forceEndlessWhileLoopToClose)
				{
					break;
				}
				if (deleteMarkers)
				{
					lock (myLockObject)
					{
						markersOverlay.Markers.Clear();
						deleteMarkers = false;
						Thread.Sleep(50);
						Application.DoEvents();
					}
				}
			}
			markersOverlay.Markers.Clear();
		}

		private void ShowOwnFlightsOnMap()
		{
			if (markersOverlay != null && markersOverlay.Markers != null)
			{
				markersOverlay.Markers.Clear();
			}
			buttonGetListOfFlights.PerformClick();
			if (!mapModeOwnFlights && !mapModePublicFlights && !mapModeSpecificUsersFlights)
			{
				return;
			}
			forceEndlessWhileLoopToClose = true;
			Application.DoEvents();
			Thread.Sleep(100);
			Application.DoEvents();
			if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("Own flights shown on maps");
			}
			if (filterLatLonString != "")
			{
				filterLatLonString = "";
				buttonGetListOfFlights.PerformClick();
			}
			forceEndlessWhileLoop2ToClose = false;
			mapModeOwnFlights = true;
			disableExport = true;
			buttonDeleteFlight.Enabled = false;
			SetListOrMapsVisible(showList: false);
			Application.DoEvents();
			Refresh();
			gmap.MapProvider = GoogleMapProvider.Instance;
			Singleton<GMaps>.Instance.Mode = AccessMode.ServerAndCache;
			gmap.MinZoom = 2;
			gmap.MaxZoom = 19;
			gmap.ShowCenter = false;
			gmap.DragButton = MouseButtons.Left;
			markersOverlay = new GMapOverlay("Markers");
			for (int i = 0; i < flightDataList.Count(); i++)
			{
				if (!(flightDataList[i].Latitude > 200.0))
				{
					GMapMarker_Text item = new GMapMarker_Text(new PointLatLng(flightDataList[i].Latitude, flightDataList[i].Longitude), "", "", new SolidBrush(Color.OrangeRed));
					markersOverlay.Markers.Add(item);
				}
			}
			gmap.Overlays.Add(markersOverlay);
			gmap.ZoomAndCenterMarkers("Markers");
			MakeClusteringOfOwnFlights();
			while (true)
			{
				Thread.Sleep(50);
				Application.DoEvents();
				if (forceEndlessWhileLoop2ToClose)
				{
					break;
				}
				if (deleteMarkers)
				{
					lock (myLockObject)
					{
						markersOverlay.Markers.Clear();
						deleteMarkers = false;
						Application.DoEvents();
						Thread.Sleep(200);
						Application.DoEvents();
					}
				}
			}
			markersOverlay.Markers.Clear();
		}

		private void gmap_OnMapZoomChanged()
		{
			labelMapZoomLevel.Text = "Map zoom level = " + gmap.Zoom + " - - Map center position: lat=" + gmap.Position.Lat.ToString("0.00000") + ", long=" + gmap.Position.Lng.ToString("0.00000");
			gmap_OnMapZoomedDraggedMovedOrResized();
		}

		private void gmap_OnMapZoomedDraggedMovedOrResized()
		{
			if (!timerMapZoomedDraggedMovedOrResized.Enabled)
			{
				timerMapZoomedDraggedMovedOrResized.Elapsed += timerMapZoomedDraggedMovedOrResized_Elapsed;
				timerMapZoomedDraggedMovedOrResized.Enabled = true;
			}
			else
			{
				timerMapZoomedDraggedMovedOrResized.Stop();
				timerMapZoomedDraggedMovedOrResized.Start();
			}
		}

		private void timerMapZoomedDraggedMovedOrResized_Elapsed(object sender, ElapsedEventArgs e)
		{
			timerMapZoomedDraggedMovedOrResized.Enabled = false;
			timerMapZoomedDraggedMovedOrResized.Elapsed -= timerMapZoomedDraggedMovedOrResized_Elapsed;
			if (mapModeOwnFlights)
			{
				MakeClusteringOfOwnFlights(calledFromEventHandler: true);
				return;
			}
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			zoomForDroneAcademy = Convert.ToInt16(gmap.Zoom) + 1;
			if (zoomForDroneAcademy > 18)
			{
				zoomForDroneAcademy = 18;
			}
			RestClient restClient = new RestClient("http://academy.ardrone.com/api3/clusters/area-clusters/" + gmap.ViewArea.Top + "/" + gmap.ViewArea.Left + "/" + gmap.ViewArea.Bottom + "/" + gmap.ViewArea.Right + "/" + zoomForDroneAcademy + "/");
			RestRequest request = new RestRequest(Method.GET);
			restClient.Authenticator = new HttpBasicAuthenticator(textBoxUserName.Text, textBoxPassword.Text);
			string[] array = restClient.Execute(request).Content.ToString().Split(new string[1]
			{
				", {"
			}, StringSplitOptions.None);
			request = null;
			ErrorInDecodingFlightList = false;
			deleteMarkers = true;
			while (deleteMarkers)
			{
				Thread.Sleep(50);
				Application.DoEvents();
			}
			lock (myLockObject)
			{
				for (int i = 0; i < array.Count(); i++)
				{
					if (array[i].Length >= 20)
					{
						int num = array[i].IndexOf("\"gps\":");
						if (num >= 0)
						{
							double lat = Convert.ToDouble(GetValueOfField(array[i], "\"gps_latitude\":", "gps_latitude", num));
							double lng = Convert.ToDouble(GetValueOfField(array[i], "\"gps_longitude\":", "gps_longitude", num));
							string valueOfField = GetValueOfField(array[i], "\"index\":", "index");
							int num2 = Convert.ToInt32(GetValueOfField(array[i], "\"count\":", "count", num));
							try
							{
								string markerTitle = num2.ToString();
								if (num2 > 999)
								{
									markerTitle = ((double)num2 / 1000.0).ToString("0") + "K";
								}
								GMapMarker_Text item = new GMapMarker_Text(new PointLatLng(lat, lng), valueOfField.ToString(), markerTitle, new SolidBrush(Color.Blue));
								markersOverlay.Markers.Add(item);
							}
							catch
							{
							}
						}
					}
				}
			}
		}

		private void gmap_Resize(object sender, EventArgs e)
		{
			if (buttonSwitchToMap.Text != "Switch to all public flights on Maps")
			{
				gmap_OnMapZoomedDraggedMovedOrResized();
			}
		}

		private void gmap_OnMarkerClick(GMapMarker item, MouseEventArgs e)
		{
			forceEndlessWhileLoopToClose = true;
			forceEndlessWhileLoop2ToClose = true;
			long num = Convert.ToInt64(item.ToolTipText);
			OnMarkerClickEventCounter++;
			if (mapModeOwnFlights || mapModeSpecificUsersFlights)
			{
				buttonSwitchToMap.Enabled = true;
				if (OnMarkerClickEventCounter < 2)
				{
					filterLatLonString = (item as GMapMarker_Text).MyTag.ToString();
					Thread.Sleep(20);
					SetListOrMapsVisible(showList: true);
					Thread.Sleep(20);
					buttonGetListOfFlights.PerformClick();
				}
				return;
			}
			if (num > 200 && gmap.Zoom < 14.0)
			{
				gmap.Zoom++;
				return;
			}
			buttonSwitchToMap.Enabled = true;
			itemTagString = (item as GMapMarker_Text).MyTag.ToString();
			SaveMapLocationAndZoomLevel();
			if (num > 50)
			{
				if (MessageBox.Show(this, "You have selected " + num + " flights \nThis will take some time to open and analyze\n\nAre you sure you want to open them all?", "Many flights selected", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
				{
					SetListOrMapsVisible(showList: true);
					buttonGetListOfFlights.PerformClick();
					if (DataExchangerClass.reportStatistics)
					{
						GoogleTracker.trackEvent("Public flights loaded from maps");
					}
				}
			}
			else
			{
				SetListOrMapsVisible(showList: true);
				buttonGetListOfFlights.PerformClick();
				if (DataExchangerClass.reportStatistics)
				{
					GoogleTracker.trackEvent("Public flights loaded from maps");
				}
			}
		}

		private void SaveMapLocationAndZoomLevel()
		{
			Settings.Default.gmapLat = gmap.Position.Lat;
			Settings.Default.gmapLon = gmap.Position.Lng;
			Settings.Default.gmapZoom = gmap.Zoom;
			Settings.Default.Save();
		}

		private void gmap_DoubleClick(object sender, EventArgs e)
		{
			PointLatLng position = gmap.FromLocalToLatLng(((MouseEventArgs)e).X, ((MouseEventArgs)e).Y);
			gmap.Position = position;
			if (((MouseEventArgs)e).Button == MouseButtons.Left)
			{
				gmap.Zoom++;
			}
			else
			{
				gmap.Zoom--;
			}
		}

		private void listView1_MouseClick(object sender, MouseEventArgs e)
		{
			int index = listViewIndexClicked = listView1.HitTest(e.X, e.Y).Item.Index;
			flightIdClicked = listView1.Items[index].SubItems[15].Text;
			if (e.Button == MouseButtons.Right)
			{
				flightIDSubToolStripMenuItem1.Text = "Flight ID = " + flightIdClicked;
				if (listView1.Items[index].SubItems[17].Text == "")
				{
					commentAddToolStripMenuItem.Enabled = true;
					commentEditToolStripMenuItem.Enabled = false;
					commentDeleteToolStripMenuItem1.Enabled = false;
				}
				else
				{
					commentAddToolStripMenuItem.Enabled = false;
					commentEditToolStripMenuItem.Enabled = true;
					commentDeleteToolStripMenuItem1.Enabled = true;
				}
				if (listView1.Items[index].SubItems[16].Text != "")
				{
					linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem.Enabled = false;
					linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem.Enabled = true;
					linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem.Enabled = true;
				}
				else
				{
					linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem.Enabled = true;
					linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem.Enabled = false;
					linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem.Enabled = false;
				}
				if (listView1.SelectedIndices.Count > 1)
				{
					deleteFlightToolStripMenuItem.Text = "Delete all selected flights";
					openFlightToolStripMenuItem.Enabled = false;
					flightIDToolStripMenuItem.Enabled = false;
					commentForFlightToolStripMenuItem.Enabled = false;
				}
				else
				{
					deleteFlightToolStripMenuItem.Text = "Delete flight";
				}
			}
			buttonDeleteFlight.Enabled = true;
			deleteFlightToolStripMenuItem.Enabled = true;
		}

		private void buttonLoadFlightUsingID_Click(object sender, EventArgs e)
		{
			new MessageBoxInputText("Load Flight using Flight ID?", "All your own flights and other users public flights can be\nloaded via a flight ID below, the flight ID can be\nfound by right clicking on a flight in the list", "Load from Flight ID", "Cancel").ShowDialog();
			if (MessageBoxInputText.dialogAdvancedResult == 1)
			{
				LoadFlight(MessageBoxInputText.textResult.Trim());
			}
		}

		private void buttonZoomIn_Click(object sender, EventArgs e)
		{
			gmap.Zoom++;
		}

		private void buttonZoomOut_Click(object sender, EventArgs e)
		{
			gmap.Zoom--;
		}

		private void buttonClearCachedInfo_Click(object sender, EventArgs e)
		{
			string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			bool flag = false;
			try
			{
				File.Delete(Path.Combine(path, "FlightStat.txt"));
			}
			catch
			{
				flag = true;
			}
			try
			{
				File.Delete(Path.Combine(path, "Addresses.txt"));
			}
			catch
			{
				flag = true;
			}
			if (!flag)
			{
				MessageBox.Show(this, "All cached info is now deleted,\n\nnext time you download your flight list, it will take a while to analyze all flights again");
			}
		}

		private void MakeClusteringOfOwnFlights(bool calledFromEventHandler = false)
		{
			if (processingClusteringOfOwnFlights)
			{
				return;
			}
			processingClusteringOfOwnFlights = true;
			if (calledFromEventHandler)
			{
				deleteMarkers = true;
				while (deleteMarkers)
				{
					Thread.Sleep(100);
					Application.DoEvents();
				}
			}
			else
			{
				markersOverlay.Markers.Clear();
			}
			int num = gmap.Size.Width / 25 + 1;
			int num2 = gmap.Size.Height / 25 + 1;
			double left = gmap.ViewArea.Left;
			double num3 = (gmap.ViewArea.Right - left) / (double)num;
			double top = gmap.ViewArea.Top;
			double bottom = gmap.ViewArea.Bottom;
			double num4 = (top - bottom) / (double)num2;
			for (int i = 1; i <= num; i++)
			{
				double num5 = left + (double)(i - 1) * num3;
				double num6 = num5 + num3;
				double lng = num5 + num3 / 2.0;
				for (int j = 1; j <= num2; j++)
				{
					double num7 = top - (double)(j - 1) * num4;
					double num8 = num7 - num4;
					double lat = num7 - num4 / 2.0;
					int num9 = 0;
					for (int k = 0; k < flightDataList.Count(); k++)
					{
						Thread.CurrentThread.CurrentCulture = originalCulture;
						if (flightDataList[k].Latitude <= num7 && flightDataList[k].Latitude > num8 && flightDataList[k].Longitude >= num5 && flightDataList[k].Longitude < num6)
						{
							num9++;
						}
					}
					if (num9 > 0)
					{
						GMapMarker_Text item = new GMapMarker_Text(new PointLatLng(lat, lng), num7 + "_" + num8 + "_" + num5 + "_" + num6, num9.ToString(), new SolidBrush(Color.OrangeRed));
						markersOverlay.Markers.Add(item);
					}
				}
			}
			gmap.Overlays.Add(markersOverlay);
			processingClusteringOfOwnFlights = false;
		}

		private void radioButtonMediaSelection_CheckedChanged(object sender, EventArgs e)
		{
			if ((sender as RadioButton).Checked)
			{
				gmap_OnMapZoomedDraggedMovedOrResized();
			}
		}

		private void radioButtonPublicOrOwnSelection_CheckedChanged(object sender, EventArgs e)
		{
			_ = (sender as RadioButton).Checked;
		}

		private void openFlightToolStripMenuItem_Click(object sender, EventArgs e)
		{
			listView1_MouseDoubleClick_1(null, null);
		}

		private void deleteFlightToolStripMenuItem_Click(object sender, EventArgs e)
		{
			buttonDeleteFlight.PerformClick();
		}

		private void copyFlightIDToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Clipboard.SetText("Flight ID = " + flightIdClicked);
		}

		private void seeSharedVideoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SetDataExchangerPicAndVideoLinks();
			new FormShowSharedMedia().ShowDialog();
			if (DataExchangerClass.mediaIdToBeUnlinkedList.Count() > 0)
			{
				foreach (string mediaIdToBeUnlinked in DataExchangerClass.mediaIdToBeUnlinkedList)
				{
					DeleteLinkToMedia(flightIdClicked, mediaIdToBeUnlinked);
				}
				buttonGetListOfFlights.PerformClick();
			}
		}

		private void SetDataExchangerPicAndVideoLinks()
		{
			foreach (FlightDataStruct flightData in flightDataList)
			{
				if (flightData.flightID == flightIdClicked)
				{
					DataExchangerClass.pictureLinksList = flightData.pictureLinksList;
					DataExchangerClass.videoLinksList = flightData.videoLinksList;
					DataExchangerClass.mediaIdList = flightData.mediaIdList;
					if (flightData.flightUserName == textBoxUserName.Text)
					{
						DataExchangerClass.myOwnFlight = true;
					}
					else
					{
						DataExchangerClass.myOwnFlight = false;
					}
					break;
				}
			}
			DataExchangerClass.mediaIdToBeUnlinkedList = new List<string>();
		}

		private void commentAddToolStripMenuItem_Click(object sender, EventArgs e)
		{
			MessageBoxInputTextWithDefaultText messageBoxInputTextWithDefaultText = new MessageBoxInputTextWithDefaultText("Add a comment for current flight", "Add a comment for current flight below\nComments are only saved on your own PC", "Save comment", "Cancel");
			MessageBoxInputTextWithDefaultText.defaultText = "Enter your comment here for current flight";
			messageBoxInputTextWithDefaultText.ShowDialog();
			if (MessageBoxInputTextWithDefaultText.dialogAdvancedResult == 1)
			{
				string text = MessageBoxInputTextWithDefaultText.textResult.Trim();
				listView1.Items[listViewIndexClicked].SubItems[17].Text = text;
				updateCommentInFlightCommentsTxtFile(listView1.Items[listViewIndexClicked].SubItems[15].Text, text);
			}
		}

		private void commentEditToolStripMenuItem_Click(object sender, EventArgs e)
		{
			MessageBoxInputTextWithDefaultText messageBoxInputTextWithDefaultText = new MessageBoxInputTextWithDefaultText("Edit a comment for current flight", "Edit a comment for current flight below\nComments are only saved on your own PC", "Save comment", "Cancel");
			MessageBoxInputTextWithDefaultText.defaultText = listView1.Items[listViewIndexClicked].SubItems[17].Text;
			messageBoxInputTextWithDefaultText.ShowDialog();
			if (MessageBoxInputTextWithDefaultText.dialogAdvancedResult == 1)
			{
				string text = MessageBoxInputTextWithDefaultText.textResult.Trim();
				listView1.Items[listViewIndexClicked].SubItems[17].Text = text;
				updateCommentInFlightCommentsTxtFile(listView1.Items[listViewIndexClicked].SubItems[15].Text, text);
			}
		}

		private void updateCommentInFlightCommentsTxtFile(string flightID, string comment)
		{
			string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\FlightComments.txt");
			try
			{
				List<string> list = new List<string>();
				try
				{
					list = File.ReadAllLines(path).ToList();
					list = list.Where((string x) => x.IndexOf(flightID + ";") < 0).ToList();
				}
				catch
				{
				}
				list.Add(flightID + ";" + comment);
				File.WriteAllLines(path, list.ToArray());
			}
			catch
			{
			}
		}

		private void commentDeleteToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			listView1.Items[listViewIndexClicked].SubItems[17].Text = "";
			string filePathAndName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\FlightComments.txt");
			DeleteLineFromTxtFile(filePathAndName, flightIdClicked + ";");
		}

		private void LinkYouTubeVideoToFlight(string flightIdToBeLinked, string videoUrlToBeLinked)
		{
			RestClient restClient = new RestClient("http://academy.ardrone.com/api3/runs/" + flightIdToBeLinked + "/videos/");
			RestRequest restRequest = new RestRequest(Method.PUT);
			restRequest.AddParameter("url", videoUrlToBeLinked);
			restRequest.AddParameter("visible", "true");
			restClient.Authenticator = new HttpBasicAuthenticator(textBoxUserName.Text, textBoxPassword.Text);
			restClient.Execute(restRequest);
			restRequest = null;
			MessageBox.Show("Flight media linked");
		}

		private void DeleteLinkToMedia(string flightIdToBeUnlinked, string mediaIdToBeUnlinked)
		{
			string text = mediaIdToBeUnlinked.Split(':')[0];
			string text2 = mediaIdToBeUnlinked.Split(':')[1];
			RestClient restClient = new RestClient("http://academy.ardrone.com/api3/runs/" + flightIdToBeUnlinked + "/" + text + "/" + text2 + "/");
			RestRequest request = new RestRequest(Method.DELETE);
			restClient.Authenticator = new HttpBasicAuthenticator(textBoxUserName.Text, textBoxPassword.Text);
			restClient.Execute(request);
			request = null;
		}

		private void linkYouTubeVideoToThisFlightToolStripMenuItem_Click(object sender, EventArgs e)
		{
			new FormLinkYouTubeVideo().ShowDialog();
			if (FormLinkYouTubeVideo.dialogAdvancedResult == 1)
			{
				string textResult = FormLinkYouTubeVideo.textResult;
				LinkYouTubeVideoToFlight(flightIdClicked, textResult);
				buttonGetListOfFlights.PerformClick();
			}
		}

		private void setVisibilityToPublicToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Cursor = Cursors.WaitCursor;
			Refresh();
			foreach (int selectedIndex in listView1.SelectedIndices)
			{
				string text = listView1.Items[selectedIndex].SubItems[15].Text;
				int num = 0;
				for (int i = 0; i < flightDataList.Count; i++)
				{
					if (flightDataList[i].flightID == text)
					{
						num = flightDataList[i].grade;
						break;
					}
				}
				RestClient restClient = new RestClient("http://academy.ardrone.com/api3/runs/" + text + "/");
				RestRequest restRequest = new RestRequest(Method.POST);
				restRequest.AddParameter("Application/Json", "{\"visible\":true, \"grade\":" + num + "}", ParameterType.RequestBody);
				restClient.Authenticator = new HttpBasicAuthenticator(textBoxUserName.Text, textBoxPassword.Text);
				restClient.Execute(restRequest);
				restRequest = null;
			}
			disableHorizontalScrollResetOfListView = true;
			Cursor = Cursors.Default;
			buttonGetListOfFlights.PerformClick();
		}

		private void setVisibilityToPrivateToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Cursor = Cursors.WaitCursor;
			Refresh();
			foreach (int selectedIndex in listView1.SelectedIndices)
			{
				string text = listView1.Items[selectedIndex].SubItems[15].Text;
				int num = 0;
				for (int i = 0; i < flightDataList.Count; i++)
				{
					if (flightDataList[i].flightID == text)
					{
						num = flightDataList[i].grade;
						break;
					}
				}
				RestClient restClient = new RestClient("http://academy.ardrone.com/api3/runs/" + text + "/");
				RestRequest restRequest = new RestRequest(Method.POST);
				restRequest.AddHeader("Accept", "application/json");
				restRequest.AddParameter("Application/Json", "{\"visible\":false, \"grade\":" + num + "}", ParameterType.RequestBody);
				restClient.Authenticator = new HttpBasicAuthenticator(textBoxUserName.Text, textBoxPassword.Text);
				restClient.Execute(restRequest);
				restRequest = null;
			}
			disableHorizontalScrollResetOfListView = true;
			Cursor = Cursors.Default;
			buttonGetListOfFlights.PerformClick();
		}

		private void gmap_OnMapDrag()
		{
			labelMapZoomLevel.Text = "Map zoom level = " + gmap.Zoom + " - - Map center position: lat=" + gmap.Position.Lat.ToString("0.00000") + ", long=" + gmap.Position.Lng.ToString("0.00000");
			gmap_OnMapZoomedDraggedMovedOrResized();
		}

		private void gmap_OnPositionChanged(PointLatLng point)
		{
			labelMapZoomLevel.Text = "Map zoom level = " + gmap.Zoom + " - - Map center position: lat=" + gmap.Position.Lat.ToString("0.00000") + ", long=" + gmap.Position.Lng.ToString("0.00000");
			gmap_OnMapZoomedDraggedMovedOrResized();
		}

		private void buttonShowSpecificUserOnMap_Click(object sender, EventArgs e)
		{
			mapModeOwnFlights = false;
			mapModePublicFlights = false;
			mapModeSpecificUsersFlights = true;
			new MessageBoxInputText("Enter the username to see all flights from this user", "Enter the username to see all flights from this user on map\n\nOnly public flights will be shown", "OK", "Cancel").ShowDialog();
			if (MessageBoxInputText.dialogAdvancedResult == 1)
			{
				SpecificUsersFlightsUserName = MessageBoxInputText.textResult.Trim();
				ShowOwnFlightsOnMap();
			}
		}

		private void buttonForgotPassword_Click(object sender, EventArgs e)
		{
			new MessageBoxInputText("Forgot username or password?", "Pressing OK will request the Parrot Cloud\nto send you an email with your username\n and a link to reset your password", "OK", "Cancel").ShowDialog();
			if (MessageBoxInputText.dialogAdvancedResult == 1)
			{
				string value = MessageBoxInputText.textResult.Trim();
				RestClient restClient = new RestClient("http://academy.ardrone.com/api3/profile/password_reset/");
				RestRequest restRequest = new RestRequest(Method.POST);
				restRequest.AddParameter("email", value);
				IRestResponse restResponse = restClient.Execute(restRequest);
				restRequest = null;
				if (restResponse.StatusCode.ToString().ToLower().Contains("failed"))
				{
					MessageBox.Show(this, "Response from server:\n" + restResponse.Content);
				}
				else
				{
					MessageBox.Show(this, "Request performed succesfully,\nNow wait a couple of minutes for an email containing your username and a link to reset the password");
				}
			}
		}

		private void textBoxUserName_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(textBoxUserName, "If you have a Parrot account created before the first of October 2017, you should enter your username.\nIf your Parrot account was created after the first of October 2017, you should enter your Email address.");
		}

		private void label1_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(labelUserName, "If you have a Parrot account created before the first of October 2017, you should enter your username.\nIf your Parrot account was created after the first of October 2017, you should enter your Email address.");
		}

		private void linkAVideofileToThisFlightToolStripMenuItem_Click(object sender, EventArgs e)
		{
			linkAVideofileToThisFlight();
		}

		private void linkAVideofileToThisFlight()
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "Mp4 files (*.mp4)|*.mp4|All files (*.*)|*.*";
			openFileDialog.InitialDirectory = Settings.Default.DefaultPath;
			openFileDialog.Title = "Please select a video file on your local harddrive to link to current flight.";
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				string fileName = openFileDialog.FileName;
				updateLinkInFlightVideoLinkTxtFile(listView1.Items[listViewIndexClicked].SubItems[15].Text, fileName);
				listView1.Items[listViewIndexClicked].SubItems[16].Text = fileName;
			}
		}

		private void updateLinkInFlightVideoLinkTxtFile(string flightID, string pathAndFileName)
		{
			string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\FlightVideoLinks.txt");
			try
			{
				List<string> list = new List<string>();
				try
				{
					list = File.ReadAllLines(path).ToList();
					list = list.Where((string x) => x.IndexOf(flightID + ";") < 0).ToList();
				}
				catch
				{
				}
				list.Add(flightID + ";" + pathAndFileName);
				File.WriteAllLines(path, list.ToArray());
			}
			catch
			{
			}
		}

		private void linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Process.Start(listView1.Items[listView1.SelectedIndices[0]].SubItems[16].Text);
		}

		private void linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			listView1.Items[listViewIndexClicked].SubItems[16].Text = "";
			string filePathAndName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\FlightVideoLinks.txt");
			DeleteLineFromTxtFile(filePathAndName, flightIdClicked + ";");
		}

		private void FormGetDataFromDroneAcademy_SizeChanged(object sender, EventArgs e)
		{
			int num = base.ClientSize.Height - (listView1.Location.Y + (int)((double)buttonZoomIn.Height * 1.4));
			listView1.Height = num;
			gmap.Height = num + labelForListView.Height;
			listView1.Width = base.ClientSize.Width - listView1.Location.X * 2;
			gmap.Width = base.ClientSize.Width - gmap.Location.X * 2;
			int y = base.ClientSize.Height - (int)((double)buttonZoomIn.Height * 1.1);
			labelStatus.Location = new Point(labelStatus.Location.X, y);
			labelMapZoomLevel.Location = new Point(labelMapZoomLevel.Location.X, y);
			labelDevelopedBy.Location = new Point(base.ClientSize.Width - (labelDevelopedBy.Width + buttonZoomIn.Height), y);
			int y2 = base.ClientSize.Height - (int)((double)buttonZoomIn.Height * 1.3);
			buttonZoomIn.Location = new Point(buttonZoomIn.Location.X, y2);
			buttonZoomOut.Location = new Point(buttonZoomOut.Location.X, y2);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.FormGetDataFromDroneAcademy));
			textBoxUserName = new System.Windows.Forms.TextBox();
			labelUserName = new System.Windows.Forms.Label();
			labelPassword = new System.Windows.Forms.Label();
			textBoxPassword = new System.Windows.Forms.TextBox();
			buttonGetListOfFlights = new System.Windows.Forms.Button();
			labelForListView = new System.Windows.Forms.Label();
			labelStatus = new System.Windows.Forms.Label();
			listView1 = new System.Windows.Forms.ListView();
			contextMenuStripListView = new System.Windows.Forms.ContextMenuStrip(components);
			openFlightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			deleteFlightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			flightIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			flightIDSubToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			copyFlightIDToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			linkedVideoFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			commentForFlightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			commentAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			commentEditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			commentDeleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			buttonDeleteFlight = new System.Windows.Forms.Button();
			checkBoxDontRememberUser = new System.Windows.Forms.CheckBox();
			buttonMissingFlights = new System.Windows.Forms.Button();
			textBoxStatus = new System.Windows.Forms.TextBox();
			buttonExport = new System.Windows.Forms.Button();
			gmap = new GMap.NET.WindowsForms.GMapControl();
			buttonSwitchToMap = new System.Windows.Forms.Button();
			buttonZoomIn = new System.Windows.Forms.Button();
			buttonZoomOut = new System.Windows.Forms.Button();
			buttonClearCachedInfo = new System.Windows.Forms.Button();
			labelDevelopedBy = new System.Windows.Forms.Label();
			labelMapZoomLevel = new System.Windows.Forms.Label();
			buttonForgotPassword = new System.Windows.Forms.Button();
			contextMenuStripListView.SuspendLayout();
			SuspendLayout();
			textBoxUserName.Location = new System.Drawing.Point(175, 15);
			textBoxUserName.Margin = new System.Windows.Forms.Padding(4);
			textBoxUserName.Name = "textBoxUserName";
			textBoxUserName.Size = new System.Drawing.Size(159, 22);
			textBoxUserName.TabIndex = 1;
			textBoxUserName.TextChanged += new System.EventHandler(textBoxUserName_TextChanged);
			textBoxUserName.MouseHover += new System.EventHandler(textBoxUserName_MouseHover);
			labelUserName.AutoSize = true;
			labelUserName.Location = new System.Drawing.Point(40, 12);
			labelUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelUserName.Name = "labelUserName";
			labelUserName.Size = new System.Drawing.Size(128, 34);
			labelUserName.TabIndex = 2;
			labelUserName.Text = "Username or Email\r\nfor Parrot Cloud:";
			labelUserName.MouseHover += new System.EventHandler(label1_MouseHover);
			labelPassword.AutoSize = true;
			labelPassword.Location = new System.Drawing.Point(369, 18);
			labelPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelPassword.Name = "labelPassword";
			labelPassword.Size = new System.Drawing.Size(73, 17);
			labelPassword.TabIndex = 4;
			labelPassword.Text = "Password:";
			textBoxPassword.Location = new System.Drawing.Point(449, 15);
			textBoxPassword.Margin = new System.Windows.Forms.Padding(4);
			textBoxPassword.Name = "textBoxPassword";
			textBoxPassword.PasswordChar = '*';
			textBoxPassword.Size = new System.Drawing.Size(105, 22);
			textBoxPassword.TabIndex = 3;
			textBoxPassword.TextChanged += new System.EventHandler(textBoxPassword_TextChanged);
			buttonGetListOfFlights.Location = new System.Drawing.Point(35, 57);
			buttonGetListOfFlights.Margin = new System.Windows.Forms.Padding(4);
			buttonGetListOfFlights.Name = "buttonGetListOfFlights";
			buttonGetListOfFlights.Size = new System.Drawing.Size(303, 28);
			buttonGetListOfFlights.TabIndex = 5;
			buttonGetListOfFlights.Text = "Get list of own flights from My.Parrot";
			buttonGetListOfFlights.UseVisualStyleBackColor = true;
			buttonGetListOfFlights.Click += new System.EventHandler(buttonGetListOfFlights_Click);
			labelForListView.AutoSize = true;
			labelForListView.Location = new System.Drawing.Point(44, 102);
			labelForListView.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelForListView.Name = "labelForListView";
			labelForListView.Size = new System.Drawing.Size(908, 17);
			labelForListView.TabIndex = 7;
			labelForListView.Text = "Double click on a flight below to download flight data into this tool  -  Click on a column header below to sort flight data on that specific data type";
			labelStatus.AutoSize = true;
			labelStatus.Location = new System.Drawing.Point(44, 824);
			labelStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelStatus.Name = "labelStatus";
			labelStatus.Size = new System.Drawing.Size(78, 17);
			labelStatus.TabIndex = 14;
			labelStatus.Text = "labelStatus";
			listView1.ContextMenuStrip = contextMenuStripListView;
			listView1.Location = new System.Drawing.Point(15, 122);
			listView1.Margin = new System.Windows.Forms.Padding(4);
			listView1.Name = "listView1";
			listView1.Size = new System.Drawing.Size(1565, 695);
			listView1.TabIndex = 15;
			listView1.UseCompatibleStateImageBehavior = false;
			listView1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(listView1_ItemSelectionChanged);
			listView1.MouseClick += new System.Windows.Forms.MouseEventHandler(listView1_MouseClick);
			listView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(listView1_MouseDoubleClick_1);
			contextMenuStripListView.ImageScalingSize = new System.Drawing.Size(20, 20);
			contextMenuStripListView.Items.AddRange(new System.Windows.Forms.ToolStripItem[5]
			{
				openFlightToolStripMenuItem,
				deleteFlightToolStripMenuItem,
				flightIDToolStripMenuItem,
				linkedVideoFileToolStripMenuItem,
				commentForFlightToolStripMenuItem
			});
			contextMenuStripListView.Name = "contextMenuStripListView";
			contextMenuStripListView.Size = new System.Drawing.Size(273, 124);
			openFlightToolStripMenuItem.Name = "openFlightToolStripMenuItem";
			openFlightToolStripMenuItem.Size = new System.Drawing.Size(272, 24);
			openFlightToolStripMenuItem.Text = "Open flight";
			openFlightToolStripMenuItem.Click += new System.EventHandler(openFlightToolStripMenuItem_Click);
			deleteFlightToolStripMenuItem.Name = "deleteFlightToolStripMenuItem";
			deleteFlightToolStripMenuItem.Size = new System.Drawing.Size(272, 24);
			deleteFlightToolStripMenuItem.Text = "Delete flight";
			deleteFlightToolStripMenuItem.Click += new System.EventHandler(deleteFlightToolStripMenuItem_Click);
			flightIDToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[2]
			{
				flightIDSubToolStripMenuItem1,
				copyFlightIDToClipboardToolStripMenuItem
			});
			flightIDToolStripMenuItem.Name = "flightIDToolStripMenuItem";
			flightIDToolStripMenuItem.Size = new System.Drawing.Size(272, 24);
			flightIDToolStripMenuItem.Text = "Flight ID";
			flightIDSubToolStripMenuItem1.Name = "flightIDSubToolStripMenuItem1";
			flightIDSubToolStripMenuItem1.Size = new System.Drawing.Size(264, 26);
			flightIDSubToolStripMenuItem1.Text = "Flight ID = ";
			copyFlightIDToClipboardToolStripMenuItem.Name = "copyFlightIDToClipboardToolStripMenuItem";
			copyFlightIDToClipboardToolStripMenuItem.Size = new System.Drawing.Size(264, 26);
			copyFlightIDToClipboardToolStripMenuItem.Text = "Copy Flight ID to clipboard";
			copyFlightIDToClipboardToolStripMenuItem.Click += new System.EventHandler(copyFlightIDToClipboardToolStripMenuItem_Click);
			linkedVideoFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[3]
			{
				linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem,
				linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem,
				linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem
			});
			linkedVideoFileToolStripMenuItem.Name = "linkedVideoFileToolStripMenuItem";
			linkedVideoFileToolStripMenuItem.Size = new System.Drawing.Size(272, 24);
			linkedVideoFileToolStripMenuItem.Text = "Linked videofile for this flight";
			linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem.Name = "linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem";
			linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem.Size = new System.Drawing.Size(295, 26);
			linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem.Text = "Link a videofile to this flight";
			linkedVideoFileLinkAVideofileToThisFlightToolStripMenuItem.Click += new System.EventHandler(linkAVideofileToThisFlightToolStripMenuItem_Click);
			linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem.Name = "linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem";
			linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem.Size = new System.Drawing.Size(295, 26);
			linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem.Text = "Play the linked videofile";
			linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem.Click += new System.EventHandler(linkedVideoFilePlayTheLinkedVideofileToolStripMenuItem_Click);
			linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem.Name = "linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem";
			linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem.Size = new System.Drawing.Size(295, 26);
			linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem.Text = "Remove the link to the videofile";
			linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem.Click += new System.EventHandler(linkedVideoFileRemoveTheLinkToTheVideofileToolStripMenuItem_Click);
			commentForFlightToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[3]
			{
				commentAddToolStripMenuItem,
				commentEditToolStripMenuItem,
				commentDeleteToolStripMenuItem1
			});
			commentForFlightToolStripMenuItem.Name = "commentForFlightToolStripMenuItem";
			commentForFlightToolStripMenuItem.Size = new System.Drawing.Size(272, 24);
			commentForFlightToolStripMenuItem.Text = "Comment for flight";
			commentAddToolStripMenuItem.Name = "commentAddToolStripMenuItem";
			commentAddToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
			commentAddToolStripMenuItem.Text = "Add comment";
			commentAddToolStripMenuItem.Click += new System.EventHandler(commentAddToolStripMenuItem_Click);
			commentEditToolStripMenuItem.Name = "commentEditToolStripMenuItem";
			commentEditToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
			commentEditToolStripMenuItem.Text = "Edit comment";
			commentEditToolStripMenuItem.Click += new System.EventHandler(commentEditToolStripMenuItem_Click);
			commentDeleteToolStripMenuItem1.Name = "commentDeleteToolStripMenuItem1";
			commentDeleteToolStripMenuItem1.Size = new System.Drawing.Size(195, 26);
			commentDeleteToolStripMenuItem1.Text = "Delete comment";
			commentDeleteToolStripMenuItem1.Click += new System.EventHandler(commentDeleteToolStripMenuItem1_Click);
			buttonDeleteFlight.Location = new System.Drawing.Point(841, 12);
			buttonDeleteFlight.Margin = new System.Windows.Forms.Padding(4);
			buttonDeleteFlight.Name = "buttonDeleteFlight";
			buttonDeleteFlight.Size = new System.Drawing.Size(163, 28);
			buttonDeleteFlight.TabIndex = 16;
			buttonDeleteFlight.Text = "Delete selected flight";
			buttonDeleteFlight.UseVisualStyleBackColor = true;
			buttonDeleteFlight.Click += new System.EventHandler(buttonDeleteFlight_Click);
			checkBoxDontRememberUser.AutoSize = true;
			checkBoxDontRememberUser.Location = new System.Drawing.Point(573, 17);
			checkBoxDontRememberUser.Margin = new System.Windows.Forms.Padding(4);
			checkBoxDontRememberUser.Name = "checkBoxDontRememberUser";
			checkBoxDontRememberUser.Size = new System.Drawing.Size(122, 21);
			checkBoxDontRememberUser.TabIndex = 17;
			checkBoxDontRememberUser.Text = "Remember me";
			checkBoxDontRememberUser.UseVisualStyleBackColor = true;
			buttonMissingFlights.Location = new System.Drawing.Point(1026, 12);
			buttonMissingFlights.Margin = new System.Windows.Forms.Padding(4);
			buttonMissingFlights.Name = "buttonMissingFlights";
			buttonMissingFlights.Size = new System.Drawing.Size(209, 28);
			buttonMissingFlights.TabIndex = 18;
			buttonMissingFlights.Text = "Missing flights in My.Parrot";
			buttonMissingFlights.UseVisualStyleBackColor = true;
			buttonMissingFlights.Click += new System.EventHandler(buttonMissingFlights_Click);
			textBoxStatus.Location = new System.Drawing.Point(336, 311);
			textBoxStatus.Margin = new System.Windows.Forms.Padding(4);
			textBoxStatus.Multiline = true;
			textBoxStatus.Name = "textBoxStatus";
			textBoxStatus.Size = new System.Drawing.Size(543, 197);
			textBoxStatus.TabIndex = 19;
			textBoxStatus.Text = "TextboxStatus";
			buttonExport.Location = new System.Drawing.Point(743, 57);
			buttonExport.Margin = new System.Windows.Forms.Padding(4);
			buttonExport.Name = "buttonExport";
			buttonExport.Size = new System.Drawing.Size(261, 28);
			buttonExport.TabIndex = 20;
			buttonExport.Text = "Export table of own flights to a csv file";
			buttonExport.UseVisualStyleBackColor = true;
			buttonExport.Click += new System.EventHandler(buttonExport_Click);
			gmap.Bearing = 0f;
			gmap.CanDragMap = true;
			gmap.EmptyTileColor = System.Drawing.Color.Navy;
			gmap.GrayScaleMode = false;
			gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
			gmap.LevelsKeepInMemmory = 5;
			gmap.Location = new System.Drawing.Point(15, 97);
			gmap.Margin = new System.Windows.Forms.Padding(4);
			gmap.MarkersEnabled = true;
			gmap.MaxZoom = 2;
			gmap.MinZoom = 2;
			gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
			gmap.Name = "gmap";
			gmap.NegativeMode = false;
			gmap.PolygonsEnabled = true;
			gmap.RetryLoadTile = 0;
			gmap.RoutesEnabled = true;
			gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
			gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(33, 65, 105, 225);
			gmap.ShowTileGridLines = false;
			gmap.Size = new System.Drawing.Size(1566, 720);
			gmap.TabIndex = 21;
			gmap.Zoom = 0.0;
			gmap.OnMarkerClick += new GMap.NET.WindowsForms.MarkerClick(gmap_OnMarkerClick);
			gmap.OnPositionChanged += new GMap.NET.PositionChanged(gmap_OnPositionChanged);
			gmap.OnMapDrag += new GMap.NET.MapDrag(gmap_OnMapDrag);
			gmap.OnMapZoomChanged += new GMap.NET.MapZoomChanged(gmap_OnMapZoomChanged);
			gmap.DoubleClick += new System.EventHandler(gmap_DoubleClick);
			gmap.Resize += new System.EventHandler(gmap_Resize);
			buttonSwitchToMap.Location = new System.Drawing.Point(360, 57);
			buttonSwitchToMap.Margin = new System.Windows.Forms.Padding(4);
			buttonSwitchToMap.Name = "buttonSwitchToMap";
			buttonSwitchToMap.Size = new System.Drawing.Size(231, 28);
			buttonSwitchToMap.TabIndex = 22;
			buttonSwitchToMap.Text = "Show own flights on a map";
			buttonSwitchToMap.UseVisualStyleBackColor = true;
			buttonSwitchToMap.Click += new System.EventHandler(buttonSwitchToMap_Click);
			buttonZoomIn.Location = new System.Drawing.Point(35, 821);
			buttonZoomIn.Margin = new System.Windows.Forms.Padding(4);
			buttonZoomIn.Name = "buttonZoomIn";
			buttonZoomIn.Size = new System.Drawing.Size(100, 25);
			buttonZoomIn.TabIndex = 24;
			buttonZoomIn.Text = "Zoom In";
			buttonZoomIn.UseVisualStyleBackColor = true;
			buttonZoomIn.Click += new System.EventHandler(buttonZoomIn_Click);
			buttonZoomOut.Location = new System.Drawing.Point(164, 821);
			buttonZoomOut.Margin = new System.Windows.Forms.Padding(4);
			buttonZoomOut.Name = "buttonZoomOut";
			buttonZoomOut.Size = new System.Drawing.Size(100, 25);
			buttonZoomOut.TabIndex = 25;
			buttonZoomOut.Text = "Zoom Out";
			buttonZoomOut.UseVisualStyleBackColor = true;
			buttonZoomOut.Click += new System.EventHandler(buttonZoomOut_Click);
			buttonClearCachedInfo.Location = new System.Drawing.Point(1026, 57);
			buttonClearCachedInfo.Margin = new System.Windows.Forms.Padding(4);
			buttonClearCachedInfo.Name = "buttonClearCachedInfo";
			buttonClearCachedInfo.Size = new System.Drawing.Size(208, 28);
			buttonClearCachedInfo.TabIndex = 26;
			buttonClearCachedInfo.Text = "Clear all cached info";
			buttonClearCachedInfo.UseVisualStyleBackColor = true;
			buttonClearCachedInfo.Click += new System.EventHandler(buttonClearCachedInfo_Click);
			labelDevelopedBy.AutoSize = true;
			labelDevelopedBy.Location = new System.Drawing.Point(1273, 824);
			labelDevelopedBy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelDevelopedBy.Name = "labelDevelopedBy";
			labelDevelopedBy.Size = new System.Drawing.Size(317, 17);
			labelDevelopedBy.TabIndex = 28;
			labelDevelopedBy.Text = "Developed by Kenth Jensen, kfj4100@gmail.com";
			labelMapZoomLevel.AutoSize = true;
			labelMapZoomLevel.Location = new System.Drawing.Point(277, 826);
			labelMapZoomLevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelMapZoomLevel.Name = "labelMapZoomLevel";
			labelMapZoomLevel.Size = new System.Drawing.Size(135, 17);
			labelMapZoomLevel.TabIndex = 32;
			labelMapZoomLevel.Text = "labelMapZoomLevel";
			buttonForgotPassword.Location = new System.Drawing.Point(699, 12);
			buttonForgotPassword.Margin = new System.Windows.Forms.Padding(4);
			buttonForgotPassword.Name = "buttonForgotPassword";
			buttonForgotPassword.Size = new System.Drawing.Size(128, 28);
			buttonForgotPassword.TabIndex = 37;
			buttonForgotPassword.Text = "Forgot password";
			buttonForgotPassword.UseVisualStyleBackColor = true;
			buttonForgotPassword.Click += new System.EventHandler(buttonForgotPassword_Click);
			base.AutoScaleDimensions = new System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(1616, 855);
			base.Controls.Add(buttonForgotPassword);
			base.Controls.Add(labelMapZoomLevel);
			base.Controls.Add(labelDevelopedBy);
			base.Controls.Add(buttonClearCachedInfo);
			base.Controls.Add(buttonZoomOut);
			base.Controls.Add(buttonZoomIn);
			base.Controls.Add(buttonSwitchToMap);
			base.Controls.Add(gmap);
			base.Controls.Add(buttonExport);
			base.Controls.Add(textBoxStatus);
			base.Controls.Add(buttonMissingFlights);
			base.Controls.Add(checkBoxDontRememberUser);
			base.Controls.Add(buttonDeleteFlight);
			base.Controls.Add(listView1);
			base.Controls.Add(labelStatus);
			base.Controls.Add(labelForListView);
			base.Controls.Add(buttonGetListOfFlights);
			base.Controls.Add(labelPassword);
			base.Controls.Add(textBoxPassword);
			base.Controls.Add(labelUserName);
			base.Controls.Add(textBoxUserName);
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.Margin = new System.Windows.Forms.Padding(4);
			MaximumSize = new System.Drawing.Size(1634, 3328);
			base.MinimizeBox = false;
			MinimumSize = new System.Drawing.Size(1000, 750);
			base.Name = "FormGetDataFromDroneAcademy";
			Text = "Get flight data from Drone Academy";
			base.FormClosing += new System.Windows.Forms.FormClosingEventHandler(FormGetDataFromDroneAcademy_FormClosing);
			base.Load += new System.EventHandler(Form1_Load);
			base.SizeChanged += new System.EventHandler(FormGetDataFromDroneAcademy_SizeChanged);
			contextMenuStripListView.ResumeLayout(false);
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
