using FlightDataManager.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class FormInstallGarminVirbTemplates : Form
	{
		private IContainer components;

		private Button buttonDeleteAllTemplates;

		private Button buttonOk;

		private RichTextBox richTextBox1;

		private Button buttonHelp;

		private WebBrowser webBrowser1;

		private GroupBox groupBoxMeter;

		private CheckBox checkBoxDiscoMeter;

		private CheckBox checkBoxBebopMeter;

		private CheckBox checkBoxAnafiMeter;

		private GroupBox groupBoxFeet;

		private CheckBox checkBoxDiscoFeet;

		private CheckBox checkBoxAnafiFeet;

		private CheckBox checkBoxBebopFeet;

		private Button buttonInstall;

		public FormInstallGarminVirbTemplates()
		{
			InitializeComponent();
		}

		private void FormInstallGarminVirbTemplates_Load(object sender, EventArgs e)
		{
			base.Location = new Point(55, 5);
			buttonOk.Select();
			string text = "";
			try
			{
				text = new WebClient().DownloadString("https://sites.google.com/site/pud2gpxkmlcsv/TemplateDemo.txt");
			}
			catch (Exception)
			{
			}
			if (text != "")
			{
				int num = (int)((double)webBrowser1.Width * 0.99);
				int num2 = (int)((double)webBrowser1.Height * 0.99);
				string text2 = "https://www.youtube.com/embed/" + text;
				webBrowser1.DocumentText = string.Format("<html><head><meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/></head><body marginwidth=\"0\" marginheight=\"0\" leftmargin = \"0\" topmargin = \"0\" rightmargin = \"0\" bottommargin = \"0\" >{0}</body></html>", "<iframe width=\"" + num + "px\" height=\"" + num2 + "px\" frameborder = \"0\" src=\"" + text2 + "?autoplay=1&vq=hd1080&loop=1&showinfo=0&playlist= " + text + "\" frameborder=\"0\" allowfullscreen></iframe>");
			}
		}

		private void buttonDeleteAllTemplates_Click(object sender, EventArgs e)
		{
			MessageBox.Show(this, "Make sure Garmin Virb Edit tool is completely closed down before continuing");
			if (MessageBox.Show(this, "Are you sure you want to delete all custom templates and gauges in Garmin Virb Edit\n\nPlease note that if you have made your own custom made templates these will also be deleted, unless you make a backup of these before pressing ok", "Confirmation", MessageBoxButtons.OKCancel) == DialogResult.OK)
			{
				string text = Environment.ExpandEnvironmentVariables("%AppData%\\Garmin\\VIRB Edit\\");
				if (!Directory.Exists(text))
				{
					MessageBox.Show(this, "Directory " + text + " was not found\nMake sure Garmin Virb Edit is installed correctly before trying to install these templates");
					return;
				}
				ClearAllReadOnlyAttributes(new DirectoryInfo(text + "Telemetry\\"));
				try
				{
					DeleteDirectoryImproved(text + "Telemetry\\Templates\\", recursive: true);
					DeleteDirectoryImproved(text + "Telemetry\\Widgets\\", recursive: true);
				}
				catch (Exception)
				{
					MessageBox.Show(this, "Failed to install GPS overlay templates to Garmin Virb Edit, make sure Garmin Virb is not running and try again");
				}
				MessageBox.Show(this, "Templates and gauges deleted succesfully");
			}
		}

		private void buttonInstallTemplatesFeet_Click(object sender, EventArgs e)
		{
			installTemplates("Bebop_Template_for_Garmin_Virb_Feets.zip", "Bebop_Gauges_for_Garmin_Virb_Feets.zip", "Feets");
		}

		private void buttonInstallTemplatesMeter_Click(object sender, EventArgs e)
		{
			installTemplates("Bebop_Template_for_Garmin_Virb_Meters.zip", "Bebop_Gauges_for_Garmin_Virb_Meters.zip", "Meters");
		}

		private void installTemplates(string file1, string file2, string units)
		{
			if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("Templates installed");
			}
			string text = Environment.ExpandEnvironmentVariables("%AppData%\\Garmin\\VIRB Edit\\");
			if (!Directory.Exists(text))
			{
				MessageBox.Show(this, "Directory " + text + " was not found\nMake sure Garmin Virb Edit is installed correctly before trying to install these templates");
				return;
			}
			string text2 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			if (!Directory.Exists(text2))
			{
				Directory.CreateDirectory(text2);
			}
			string text3 = Path.Combine(text2, file1);
			string text4 = Path.Combine(text2, file2);
			if (Directory.Exists(Path.Combine(text2, "Templates")))
			{
				DeleteDirectoryImproved(Path.Combine(text2, "Templates"), recursive: true);
			}
			if (Directory.Exists(Path.Combine(text2, "Widgets")))
			{
				DeleteDirectoryImproved(Path.Combine(text2, "Widgets"), recursive: true);
			}
			if (File.Exists(text3))
			{
				File.Delete(text3);
			}
			if (File.Exists(text4))
			{
				File.Delete(text4);
			}
			byte[] bytes = null;
			byte[] bytes2 = null;
			if (file1.Contains("Anafi_Meter_Template_for_Garmin_Virb"))
			{
				bytes = Resources.Anafi_Meter_Template_for_Garmin_Virb;
				bytes2 = Resources.Gauges_for_Garmin_Virb;
			}
			else if (file1.Contains("Anafi_Feet_Template_for_Garmin_Virb"))
			{
				bytes = Resources.Anafi_Feet_Template_for_Garmin_Virb;
				bytes2 = Resources.Gauges_for_Garmin_Virb;
			}
			else if (file1.Contains("Bebop_Meter_Template_for_Garmin_Virb"))
			{
				bytes = Resources.Bebop_Meter_Template_for_Garmin_Virb;
				bytes2 = Resources.Gauges_for_Garmin_Virb;
			}
			else if (file1.Contains("Bebop_Feet_Template_for_Garmin_Virb"))
			{
				bytes = Resources.Bebop_Feet_Template_for_Garmin_Virb;
				bytes2 = Resources.Gauges_for_Garmin_Virb;
			}
			else if (file1.Contains("Disco_Meter_Template_for_Garmin_Virb"))
			{
				bytes = Resources.Disco_Meter_Template_for_Garmin_Virb;
				bytes2 = Resources.Gauges_for_Garmin_Virb;
			}
			else if (file1.Contains("Disco_Feet_Template_for_Garmin_Virb"))
			{
				bytes = Resources.Disco_Feet_Template_for_Garmin_Virb;
				bytes2 = Resources.Gauges_for_Garmin_Virb;
			}
			File.WriteAllBytes(text3, bytes);
			File.WriteAllBytes(text4, bytes2);
			ZipFile.ExtractToDirectory(text3, Path.Combine(text2, "Templates"));
			ZipFile.ExtractToDirectory(text4, Path.Combine(text2, "Widgets"));
			ClearAllReadOnlyAttributes(new DirectoryInfo(text + "Telemetry\\"));
			string[] directories = Directory.GetDirectories(Path.Combine(text2, "Widgets"));
			try
			{
				for (int i = 0; i < directories.Count(); i++)
				{
					directories[i] = directories[i].Split(Path.DirectorySeparatorChar).Last();
					if (Directory.Exists(text + "Telemetry\\Widgets\\" + directories[i]))
					{
						DeleteDirectoryImproved(text + "Telemetry\\Widgets\\" + directories[i], recursive: true);
					}
				}
				CopyDirectory(Path.Combine(text2, "Templates"), text + "Telemetry\\Templates\\");
				CopyDirectory(Path.Combine(text2, "Widgets"), text + "Telemetry\\Widgets\\");
			}
			catch (Exception)
			{
				MessageBox.Show(this, "Failed to install GPS overlay templates to Garmin Virb Edit, make sure Garmin Virb is not running and try again");
			}
		}

		private void buttonOk_Click(object sender, EventArgs e)
		{
			Close();
		}

		public void ClearAllReadOnlyAttributes(DirectoryInfo parentDirectory)
		{
			if (parentDirectory != null)
			{
				parentDirectory.Attributes = FileAttributes.Normal;
				FileInfo[] files = parentDirectory.GetFiles();
				for (int i = 0; i < files.Length; i++)
				{
					files[i].Attributes = FileAttributes.Normal;
				}
				DirectoryInfo[] directories = parentDirectory.GetDirectories();
				foreach (DirectoryInfo parentDirectory2 in directories)
				{
					ClearAllReadOnlyAttributes(parentDirectory2);
				}
			}
		}

		public static void DeleteDirectoryImproved(string path, bool recursive)
		{
			string[] directories;
			if (recursive)
			{
				directories = Directory.GetDirectories(path);
				for (int i = 0; i < directories.Length; i++)
				{
					DeleteDirectoryImproved(directories[i], recursive);
				}
			}
			directories = Directory.GetFiles(path);
			foreach (string path2 in directories)
			{
				FileAttributes attributes = File.GetAttributes(path2);
				if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
				{
					File.SetAttributes(path2, attributes ^ FileAttributes.ReadOnly);
				}
				File.Delete(path2);
			}
			Directory.Delete(path);
		}

		private void CopyDirectory(string sourceDir, string targetDir)
		{
			Directory.CreateDirectory(targetDir);
			string[] files = Directory.GetFiles(sourceDir);
			foreach (string text in files)
			{
				File.Copy(text, Path.Combine(targetDir, Path.GetFileName(text)), overwrite: true);
			}
			files = Directory.GetDirectories(sourceDir);
			foreach (string text2 in files)
			{
				CopyDirectory(text2, Path.Combine(targetDir, Path.GetFileName(text2)));
			}
		}

		private void buttonHelp_Click(object sender, EventArgs e)
		{
			new FormInstallTemplatesHelp().ShowDialog();
		}

		private void buttonInstall_Click(object sender, EventArgs e)
		{
			if (!checkBoxAnafiMeter.Checked && !checkBoxBebopMeter.Checked && !checkBoxDiscoMeter.Checked && !checkBoxAnafiFeet.Checked && !checkBoxBebopFeet.Checked && !checkBoxDiscoFeet.Checked)
			{
				MessageBox.Show(this, "Please select at least one of the types of templates above");
				return;
			}
			if (checkBoxAnafiMeter.Checked)
			{
				installTemplates("Anafi_Meter_Template_for_Garmin_Virb.zip", "Gauges_for_Garmin_Virb.zip", "Meters");
			}
			if (checkBoxBebopMeter.Checked)
			{
				installTemplates("Bebop_Meter_Template_for_Garmin_Virb.zip", "Gauges_for_Garmin_Virb.zip", "Meters");
			}
			if (checkBoxDiscoMeter.Checked)
			{
				installTemplates("Disco_Meter_Template_for_Garmin_Virb.zip", "Gauges_for_Garmin_Virb.zip", "Meters");
			}
			if (checkBoxAnafiFeet.Checked)
			{
				installTemplates("Anafi_Feet_Template_for_Garmin_Virb.zip", "Gauges_for_Garmin_Virb.zip", "Feets");
			}
			if (checkBoxBebopFeet.Checked)
			{
				installTemplates("Bebop_Feet_Template_for_Garmin_Virb.zip", "Gauges_for_Garmin_Virb.zip", "Feets");
			}
			if (checkBoxDiscoFeet.Checked)
			{
				installTemplates("Disco_Feet_Template_for_Garmin_Virb.zip", "Gauges_for_Garmin_Virb.zip", "Feets");
			}
			MessageBox.Show(this, "Gauges and templates installed into Garmin Virb Edit succesfully");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.FormInstallGarminVirbTemplates));
			buttonDeleteAllTemplates = new System.Windows.Forms.Button();
			buttonOk = new System.Windows.Forms.Button();
			richTextBox1 = new System.Windows.Forms.RichTextBox();
			buttonHelp = new System.Windows.Forms.Button();
			webBrowser1 = new System.Windows.Forms.WebBrowser();
			groupBoxMeter = new System.Windows.Forms.GroupBox();
			checkBoxDiscoMeter = new System.Windows.Forms.CheckBox();
			checkBoxBebopMeter = new System.Windows.Forms.CheckBox();
			checkBoxAnafiMeter = new System.Windows.Forms.CheckBox();
			groupBoxFeet = new System.Windows.Forms.GroupBox();
			checkBoxDiscoFeet = new System.Windows.Forms.CheckBox();
			checkBoxAnafiFeet = new System.Windows.Forms.CheckBox();
			checkBoxBebopFeet = new System.Windows.Forms.CheckBox();
			buttonInstall = new System.Windows.Forms.Button();
			groupBoxMeter.SuspendLayout();
			groupBoxFeet.SuspendLayout();
			SuspendLayout();
			buttonDeleteAllTemplates.Location = new System.Drawing.Point(20, 98);
			buttonDeleteAllTemplates.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			buttonDeleteAllTemplates.Name = "buttonDeleteAllTemplates";
			buttonDeleteAllTemplates.Size = new System.Drawing.Size(243, 44);
			buttonDeleteAllTemplates.TabIndex = 0;
			buttonDeleteAllTemplates.Text = "Delete all custom templates in Garmin Virb Edit on PC";
			buttonDeleteAllTemplates.UseVisualStyleBackColor = true;
			buttonDeleteAllTemplates.Click += new System.EventHandler(buttonDeleteAllTemplates_Click);
			buttonOk.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			buttonOk.Location = new System.Drawing.Point(20, 780);
			buttonOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			buttonOk.Name = "buttonOk";
			buttonOk.Size = new System.Drawing.Size(243, 28);
			buttonOk.TabIndex = 2;
			buttonOk.Text = "OK";
			buttonOk.UseVisualStyleBackColor = true;
			buttonOk.Click += new System.EventHandler(buttonOk_Click);
			richTextBox1.BackColor = System.Drawing.SystemColors.Control;
			richTextBox1.Location = new System.Drawing.Point(20, 15);
			richTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			richTextBox1.Name = "richTextBox1";
			richTextBox1.ReadOnly = true;
			richTextBox1.Size = new System.Drawing.Size(1123, 63);
			richTextBox1.TabIndex = 11;
			richTextBox1.Text = resources.GetString("richTextBox1.Text");
			buttonHelp.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			buttonHelp.Location = new System.Drawing.Point(20, 703);
			buttonHelp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			buttonHelp.Name = "buttonHelp";
			buttonHelp.Size = new System.Drawing.Size(243, 28);
			buttonHelp.TabIndex = 13;
			buttonHelp.Text = "It does not work, Help";
			buttonHelp.UseVisualStyleBackColor = true;
			buttonHelp.Click += new System.EventHandler(buttonHelp_Click);
			webBrowser1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			webBrowser1.Location = new System.Drawing.Point(271, 98);
			webBrowser1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			webBrowser1.MinimumSize = new System.Drawing.Size(27, 25);
			webBrowser1.Name = "webBrowser1";
			webBrowser1.ScrollBarsEnabled = false;
			webBrowser1.Size = new System.Drawing.Size(1095, 718);
			webBrowser1.TabIndex = 14;
			groupBoxMeter.Controls.Add(checkBoxDiscoMeter);
			groupBoxMeter.Controls.Add(checkBoxBebopMeter);
			groupBoxMeter.Controls.Add(checkBoxAnafiMeter);
			groupBoxMeter.Location = new System.Drawing.Point(13, 174);
			groupBoxMeter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			groupBoxMeter.Name = "groupBoxMeter";
			groupBoxMeter.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
			groupBoxMeter.Size = new System.Drawing.Size(247, 103);
			groupBoxMeter.TabIndex = 18;
			groupBoxMeter.TabStop = false;
			groupBoxMeter.Text = "Virb templates in meter and km/h";
			checkBoxDiscoMeter.AutoSize = true;
			checkBoxDiscoMeter.Location = new System.Drawing.Point(16, 80);
			checkBoxDiscoMeter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			checkBoxDiscoMeter.Name = "checkBoxDiscoMeter";
			checkBoxDiscoMeter.Size = new System.Drawing.Size(214, 21);
			checkBoxDiscoMeter.TabIndex = 2;
			checkBoxDiscoMeter.Text = "Disco Virb templates in meter";
			checkBoxDiscoMeter.UseVisualStyleBackColor = true;
			checkBoxBebopMeter.AutoSize = true;
			checkBoxBebopMeter.Location = new System.Drawing.Point(16, 52);
			checkBoxBebopMeter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			checkBoxBebopMeter.Name = "checkBoxBebopMeter";
			checkBoxBebopMeter.Size = new System.Drawing.Size(220, 21);
			checkBoxBebopMeter.TabIndex = 1;
			checkBoxBebopMeter.Text = "Bebop Virb templates in meter";
			checkBoxBebopMeter.UseVisualStyleBackColor = true;
			checkBoxAnafiMeter.AutoSize = true;
			checkBoxAnafiMeter.Location = new System.Drawing.Point(16, 23);
			checkBoxAnafiMeter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			checkBoxAnafiMeter.Name = "checkBoxAnafiMeter";
			checkBoxAnafiMeter.Size = new System.Drawing.Size(211, 21);
			checkBoxAnafiMeter.TabIndex = 0;
			checkBoxAnafiMeter.Text = "Anafi Virb templates in meter";
			checkBoxAnafiMeter.UseVisualStyleBackColor = true;
			groupBoxFeet.Controls.Add(checkBoxDiscoFeet);
			groupBoxFeet.Controls.Add(checkBoxAnafiFeet);
			groupBoxFeet.Controls.Add(checkBoxBebopFeet);
			groupBoxFeet.Location = new System.Drawing.Point(13, 295);
			groupBoxFeet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			groupBoxFeet.Name = "groupBoxFeet";
			groupBoxFeet.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
			groupBoxFeet.Size = new System.Drawing.Size(247, 103);
			groupBoxFeet.TabIndex = 19;
			groupBoxFeet.TabStop = false;
			groupBoxFeet.Text = "Virb templates in feet and mph";
			checkBoxDiscoFeet.AutoSize = true;
			checkBoxDiscoFeet.Location = new System.Drawing.Point(16, 80);
			checkBoxDiscoFeet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			checkBoxDiscoFeet.Name = "checkBoxDiscoFeet";
			checkBoxDiscoFeet.Size = new System.Drawing.Size(202, 21);
			checkBoxDiscoFeet.TabIndex = 5;
			checkBoxDiscoFeet.Text = "Disco Virb templates in feet";
			checkBoxDiscoFeet.UseVisualStyleBackColor = true;
			checkBoxAnafiFeet.AutoSize = true;
			checkBoxAnafiFeet.Location = new System.Drawing.Point(16, 23);
			checkBoxAnafiFeet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			checkBoxAnafiFeet.Name = "checkBoxAnafiFeet";
			checkBoxAnafiFeet.Size = new System.Drawing.Size(199, 21);
			checkBoxAnafiFeet.TabIndex = 3;
			checkBoxAnafiFeet.Text = "Anafi Virb templates in feet";
			checkBoxAnafiFeet.UseVisualStyleBackColor = true;
			checkBoxBebopFeet.AutoSize = true;
			checkBoxBebopFeet.Location = new System.Drawing.Point(16, 52);
			checkBoxBebopFeet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			checkBoxBebopFeet.Name = "checkBoxBebopFeet";
			checkBoxBebopFeet.Size = new System.Drawing.Size(208, 21);
			checkBoxBebopFeet.TabIndex = 4;
			checkBoxBebopFeet.Text = "Bebop Virb templates in feet";
			checkBoxBebopFeet.UseVisualStyleBackColor = true;
			buttonInstall.Location = new System.Drawing.Point(20, 414);
			buttonInstall.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			buttonInstall.Name = "buttonInstall";
			buttonInstall.Size = new System.Drawing.Size(243, 54);
			buttonInstall.TabIndex = 20;
			buttonInstall.Text = "Install the templates selected above in Garmin Virb Edit";
			buttonInstall.UseVisualStyleBackColor = true;
			buttonInstall.Click += new System.EventHandler(buttonInstall_Click);
			base.AutoScaleDimensions = new System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(1382, 831);
			base.Controls.Add(groupBoxMeter);
			base.Controls.Add(groupBoxFeet);
			base.Controls.Add(buttonInstall);
			base.Controls.Add(webBrowser1);
			base.Controls.Add(buttonHelp);
			base.Controls.Add(richTextBox1);
			base.Controls.Add(buttonOk);
			base.Controls.Add(buttonDeleteAllTemplates);
			base.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			base.MaximizeBox = false;
			MaximumSize = new System.Drawing.Size(1400, 878);
			base.MinimizeBox = false;
			MinimumSize = new System.Drawing.Size(794, 679);
			base.Name = "FormInstallGarminVirbTemplates";
			Text = "Install Garmin Virb Edit Templates custom made for Parrot Anafi, Bebop and Disco";
			base.Load += new System.EventHandler(FormInstallGarminVirbTemplates_Load);
			groupBoxMeter.ResumeLayout(false);
			groupBoxMeter.PerformLayout();
			groupBoxFeet.ResumeLayout(false);
			groupBoxFeet.PerformLayout();
			ResumeLayout(false);
		}
	}
}
