using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class FormInstallTemplatesHelp : Form
	{
		private IContainer components;

		private Button buttonOK;

		private RichTextBox richTextBox1;

		public FormInstallTemplatesHelp()
		{
			InitializeComponent();
		}

		private void buttonOK_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FormInstallTemplatesHelp_Load(object sender, EventArgs e)
		{
			base.MaximizeBox = false;
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Location = new Point(110, 30);
			string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			string text2 = Environment.ExpandEnvironmentVariables("%AppData%\\Garmin\\VIRB Edit\\");
			richTextBox1.Text = "If installation of Garmin Vrib Edit templates fails, make sure you have Garmin Virb Edit correctly installed on your PC\notherwise it can be due to missing access rights in the Garmin Virb folder\n\nPlease try to install it manual by following these steps:\n\nOpen a File Explorer and go to the temporary folder: " + text + " and copy the two folders 'Templates' and 'Widgets'\n\nWith the File Explorer go to this hidden folder: " + text2 + "Telemetry\\\n\nPaste the folders you copied into this location, when asked select to merge the folders and overwrite files\n\nIf your File Explorer is not set up to show hidden folders, then follow this guide:\nhttp://www.howtogeek.com/howto/windows-vista/show-hidden-files-and-folders-in-windows-vista/";
		}

		private void richTextBox1_LinkClicked(object sender, LinkClickedEventArgs e)
		{
			Process.Start(e.LinkText);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			buttonOK = new System.Windows.Forms.Button();
			richTextBox1 = new System.Windows.Forms.RichTextBox();
			SuspendLayout();
			buttonOK.Location = new System.Drawing.Point(368, 275);
			buttonOK.Name = "buttonOK";
			buttonOK.Size = new System.Drawing.Size(75, 23);
			buttonOK.TabIndex = 0;
			buttonOK.Text = "OK";
			buttonOK.UseVisualStyleBackColor = true;
			buttonOK.Click += new System.EventHandler(buttonOK_Click);
			richTextBox1.Location = new System.Drawing.Point(12, 12);
			richTextBox1.Name = "richTextBox1";
			richTextBox1.Size = new System.Drawing.Size(787, 253);
			richTextBox1.TabIndex = 1;
			richTextBox1.Text = "";
			richTextBox1.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(richTextBox1_LinkClicked);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(811, 305);
			base.Controls.Add(richTextBox1);
			base.Controls.Add(buttonOK);
			base.MinimizeBox = false;
			base.Name = "FormInstallTemplatesHelp";
			Text = "Help for installing Garmin Virb templates";
			base.Load += new System.EventHandler(FormInstallTemplatesHelp_Load);
			ResumeLayout(false);
		}
	}
}
