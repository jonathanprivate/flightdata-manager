using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace FlightDataManager
{
	public class FormLargeGraph : Form
	{
		private List<string> checkBoxCheckedList = new List<string>
		{
			"Graph1",
			"Graph2",
			"Graph3",
			"Graph4",
			"Graph5"
		};

		private List<string> checkBoxCheckedListAlias = new List<string>
		{
			"Altitude\nmeter",
			"Speed\nm/s",
			"Battery\n%",
			"Distance\nmeter",
			"Distance\nFlown, m"
		};

		private List<double> chartValueList1 = new List<double>
		{
			7.0,
			10.0,
			15.0,
			20.0,
			125.0,
			30.0,
			25.0,
			30.0,
			7.0,
			10.0,
			15.0,
			20.0,
			125.0,
			30.0,
			25.0,
			30.0,
			7.0,
			10.0,
			15.0,
			20.0,
			125.0,
			30.0,
			25.0,
			30.0,
			7.0,
			10.0,
			15.0,
			20.0,
			125.0,
			30.0,
			25.0,
			30.0
		};

		private List<double> chartValueList2 = new List<double>
		{
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			45.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			45.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			45.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			45.0
		};

		private List<double> chartValueList3 = new List<double>
		{
			100.0,
			95.0,
			91.0,
			84.0,
			77.0,
			70.0,
			63.0,
			56.0,
			49.0,
			42.0,
			35.0,
			28.0,
			21.0,
			14.0,
			7.0,
			0.0,
			100.0,
			95.0,
			91.0,
			84.0,
			77.0,
			70.0,
			63.0,
			56.0,
			49.0,
			42.0,
			35.0,
			28.0,
			21.0,
			14.0,
			7.0,
			0.0
		};

		private List<double> chartValueList4 = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private List<double> chartValueList5 = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private Point? prevMousePosition;

		private ToolTip tooltip = new ToolTip();

		private IContainer components;

		private Button buttonClose;

		private Chart chart1;

		private Button buttonExportGraph;

		private Label label7;

		private Label label8;

		private Label label9;

		private Label label10;

		private CheckBox checkBoxGraph4;

		private CheckBox checkBoxGraph3;

		private CheckBox checkBoxGraph2;

		private CheckBox checkBoxGraph1;

		private Label label1;

		private CheckBox checkBoxGraph5;

		public FormLargeGraph()
		{
			InitializeComponent();
		}

		private void buttonClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FormLargeGraph_Load(object sender, EventArgs e)
		{
			base.MaximizeBox = false;
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Location = DataExchangerClass.startPointOfForm;
			chartValueList1 = DataExchangerClass.chartValueList1;
			chartValueList2 = DataExchangerClass.chartValueList2;
			chartValueList3 = DataExchangerClass.chartValueList3;
			chartValueList4 = DataExchangerClass.chartValueList4;
			chartValueList5 = DataExchangerClass.chartValueList5;
			UpdateGraphs();
		}

		public void UpdateGraphs()
		{
			chart1.Series.Clear();
			chart1.ChartAreas.Clear();
			chart1.ChartAreas.Add("ChartAreaNew");
			DateTime dateTime = new DateTime(0L);
			bool flag = false;
			if (checkBoxCheckedList.Contains("Graph1"))
			{
				chart1.Series.Add("Graph1");
				int num = chart1.Series.Count();
				chart1.Series[num - 1].ChartType = SeriesChartType.Line;
				chart1.Series[num - 1].BorderWidth = 3;
				foreach (double item in chartValueList1)
				{
					if (flag)
					{
						chart1.Series[num - 1].Points.AddY(item);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						chart1.Series[num - 1].Points.AddXY(dateTime, item);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph2"))
			{
				chart1.Series.Add("Graph2");
				int num2 = chart1.Series.Count();
				chart1.Series[num2 - 1].ChartType = SeriesChartType.Line;
				chart1.Series[num2 - 1].BorderWidth = 3;
				foreach (double item2 in chartValueList2)
				{
					if (flag)
					{
						chart1.Series[num2 - 1].Points.AddY(item2);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						chart1.Series[num2 - 1].Points.AddXY(dateTime, item2);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph3"))
			{
				chart1.Series.Add("Graph3");
				int num3 = chart1.Series.Count();
				chart1.Series[num3 - 1].ChartType = SeriesChartType.Line;
				chart1.Series[num3 - 1].BorderWidth = 3;
				foreach (double item3 in chartValueList3)
				{
					if (flag)
					{
						chart1.Series[num3 - 1].Points.AddY(item3);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						chart1.Series[num3 - 1].Points.AddXY(dateTime, item3);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph4"))
			{
				chart1.Series.Add("Graph4");
				int num4 = chart1.Series.Count();
				chart1.Series[num4 - 1].ChartType = SeriesChartType.Line;
				chart1.Series[num4 - 1].BorderWidth = 3;
				foreach (double item4 in chartValueList4)
				{
					if (flag)
					{
						chart1.Series[num4 - 1].Points.AddY(item4);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						chart1.Series[num4 - 1].Points.AddXY(dateTime, item4);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph5"))
			{
				chart1.Series.Add("Graph5");
				int num5 = chart1.Series.Count();
				chart1.Series[num5 - 1].ChartType = SeriesChartType.Line;
				chart1.Series[num5 - 1].BorderWidth = 3;
				foreach (double item5 in chartValueList5)
				{
					if (flag)
					{
						chart1.Series[num5 - 1].Points.AddY(item5);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						chart1.Series[num5 - 1].Points.AddXY(dateTime, item5);
					}
				}
				flag = true;
			}
			chart1.ChartAreas[0].AxisX.LabelStyle.Enabled = true;
			chart1.ChartAreas[0].AxisX.LabelStyle.Angle = 0;
			chart1.ChartAreas[0].AxisX.LabelStyle.Format = "mm:ss";
			chart1.ChartAreas[0].Position = new ElementPosition(7f, 10f, 43f, 62f);
			chart1.ChartAreas[0].InnerPlotPosition = new ElementPosition(10f, 0f, 183f, 140f);
			if (checkBoxCheckedList.Count() > 1)
			{
				CreateYAxis(chart1, chart1.ChartAreas[0], chart1.Series[1], -83f, 10f);
			}
			if (checkBoxCheckedList.Count() > 2)
			{
				CreateYAxis(chart1, chart1.ChartAreas[0], chart1.Series[2], 4f, 0f);
			}
			if (checkBoxCheckedList.Count() > 3)
			{
				CreateYAxis(chart1, chart1.ChartAreas[0], chart1.Series[3], -88f, 30f);
			}
			if (checkBoxCheckedList.Count() > 4)
			{
				CreateYAxis(chart1, chart1.ChartAreas[0], chart1.Series[4], 7f, 0f);
			}
			for (int i = 0; i < chart1.Series.Count(); i++)
			{
				chart1.Series[i].IsVisibleInLegend = false;
			}
			chart1.ChartAreas[0].AxisY.LabelStyle.Font = new Font("Microsoft Sans Serif", 10f);
			chart1.Titles.Clear();
			try
			{
				chart1.Series["Graph1"].Color = Color.Red;
			}
			catch (Exception)
			{
			}
			try
			{
				chart1.Series["Graph2"].Color = Color.Green;
			}
			catch (Exception)
			{
			}
			try
			{
				chart1.Series["Graph3"].Color = Color.Blue;
			}
			catch (Exception)
			{
			}
			try
			{
				chart1.Series["Graph4"].Color = Color.Orange;
			}
			catch (Exception)
			{
			}
			try
			{
				chart1.Series["Graph5"].Color = Color.Magenta;
			}
			catch (Exception)
			{
			}
			Title title = new Title("Bebop flight data statistics", Docking.Left, new Font("Microsoft Sans Serif", 14f, FontStyle.Bold), Color.Black);
			chart1.Titles.Add(title);
			title.Position = new ElementPosition(49f, 2f, 0f, 0f);
			title = new Title("Generated by FlightData Manager", Docking.Left, new Font("Microsoft Sans Serif", 11f, FontStyle.Regular), Color.Black);
			chart1.Titles.Add(title);
			title.Position = new ElementPosition(49f, 5f, 0f, 0f);
			title = new Title("Start date and time: " + DataExchangerClass.droneAcademyFlightStartDateTime.ToString("dd. MMM. yyyy - HH:mm:ss"), Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Regular), Color.Black);
			chart1.Titles.Add(title);
			title.Position = new ElementPosition(49f, 8f, 0f, 0f);
			Title title2 = new Title(checkBoxCheckedListAlias[0], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), chart1.Series[0].Color);
			title2.Docking = Docking.Left;
			chart1.Titles.Add(title2);
			title2.TextOrientation = TextOrientation.Rotated270;
			title2.Position = new ElementPosition(10f, 4f, 0f, 0f);
			if (checkBoxCheckedList.Count() > 1)
			{
				title2 = new Title(checkBoxCheckedListAlias[1], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), chart1.Series[1].Color);
				title2.Docking = Docking.Left;
				chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(92f, 4f, 0f, 0f);
			}
			if (checkBoxCheckedList.Count() > 2)
			{
				title2 = new Title(checkBoxCheckedListAlias[2], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), chart1.Series[2].Color);
				title2.Docking = Docking.Left;
				chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(6f, 4f, 0f, 0f);
			}
			if (checkBoxCheckedList.Count() > 3)
			{
				title2 = new Title(checkBoxCheckedListAlias[3], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), chart1.Series[3].Color);
				title2.Docking = Docking.Left;
				chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(96f, 4f, 0f, 0f);
			}
			if (checkBoxCheckedList.Count() > 4)
			{
				title2 = new Title(checkBoxCheckedListAlias[4], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), chart1.Series[4].Color);
				title2.Docking = Docking.Left;
				chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(2f, 4f, 0f, 0f);
			}
			int num6 = checkBoxCheckedList.IndexOf("Graph2");
			if (num6 >= 0)
			{
				chart1.ChartAreas[num6 * 2].AxisY.Maximum = 18.0;
				if (num6 >= 1)
				{
					chart1.ChartAreas[num6 * 2 - 1].AxisY.Maximum = 18.0;
				}
			}
			int num7 = checkBoxCheckedList.IndexOf("Graph3");
			if (num7 >= 0)
			{
				chart1.ChartAreas[num7 * 2].AxisY.Maximum = 100.0;
				if (num7 >= 1)
				{
					chart1.ChartAreas[num7 * 2 - 1].AxisY.Maximum = 100.0;
				}
			}
		}

		public void CreateYAxis(Chart chart, ChartArea area, Series series, float axisOffset, float labelsSize)
		{
			ChartArea chartArea = chart.ChartAreas.Add("ChartArea_" + series.Name);
			chartArea.BackColor = Color.Transparent;
			chartArea.BorderColor = Color.Transparent;
			chartArea.Position.FromRectangleF(area.Position.ToRectangleF());
			chartArea.InnerPlotPosition.FromRectangleF(area.InnerPlotPosition.ToRectangleF());
			chartArea.AxisX.MajorGrid.Enabled = false;
			chartArea.AxisX.MajorTickMark.Enabled = false;
			chartArea.AxisX.LabelStyle.Enabled = false;
			chartArea.AxisY.MajorGrid.Enabled = false;
			chartArea.AxisY.MajorTickMark.Enabled = false;
			chartArea.AxisY.LabelStyle.Enabled = false;
			chartArea.AxisY.IsStartedFromZero = area.AxisY.IsStartedFromZero;
			series.ChartArea = chartArea.Name;
			ChartArea chartArea2 = chart.ChartAreas.Add("AxisY_" + series.ChartArea);
			chartArea2.BackColor = Color.Transparent;
			chartArea2.BorderColor = Color.Transparent;
			chartArea2.Position.FromRectangleF(chart.ChartAreas[series.ChartArea].Position.ToRectangleF());
			chartArea2.InnerPlotPosition.FromRectangleF(chart.ChartAreas[series.ChartArea].InnerPlotPosition.ToRectangleF());
			Series series2 = chart.Series.Add(series.Name + "_Copy");
			series2.ChartType = series.ChartType;
			foreach (DataPoint point in series.Points)
			{
				series2.Points.AddXY(point.XValue, point.YValues[0]);
			}
			series2.IsVisibleInLegend = false;
			series2.Color = Color.Transparent;
			series2.BorderColor = Color.Transparent;
			series2.ChartArea = chartArea2.Name;
			chartArea2.AxisX.LineWidth = 0;
			chartArea2.AxisX.MajorGrid.Enabled = false;
			chartArea2.AxisX.MajorTickMark.Enabled = false;
			chartArea2.AxisX.LabelStyle.Enabled = false;
			chartArea2.AxisY.MajorGrid.Enabled = false;
			chartArea2.AxisY.IsStartedFromZero = area.AxisY.IsStartedFromZero;
			chartArea2.AxisY.LabelStyle.Font = area.AxisY.LabelStyle.Font;
			chartArea2.Position.X -= axisOffset;
			chartArea2.InnerPlotPosition.X += labelsSize;
		}

		private void checkBoxGraphs_MouseUp(object sender, MouseEventArgs e)
		{
			checkBoxCheckedList = new List<string>();
			checkBoxCheckedListAlias = new List<string>();
			if (checkBoxGraph1.Checked)
			{
				checkBoxCheckedList.Add("Graph1");
				checkBoxCheckedListAlias.Add("Altitude\nmeter");
			}
			if (checkBoxGraph2.Checked)
			{
				checkBoxCheckedList.Add("Graph2");
				checkBoxCheckedListAlias.Add("Speed\nm/s");
			}
			if (checkBoxGraph3.Checked)
			{
				checkBoxCheckedList.Add("Graph3");
				checkBoxCheckedListAlias.Add("Battery\n%");
			}
			if (checkBoxGraph4.Checked)
			{
				checkBoxCheckedList.Add("Graph4");
				checkBoxCheckedListAlias.Add("Distance\nmeter");
			}
			if (checkBoxGraph5.Checked)
			{
				checkBoxCheckedList.Add("Graph5");
				checkBoxCheckedListAlias.Add("Distance\nFlown, m");
			}
			if (checkBoxCheckedList.Count < 1)
			{
				if (((CheckBox)sender).Name == "checkBoxGraph1")
				{
					checkBoxGraph1.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph2")
				{
					checkBoxGraph2.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph3")
				{
					checkBoxGraph3.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph4")
				{
					checkBoxGraph4.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph5")
				{
					checkBoxGraph5.Checked = true;
				}
			}
			else
			{
				UpdateGraphs();
			}
		}

		private void chart1_MouseMove(object sender, MouseEventArgs e)
		{
			try
			{
				Point location = e.Location;
				if (!prevMousePosition.HasValue || !(location == prevMousePosition.Value))
				{
					tooltip.RemoveAll();
					prevMousePosition = location;
					HitTestResult[] array = chart1.HitTest(location.X, location.Y, true, ChartElementType.DataPoint);
					foreach (HitTestResult hitTestResult in array)
					{
						if (hitTestResult.ChartElementType == ChartElementType.DataPoint)
						{
							DataPoint dataPoint = hitTestResult.Object as DataPoint;
							if (dataPoint != null)
							{
								hitTestResult.ChartArea.AxisX.ValueToPixelPosition(dataPoint.XValue);
								hitTestResult.ChartArea.AxisY.ValueToPixelPosition(dataPoint.YValues[0]);
								int pointIndex = hitTestResult.PointIndex;
								int num = pointIndex / 60;
								string text = string.Concat(str3: (pointIndex % 60).ToString("00"), str0: "Time = ", str1: num.ToString("00"), str2: ":");
								string text2 = "-";
								string name = hitTestResult.Series.Name;
								switch (name)
								{
								case "Graph1":
									text2 = "Altitude = " + dataPoint.YValues[0].ToString("0") + " meter - " + (dataPoint.YValues[0] * 3.2808).ToString("0.0") + " feet \n - " + text;
									break;
								case "Graph2":
									text2 = "Speed = " + dataPoint.YValues[0].ToString("0.0") + " m/s - " + (dataPoint.YValues[0] * 3.6).ToString("0.0") + " km/h - " + (dataPoint.YValues[0] * 2.2369).ToString("0.0") + " mph \n - " + text;
									break;
								case "Graph3":
									text2 = "Battery = " + dataPoint.YValues[0].ToString("0") + " % \n - " + text;
									break;
								case "Graph4":
									text2 = "Distance to drone = " + dataPoint.YValues[0].ToString("0") + " meter / " + (dataPoint.YValues[0] * 3.2808).ToString("0.0") + " feet \n - " + text;
									break;
								case "Graph5":
									text2 = "Distance flown = " + dataPoint.YValues[0].ToString("0") + " meter / " + (dataPoint.YValues[0] * 3.2808).ToString("0.0") + " feet \n - " + text;
									break;
								}
								int index = checkBoxCheckedList.IndexOf(name);
								_ = checkBoxCheckedListAlias[index];
								tooltip.Show(text2, chart1, location.X, location.Y - 15);
							}
						}
					}
				}
			}
			catch (Exception)
			{
			}
		}

		private void buttonExportGraph_Click(object sender, EventArgs e)
		{
			int x = base.Location.X;
			int y = base.Location.Y;
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			if (checkBoxCheckedList.Count() > 3)
			{
				num = 47;
				num2 = 234;
				num3 = 810;
				num4 = 455;
			}
			else if (checkBoxCheckedList.Count() > 2)
			{
				num = 47;
				num2 = 234;
				num3 = 780;
				num4 = 455;
			}
			else if (checkBoxCheckedList.Count() > 1)
			{
				num = 77;
				num2 = 234;
				num3 = 750;
				num4 = 455;
			}
			else
			{
				num = 77;
				num2 = 234;
				num3 = 713;
				num4 = 455;
			}
			MessageBox.Show(this, "Make sure to select *.bmp for best quality or *.jpg for lowest file size");
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = "Bitmap Image|*.bmp|Jpeg Image|*.jpg";
			saveFileDialog.Title = "Select file to export flight data chart to - Select bmp for best quality or jpg for lowest file size";
			saveFileDialog.ShowDialog();
			if (!(saveFileDialog.FileName != ""))
			{
				return;
			}
			if (saveFileDialog.FileName.ToLower().EndsWith(".jpg") || saveFileDialog.FileName.ToLower().EndsWith(".bmp"))
			{
				Rectangle rectangle = new Rectangle(x + num, y + num2, num3, num4);
				Bitmap bitmap = new Bitmap(rectangle.Width, rectangle.Height, PixelFormat.Format64bppPArgb);
				Graphics.FromImage(bitmap).CopyFromScreen(rectangle.Left, rectangle.Top, 0, 0, bitmap.Size, CopyPixelOperation.SourceCopy);
				if (saveFileDialog.FileName.ToLower().EndsWith(".bmp"))
				{
					bitmap.Save(saveFileDialog.FileName, ImageFormat.Bmp);
				}
				else
				{
					bitmap.Save(saveFileDialog.FileName, ImageFormat.Jpeg);
				}
			}
			else
			{
				MessageBox.Show("Selected filename shall contain extension .bmp or .jpg");
			}
		}

		private void buttonExportGraph_Click_1(object sender, EventArgs e)
		{
			int x = base.Location.X;
			int y = base.Location.Y;
			x += chart1.Location.X;
			y += chart1.Location.Y;
			int num = (base.Width - base.ClientSize.Width) / 2;
			int num2 = base.Height - base.ClientSize.Height - num;
			x += num;
			y += num2;
			int width = chart1.Size.Width;
			int height = chart1.Size.Height;
			if (checkBoxCheckedList.Count() > 4)
			{
				width -= (int)((double)chart1.Size.Width * 0.01);
			}
			else if (checkBoxCheckedList.Count() > 3)
			{
				x += (int)((double)chart1.Size.Width * 0.03);
				width -= (int)((double)chart1.Size.Width * 0.04);
			}
			else if (checkBoxCheckedList.Count() > 2)
			{
				x += (int)((double)chart1.Size.Width * 0.02);
				width -= (int)((double)chart1.Size.Width * 0.08);
			}
			else if (checkBoxCheckedList.Count() > 1)
			{
				x += (int)((double)chart1.Size.Width * 0.07);
				width -= (int)((double)chart1.Size.Width * 0.06 * 2.0);
			}
			else
			{
				x += (int)((double)chart1.Size.Width * 0.07);
				width -= (int)((double)chart1.Size.Width * 0.14);
			}
			Rectangle rectangle = new Rectangle(x, y, width, height);
			Bitmap bitmap = new Bitmap(rectangle.Width, rectangle.Height, PixelFormat.Format64bppPArgb);
			Graphics.FromImage(bitmap).CopyFromScreen(rectangle.Left, rectangle.Top, 0, 0, bitmap.Size, CopyPixelOperation.SourceCopy);
			MessageBox.Show(this, "Select *.bmp for best quality or *.jpg for lowest file size");
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = "Bitmap Image|*.bmp|Jpeg Image|*.jpg";
			saveFileDialog.Title = "Select file to export flight data chart to - Select bmp for best quality or jpg for lowest file size";
			saveFileDialog.ShowDialog();
			if (!(saveFileDialog.FileName != ""))
			{
				return;
			}
			if (saveFileDialog.FileName.ToLower().EndsWith(".jpg") || saveFileDialog.FileName.ToLower().EndsWith(".bmp"))
			{
				if (saveFileDialog.FileName.ToLower().EndsWith(".bmp"))
				{
					bitmap.Save(saveFileDialog.FileName, ImageFormat.Bmp);
				}
				else
				{
					bitmap.Save(saveFileDialog.FileName, ImageFormat.Jpeg);
				}
			}
			else
			{
				MessageBox.Show("Selected filename shall contain extension .bmp or .jpg");
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.FormLargeGraph));
			buttonClose = new System.Windows.Forms.Button();
			chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			buttonExportGraph = new System.Windows.Forms.Button();
			label7 = new System.Windows.Forms.Label();
			label8 = new System.Windows.Forms.Label();
			label9 = new System.Windows.Forms.Label();
			label10 = new System.Windows.Forms.Label();
			checkBoxGraph4 = new System.Windows.Forms.CheckBox();
			checkBoxGraph3 = new System.Windows.Forms.CheckBox();
			checkBoxGraph2 = new System.Windows.Forms.CheckBox();
			checkBoxGraph1 = new System.Windows.Forms.CheckBox();
			label1 = new System.Windows.Forms.Label();
			checkBoxGraph5 = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)chart1).BeginInit();
			SuspendLayout();
			buttonClose.Location = new System.Drawing.Point(780, 667);
			buttonClose.Name = "buttonClose";
			buttonClose.Size = new System.Drawing.Size(100, 23);
			buttonClose.TabIndex = 0;
			buttonClose.Text = "Close large graph";
			buttonClose.UseVisualStyleBackColor = true;
			buttonClose.Click += new System.EventHandler(buttonClose_Click);
			chartArea.Name = "ChartArea1";
			chart1.ChartAreas.Add(chartArea);
			legend.Name = "Legend1";
			chart1.Legends.Add(legend);
			chart1.Location = new System.Drawing.Point(2, 2);
			chart1.Name = "chart1";
			chart1.Size = new System.Drawing.Size(1082, 661);
			chart1.TabIndex = 20;
			chart1.Text = "chart1";
			chart1.MouseMove += new System.Windows.Forms.MouseEventHandler(chart1_MouseMove);
			buttonExportGraph.Location = new System.Drawing.Point(681, 667);
			buttonExportGraph.Name = "buttonExportGraph";
			buttonExportGraph.Size = new System.Drawing.Size(75, 23);
			buttonExportGraph.TabIndex = 43;
			buttonExportGraph.Text = "Export graph";
			buttonExportGraph.UseVisualStyleBackColor = true;
			buttonExportGraph.Click += new System.EventHandler(buttonExportGraph_Click_1);
			label7.AutoSize = true;
			label7.BackColor = System.Drawing.Color.Orange;
			label7.Location = new System.Drawing.Point(458, 677);
			label7.MaximumSize = new System.Drawing.Size(0, 6);
			label7.MinimumSize = new System.Drawing.Size(30, 6);
			label7.Name = "label7";
			label7.Size = new System.Drawing.Size(30, 6);
			label7.TabIndex = 42;
			label7.Text = " ";
			label8.AutoSize = true;
			label8.BackColor = System.Drawing.Color.Blue;
			label8.Location = new System.Drawing.Point(334, 677);
			label8.MaximumSize = new System.Drawing.Size(0, 6);
			label8.MinimumSize = new System.Drawing.Size(30, 6);
			label8.Name = "label8";
			label8.Size = new System.Drawing.Size(30, 6);
			label8.TabIndex = 41;
			label8.Text = " ";
			label9.AutoSize = true;
			label9.BackColor = System.Drawing.Color.Green;
			label9.Location = new System.Drawing.Point(222, 677);
			label9.MaximumSize = new System.Drawing.Size(0, 6);
			label9.MinimumSize = new System.Drawing.Size(30, 6);
			label9.Name = "label9";
			label9.Size = new System.Drawing.Size(30, 6);
			label9.TabIndex = 40;
			label9.Text = " ";
			label10.AutoSize = true;
			label10.BackColor = System.Drawing.Color.Red;
			label10.Location = new System.Drawing.Point(112, 677);
			label10.MaximumSize = new System.Drawing.Size(0, 6);
			label10.MinimumSize = new System.Drawing.Size(30, 6);
			label10.Name = "label10";
			label10.Size = new System.Drawing.Size(30, 6);
			label10.TabIndex = 39;
			label10.Text = " ";
			checkBoxGraph4.AutoSize = true;
			checkBoxGraph4.Checked = true;
			checkBoxGraph4.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph4.Location = new System.Drawing.Point(392, 671);
			checkBoxGraph4.Name = "checkBoxGraph4";
			checkBoxGraph4.Size = new System.Drawing.Size(68, 17);
			checkBoxGraph4.TabIndex = 38;
			checkBoxGraph4.Text = "Distance";
			checkBoxGraph4.UseVisualStyleBackColor = true;
			checkBoxGraph4.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			checkBoxGraph3.AutoSize = true;
			checkBoxGraph3.Checked = true;
			checkBoxGraph3.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph3.Location = new System.Drawing.Point(276, 671);
			checkBoxGraph3.Name = "checkBoxGraph3";
			checkBoxGraph3.Size = new System.Drawing.Size(59, 17);
			checkBoxGraph3.TabIndex = 37;
			checkBoxGraph3.Text = "Battery";
			checkBoxGraph3.UseVisualStyleBackColor = true;
			checkBoxGraph3.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			checkBoxGraph2.AutoSize = true;
			checkBoxGraph2.Checked = true;
			checkBoxGraph2.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph2.Location = new System.Drawing.Point(168, 671);
			checkBoxGraph2.Name = "checkBoxGraph2";
			checkBoxGraph2.Size = new System.Drawing.Size(57, 17);
			checkBoxGraph2.TabIndex = 36;
			checkBoxGraph2.Text = "Speed";
			checkBoxGraph2.UseVisualStyleBackColor = true;
			checkBoxGraph2.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			checkBoxGraph1.AutoSize = true;
			checkBoxGraph1.Checked = true;
			checkBoxGraph1.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph1.Location = new System.Drawing.Point(53, 671);
			checkBoxGraph1.Name = "checkBoxGraph1";
			checkBoxGraph1.Size = new System.Drawing.Size(61, 17);
			checkBoxGraph1.TabIndex = 35;
			checkBoxGraph1.Text = "Altitude";
			checkBoxGraph1.UseVisualStyleBackColor = true;
			checkBoxGraph1.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			label1.AutoSize = true;
			label1.BackColor = System.Drawing.Color.Magenta;
			label1.Location = new System.Drawing.Point(608, 677);
			label1.MaximumSize = new System.Drawing.Size(0, 6);
			label1.MinimumSize = new System.Drawing.Size(30, 6);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(30, 6);
			label1.TabIndex = 45;
			label1.Text = " ";
			checkBoxGraph5.AutoSize = true;
			checkBoxGraph5.Checked = true;
			checkBoxGraph5.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph5.Location = new System.Drawing.Point(514, 671);
			checkBoxGraph5.Name = "checkBoxGraph5";
			checkBoxGraph5.Size = new System.Drawing.Size(96, 17);
			checkBoxGraph5.TabIndex = 44;
			checkBoxGraph5.Text = "Distance flown";
			checkBoxGraph5.UseVisualStyleBackColor = true;
			checkBoxGraph5.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(1084, 694);
			base.Controls.Add(label1);
			base.Controls.Add(checkBoxGraph5);
			base.Controls.Add(buttonExportGraph);
			base.Controls.Add(label7);
			base.Controls.Add(label8);
			base.Controls.Add(label9);
			base.Controls.Add(label10);
			base.Controls.Add(checkBoxGraph4);
			base.Controls.Add(checkBoxGraph3);
			base.Controls.Add(checkBoxGraph2);
			base.Controls.Add(checkBoxGraph1);
			base.Controls.Add(chart1);
			base.Controls.Add(buttonClose);
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.MinimizeBox = false;
			base.Name = "FormLargeGraph";
			Text = "Bebop flight data statistics, large graph";
			base.Load += new System.EventHandler(FormLargeGraph_Load);
			((System.ComponentModel.ISupportInitialize)chart1).EndInit();
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
