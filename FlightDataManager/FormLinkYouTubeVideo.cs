using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class FormLinkYouTubeVideo : Form
	{
		public static int dialogAdvancedResult = 0;

		public static string textResult = "";

		private IContainer components;

		private TextBox textBox1;

		private Button buttonCancel;

		private Button buttonOK;

		private RichTextBox richTextBoxInfo;

		private Button buttonCheckAndAutoCorrect;

		private Label labelVerificationStatus;

		private RichTextBox richTextBoxCheckLink;

		public FormLinkYouTubeVideo()
		{
			InitializeComponent();
		}

		private void FormLinkYouTubeVideo_Load(object sender, EventArgs e)
		{
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Location = new Point(220, 100);
		}

		private void richTextBox1_LinkClicked(object sender, LinkClickedEventArgs e)
		{
			Process.Start(e.LinkText);
		}

		private void buttonCheckAndAutoCorrect_Click(object sender, EventArgs e)
		{
			string text = textBox1.Text.Trim();
			if (!text.Contains("youtu.be") && !text.Contains("youtube.com"))
			{
				labelVerificationStatus.Text = "Status: Link was not recognised as a YouTube link";
				return;
			}
			if (text.StartsWith("www."))
			{
				text = text.Replace("www.", "http://www.");
			}
			text = text.Replace("https://www", "http://www");
			text = text.Replace("http://www.youtube.com/v/", "http://www.youtube.com/watch?v=");
			text = text.Replace("http://youtu.be/", "http://www.youtube.com/watch?v=");
			textBox1.Text = text;
			if (text.Length > 52)
			{
				labelVerificationStatus.Text = "Status: Link seems to be too long, check that the format is correct, length of link=" + text.Length + ", max allowed length=52";
				return;
			}
			if (!text.StartsWith("http://www.youtube.com/watch?v="))
			{
				labelVerificationStatus.Text = "Status: Link does not start with http://www.youtube.com/watch?v= as expected";
				return;
			}
			labelVerificationStatus.Text = "Status: YouTube link verified with success";
			richTextBoxCheckLink.Lines = new string[2]
			{
				"Check that the link below works by clicking it and verify that it opens correctly, before linking video to current flight",
				text
			};
			buttonOK.Enabled = true;
		}

		private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
		{
			labelVerificationStatus.Text = "Status: YouTube link not verfied yet";
			buttonOK.Enabled = false;
			richTextBoxCheckLink.Lines = new string[1]
			{
				""
			};
		}

		private void buttonOK_Click(object sender, EventArgs e)
		{
			if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("YouTube video linked to a flight");
			}
			textResult = textBox1.Text;
			dialogAdvancedResult = 1;
			Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			dialogAdvancedResult = 2;
			Close();
		}

		private void richTextBoxCheckLink_LinkClicked(object sender, LinkClickedEventArgs e)
		{
			Process.Start(e.LinkText);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.FormLinkYouTubeVideo));
			textBox1 = new System.Windows.Forms.TextBox();
			buttonCancel = new System.Windows.Forms.Button();
			buttonOK = new System.Windows.Forms.Button();
			richTextBoxInfo = new System.Windows.Forms.RichTextBox();
			buttonCheckAndAutoCorrect = new System.Windows.Forms.Button();
			labelVerificationStatus = new System.Windows.Forms.Label();
			richTextBoxCheckLink = new System.Windows.Forms.RichTextBox();
			SuspendLayout();
			textBox1.Location = new System.Drawing.Point(12, 249);
			textBox1.Name = "textBox1";
			textBox1.Size = new System.Drawing.Size(542, 20);
			textBox1.TabIndex = 12;
			textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(textBox1_KeyPress);
			buttonCancel.AutoSize = true;
			buttonCancel.Location = new System.Drawing.Point(479, 400);
			buttonCancel.Name = "buttonCancel";
			buttonCancel.Size = new System.Drawing.Size(75, 23);
			buttonCancel.TabIndex = 10;
			buttonCancel.Text = "Cancel";
			buttonCancel.UseVisualStyleBackColor = true;
			buttonCancel.Click += new System.EventHandler(buttonCancel_Click);
			buttonOK.AutoSize = true;
			buttonOK.Enabled = false;
			buttonOK.Location = new System.Drawing.Point(15, 400);
			buttonOK.Name = "buttonOK";
			buttonOK.Size = new System.Drawing.Size(186, 23);
			buttonOK.TabIndex = 9;
			buttonOK.Text = "Link YouTube video to current flight";
			buttonOK.UseVisualStyleBackColor = true;
			buttonOK.Click += new System.EventHandler(buttonOK_Click);
			richTextBoxInfo.BackColor = System.Drawing.SystemColors.Menu;
			richTextBoxInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
			richTextBoxInfo.Location = new System.Drawing.Point(12, 12);
			richTextBoxInfo.Name = "richTextBoxInfo";
			richTextBoxInfo.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			richTextBoxInfo.Size = new System.Drawing.Size(542, 236);
			richTextBoxInfo.TabIndex = 13;
			richTextBoxInfo.Text = resources.GetString("richTextBoxInfo.Text");
			richTextBoxInfo.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(richTextBox1_LinkClicked);
			buttonCheckAndAutoCorrect.Location = new System.Drawing.Point(12, 279);
			buttonCheckAndAutoCorrect.Name = "buttonCheckAndAutoCorrect";
			buttonCheckAndAutoCorrect.Size = new System.Drawing.Size(186, 23);
			buttonCheckAndAutoCorrect.TabIndex = 14;
			buttonCheckAndAutoCorrect.Text = "Check and correct link";
			buttonCheckAndAutoCorrect.UseVisualStyleBackColor = true;
			buttonCheckAndAutoCorrect.Click += new System.EventHandler(buttonCheckAndAutoCorrect_Click);
			labelVerificationStatus.AutoSize = true;
			labelVerificationStatus.Location = new System.Drawing.Point(12, 314);
			labelVerificationStatus.Name = "labelVerificationStatus";
			labelVerificationStatus.Size = new System.Drawing.Size(176, 13);
			labelVerificationStatus.TabIndex = 15;
			labelVerificationStatus.Text = "Status: YouTube link not verfied yet";
			richTextBoxCheckLink.BackColor = System.Drawing.SystemColors.Menu;
			richTextBoxCheckLink.BorderStyle = System.Windows.Forms.BorderStyle.None;
			richTextBoxCheckLink.Location = new System.Drawing.Point(12, 342);
			richTextBoxCheckLink.Name = "richTextBoxCheckLink";
			richTextBoxCheckLink.Size = new System.Drawing.Size(553, 46);
			richTextBoxCheckLink.TabIndex = 16;
			richTextBoxCheckLink.Text = "";
			richTextBoxCheckLink.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(richTextBoxCheckLink_LinkClicked);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(566, 429);
			base.Controls.Add(richTextBoxCheckLink);
			base.Controls.Add(labelVerificationStatus);
			base.Controls.Add(buttonCheckAndAutoCorrect);
			base.Controls.Add(richTextBoxInfo);
			base.Controls.Add(textBox1);
			base.Controls.Add(buttonCancel);
			base.Controls.Add(buttonOK);
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "FormLinkYouTubeVideo";
			Text = "Link YouTube Video to current flight";
			base.Load += new System.EventHandler(FormLinkYouTubeVideo_Load);
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
