using FlightDataManager.Properties;
using FolderSelect;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace FlightDataManager
{
	public class FormMain : Form
	{
		private int pixelsToMoveInLowHighResolution;

		private bool formSizeComplete;

		private int limitForHighLowResolution;

		private bool reportStatistics = true;

		private bool highScreenResolution = true;

		private string currentToolVersion = "4.1.8";

		private List<int> altitudeColorLimits = new List<int>();

		private List<string> altitudeColors = new List<string>();

		private List<PointLatLng> myMapPointsLatLng;

		private string fileName = "";

		private string filePath = "";

		private string fileNameWithoutExtension = "";

		private byte[] allPudData;

		private DateTime myDateTimeRef;

		private DateTime realDateTime;

		private DateTime startDateTime;

		private int absoluteDistanceInMeters;

		private string[] commandLineArgs = new string[1]
		{
			""
		};

		private bool dataLoadedFromDroneAcademy;

		private bool dataLoadedFromJsonFile;

		private int pointerPudData;

		private int valueTimeRawInclMilliSec;

		private int valueBatteryLevel;

		private double valueControllerGpsLongitude;

		private double valueControllerGpsLatitude;

		private string valueFlyingState;

		private string valueAlertState;

		private int valueWifiSignal;

		private int valueNrOfSvs;

		private byte valueProductGpsAvailable;

		private double valueProductGpsLongitude;

		private double valueProductGpsLatitude;

		private double valueProductGpsLongitudePrevSec;

		private double valueProductGpsLatitudePrevSec;

		private int valueProductGpsPosErr;

		private float valueSpeedVX;

		private float valueSpeedVY;

		private float valueSpeedVZ;

		private float valuePitotSpeed;

		private float valueAnglePhi;

		private float valueAngleTheta;

		private float valueAnglePsi;

		private double valueAltitudeInMeter;

		private int valueAltitudeInMilliMeter;

		private int valueFlipType;

		private double takeoffLatitude;

		private double takeoffLongitude;

		private int takeoffAltitude;

		private double landingLatitude;

		private double landingLongitude;

		private int landingAltitude;

		private List<string> csvFileLines;

		private List<string> gpxFileLines;

		private List<string> kmlFileLines;

		private List<string> kmlFileLinesController;

		private List<string> kmlFileLinesDrone3D;

		private List<string> kmlFileLinesPlacemark;

		private List<string> kmlFileLinesDrone2D;

		private int fixCounter;

		private int droneFixCounter;

		private int controllerFixCounter;

		private DateTime dateTime;

		private int timeInSecPrev;

		private int timeInSecPrevInclVideoGpsSyncOffset;

		private DateTime dateTimeConvertFrom;

		private DateTime dateTimeConvertTo;

		private string listSep = "";

		private bool ErrorInDecodingFlightList;

		private int nrOfDecimalsInLatLonString = 10;

		private List<List<int>> fitValueAccelerationXyzMilliSecTimeStamp = new List<List<int>>();

		private List<int> fitValueAccelerationXyzMilliSecTimeStampSub = new List<int>();

		private List<List<float>> fitValueAccelerationXyzYaw = new List<List<float>>();

		private List<float> fitValueAccelerationXyzYawSub = new List<float>();

		private List<List<float>> fitValueAccelerationXyzPitch = new List<List<float>>();

		private List<float> fitValueAccelerationXyzPitchSub = new List<float>();

		private List<List<float>> fitValueAccelerationXyzRoll = new List<List<float>>();

		private List<float> fitValueAccelerationXyzRollSub = new List<float>();

		private List<double> fitValueLat = new List<double>();

		private List<double> fitValueLon = new List<double>();

		private List<int> fitValueBattery = new List<int>();

		private List<int> fitValueNrOfSvs = new List<int>();

		private List<int> fitValueWifiSignal = new List<int>();

		private List<float> fitValueSpeed = new List<float>();

		private List<float> fitValueAirSpeed = new List<float>();

		private List<int> fitValueDistanceFlown = new List<int>();

		private List<int> fitValueDistanceToDrone = new List<int>();

		private List<double> fitValueAltitude = new List<double>();

		private List<double> fitValueAltitudeRelative = new List<double>();

		private int nrOfDataPrMeasurementInPudFile = 77;

		private List<string> latLonStrings = new List<string>();

		private List<double> terrainLevelAbsolute = new List<double>();

		private int AltitudeAboveSeaLevelOffset = -2000;

		private bool useOwnCalculatedSpeed;

		private System.Windows.Forms.Timer mouseNotMovedTimer;

		private CultureInfo originalCulture;

		private List<string> checkBoxCheckedList = new List<string>
		{
			"Graph01",
			"Graph2",
			"Graph3",
			"Graph4"
		};

		private List<string> checkBoxCheckedListAlias = new List<string>
		{
			"Altitude\nmeter",
			"Speed\nm/s",
			"Battery\n%",
			"Distance\nmeter",
			"Distance\nFlown, m"
		};

		private List<double> chartValueList1 = new List<double>
		{
			7.0,
			10.0,
			15.0,
			20.0,
			125.0,
			30.0,
			25.0,
			30.0,
			7.0,
			10.0,
			15.0,
			20.0,
			125.0,
			30.0,
			25.0,
			30.0
		};

		private List<double> chartValueList2 = new List<double>
		{
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			45.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			55.0,
			50.0,
			45.0
		};

		private List<double> chartValueList3 = new List<double>
		{
			100.0,
			95.0,
			91.0,
			84.0,
			77.0,
			70.0,
			63.0,
			56.0,
			49.0,
			42.0,
			35.0,
			28.0,
			21.0,
			14.0,
			7.0,
			0.0
		};

		private List<double> chartValueList4 = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private List<double> chartValueList5 = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private List<double> chartValueList6 = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private List<double> chartValueList7 = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private List<double> chartValueList8 = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private List<double> chartValueList9 = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private List<double> chartValueList10 = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private List<double> chartValueListTemp = new List<double>
		{
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0,
			95.0,
			290.0,
			125.0,
			85.0,
			87.0,
			85.0,
			87.0,
			90.0
		};

		private Point? prevMousePosition;

		private ToolTip myTooltip = new ToolTip();

		private double distanceFlownAccumulated;

		private double previousLat = 500.0;

		private double previousLon = 500.0;

		private int previousValueAltitudeInMilliMeter;

		private Button buttonLoadPudFile;

		private TextBox textBoxFileName;

		private Button buttonConvertFiles;

		private TextBox textBoxStatus;

		private DateTimePicker dateTimePickerConvertFrom;

		private DateTimePicker dateTimePickerConvertTo;

		private Label labelStartConvertingAt;

		private Label labelStopConvertingAt;

		private Button buttonInstallGarminVirbTemplates;

		private Button buttonDonate;

		private Chart Chart1;

		private Label labelColorGraph4;

		private Label labelColorGraph3;

		private Label labelColorGraph2;

		private Label labelColorGraph1;

		private CheckBox checkBoxGraph4;

		private CheckBox checkBoxGraph3;

		private CheckBox checkBoxGraph2;

		private CheckBox checkBoxGraph1;

		private Button buttonToolWebsite;

		private Button buttonGarminVirbWebsite;

		private Label labelSolidLineLeft;

		private Label labelDevelopedBy;

		private Button buttonExportGraph;

		private Label labelMailAddress;

		private Button buttonLoadDataFromDroneAcademy;

		private Button buttonExportFiles;

		private GMapControl gmap;

		private Button buttonSwitchMapsGraphs;

		private Button buttonZoomIn;

		private Button buttonZoomOut;

		private Label labelPanMapInfo;

		private Button buttonSwitchMapMode;

		private Button buttonConfigureKmlFile;

		private Label LabelSolidLineRight;

		private Label labelColorGraph5;

		private CheckBox checkBoxGraph5;

		private CheckBox checkBoxIgnoreNegativeAltitude;

		private NumericUpDown numericUpDownVideoGpsSyncOffset;

		private Label labelSyncOffset0;

		private Label labelSyncOffset1;

		private CheckBox checkBoxGraph6;

		private CheckBox checkBoxGraph7;

		private Label labelColorGraph6;

		private Label labelColorGraph7;

		private CheckBox checkBoxGraph8;

		private CheckBox checkBoxGraph9;

		private Label labelColorGraph8;

		private Label labelColorGraph9;

		private PictureBox pictureBox1;

		private TextBox textBoxStatMaxSpeed;

		private TextBox textBoxStatMaxAlt;

		private TextBox textBoxStatDistFlown;

		private TextBox textBoxStatMaxDist;

		private TextBox textBoxStatComments;

		private CheckBox checkBoxGraph10;

		private Label labelColorGraph10;

		private Label labelSyncOffset2;

		private Label labelSyncOffset3;

		public FormMain()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			base.MouseMove += Form1_MouseMove;
			mouseNotMovedTimer = new System.Windows.Forms.Timer();
			mouseNotMovedTimer.Interval = 8000;
			mouseNotMovedTimer.Tick += mouseNotMovedTimer_Tick;
			mouseNotMovedTimer.Start();
			listSep = CultureInfo.CurrentCulture.TextInfo.ListSeparator.ToString();
			DataExchangerClass.listSep = listSep;
			if (Debugger.IsAttached)
			{
				reportStatistics = false;
			}
			DataExchangerClass.regionInfo = RegionInfo.CurrentRegion;
			DataExchangerClass.currentToolVersion = currentToolVersion;
			DataExchangerClass.reportStatistics = reportStatistics;
			Text = "FlightData Manager - for Parrot Anafi, Parrot Bebop and Parrot Disco drones - Version " + currentToolVersion;
			MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;
			formSizeComplete = true;
			if (Settings.Default.WindowMainStateMaximized)
			{
				base.WindowState = FormWindowState.Maximized;
			}
			else
			{
				if (Settings.Default.WindowMainPosX < -5000 || Settings.Default.WindowMainPosX > 5000)
				{
					Settings.Default.WindowMainPosX = 10;
				}
				if (Settings.Default.WindowMainPosY < -100 || Settings.Default.WindowMainPosY > 5000)
				{
					Settings.Default.WindowMainPosY = 10;
				}
				base.Location = new Point(Settings.Default.WindowMainPosX, Settings.Default.WindowMainPosY);
				base.Width = Settings.Default.WindowMainSizeX;
				base.Height = Settings.Default.WindowMainSizeY;
			}
			commandLineArgs = Environment.GetCommandLineArgs();
			dataLoadedFromDroneAcademy = false;
			dataLoadedFromJsonFile = false;
			SetChartVisibility(visible: false);
			SetMapVisibility(visible: false);
			buttonSwitchMapsGraphs.Visible = false;
			CheckIfSameVersionAsPrevious();
			CheckIfLatestVersion();
			initMaps();
			chartValueList1 = new List<double>();
			chartValueList2 = new List<double>();
			chartValueList3 = new List<double>();
			chartValueList4 = new List<double>();
			chartValueList5 = new List<double>();
			chartValueList6 = new List<double>();
			chartValueList7 = new List<double>();
			chartValueList8 = new List<double>();
			chartValueList9 = new List<double>();
			chartValueList10 = new List<double>();
			chartValueListTemp = new List<double>();
			originalCulture = Thread.CurrentThread.CurrentCulture;
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			if (Settings.Default.IgnoreNegativeAltitude)
			{
				checkBoxIgnoreNegativeAltitude.Checked = true;
			}
			else
			{
				checkBoxIgnoreNegativeAltitude.Checked = false;
			}
			numericUpDownVideoGpsSyncOffset.Value = Settings.Default.OffsetGpsVideoSync;
			buttonConvertFiles.Enabled = false;
			buttonExportFiles.Enabled = false;
			textBoxFileName.Text = "Load flight data from My.Parrot (Parrot Cloud) or select Json file by pressing one of the buttons here --->>";
			myDateTimeRef = new DateTime(2000, 1, 1, 0, 0, 0);
			dateTimePickerConvertFrom.Enabled = false;
			dateTimePickerConvertTo.Enabled = false;
			checkBoxIgnoreNegativeAltitude.Enabled = false;
			numericUpDownVideoGpsSyncOffset.Enabled = false;
			labelSyncOffset0.Enabled = false;
			labelSyncOffset1.Enabled = false;
			labelSyncOffset2.Enabled = false;
			labelSyncOffset3.Enabled = false;
			dateTimePickerConvertFrom.Format = DateTimePickerFormat.Custom;
			dateTimePickerConvertFrom.CustomFormat = "mm:ss";
			dateTimePickerConvertFrom.ShowUpDown = true;
			dateTimePickerConvertFrom.Value = myDateTimeRef;
			dateTimePickerConvertTo.Format = DateTimePickerFormat.Custom;
			dateTimePickerConvertTo.CustomFormat = "mm:ss";
			dateTimePickerConvertTo.ShowUpDown = true;
			dateTimePickerConvertTo.Value = myDateTimeRef;
			checkBoxIgnoreNegativeAltitude.CheckedChanged += checkBoxIgnoreNegativeAltitude_CheckedChanged;
			if (reportStatistics)
			{
				GoogleTracker.trackEvent("Program started");
			}
			if (commandLineArgs.Length == 2)
			{
				buttonLoadPudFile.PerformClick();
			}
		}

		private void mouseNotMovedTimer_Tick(object sender, EventArgs e)
		{
			mouseNotMovedTimer.Enabled = false;
			base.MouseMove -= Form1_MouseMove;
			mouseNotMovedTimer.Tick -= mouseNotMovedTimer_Tick;
			base.WindowState = FormWindowState.Maximized;
		}

		private void Form1_MouseMove(object sender, MouseEventArgs e)
		{
			base.MouseMove -= Form1_MouseMove;
			mouseNotMovedTimer.Tick -= mouseNotMovedTimer_Tick;
			mouseNotMovedTimer.Enabled = false;
		}

		private void CheckIfLatestVersion()
		{
			try
			{
				new CheckVersionForm().ShowDialog();
			}
			catch
			{
			}
		}

		private void CheckIfSameVersionAsPrevious()
		{
			bool flag = false;
			string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
				flag = true;
				if (reportStatistics)
				{
					GoogleTracker.trackEvent("Tool installed or updated");
				}
			}
			string path = Path.Combine(text, "gkc3.gkc");
			if (File.Exists(path))
			{
				try
				{
					StreamReader streamReader = new StreamReader(path);
					string b = streamReader.ReadLine();
					if (currentToolVersion != b)
					{
						if (!flag)
						{
							flag = true;
							if (reportStatistics)
							{
								GoogleTracker.trackEvent("Tool installed or updated");
							}
						}
						streamReader.Close();
					}
				}
				catch
				{
					if (!flag)
					{
						flag = true;
					}
				}
			}
			else
			{
				DeleteFlightStatTxtFile();
				if (!flag)
				{
					flag = true;
					if (reportStatistics)
					{
						GoogleTracker.trackEvent("Tool installed or updated");
					}
				}
			}
			if (flag)
			{
				using (StreamWriter streamWriter = new StreamWriter(path, append: false))
				{
					streamWriter.WriteLine(currentToolVersion);
				}
			}
		}

		private void DeleteFlightStatTxtFile()
		{
			try
			{
				File.Delete(Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\"), "FlightStat.txt"));
			}
			catch
			{
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			Settings.Default.IgnoreNegativeAltitude = checkBoxIgnoreNegativeAltitude.Checked;
			Settings.Default.RadioButtonPublicOrOwnFlight = 0;
			Settings.Default.RadioButtonShowFlightWithMedia = 0;
			Settings.Default.OffsetGpsVideoSync = (int)numericUpDownVideoGpsSyncOffset.Value;
			if (base.WindowState == FormWindowState.Maximized)
			{
				Settings.Default.WindowMainStateMaximized = true;
			}
			else
			{
				Settings.Default.WindowMainStateMaximized = false;
				Settings.Default.WindowMainPosX = base.Location.X;
				Settings.Default.WindowMainPosY = base.Location.Y;
				Settings.Default.WindowMainSizeX = base.Size.Width;
				Settings.Default.WindowMainSizeY = base.Size.Height;
			}
			Settings.Default.Save();
			base.Visible = false;
			try
			{
				if (gmap != null)
				{
					gmap.Manager.CancelTileCaching();
					gmap.Dispose();
					gmap = null;
				}
			}
			catch (Exception)
			{
			}
		}

		private void buttonLoadPudFile_Click(object sender, EventArgs e)
		{
			DisableStatTextBoxes();
			textBoxStatComments.Text = "Comments can't be added when flights loaded from json files";
			buttonSwitchMapMode.Text = "Switch to Google Maps View";
			gmap.MapProvider = BingHybridMapProvider.Instance;
			buttonSwitchMapsGraphs.Text = "Switch to Maps";
			dataLoadedFromDroneAcademy = false;
			dataLoadedFromJsonFile = false;
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			SetChartVisibility(visible: false);
			SetMapVisibility(visible: false);
			buttonSwitchMapsGraphs.Visible = false;
			textBoxStatus.BackColor = Color.White;
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "Json files (*.json)|*.json";
			openFileDialog.InitialDirectory = Settings.Default.DefaultPath;
			openFileDialog.Title = "Please select a Json file from the My.Parrot Cloud to convert.";
			if (commandLineArgs.Length == 2)
			{
				fileName = commandLineArgs[1];
			}
			else
			{
				if (openFileDialog.ShowDialog() != DialogResult.OK)
				{
					buttonConvertFiles.Enabled = false;
					buttonConvertFiles.BackColor = SystemColors.Control;
					buttonExportFiles.BackColor = SystemColors.Control;
					buttonExportFiles.Enabled = false;
					textBoxFileName.Text = "Load flight data from My.Parrot cloud or select Json file by pressing one of the buttons here --->>";
					textBoxStatus.Text = "";
					dateTimePickerConvertFrom.Enabled = false;
					dateTimePickerConvertTo.Enabled = false;
					checkBoxIgnoreNegativeAltitude.Enabled = false;
					numericUpDownVideoGpsSyncOffset.Enabled = false;
					labelSyncOffset0.Enabled = false;
					labelSyncOffset1.Enabled = false;
					labelSyncOffset2.Enabled = false;
					labelSyncOffset3.Enabled = false;
					DisableStatTextBoxes();
					return;
				}
				fileName = openFileDialog.FileName;
			}
			if (fileName.EndsWith(".json"))
			{
				dataLoadedFromJsonFile = true;
				if (DataExchangerClass.reportStatistics)
				{
					GoogleTracker.trackEvent("Json file loaded");
				}
			}
			else if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("Pud file loaded");
			}
			filePath = Path.GetDirectoryName(fileName);
			fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
			Settings.Default.DefaultPath = filePath;
			Settings.Default.Save();
			textBoxFileName.Text = fileName;
			textBoxFileName.Select(textBoxFileName.Text.Length, 0);
			buttonConvertFiles.Enabled = true;
			buttonConvertFiles.BackColor = Color.LightGreen;
			int num = 0;
			if (!dataLoadedFromJsonFile)
			{
				realDateTime = ReadStartDateTimeFromPudFile(fileName);
				DataExchangerClass.droneAcademyFlightStartDateTime = realDateTime;
				DataExchangerClass.flightDataFieldNrOfSvsPresent = false;
				DataExchangerClass.flightDataFieldPitotSpeedPresent = false;
				startDateTime = realDateTime;
				TimeSpan utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);
				startDateTime += utcOffset;
				switch (ReadProductIdFromPudFile(fileName))
				{
				case "2318":
					DataExchangerClass.droneModel = "Disco";
					break;
				case "2316":
					DataExchangerClass.droneModel = "Bebop2";
					break;
				case "2305":
					DataExchangerClass.droneModel = "Bebop1";
					break;
				default:
					DataExchangerClass.droneModel = "Unknown";
					break;
				}
				if (DetectIfNrOfSatsInPudFile(fileName))
				{
					DataExchangerClass.flightDataFieldNrOfSvsPresent = true;
					nrOfDataPrMeasurementInPudFile = 79;
				}
				if (DetectIfPitotSpeedInPudFile(fileName))
				{
					DataExchangerClass.flightDataFieldPitotSpeedPresent = true;
					nrOfDataPrMeasurementInPudFile += 4;
				}
				else
				{
					DataExchangerClass.flightDataFieldPitotSpeedPresent = false;
				}
				allPudData = ReadPudFile(fileName);
				int num2 = LocateStartOfData(allPudData);
				int num3 = (allPudData.Count() - num2) / nrOfDataPrMeasurementInPudFile;
				int pointer = num2 + (num3 - 1) * nrOfDataPrMeasurementInPudFile;
				num = Convert4BytesToInt(allPudData, pointer) / 1000;
				int num4 = num / 60;
				int num5 = num % 60;
				textBoxStatus.Text = "Total length of pud file is " + num4.ToString("00") + ":" + num5.ToString("00") + " [mm:ss]   -   Press button 'Convert file' to convert flight data to Garmin Fit, Kml and Csv --->>";
			}
			else
			{
				string text = new StreamReader(fileName).ReadLine();
				string valueOfField = GetValueOfField(text, "\"product_id\":", "product_id");
				if (text.Contains("product_gps_sv_number"))
				{
					DataExchangerClass.flightDataFieldNrOfSvsPresent = true;
				}
				else
				{
					DataExchangerClass.flightDataFieldNrOfSvsPresent = false;
				}
				if (text.Contains("pitot_speed"))
				{
					DataExchangerClass.flightDataFieldPitotSpeedPresent = true;
				}
				else
				{
					DataExchangerClass.flightDataFieldPitotSpeedPresent = false;
				}
				if (valueOfField != "2305" && valueOfField != "2316" && valueOfField != "2318" && valueOfField != "2324")
				{
					MessageBox.Show(this, "Selected Json file is not from a Parrot Anafi, Bebop1, Bebop2 drone or Disco, and is not supported");
					return;
				}
				switch (valueOfField)
				{
				case "2318":
					DataExchangerClass.droneModel = "Disco";
					break;
				case "2316":
					DataExchangerClass.droneModel = "Bebop2";
					break;
				case "2305":
					DataExchangerClass.droneModel = "Bebop1";
					break;
				case "2324":
					DataExchangerClass.droneModel = "Anafi";
					break;
				}
				string valueOfField2 = GetValueOfField(text, "\"date\":", "Date");
				if (!ErrorInDecodingFlightList)
				{
					int year = Convert.ToInt16(valueOfField2.Substring(0, 4));
					int month = Convert.ToInt16(valueOfField2.Substring(5, 2));
					int day = Convert.ToInt16(valueOfField2.Substring(8, 2));
					int hour = Convert.ToInt16(valueOfField2.Substring(11, 2));
					int minute = Convert.ToInt16(valueOfField2.Substring(13, 2));
					int second = Convert.ToInt16(valueOfField2.Substring(15, 2));
					realDateTime = new DateTime(year, month, day, hour, minute, second);
					DataExchangerClass.droneAcademyFlightStartDateTime = realDateTime;
					startDateTime = realDateTime;
				}
				string valueOfField3 = GetValueOfField(text, "\"total_run_time\":", "Total run time");
				if (!ErrorInDecodingFlightList)
				{
					num = Convert.ToInt32(valueOfField3) / 1000;
					int num6 = num / 60;
					int num7 = num % 60;
					textBoxStatus.Text = "Total length of Json file is " + num6.ToString("00") + ":" + num7.ToString("00") + " [mm:ss]   -   Press button 'Convert file' to convert flight data to Garmin Fit, Kml and Csv --->>";
				}
				string[] array = text.Split('[');
				array = (DataExchangerClass.droneAcademyFlightData = new List<string>(array).GetRange(1, array.Length - 1).ToArray());
			}
			DateTime value = myDateTimeRef.AddSeconds(num);
			dateTimePickerConvertTo.Value = value;
			dateTimePickerConvertFrom.Value = myDateTimeRef;
			dateTimePickerConvertFrom.Enabled = true;
			dateTimePickerConvertTo.Enabled = true;
			checkBoxIgnoreNegativeAltitude.Enabled = true;
			numericUpDownVideoGpsSyncOffset.Enabled = true;
			labelSyncOffset0.Enabled = true;
			labelSyncOffset1.Enabled = true;
			labelSyncOffset2.Enabled = true;
			labelSyncOffset3.Enabled = true;
			labelStartConvertingAt.Enabled = true;
			labelStopConvertingAt.Enabled = true;
			buttonConvertFiles.PerformClick();
			commandLineArgs = new string[1]
			{
				""
			};
			buttonExportFiles.Enabled = true;
			EnableAndUpdateStatTextBoxes();
		}

		private void buttonConvertFiles_Click(object sender, EventArgs e)
		{
			valueNrOfSvs = 0;
			previousLat = 500.0;
			previousLon = 500.0;
			distanceFlownAccumulated = 0.0;
			useOwnCalculatedSpeed = false;
			if (reportStatistics)
			{
				GoogleTracker.trackEvent("Flight data converted");
				GoogleTracker.trackEvent("FlightData converted - " + DataExchangerClass.droneModel);
			}
			buttonSwitchMapsGraphs.Text = "Switch to Maps";
			myMapPointsLatLng = new List<PointLatLng>();
			Cursor = Cursors.WaitCursor;
			takeoffLatitude = 0.0;
			takeoffLongitude = 0.0;
			takeoffAltitude = 0;
			latLonStrings = new List<string>();
			terrainLevelAbsolute = new List<double>();
			chartValueList1 = new List<double>();
			chartValueList2 = new List<double>();
			chartValueList3 = new List<double>();
			chartValueList4 = new List<double>();
			chartValueList5 = new List<double>();
			chartValueList6 = new List<double>();
			chartValueList7 = new List<double>();
			chartValueList8 = new List<double>();
			chartValueList9 = new List<double>();
			chartValueList10 = new List<double>();
			chartValueListTemp = new List<double>();
			checkBoxGraph1.Checked = true;
			checkBoxGraph2.Checked = true;
			checkBoxGraph3.Checked = true;
			checkBoxGraph4.Checked = true;
			checkBoxGraph5.Checked = true;
			checkBoxGraph6.Checked = false;
			checkBoxGraph7.Checked = false;
			checkBoxGraph8.Checked = false;
			checkBoxGraph9.Checked = false;
			checkBoxGraph10.Checked = false;
			fitValueAccelerationXyzMilliSecTimeStamp = new List<List<int>>();
			fitValueAccelerationXyzMilliSecTimeStampSub = new List<int>();
			fitValueAccelerationXyzYaw = new List<List<float>>();
			fitValueAccelerationXyzYawSub = new List<float>();
			fitValueAccelerationXyzPitch = new List<List<float>>();
			fitValueAccelerationXyzPitchSub = new List<float>();
			fitValueAccelerationXyzRoll = new List<List<float>>();
			fitValueAccelerationXyzRollSub = new List<float>();
			fitValueLat = new List<double>();
			fitValueLon = new List<double>();
			fitValueBattery = new List<int>();
			fitValueNrOfSvs = new List<int>();
			fitValueWifiSignal = new List<int>();
			fitValueSpeed = new List<float>();
			fitValueAirSpeed = new List<float>();
			fitValueDistanceFlown = new List<int>();
			fitValueDistanceToDrone = new List<int>();
			fitValueAltitude = new List<double>();
			fitValueAltitudeRelative = new List<double>();
			dateTimeConvertFrom = dateTimePickerConvertFrom.Value;
			dateTimeConvertTo = dateTimePickerConvertTo.Value;
			if ((int)dateTimeConvertTo.Subtract(dateTimeConvertFrom).TotalSeconds < 5)
			{
				textBoxStatus.BackColor = Color.Red;
				textBoxStatus.Text = "Start or stop converting time below is set incorrectly!";
				return;
			}
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			dateTime = myDateTimeRef;
			int num = 0;
			if (!dataLoadedFromDroneAcademy && !dataLoadedFromJsonFile)
			{
				num = (pointerPudData = LocateStartOfData(allPudData));
			}
			droneFixCounter = 0;
			controllerFixCounter = 0;
			kmlFileLines = new List<string>();
			kmlFileLinesDrone3D = new List<string>();
			kmlFileLinesPlacemark = new List<string>();
			kmlFileLinesDrone2D = new List<string>();
			kmlFileLinesController = new List<string>();
			MakeKmlHeader(ref kmlFileLines);
			MakeKmlDrone2DStart(ref kmlFileLinesDrone2D);
			MakeKmlControllerStart(ref kmlFileLinesController);
			MakeKmlPlacemarkStart(ref kmlFileLinesPlacemark);
			gpxFileLines = new List<string>();
			MakeGpxHeader(ref gpxFileLines);
			csvFileLines = new List<string>();
			if (DataExchangerClass.flightDataFieldPitotSpeedPresent)
			{
				csvFileLines.Add("Time [hh:mm:ss]" + listSep + "Time [milliSec]" + listSep + "Battery level" + listSep + "Controller GPS Longitude" + listSep + "Controller GPS Latitude" + listSep + "Flying State" + listSep + "Alert State" + listSep + "WiFi Signal" + listSep + "Drone GPS Available" + listSep + "Drone GPS Longitude" + listSep + "Drone GPS Latitude" + listSep + "Drone GPS Position Error" + listSep + "Nr Of GPS satellites" + listSep + "GroundSpeed [m/s]" + listSep + "AirSpeed [m/s]" + listSep + "speed vx" + listSep + "Speed vy" + listSep + "Speed vz" + listSep + "Angle phi / Roll" + listSep + "Angle theta / Pitch" + listSep + "Angle psi / Yaw" + listSep + "Altitude in meters" + listSep + "Flip Type");
			}
			else
			{
				csvFileLines.Add("Time [hh:mm:ss]" + listSep + "Time [milliSec]" + listSep + "Battery level" + listSep + "Controller GPS Longitude" + listSep + "Controller GPS Latitude" + listSep + "Flying State" + listSep + "Alert State" + listSep + "WiFi Signal" + listSep + "Drone GPS Available" + listSep + "Drone GPS Longitude" + listSep + "Drone GPS Latitude" + listSep + "Drone GPS Position Error" + listSep + "Nr Of GPS satellites" + listSep + "GroundSpeed [m/s]" + listSep + "speed vx" + listSep + "Speed vy" + listSep + "Speed vz" + listSep + "Angle phi / Roll" + listSep + "Angle theta / Pitch" + listSep + "Angle psi / Yaw" + listSep + "Altitude in meters" + listSep + "Flip Type");
			}
			timeInSecPrev = -1;
			timeInSecPrevInclVideoGpsSyncOffset = -1;
			fixCounter = 0;
			ReadAltitudeLimitColors();
			if (dataLoadedFromDroneAcademy || dataLoadedFromJsonFile)
			{
				if (dataLoadedFromJsonFile)
				{
					FormGetDataFromDroneAcademy.FindIndexOfDataFields(DataExchangerClass.droneAcademyFlightData);
				}
				for (int i = 0; i < DataExchangerClass.droneAcademyFlightData.Length; i++)
				{
					string[] array = DataExchangerClass.droneAcademyFlightData[i].Split(',');
					if (array.Length <= DataExchangerClass.flightDataFieldIndexTime)
					{
						continue;
					}
					if (array.Count() >= 19 && int.TryParse(array[DataExchangerClass.flightDataFieldIndexTime], out valueTimeRawInclMilliSec))
					{
						valueBatteryLevel = Convert.ToInt32(array[DataExchangerClass.flightDataFieldIndexBattery]);
						valueControllerGpsLongitude = Convert.ToDouble(array[DataExchangerClass.flightDataFieldIndexControllerGpsLon]);
						valueControllerGpsLatitude = Convert.ToDouble(array[DataExchangerClass.flightDataFieldIndexControllerGpsLat]);
						valueFlyingState = array[DataExchangerClass.flightDataFieldIndexFlyingState];
						valueAlertState = array[DataExchangerClass.flightDataFieldIndexAlertState];
						valueWifiSignal = Convert.ToInt32(array[DataExchangerClass.flightDataFieldIndexWifiSignal]);
						valueProductGpsAvailable = (byte)((array[DataExchangerClass.flightDataFieldIndexProductGpsAvailable].ToLower().Trim() == "true") ? 1 : 0);
						valueProductGpsLongitude = Convert.ToDouble(array[DataExchangerClass.flightDataFieldIndexProductGpsLon]);
						valueProductGpsLatitude = Convert.ToDouble(array[DataExchangerClass.flightDataFieldIndexProductGpsLat]);
						valueProductGpsPosErr = Convert.ToInt32(array[DataExchangerClass.flightDataFieldIndexProductGpsPosError]);
						valueNrOfSvs = 0;
						if (DataExchangerClass.flightDataFieldNrOfSvsPresent)
						{
							valueNrOfSvs = Convert.ToInt32(array[DataExchangerClass.flightDataFieldIndexProductGpsSvNr]);
						}
						valueSpeedVX = Convert.ToSingle(array[DataExchangerClass.flightDataFieldIndexSpeedVx]);
						valueSpeedVY = Convert.ToSingle(array[DataExchangerClass.flightDataFieldIndexSpeedVy]);
						valueSpeedVZ = Convert.ToSingle(array[DataExchangerClass.flightDataFieldIndexSpeedVz]);
						if (DataExchangerClass.flightDataFieldPitotSpeedPresent)
						{
							valuePitotSpeed = Convert.ToSingle(array[DataExchangerClass.flightDataFieldIndexPitotSpeed]);
						}
						valueAnglePhi = Convert.ToSingle(array[DataExchangerClass.flightDataFieldIndexAnglePhi]);
						valueAngleTheta = Convert.ToSingle(array[DataExchangerClass.flightDataFieldIndexAngleTheta]);
						valueAnglePsi = Convert.ToSingle(array[DataExchangerClass.flightDataFieldIndexAnglePsi]);
						if (array[DataExchangerClass.flightDataFieldIndexAltitude].Contains("."))
						{
							valueAltitudeInMeter = Convert.ToDouble(array[DataExchangerClass.flightDataFieldIndexAltitude]);
							valueAltitudeInMilliMeter = (int)(Convert.ToDouble(array[DataExchangerClass.flightDataFieldIndexAltitude]) * 1000.0);
						}
						else
						{
							valueAltitudeInMeter = Convert.ToDouble(array[DataExchangerClass.flightDataFieldIndexAltitude]) / 1000.0;
							valueAltitudeInMilliMeter = Convert.ToInt32(array[DataExchangerClass.flightDataFieldIndexAltitude]);
						}
						valueFlipType = Convert.ToInt32(array[DataExchangerClass.flightDataFieldIndexFlipType]);
						AddMeasurementToFlightData();
					}
				}
			}
			else
			{
				for (int j = 0; j * nrOfDataPrMeasurementInPudFile < allPudData.Count() - (num + 100); j++)
				{
					byte b = 0;
					valueTimeRawInclMilliSec = Convert4BytesToInt(allPudData, pointerPudData);
					valueBatteryLevel = Convert4BytesToInt(allPudData, pointerPudData + 4);
					valueControllerGpsLongitude = BitConverter.ToDouble(allPudData, pointerPudData + 8);
					valueControllerGpsLatitude = BitConverter.ToDouble(allPudData, pointerPudData + 16);
					valueFlyingState = allPudData[pointerPudData + 24].ToString();
					valueAlertState = allPudData[pointerPudData + 25].ToString();
					if (DataExchangerClass.flightDataFieldNrOfSvsPresent)
					{
						byte[] value = new byte[2]
						{
							allPudData[pointerPudData + 26],
							allPudData[pointerPudData + 27]
						};
						valueWifiSignal = BitConverter.ToInt16(value, 0);
						b = (byte)(b + 1);
					}
					else
					{
						valueWifiSignal = allPudData[pointerPudData + 26];
					}
					valueProductGpsAvailable = allPudData[pointerPudData + 27 + b];
					valueProductGpsLongitude = BitConverter.ToDouble(allPudData, pointerPudData + 28 + b);
					valueProductGpsLatitude = BitConverter.ToDouble(allPudData, pointerPudData + 36 + b);
					valueProductGpsPosErr = Convert4BytesToInt(allPudData, pointerPudData + 44 + b);
					if (DataExchangerClass.flightDataFieldNrOfSvsPresent)
					{
						valueNrOfSvs = allPudData[pointerPudData + 48 + b];
						b = (byte)(b + 1);
					}
					valueSpeedVX = BitConverter.ToSingle(allPudData, pointerPudData + 48 + b);
					valueSpeedVY = BitConverter.ToSingle(allPudData, pointerPudData + 52 + b);
					valueSpeedVZ = BitConverter.ToSingle(allPudData, pointerPudData + 56 + b);
					if (DataExchangerClass.flightDataFieldPitotSpeedPresent)
					{
						valuePitotSpeed = BitConverter.ToSingle(allPudData, pointerPudData + 60 + b);
						b = (byte)(b + 4);
					}
					valueAnglePhi = BitConverter.ToSingle(allPudData, pointerPudData + 60 + b);
					valueAngleTheta = BitConverter.ToSingle(allPudData, pointerPudData + 64 + b);
					valueAnglePsi = BitConverter.ToSingle(allPudData, pointerPudData + 68 + b);
					valueAltitudeInMeter = Convert4BytesToInt(allPudData, pointerPudData + 72 + b) / 1000;
					valueAltitudeInMilliMeter = Convert4BytesToInt(allPudData, pointerPudData + 72 + b);
					valueFlipType = allPudData[pointerPudData + 76 + b];
					AddMeasurementToFlightData();
				}
			}
			if (DataExchangerClass.droneModel == "Disco")
			{
				checkBoxGraph10.Enabled = true;
			}
			else
			{
				checkBoxGraph10.Enabled = false;
			}
			GMapOverlay gMapOverlay = new GMapOverlay("routes");
			GMapRoute gMapRoute = new GMapRoute(myMapPointsLatLng, "myName");
			gMapRoute.Stroke.Color = Color.Red;
			gMapRoute.Stroke.Width = 4f;
			gMapOverlay.Routes.Add(gMapRoute);
			gmap.Overlays.Clear();
			gmap.Overlays.Add(gMapOverlay);
			if (myMapPointsLatLng.Count > 2)
			{
				GMapOverlay gMapOverlay2 = new GMapOverlay("Markers");
				GMapMarker_StartEnd item = new GMapMarker_StartEnd(myMapPointsLatLng[0], "", "S", new SolidBrush(Color.Green));
				gMapOverlay2.Markers.Add(item);
				item = new GMapMarker_StartEnd(myMapPointsLatLng[myMapPointsLatLng.Count - 1], "", "E", new SolidBrush(Color.Blue));
				gMapOverlay2.Markers.Add(item);
				gmap.Overlays.Add(gMapOverlay2);
			}
			gmap.ZoomAndCenterRoute(gMapRoute);
			gmap.DragButton = MouseButtons.Left;
			ChangeAltitudeColorOnKmlLines3D(ref kmlFileLinesDrone3D);
			MakeKmlControllerEnd(ref kmlFileLinesController);
			MakeKmlDrone2DEnd(ref kmlFileLinesDrone2D);
			MakeKmlDrone3DEnd(ref kmlFileLinesDrone3D);
			MakeKmlPlacemarksEnd(ref kmlFileLinesPlacemark);
			kmlFileLines.AddRange(kmlFileLinesPlacemark);
			kmlFileLines.AddRange(kmlFileLinesDrone2D);
			kmlFileLines.AddRange(kmlFileLinesDrone3D);
			MakeKmlFooter(ref kmlFileLines);
			MakeGpxFooter(ref gpxFileLines);
			if (droneFixCounter > 0)
			{
				double num2 = (double)droneFixCounter / (double)fixCounter * 100.0;
				if (controllerFixCounter > 0)
				{
					double num3 = (double)controllerFixCounter / (double)fixCounter * 100.0;
					textBoxStatus.Text = "File converted succesfully, Drone got GPS fix " + num2.ToString("0.0") + "% of the time, Controller got GPS fix " + num3.ToString("0.0") + "% of the time";
					textBoxStatus.BackColor = Color.LightGreen;
				}
				else
				{
					textBoxStatus.Text = "File converted succesfully, but no GPS positions found for SkyController, Drone got GPS fix " + num2.ToString("0.0") + "% of the time";
					textBoxStatus.BackColor = Color.Yellow;
				}
			}
			else
			{
				textBoxStatus.Text = "No valid GPS positions found for drone, check that GPS lock is established before takeoff";
				textBoxStatus.BackColor = Color.Red;
			}
			SetChartVisibility(visible: true);
			buttonSwitchMapsGraphs.Visible = true;
			checkBoxCheckedList = new List<string>
			{
				"Graph01",
				"Graph2",
				"Graph3",
				"Graph4",
				"Graph5"
			};
			checkBoxCheckedListAlias = new List<string>
			{
				"Altitude\nmeter",
				"Speed\nm/s",
				"Battery\n%",
				"Distance\nmeter",
				"Distance\nFlown, m"
			};
			if (DataExchangerClass.droneModel == "Disco" && !DataExchangerClass.flightDataFieldPitotSpeedPresent)
			{
				chartValueListTemp = chartValueList2;
				chartValueList2 = chartValueList10;
				chartValueList10 = chartValueListTemp;
				fitValueSpeed = new List<float>();
				foreach (double item2 in chartValueList2)
				{
					fitValueSpeed.Add((float)item2);
				}
			}
			else if (!DataExchangerClass.flightDataFieldPitotSpeedPresent)
			{
				chartValueList10 = chartValueList2;
			}
			foreach (double item3 in chartValueList10)
			{
				fitValueAirSpeed.Add((float)item3);
			}
			UpdateGraphs();
			Cursor = Cursors.Default;
			buttonExportFiles.Enabled = true;
			buttonConvertFiles.BackColor = SystemColors.Control;
			buttonExportFiles.BackColor = Color.LightGreen;
			AltitudeAboveSeaLevelArray();
		}

		private void addAltitudeExtraInfoInKmlFile()
		{
			if (chartValueList8.Count() <= 2)
			{
				return;
			}
			int num = 2;
			for (int i = 0; i < kmlFileLines.Count(); i++)
			{
				if (kmlFileLines[i].Contains("Altitude =") && kmlFileLines[i + 1].Contains("Speed ") && num < chartValueList8.Count())
				{
					string text = kmlFileLines[i - 2].Trim();
					if (!text.Contains("<name>"))
					{
						break;
					}
					int num2 = 0;
					int num3 = 0;
					if (text.Length < 19)
					{
						num2 = Convert.ToInt32(text.Substring(6, 2));
						num3 = Convert.ToInt32(text.Substring(9, 2));
					}
					else
					{
						num2 = Convert.ToInt32(text.Substring(6, 3));
						num3 = Convert.ToInt32(text.Substring(10, 2));
					}
					num3 = num2 * 60 + num3 - 1;
					if (num3 < chartValueList8.Count())
					{
						kmlFileLines.Insert(i + 1, "  Altitude, relative to terrain level = " + chartValueList8[num3].ToString("0.0") + " meter - " + (chartValueList8[num3] * 3.2808).ToString("0.0") + " feet <BR><BR>");
						kmlFileLines.Insert(i + 2, "  Relative terrain level = " + chartValueList9[num3].ToString("0.0") + " meter - " + (chartValueList9[num3] * 3.2808).ToString("0.0") + " feet<BR><BR>");
						kmlFileLines.Insert(i + 3, "  Terrain level over sea level= " + (chartValueList9[num3] + (double)AltitudeAboveSeaLevelOffset).ToString("0.0") + " meter - " + ((chartValueList9[num3] + (double)AltitudeAboveSeaLevelOffset) * 3.2808).ToString("0.0") + " feet<BR><BR>");
						num++;
					}
				}
			}
		}

		private void AltitudeAboveSeaLevelArray()
		{
			List<string> list = new List<string>();
			List<string> list2 = new List<string>();
			List<double> list3 = new List<double>();
			for (int i = 0; i < latLonStrings.Count(); i++)
			{
				bool flag = false;
				for (int j = 0; j < list.Count(); j++)
				{
					if (latLonStrings[i] == list[j])
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					list.Add(latLonStrings[i]);
				}
			}
			RestRequest request = new RestRequest(Method.GET);
			for (int k = 1; k < 120; k++)
			{
				if (list.Count <= 0)
				{
					break;
				}
				Thread.Sleep(50);
				string text = "http://elevation-api.io/api/elevation?points=(";
				int num = 0;
				while (text.Length < 1700 && list.Count() > num && num < 48)
				{
					text = text + list[num] + "),(";
					num++;
				}
				text = text.Substring(0, text.Length - 2);
				string[] array = new RestClient(text)
				{
					Timeout = 5000
				}.Execute(request).Content.Split(new string[1]
				{
					"}"
				}, StringSplitOptions.None);
				for (int l = 0; l < array.Count() && array[l].IndexOf("elevation\"") >= 1; l++)
				{
					string value = array[l].Substring(array[l].IndexOf("elevation\"") + 11).Trim();
					string text2 = array[l].Substring(array[l].IndexOf("lat\"") + 5).Trim();
					text2 = text2.Substring(0, text2.IndexOf(","));
					text2 = Convert.ToDouble(text2).ToString();
					string text3 = array[l].Substring(array[l].IndexOf("lon\"") + 5).Trim();
					text3 = text3.Substring(0, text3.IndexOf(","));
					text3 = Convert.ToDouble(text3).ToString();
					string text4 = text2 + "," + text3;
					for (int m = 0; m < list.Count; m++)
					{
						if (list[m] == text4)
						{
							list.RemoveAt(m);
							list2.Add(text4);
							list3.Add(Convert.ToDouble(value));
							break;
						}
					}
				}
			}
			request = null;
			for (int n = 0; n < latLonStrings.Count(); n++)
			{
				for (int num2 = 0; num2 < list2.Count(); num2++)
				{
					if (latLonStrings[n] == list2[num2])
					{
						terrainLevelAbsolute.Add(list3[num2]);
						break;
					}
				}
			}
			using (StreamWriter streamWriter = new StreamWriter(Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\"), "RelativeAltitudes.txt"), append: false))
			{
				for (int num3 = 0; num3 < list2.Count() && num3 < list3.Count(); num3++)
				{
					streamWriter.WriteLine(list2[num3] + "," + list3[num3].ToString("0.00") + "," + (list3[num3] - terrainLevelAbsolute[0]).ToString("0.00"));
				}
			}
			if (latLonStrings.Count() == terrainLevelAbsolute.Count())
			{
				double num4 = 0.0;
				for (int num5 = 0; num5 < terrainLevelAbsolute.Count(); num5++)
				{
					num4 = terrainLevelAbsolute[num5] - terrainLevelAbsolute[0];
					chartValueList8.Add(chartValueList1[num5] - num4);
					chartValueList9.Add(num4);
				}
				checkBoxGraph8.Enabled = true;
				checkBoxGraph9.Enabled = true;
			}
			else
			{
				checkBoxGraph8.Enabled = false;
				checkBoxGraph9.Enabled = false;
			}
		}

		private int AltitudeAboveSeaLevel(double lat, double lon)
		{
			int result = -1000;
			RestClient restClient = new RestClient("http://elevation-api.io/api/elevation?points=(" + lat + "," + lon + ")");
			RestRequest request = new RestRequest(Method.GET);
			restClient.Timeout = 5000;
			try
			{
				string content = restClient.Execute(request).Content;
				string text = content.Substring(content.IndexOf("elevation\"") + 11);
				result = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(text.Substring(0, text.IndexOf("}")).Trim())));
				restClient = null;
				request = null;
				return result;
			}
			catch (Exception)
			{
				return result;
			}
		}

		private void ReadAltitudeLimitColors()
		{
			altitudeColorLimits = new List<int>();
			altitudeColors = new List<string>();
			string text = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\"), "AltitudeColorsAndLimitsForKmlFile.txt");
			if (File.Exists(text))
			{
				string text2 = "";
				StreamReader streamReader = new StreamReader(text);
				while ((text2 = streamReader.ReadLine()) != null)
				{
					try
					{
						string[] array = text2.Split(';');
						if (array.Count() >= 2)
						{
							altitudeColorLimits.Add(Convert.ToInt32(array[0]));
							altitudeColors.Add(array[1]);
							if (altitudeColors[altitudeColors.Count - 1].Length != 8)
							{
								MessageBox.Show(this, "Error in file: " + text + "\n\nColor does not contain 8 characters");
								break;
							}
						}
					}
					catch (Exception ex)
					{
						MessageBox.Show(this, "Error in file: " + text + "\n\n" + ex.Message);
						break;
					}
				}
				streamReader.Close();
				if (altitudeColors.Count < 2)
				{
					MessageBox.Show(this, "Too few lines in file: " + text);
				}
			}
			else
			{
				altitudeColorLimits = new List<int>
				{
					20,
					40,
					60,
					90,
					120,
					150
				};
				altitudeColors = new List<string>
				{
					"ff00ff00",
					"ffffff00",
					"ffff0000",
					"ff00ffff",
					"ff8080ff",
					"ff0000ff"
				};
			}
		}

		private void ChangeAltitudeColorOnKmlLines3D(ref List<string> kmlFileLines)
		{
			int num = 0;
			kmlFileLines.Insert(0, "");
			kmlFileLines.Insert(1, "");
			kmlFileLines.Insert(2, "<Folder><name>Drone 3D</name>");
			kmlFileLines.Insert(3, "<Placemark>");
			kmlFileLines.Insert(4, "   <Style id=\"default\">");
			kmlFileLines.Insert(5, "       <LineStyle>");
			kmlFileLines.Insert(6, "           <color>" + altitudeColors[0] + "</color>");
			kmlFileLines.Insert(7, "           <width>3</width>");
			kmlFileLines.Insert(8, "       </LineStyle>");
			kmlFileLines.Insert(9, "   </Style>");
			kmlFileLines.Insert(10, "   <LineString>");
			kmlFileLines.Insert(11, "       <tessellate>1</tessellate>");
			if (AltitudeAboveSeaLevelOffset < -900)
			{
				kmlFileLines.Insert(12, "       <altitudeMode>relativeToGround</altitudeMode>");
			}
			else
			{
				kmlFileLines.Insert(12, "       <altitudeMode>absolute</altitudeMode>");
			}
			kmlFileLines.Insert(13, "       <coordinates>");
			int num2 = 0;
			for (int i = 0; i < kmlFileLines.Count(); i++)
			{
				string text = kmlFileLines[i];
				if (text.Split(',').Count() != 3)
				{
					continue;
				}
				int num3 = 0;
				try
				{
					num3 = ((AltitudeAboveSeaLevelOffset >= -900) ? (Convert.ToInt32(text.Split(',')[2]) - AltitudeAboveSeaLevelOffset) : Convert.ToInt32(text.Split(',')[2]));
				}
				catch
				{
					continue;
				}
				if (num < 1)
				{
					num++;
					continue;
				}
				if (num2 + 1 < altitudeColorLimits.Count() && num3 > altitudeColorLimits[num2])
				{
					num2++;
					num = 0;
				}
				else
				{
					if (num2 <= 0 || num3 >= altitudeColorLimits[num2 - 1])
					{
						num++;
						continue;
					}
					num2--;
					num = 0;
				}
				List<string> list = new List<string>
				{
					"</coordinates>",
					"</LineString>",
					"</Placemark>",
					"<Placemark>",
					"<Style>",
					"<LineStyle>"
				};
				list.Add("<color>" + altitudeColors[num2] + "</color>");
				list.Add("<width>3</width>");
				list.Add("</LineStyle>");
				list.Add("</Style>");
				list.Add("<LineString>");
				list.Add("<tessellate>1</tessellate>");
				if (AltitudeAboveSeaLevelOffset < -900)
				{
					list.Add("       <altitudeMode>relativeToGround</altitudeMode>");
				}
				else
				{
					list.Add("       <altitudeMode>absolute</altitudeMode>");
				}
				list.Add("<coordinates>");
				string item = kmlFileLines[i - 1];
				kmlFileLines.InsertRange(i, list);
				kmlFileLines.Insert(i + list.Count, item);
				i += list.Count;
			}
		}

		public void ReplaceFlyingAndAlertState()
		{
			valueFlyingState = valueFlyingState.Replace("0", "0 - Landed state");
			valueFlyingState = valueFlyingState.Replace("1", "1 - Taking off state");
			if (DataExchangerClass.droneModel == "Disco")
			{
				valueFlyingState = valueFlyingState.Replace("2", "2 - Circling state");
			}
			else
			{
				valueFlyingState = valueFlyingState.Replace("2", "2 - Hovering state");
			}
			valueFlyingState = valueFlyingState.Replace("3", "3 - Flying state");
			valueFlyingState = valueFlyingState.Replace("4", "4 - Landing state");
			valueFlyingState = valueFlyingState.Replace("5", "5 - Emergency state");
			valueFlyingState = valueFlyingState.Replace("6", "6 - User take off state");
			valueFlyingState = valueFlyingState.Replace("7", "7 - Motor ramping state");
			valueAlertState = valueAlertState.Replace("0", "0 - No Alert");
			valueAlertState = valueAlertState.Replace("1", "1 - User emergency");
			valueAlertState = valueAlertState.Replace("2", "2 - Cut out Alert");
			valueAlertState = valueAlertState.Replace("3", "3 - Critical battery");
			valueAlertState = valueAlertState.Replace("4", "4 - Low battery");
			valueAlertState = valueAlertState.Replace("5", "5 - Angle too high");
		}

		public void AddMeasurementToFlightData()
		{
			ReplaceFlyingAndAlertState();
			pointerPudData += nrOfDataPrMeasurementInPudFile;
			float a = PythagorasSingle(Math.Abs(valueSpeedVX), Math.Abs(valueSpeedVY));
			float num = PythagorasSingle(a, Math.Abs(valueSpeedVZ));
			double num2 = (double)(-Rad2DegreesAsInInt32(valueAngleTheta)) / 57.3;
			double num3 = (double)Rad2DegreesAsInInt32(valueAnglePhi) / 57.3 - 1.570681;
			Math.Cos(num3);
			Math.Cos(num2);
			Math.Sin(num3);
			Math.Cos(num2);
			Math.Sin(num2);
			int num4 = valueTimeRawInclMilliSec % 1000;
			int num5 = valueTimeRawInclMilliSec / 1000;
			int num6 = num5 / 60;
			int num7 = num5 % 60;
			if (DataExchangerClass.flightDataFieldPitotSpeedPresent)
			{
				csvFileLines.Add("00:" + num6.ToString("00") + ":" + num7.ToString("00") + listSep + num4 + listSep + valueBatteryLevel + listSep + valueControllerGpsLongitude.ToString(originalCulture) + listSep + valueControllerGpsLatitude.ToString(originalCulture) + listSep + valueFlyingState + listSep + valueAlertState + listSep + valueWifiSignal + listSep + valueProductGpsAvailable + listSep + valueProductGpsLongitude.ToString(originalCulture) + listSep + valueProductGpsLatitude.ToString(originalCulture) + listSep + valueProductGpsPosErr + listSep + valueNrOfSvs + listSep + num.ToString(originalCulture) + listSep + valuePitotSpeed.ToString(originalCulture) + listSep + valueSpeedVX.ToString(originalCulture) + listSep + valueSpeedVY.ToString(originalCulture) + listSep + valueSpeedVZ.ToString(originalCulture) + listSep + valueAnglePhi.ToString(originalCulture) + listSep + valueAngleTheta.ToString(originalCulture) + listSep + valueAnglePsi.ToString(originalCulture) + listSep + valueAltitudeInMeter.ToString(originalCulture) + listSep + valueFlipType);
			}
			else
			{
				csvFileLines.Add("00:" + num6.ToString("00") + ":" + num7.ToString("00") + listSep + num4 + listSep + valueBatteryLevel + listSep + valueControllerGpsLongitude.ToString(originalCulture) + listSep + valueControllerGpsLatitude.ToString(originalCulture) + listSep + valueFlyingState + listSep + valueAlertState + listSep + valueWifiSignal + listSep + valueProductGpsAvailable + listSep + valueProductGpsLongitude.ToString(originalCulture) + listSep + valueProductGpsLatitude.ToString(originalCulture) + listSep + valueProductGpsPosErr + listSep + valueNrOfSvs + listSep + num.ToString(originalCulture) + listSep + valueSpeedVX.ToString(originalCulture) + listSep + valueSpeedVY.ToString(originalCulture) + listSep + valueSpeedVZ.ToString(originalCulture) + listSep + valueAnglePhi.ToString(originalCulture) + listSep + valueAngleTheta.ToString(originalCulture) + listSep + valueAnglePsi.ToString(originalCulture) + listSep + valueAltitudeInMeter.ToString(originalCulture) + listSep + valueFlipType);
			}
			if (valueAltitudeInMeter < 0.0 && checkBoxIgnoreNegativeAltitude.Checked)
			{
				valueAltitudeInMeter = 0.0;
			}
			if (valueAltitudeInMilliMeter < 0 && checkBoxIgnoreNegativeAltitude.Checked)
			{
				valueAltitudeInMilliMeter = 0;
			}
			float num8 = 0f;
			num8 = ((!(valueAnglePsi >= 0f)) ? ((float)Math.PI * 2f + valueAnglePsi) : valueAnglePsi);
			int num9 = valueTimeRawInclMilliSec + (int)numericUpDownVideoGpsSyncOffset.Value;
			if (valueProductGpsLatitude > 200.0 || valueProductGpsLongitude > 200.0)
			{
				return;
			}
			if ((num9 / 1000 > timeInSecPrevInclVideoGpsSyncOffset && valueProductGpsLongitudePrevSec != valueProductGpsLongitude) || (num9 / 1000 > timeInSecPrevInclVideoGpsSyncOffset && valueProductGpsLatitudePrevSec != valueProductGpsLatitude) || num9 / 1000 > timeInSecPrevInclVideoGpsSyncOffset + 1)
			{
				dateTime = dateTime.AddSeconds(1.0);
				realDateTime = realDateTime.AddSeconds(1.0);
				if (!(dateTime > dateTimeConvertFrom) || !(dateTime < dateTimeConvertTo))
				{
					return;
				}
				fitValueAccelerationXyzMilliSecTimeStamp.Add(fitValueAccelerationXyzMilliSecTimeStampSub);
				fitValueAccelerationXyzMilliSecTimeStampSub = new List<int>();
				if (num9 > 0)
				{
					fitValueAccelerationXyzMilliSecTimeStampSub.Add(num9);
				}
				fitValueAccelerationXyzYaw.Add(fitValueAccelerationXyzYawSub);
				fitValueAccelerationXyzYawSub = new List<float>();
				fitValueAccelerationXyzYawSub.Add(num8);
				fitValueAccelerationXyzPitch.Add(fitValueAccelerationXyzPitchSub);
				fitValueAccelerationXyzPitchSub = new List<float>();
				fitValueAccelerationXyzPitchSub.Add(valueAngleTheta);
				fitValueAccelerationXyzRoll.Add(fitValueAccelerationXyzRollSub);
				fitValueAccelerationXyzRollSub = new List<float>();
				fitValueAccelerationXyzRollSub.Add(valueAnglePhi);
				if (num9 / 1000 > timeInSecPrevInclVideoGpsSyncOffset + 1 && valueProductGpsLongitudePrevSec == valueProductGpsLongitude && valueProductGpsLatitudePrevSec == valueProductGpsLatitude)
				{
					timeInSecPrevInclVideoGpsSyncOffset = num9 / 1000 - 1;
				}
				else
				{
					timeInSecPrevInclVideoGpsSyncOffset = num9 / 1000;
				}
				valueProductGpsLongitudePrevSec = valueProductGpsLongitude;
				valueProductGpsLatitudePrevSec = valueProductGpsLatitude;
				if (valueTimeRawInclMilliSec / 1000 <= timeInSecPrev)
				{
					return;
				}
				valueProductGpsLongitudePrevSec = valueProductGpsLongitude;
				valueProductGpsLatitudePrevSec = valueProductGpsLatitude;
				int num10 = num5 - timeInSecPrev;
				if (num10 > 1 && dateTime > dateTimeConvertFrom && dateTime < dateTimeConvertTo)
				{
					for (int i = 1; i < num10; i++)
					{
						latLonStrings.Add(Math.Round(valueProductGpsLatitude, nrOfDecimalsInLatLonString) + "," + Math.Round(valueProductGpsLongitude, nrOfDecimalsInLatLonString));
						chartValueList1.Add((double)valueAltitudeInMilliMeter / 1000.0);
						chartValueList2.Add(num);
						chartValueList3.Add(valueBatteryLevel);
						chartValueList4.Add(absoluteDistanceInMeters);
						chartValueList5.Add(distanceFlownAccumulated);
						if (valueWifiSignal > -2)
						{
							chartValueList6.Add(-200.0);
						}
						else
						{
							chartValueList6.Add(valueWifiSignal);
						}
						chartValueList7.Add(valueNrOfSvs);
						if (chartValueList10.Count() > 1)
						{
							chartValueList10.Add(chartValueList10[chartValueList10.Count() - 1]);
						}
						else
						{
							chartValueList10.Add(0.0);
						}
						fitValueLat.Add(valueProductGpsLatitude);
						fitValueLon.Add(valueProductGpsLongitude);
						fitValueAltitude.Add(valueAltitudeInMeter);
						fitValueAltitudeRelative.Add(valueAltitudeInMeter);
						fitValueBattery.Add(valueBatteryLevel);
						fitValueNrOfSvs.Add(valueNrOfSvs);
						fitValueWifiSignal.Add(Math.Abs(valueWifiSignal));
						fitValueDistanceToDrone.Add(absoluteDistanceInMeters);
						fitValueSpeed.Add(num);
						fitValueDistanceFlown.Add((int)distanceFlownAccumulated);
					}
				}
				timeInSecPrev = valueTimeRawInclMilliSec / 1000;
				if (valueProductGpsLatitude < 200.0 && Math.Abs(takeoffLatitude) < 1E-05 && dateTime >= dateTimeConvertFrom)
				{
					takeoffLatitude = valueProductGpsLatitude;
					takeoffLongitude = valueProductGpsLongitude;
					takeoffAltitude = (int)valueAltitudeInMeter;
					AltitudeAboveSeaLevelOffset = AltitudeAboveSeaLevel(takeoffLatitude, takeoffLongitude);
				}
				if (dateTime < dateTimeConvertFrom || dateTime > dateTimeConvertTo)
				{
					return;
				}
				fixCounter++;
				if (valueControllerGpsLatitude > 200.0 && valueProductGpsLatitude > 200.0)
				{
					return;
				}
				landingLatitude = valueProductGpsLatitude;
				landingLongitude = valueProductGpsLongitude;
				landingAltitude = (int)valueAltitudeInMeter;
				float num11 = 0f;
				float num12 = 0f;
				if (previousLat < 200.0 && previousLon < 200.0 && valueProductGpsLatitude < 200.0 && valueProductGpsLongitude < 200.0)
				{
					num11 = (float)distanceBetweenTwoGpsCoordinatesinMeters(previousLat, previousLon, valueProductGpsLatitude, valueProductGpsLongitude);
					num12 = Math.Abs(previousValueAltitudeInMilliMeter - valueAltitudeInMilliMeter) / 1000;
					previousValueAltitudeInMilliMeter = valueAltitudeInMilliMeter;
					double num13 = PythagorasSingle(num11, num12);
					if (!DataExchangerClass.flightDataFieldPitotSpeedPresent)
					{
						chartValueList10.Add(num13);
					}
					if (!useOwnCalculatedSpeed && DataExchangerClass.droneModel == "Anafi" && chartValueList2.Count > 5)
					{
						double num14 = (chartValueList2[chartValueList2.Count - 1] + chartValueList2[chartValueList2.Count - 2] + chartValueList2[chartValueList2.Count - 3] + chartValueList2[chartValueList2.Count - 4] + chartValueList2[chartValueList2.Count - 5]) / 5.0;
						if ((chartValueList5[chartValueList5.Count - 1] - chartValueList5[chartValueList5.Count - 5]) / 5.0 > num14 * 5.0)
						{
							useOwnCalculatedSpeed = true;
						}
					}
					if (useOwnCalculatedSpeed)
					{
						num = (float)num13;
						if (chartValueList2.Count > 5)
						{
							double num15 = (num13 + chartValueList2[chartValueList2.Count - 2] + chartValueList2[chartValueList2.Count - 3] + chartValueList2[chartValueList2.Count - 4] + chartValueList2[chartValueList2.Count - 5]) / 5.0;
							chartValueList2.RemoveAt(chartValueList2.Count - 1);
							chartValueList2.Add(num15);
							fitValueSpeed.RemoveAt(fitValueSpeed.Count - 1);
							fitValueSpeed.Add((float)num15);
						}
					}
					int num16 = chartValueList10.Count();
					if (num16 > 3 && Math.Abs(chartValueList10[num16 - 2]) < 0.0001 && !DataExchangerClass.flightDataFieldPitotSpeedPresent)
					{
						chartValueList10[num16 - 2] = chartValueList10[num16 - 3];
						chartValueList10[num16 - 1] = chartValueList10[num16 - 3];
					}
					else if (num16 > 3 && chartValueList10[num16 - 2] > chartValueList10[num16 - 1] * 1.7 && chartValueList10[num16 - 2] > chartValueList10[num16 - 3] * 1.7)
					{
						chartValueList10[num16 - 2] = chartValueList10[num16 - 3];
					}
					distanceFlownAccumulated += num13;
				}
				else
				{
					chartValueList10.Add(0.0);
				}
				previousLat = valueProductGpsLatitude;
				previousLon = valueProductGpsLongitude;
				chartValueList5.Add(distanceFlownAccumulated);
				absoluteDistanceInMeters = 0;
				if (valueProductGpsLatitude < 200.0)
				{
					int a2 = Convert.ToInt32(distanceBetweenTwoGpsCoordinatesinMeters(valueProductGpsLatitude, valueProductGpsLongitude, takeoffLatitude, takeoffLongitude));
					absoluteDistanceInMeters = Pythagoras(a2, (int)valueAltitudeInMeter - takeoffAltitude);
				}
				controllerFixCounter++;
				if (controllerFixCounter == 1)
				{
					kmlFileLinesController.Add("      <coordinates>" + takeoffLongitude + "," + takeoffLatitude + "," + 0 + "</coordinates>");
				}
				if (valueProductGpsLatitude < 200.0)
				{
					if (AltitudeAboveSeaLevelOffset < -900)
					{
						kmlFileLinesDrone3D.Add("        " + valueProductGpsLongitude + "," + valueProductGpsLatitude + "," + (int)valueAltitudeInMeter);
					}
					else
					{
						kmlFileLinesDrone3D.Add("        " + valueProductGpsLongitude + "," + valueProductGpsLatitude + "," + ((int)valueAltitudeInMeter + AltitudeAboveSeaLevelOffset));
					}
					kmlFileLinesDrone2D.Add("        " + valueProductGpsLongitude + "," + valueProductGpsLatitude + "," + valueAltitudeInMeter);
					KmlAddPlacemark(ref kmlFileLinesPlacemark, valueProductGpsLatitude, valueProductGpsLongitude, (double)valueAltitudeInMilliMeter / 1000.0, chartValueList1.Count() + 1, valueBatteryLevel, num, chartValueList10[chartValueList10.Count() - 1], absoluteDistanceInMeters, distanceFlownAccumulated, Rad2DegreesAsInInt32(valueAnglePsi), rad2deg(valueAngleTheta), rad2deg(valueAnglePhi), realDateTime + TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now));
				}
				if (valueProductGpsLatitude < 200.0)
				{
					droneFixCounter++;
					myMapPointsLatLng.Add(new PointLatLng(valueProductGpsLatitude, valueProductGpsLongitude));
					gpxFileLines.Add("           <trkpt lat=\"" + valueProductGpsLatitude + "\" lon=\"" + valueProductGpsLongitude + "\">");
					gpxFileLines.Add("               <ele>" + valueAltitudeInMeter + "</ele>");
					gpxFileLines.Add("               <time>" + realDateTime.ToString("yyyy-MM-ddTHH:mm:ss") + "Z</time>");
					gpxFileLines.Add("          </trkpt>");
					gpxFileLines.Add("");
					latLonStrings.Add(Math.Round(valueProductGpsLatitude, nrOfDecimalsInLatLonString) + "," + Math.Round(valueProductGpsLongitude, nrOfDecimalsInLatLonString));
					chartValueList1.Add((double)valueAltitudeInMilliMeter / 1000.0);
					chartValueList2.Add(num);
					chartValueList3.Add(valueBatteryLevel);
					chartValueList4.Add(absoluteDistanceInMeters);
					if (valueWifiSignal > -2)
					{
						chartValueList6.Add(-200.0);
					}
					else
					{
						chartValueList6.Add(valueWifiSignal);
					}
					chartValueList7.Add(valueNrOfSvs);
					if (DataExchangerClass.flightDataFieldPitotSpeedPresent)
					{
						chartValueList10.Add(Convert.ToDouble(valuePitotSpeed));
					}
					fitValueLat.Add(valueProductGpsLatitude);
					fitValueLon.Add(valueProductGpsLongitude);
					fitValueAltitude.Add(valueAltitudeInMeter);
					fitValueAltitudeRelative.Add(valueAltitudeInMeter);
					fitValueBattery.Add(valueBatteryLevel);
					fitValueNrOfSvs.Add(valueNrOfSvs);
					fitValueWifiSignal.Add(Math.Abs(valueWifiSignal));
					fitValueDistanceToDrone.Add(absoluteDistanceInMeters);
					fitValueSpeed.Add(num);
					fitValueDistanceFlown.Add((int)distanceFlownAccumulated);
				}
			}
			else if (dateTime > dateTimeConvertFrom && dateTime < dateTimeConvertTo && num9 > 0)
			{
				fitValueAccelerationXyzMilliSecTimeStampSub.Add(num9);
				fitValueAccelerationXyzYawSub.Add(num8);
				fitValueAccelerationXyzPitchSub.Add(valueAngleTheta);
				fitValueAccelerationXyzRollSub.Add(valueAnglePhi);
			}
		}

		public void SetChartVisibility(bool visible)
		{
			if (visible)
			{
				Chart1.Visible = true;
				checkBoxGraph1.Visible = true;
				checkBoxGraph2.Visible = true;
				checkBoxGraph3.Visible = true;
				checkBoxGraph4.Visible = true;
				checkBoxGraph5.Visible = true;
				checkBoxGraph6.Visible = true;
				checkBoxGraph7.Visible = true;
				checkBoxGraph8.Visible = true;
				checkBoxGraph9.Visible = true;
				checkBoxGraph10.Visible = true;
				labelColorGraph10.Visible = true;
				labelColorGraph4.Visible = true;
				labelColorGraph3.Visible = true;
				labelColorGraph2.Visible = true;
				labelColorGraph1.Visible = true;
				labelColorGraph5.Visible = true;
				labelColorGraph6.Visible = true;
				labelColorGraph7.Visible = true;
				labelColorGraph8.Visible = true;
				labelColorGraph9.Visible = true;
				buttonExportGraph.Visible = true;
				if (chartValueList6.Count > 5 && chartValueList6.Max() > -180.0)
				{
					checkBoxGraph6.Enabled = true;
				}
				else
				{
					checkBoxGraph6.Enabled = false;
				}
				if (chartValueList7.Count > 5 && chartValueList7.Max() > 1.0)
				{
					checkBoxGraph7.Enabled = true;
				}
				else
				{
					checkBoxGraph7.Enabled = false;
				}
			}
			else
			{
				Chart1.Visible = false;
				checkBoxGraph1.Visible = false;
				checkBoxGraph2.Visible = false;
				checkBoxGraph3.Visible = false;
				checkBoxGraph4.Visible = false;
				checkBoxGraph5.Visible = false;
				checkBoxGraph6.Visible = false;
				checkBoxGraph7.Visible = false;
				checkBoxGraph8.Visible = false;
				checkBoxGraph9.Visible = false;
				checkBoxGraph10.Visible = false;
				labelColorGraph10.Visible = false;
				labelColorGraph4.Visible = false;
				labelColorGraph3.Visible = false;
				labelColorGraph2.Visible = false;
				labelColorGraph1.Visible = false;
				labelColorGraph5.Visible = false;
				labelColorGraph6.Visible = false;
				labelColorGraph7.Visible = false;
				labelColorGraph8.Visible = false;
				labelColorGraph9.Visible = false;
				buttonExportGraph.Visible = false;
			}
		}

		public void SetMapVisibility(bool visible)
		{
			if (visible)
			{
				gmap.Visible = true;
				buttonZoomIn.Visible = true;
				buttonZoomOut.Visible = true;
				labelPanMapInfo.Visible = true;
				buttonSwitchMapMode.Visible = true;
			}
			else
			{
				gmap.Visible = false;
				buttonZoomIn.Visible = false;
				buttonZoomOut.Visible = false;
				labelPanMapInfo.Visible = false;
				buttonSwitchMapMode.Visible = false;
			}
		}

		private int Pythagoras(int a, int b)
		{
			return Convert.ToInt32(Math.Sqrt(a * a + b * b));
		}

		private float PythagorasSingle(float a, float b)
		{
			return Convert.ToSingle(Math.Sqrt(a * a + b * b));
		}

		private int Rad2DegreesAsInInt32(float radians)
		{
			int num = Convert.ToInt32((double)radians / (Math.PI * 2.0) * 360.0);
			if (num < 0)
			{
				num = 360 + num;
			}
			return num;
		}

		private void MakeKmlHeader(ref List<string> kmlFileLines)
		{
			kmlFileLines.Add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			kmlFileLines.Add("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
			kmlFileLines.Add("<Document>");
			kmlFileLines.Add("<open>1</open>");
			kmlFileLines.Add("");
			kmlFileLines.Add("");
			kmlFileLines.Add("<Style id=\"placemarkStyle\">");
			kmlFileLines.Add("  <IconStyle><scale>0.5</scale>");
			kmlFileLines.Add("    <Icon><href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href></Icon>");
			kmlFileLines.Add("    <color>90f00000</color>");
			kmlFileLines.Add("  </IconStyle>");
			kmlFileLines.Add("  <LabelStyle>");
			kmlFileLines.Add("    <scale>0.5</scale>");
			kmlFileLines.Add("    <color>60ffffff</color>");
			kmlFileLines.Add("  </LabelStyle>");
			kmlFileLines.Add("</Style>");
		}

		private void MakeKmlFooter(ref List<string> kmlFileLines)
		{
			kmlFileLines.Add("");
			kmlFileLines.Add("");
			kmlFileLines.Add("</Document>");
			kmlFileLines.Add("</kml>");
		}

		private void MakeKmlControllerStart(ref List<string> kmlFileLines)
		{
			kmlFileLines.Add("");
			kmlFileLines.Add("");
			kmlFileLines.Add("<Folder><name>SkyController</name>");
			kmlFileLines.Add("<visibility>0</visibility>");
			kmlFileLines.Add("<Placemark>");
			kmlFileLines.Add("  <visibility>0</visibility>");
			kmlFileLines.Add("   <Style>");
			kmlFileLines.Add("       <IconStyle>");
			kmlFileLines.Add("         <scale>1.0</scale>");
			kmlFileLines.Add("         <Icon>");
			kmlFileLines.Add("           <href>http://maps.google.com/mapfiles/kml/shapes/target.png</href>");
			kmlFileLines.Add("         </Icon>");
			kmlFileLines.Add("       </IconStyle>");
			kmlFileLines.Add("     </Style>");
			kmlFileLines.Add("     <Point>");
		}

		private void MakeKmlPlacemarksEnd(ref List<string> kmlFileLines)
		{
			kmlFileLines.Add("");
			kmlFileLines.Add("</Folder>");
		}

		private void MakeKmlDrone2DEnd(ref List<string> kmlFileLines)
		{
			kmlFileLines.Add("      </coordinates>");
			kmlFileLines.Add("    </LineString>");
			kmlFileLines.Add("  </Placemark>");
			kmlFileLines.Add("</Folder>");
		}

		private void MakeKmlDrone3DEnd(ref List<string> kmlFileLines)
		{
			kmlFileLines.Add("      </coordinates>");
			kmlFileLines.Add("    </LineString>");
			kmlFileLines.Add("  </Placemark>");
			kmlFileLines.Add("");
			kmlFileLines.Add("  <Placemark>");
			kmlFileLines.Add("  <name>Start</name>");
			kmlFileLines.Add("    <Style>");
			kmlFileLines.Add("      <IconStyle>");
			kmlFileLines.Add("        <scale>1.0</scale>");
			kmlFileLines.Add("        <Icon>");
			kmlFileLines.Add("          <href>http://maps.google.com/mapfiles/kml/shapes/star.png</href>");
			kmlFileLines.Add("        </Icon>");
			kmlFileLines.Add("      </IconStyle>");
			kmlFileLines.Add("    </Style>");
			kmlFileLines.Add("    <Point>");
			if (AltitudeAboveSeaLevelOffset < -900)
			{
				kmlFileLines.Insert(12, "       <altitudeMode>relativeToGround</altitudeMode>");
				if (takeoffAltitude < 1)
				{
					kmlFileLines.Add("      <coordinates>" + takeoffLongitude + "," + takeoffLatitude + ",1</coordinates>");
				}
				else
				{
					kmlFileLines.Add("      <coordinates>" + takeoffLongitude + "," + takeoffLatitude + "," + takeoffAltitude + "</coordinates>");
				}
			}
			else
			{
				kmlFileLines.Insert(12, "       <altitudeMode>absolute</altitudeMode>");
				if (takeoffAltitude < 1)
				{
					kmlFileLines.Add("      <coordinates>" + takeoffLongitude + "," + takeoffLatitude + ",(AltitudeAboveSeaLevelOffset+1)</coordinates>");
				}
				else
				{
					kmlFileLines.Add("      <coordinates>" + takeoffLongitude + "," + takeoffLatitude + "," + (takeoffAltitude + AltitudeAboveSeaLevelOffset) + "</coordinates>");
				}
			}
			kmlFileLines.Add("    </Point>");
			kmlFileLines.Add("  </Placemark>");
			kmlFileLines.Add("");
			kmlFileLines.Add("  <Placemark>");
			kmlFileLines.Add("  <name>End</name>");
			kmlFileLines.Add("    <Style>");
			kmlFileLines.Add("      <IconStyle>");
			kmlFileLines.Add("        <scale>1.0</scale>");
			kmlFileLines.Add("        <Icon>");
			kmlFileLines.Add("          <href>http://maps.google.com/mapfiles/kml/shapes/forbidden.png</href>");
			kmlFileLines.Add("        </Icon>");
			kmlFileLines.Add("      </IconStyle>");
			kmlFileLines.Add("    </Style>");
			kmlFileLines.Add("    <Point>");
			if (AltitudeAboveSeaLevelOffset < -900)
			{
				kmlFileLines.Insert(12, "       <altitudeMode>relativeToGround</altitudeMode>");
				if (landingAltitude < 1)
				{
					kmlFileLines.Add("      <coordinates>" + landingLongitude + "," + landingLatitude + "1</coordinates>");
				}
				else
				{
					kmlFileLines.Add("      <coordinates>" + landingLongitude + "," + landingLatitude + "," + landingAltitude + "</coordinates>");
				}
			}
			else
			{
				kmlFileLines.Insert(12, "       <altitudeMode>absolute</altitudeMode>");
				if (landingAltitude < 1)
				{
					kmlFileLines.Add("      <coordinates>" + landingLongitude + "," + landingLatitude + ",(AltitudeAboveSeaLevelOffset+1)</coordinates>");
				}
				else
				{
					kmlFileLines.Add("      <coordinates>" + landingLongitude + "," + landingLatitude + "," + (landingAltitude + AltitudeAboveSeaLevelOffset) + "</coordinates>");
				}
			}
			kmlFileLines.Add("    </Point>");
			kmlFileLines.Add("  </Placemark>");
			kmlFileLines.Add("");
			kmlFileLines.Add("</Folder>");
		}

		private void MakeKmlControllerEnd(ref List<string> kmlFileLines)
		{
			kmlFileLines.Add("   </Point>");
			kmlFileLines.Add("  </Placemark>");
			kmlFileLines.Add("</Folder>");
		}

		private void KmlAddPlacemark(ref List<string> kmlFileLines, double Lat, double Lon, double altitude, int timeInTotalSec, int valueBatteryLevel, float valueSpeedHorizontal, double valueSpeedExtra, int absoluteDistanceInMeters, double distanceFlownAccumulated, int orientationValue, double pitchValue, double rollValue, DateTime dateTime)
		{
			if (timeInTotalSec < 2)
			{
				return;
			}
			if (pitchValue > 180.0)
			{
				pitchValue -= 360.0;
			}
			int num = timeInTotalSec / 60;
			string str2 = string.Concat(str2: (timeInTotalSec % 60).ToString("00"), str0: num.ToString("00"), str1: ":");
			string str3 = "";
			for (int i = 0; i < altitudeColorLimits.Count; i++)
			{
				if (altitude < (double)altitudeColorLimits[i])
				{
					str3 = altitudeColors[i];
					break;
				}
			}
			kmlFileLines.Add("");
			kmlFileLines.Add("<Placemark>");
			kmlFileLines.Add("  <name>" + str2 + "</name>");
			kmlFileLines.Add("  <description><![CDATA[");
			kmlFileLines.Add("  Altitude = " + altitude.ToString("0.0") + " meter - " + (altitude * 3.2808).ToString("0.0") + " feet<BR><BR>");
			if (DataExchangerClass.droneModel.Contains("Disco"))
			{
				kmlFileLines.Add("  Speed over ground = " + valueSpeedExtra.ToString("0.0") + " m/s - " + (valueSpeedExtra * 3.6).ToString("0.0") + "km/h - " + (valueSpeedExtra * 2.2369).ToString("0.0") + " mph<BR><BR>");
				kmlFileLines.Add("  AirSpeed = " + valueSpeedHorizontal.ToString("0.0") + " m/s - " + ((double)valueSpeedHorizontal * 3.6).ToString("0.0") + "km/h - " + ((double)valueSpeedHorizontal * 2.2369).ToString("0.0") + " mph<BR><BR>");
			}
			else
			{
				kmlFileLines.Add("  Speed = " + valueSpeedHorizontal.ToString("0.0") + " m/s - " + ((double)valueSpeedHorizontal * 3.6).ToString("0.0") + "km/h - " + ((double)valueSpeedHorizontal * 2.2369).ToString("0.0") + " mph<BR><BR>");
			}
			kmlFileLines.Add("  Distance between drone and Controller = " + absoluteDistanceInMeters + " meter - " + ((double)absoluteDistanceInMeters * 3.2808).ToString("0.0") + " feet<BR><BR>");
			kmlFileLines.Add("  Distance flown = " + distanceFlownAccumulated.ToString("0") + " meter - " + (distanceFlownAccumulated * 3.2808).ToString("0.0") + " feet<BR><BR>");
			kmlFileLines.Add("  Orientation = " + orientationValue + "°<BR><BR>");
			kmlFileLines.Add("  Pitch = " + pitchValue.ToString("0.0") + "°<BR><BR>");
			kmlFileLines.Add("  Roll = " + rollValue.ToString("0.0") + "°<BR><BR>");
			kmlFileLines.Add("  Battery = " + valueBatteryLevel + " %<BR><BR>");
			kmlFileLines.Add("  Wi-Fi signal level = " + valueWifiSignal + " dBm<BR><BR>");
			kmlFileLines.Add("  Number of GPS satellites = " + valueNrOfSvs + "<BR><BR>");
			kmlFileLines.Add("  Flying state = " + valueFlyingState + "<BR><BR>");
			kmlFileLines.Add("  Alert state = " + valueAlertState + "<BR><BR>");
			kmlFileLines.Add("  " + dateTime.ToString("yyyy MMM. dd. - HH:mm:ss") + "<BR><BR>");
			kmlFileLines.Add("  <BR><HR><BR>]]></description>");
			kmlFileLines.Add("  <styleUrl>#placemarkStyle</styleUrl>");
			kmlFileLines.Add("  <Style><IconStyle><color>" + str3 + "</color></IconStyle></Style>");
			kmlFileLines.Add("  <Point>");
			if (AltitudeAboveSeaLevelOffset < -900)
			{
				kmlFileLines.Add("       <altitudeMode>relativeToGround</altitudeMode>");
				kmlFileLines.Add("    <coordinates>" + Lon + "," + Lat + "," + altitude.ToString("0.0") + "</coordinates>");
			}
			else
			{
				kmlFileLines.Add("       <altitudeMode>absolute</altitudeMode>");
				kmlFileLines.Add("    <coordinates>" + Lon + "," + Lat + "," + (altitude + (double)AltitudeAboveSeaLevelOffset) + "</coordinates>");
			}
			kmlFileLines.Add("  </Point>");
			kmlFileLines.Add("</Placemark>");
		}

		private void MakeKmlDrone2DStart(ref List<string> kmlFileLines)
		{
			kmlFileLines.Add("");
			kmlFileLines.Add("");
			kmlFileLines.Add("<Folder><name>Drone 2D</name>");
			kmlFileLines.Add("<visibility>0</visibility>");
			kmlFileLines.Add("<Placemark>");
			kmlFileLines.Add("<visibility>0</visibility>");
			kmlFileLines.Add("   <Style id=\"default\">");
			kmlFileLines.Add("       <LineStyle>");
			kmlFileLines.Add("           <color>ffffff00</color>");
			kmlFileLines.Add("           <width>3</width>");
			kmlFileLines.Add("       </LineStyle>");
			kmlFileLines.Add("   </Style>");
			kmlFileLines.Add("   <LineString>");
			kmlFileLines.Add("       <tessellate>1</tessellate>");
			kmlFileLines.Add("       <altitudeMode>ClampedToGround</altitudeMode>");
			kmlFileLines.Add("       <coordinates>");
		}

		private void MakeKmlPlacemarkStart(ref List<string> kmlFileLines)
		{
			kmlFileLines.Add("");
			kmlFileLines.Add("");
			kmlFileLines.Add("<Folder><name>Detailed Flight Info</name>");
		}

		private void MakeGpxHeader(ref List<string> gpxFileLines)
		{
			gpxFileLines.Add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			gpxFileLines.Add("<gpx version=\"1.1\" creator=\"FlightData Manager\"");
			gpxFileLines.Add("xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1");
			gpxFileLines.Add("http://www.topografix.com/GPX/1/1/gpx.xsd\"");
			gpxFileLines.Add("xmlns=\"http://www.topografix.com/GPX/1/1\"");
			gpxFileLines.Add("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
			gpxFileLines.Add("   <trk>");
			gpxFileLines.Add("       <name><![CDATA[Track]]></name>");
			gpxFileLines.Add("       <desc><![CDATA[Track]]></desc>");
			gpxFileLines.Add("       <number>1</number>");
			gpxFileLines.Add("       <trkseg>");
			gpxFileLines.Add("");
		}

		private void MakeGpxFooter(ref List<string> gpxFileLines)
		{
			gpxFileLines.Add("       </trkseg>");
			gpxFileLines.Add("   </trk>");
			gpxFileLines.Add("</gpx>");
		}

		private static int Convert2BytesToInt(byte[] allData, int pointer)
		{
			return allData[pointer] + allData[pointer + 1] * 255;
		}

		private static int Convert4BytesToInt(byte[] allData, int pointer)
		{
			return allData[pointer] + allData[pointer + 1] * 255 + allData[pointer + 2] * 65535 + allData[pointer + 3] * 16777215;
		}

		private byte[] ReadPudFile(string fileName)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			BinaryReader binaryReader = new BinaryReader(fileStream);
			byte[] result = binaryReader.ReadBytes((int)binaryReader.BaseStream.Length);
			fileStream.Close();
			return result;
		}

		private DateTime ReadStartDateTimeFromPudFile(string fileName)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			string text = new BinaryReader(fileStream).ReadString();
			fileStream.Close();
			string text2 = "";
			try
			{
				text2 = text.Substring(text.IndexOf("date"), 30);
			}
			catch (Exception)
			{
				MessageBox.Show(this, "Something wrong with the selected file");
				return default(DateTime);
			}
			text2.Substring(19, 2);
			int year = Convert.ToInt32(text2.Substring(8, 4));
			int month = Convert.ToInt32(text2.Substring(13, 2));
			int day = Convert.ToInt32(text2.Substring(16, 2));
			int hour = Convert.ToInt32(text2.Substring(19, 2));
			int minute = Convert.ToInt32(text2.Substring(21, 2));
			int second = Convert.ToInt32(text2.Substring(23, 2));
			DateTime d = new DateTime(year, month, day, hour, minute, second);
			TimeSpan utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);
			return d - utcOffset;
		}

		private string ReadProductIdFromPudFile(string fileName)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			string text = new BinaryReader(fileStream).ReadString();
			fileStream.Close();
			string text2 = "";
			try
			{
				int num = text.IndexOf("product_id");
				int num2 = text.IndexOf(",", num);
				return text.Substring(num + 13, num2 - num - 13);
			}
			catch (Exception)
			{
				MessageBox.Show(this, "Something wrong with the selected file");
				return "";
			}
		}

		private bool DetectIfNrOfSatsInPudFile(string fileName)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			char[] value = new BinaryReader(fileStream).ReadChars(1500);
			fileStream.Close();
			if (new string(value).Contains("product_gps_sv_number"))
			{
				return true;
			}
			return false;
		}

		private bool DetectIfPitotSpeedInPudFile(string fileName)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			char[] value = new BinaryReader(fileStream).ReadChars(1500);
			fileStream.Close();
			if (new string(value).Contains("pitot_speed"))
			{
				return true;
			}
			return false;
		}

		private int LocateStartOfData(byte[] allData)
		{
			int i;
			for (i = 0; i < allData.Count() && (allData[i] != 125 || allData[i - 1] != 32 || allData[i - 2] != 93 || allData[i - 3] != 32 || allData[i - 4] != 125 || allData[i - 5] != 32); i++)
			{
			}
			if (i + 2 > allData.Count())
			{
				MessageBox.Show(this, "Start of data in pud file was not found");
			}
			return i + 2;
		}

		private double distanceBetweenTwoGpsCoordinatesinMeters(double lat1, double lon1, double lat2, double lon2)
		{
			double deg = lon1 - lon2;
			double d = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(deg));
			d = Math.Acos(d);
			d = rad2deg(d);
			d = d * 60.0 * 1853.159616;
			if (double.IsNaN(d))
			{
				d = 0.0;
			}
			return d;
		}

		private static double deg2rad(double deg)
		{
			return deg * Math.PI / 180.0;
		}

		private static double rad2deg(double rad)
		{
			return rad / Math.PI * 180.0;
		}

		private void richTextBox1_LinkClicked(object sender, LinkClickedEventArgs e)
		{
			Process.Start(e.LinkText);
		}

		private void buttonInstallGarminVirbTemplates_Click(object sender, EventArgs e)
		{
			new FormInstallGarminVirbTemplates().ShowDialog();
		}

		private void buttonDonate_Click(object sender, EventArgs e)
		{
			new FormDonate().ShowDialog();
		}

		public void UpdateGraphs()
		{
			if (chartValueList3.Count > 5)
			{
				int num = 100;
				if (chartValueList3.Count < num + 2)
				{
					num = chartValueList3.Count - 2;
				}
				for (int num2 = num; num2 > 0; num2--)
				{
					if (chartValueList3[num2] > chartValueList3[num2 - 1])
					{
						chartValueList3[num2 - 1] = chartValueList3[num2];
					}
				}
			}
			Chart1.Series.Clear();
			Chart1.ChartAreas.Clear();
			Chart1.ChartAreas.Add("ChartAreaNew");
			DateTime dateTime = new DateTime(0L);
			bool flag = false;
			if (checkBoxCheckedList.Contains("Graph01"))
			{
				Chart1.Series.Add("Graph01");
				int num3 = Chart1.Series.Count();
				Chart1.Series[num3 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num3 - 1].BorderWidth = 3;
				foreach (double item in chartValueList1)
				{
					if (flag)
					{
						Chart1.Series[num3 - 1].Points.AddY(item);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num3 - 1].Points.AddXY(dateTime, item);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph2"))
			{
				Chart1.Series.Add("Graph2");
				int num4 = Chart1.Series.Count();
				Chart1.Series[num4 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num4 - 1].BorderWidth = 3;
				foreach (double item2 in chartValueList2)
				{
					if (flag)
					{
						Chart1.Series[num4 - 1].Points.AddY(item2);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num4 - 1].Points.AddXY(dateTime, item2);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph3"))
			{
				Chart1.Series.Add("Graph3");
				int num5 = Chart1.Series.Count();
				Chart1.Series[num5 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num5 - 1].BorderWidth = 3;
				foreach (double item3 in chartValueList3)
				{
					if (flag)
					{
						Chart1.Series[num5 - 1].Points.AddY(item3);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num5 - 1].Points.AddXY(dateTime, item3);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph4"))
			{
				Chart1.Series.Add("Graph4");
				int num6 = Chart1.Series.Count();
				Chart1.Series[num6 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num6 - 1].BorderWidth = 3;
				foreach (double item4 in chartValueList4)
				{
					if (flag)
					{
						Chart1.Series[num6 - 1].Points.AddY(item4);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num6 - 1].Points.AddXY(dateTime, item4);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph5"))
			{
				Chart1.Series.Add("Graph5");
				int num7 = Chart1.Series.Count();
				Chart1.Series[num7 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num7 - 1].BorderWidth = 3;
				foreach (double item5 in chartValueList5)
				{
					if (flag)
					{
						Chart1.Series[num7 - 1].Points.AddY(item5);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num7 - 1].Points.AddXY(dateTime, item5);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph6"))
			{
				Chart1.Series.Add("Graph6");
				int num8 = Chart1.Series.Count();
				Chart1.Series[num8 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num8 - 1].BorderWidth = 3;
				foreach (double item6 in chartValueList6)
				{
					if (flag)
					{
						Chart1.Series[num8 - 1].Points.AddY(item6);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num8 - 1].Points.AddXY(dateTime, item6);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph7"))
			{
				Chart1.Series.Add("Graph7");
				int num9 = Chart1.Series.Count();
				Chart1.Series[num9 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num9 - 1].BorderWidth = 3;
				foreach (double item7 in chartValueList7)
				{
					if (flag)
					{
						Chart1.Series[num9 - 1].Points.AddY(item7);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num9 - 1].Points.AddXY(dateTime, item7);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph8"))
			{
				Chart1.Series.Add("Graph8");
				int num10 = Chart1.Series.Count();
				Chart1.Series[num10 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num10 - 1].BorderWidth = 3;
				foreach (double item8 in chartValueList8)
				{
					if (flag)
					{
						Chart1.Series[num10 - 1].Points.AddY(item8);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num10 - 1].Points.AddXY(dateTime, item8);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph9"))
			{
				Chart1.Series.Add("Graph9");
				int num11 = Chart1.Series.Count();
				Chart1.Series[num11 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num11 - 1].BorderWidth = 3;
				foreach (double item9 in chartValueList9)
				{
					if (flag)
					{
						Chart1.Series[num11 - 1].Points.AddY(item9);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num11 - 1].Points.AddXY(dateTime, item9);
					}
				}
				flag = true;
			}
			if (checkBoxCheckedList.Contains("Graph10"))
			{
				Chart1.Series.Add("Graph10");
				int num12 = Chart1.Series.Count();
				Chart1.Series[num12 - 1].ChartType = SeriesChartType.Line;
				Chart1.Series[num12 - 1].BorderWidth = 3;
				foreach (double item10 in chartValueList10)
				{
					if (flag)
					{
						Chart1.Series[num12 - 1].Points.AddY(item10);
					}
					else
					{
						dateTime = dateTime.Add(TimeSpan.FromSeconds(1.0));
						Chart1.Series[num12 - 1].Points.AddXY(dateTime, item10);
					}
				}
				flag = true;
			}
			Chart1.ChartAreas[0].AxisX.LabelStyle.Enabled = true;
			Chart1.ChartAreas[0].AxisX.LabelStyle.Angle = 0;
			Chart1.ChartAreas[0].AxisX.LabelStyle.Format = "mm:ss";
			Chart1.ChartAreas[0].Position = new ElementPosition(12f, 14f, 39f, 59f);
			Chart1.ChartAreas[0].InnerPlotPosition = new ElementPosition(8f, 0f, 183f, 138f);
			if (checkBoxCheckedList.Count() > 1)
			{
				CreateYAxis(Chart1, Chart1.ChartAreas[0], Chart1.Series[1], -75f, 10f);
			}
			if (checkBoxCheckedList.Count() > 2)
			{
				CreateYAxis(Chart1, Chart1.ChartAreas[0], Chart1.Series[2], 4f, 0f);
			}
			if (checkBoxCheckedList.Count() > 3)
			{
				CreateYAxis(Chart1, Chart1.ChartAreas[0], Chart1.Series[3], -79f, 20f);
			}
			if (checkBoxCheckedList.Count() > 4)
			{
				CreateYAxis(Chart1, Chart1.ChartAreas[0], Chart1.Series[4], 8f, 0f);
			}
			if (checkBoxCheckedList.Count() > 5)
			{
				CreateYAxis(Chart1, Chart1.ChartAreas[0], Chart1.Series[5], -84f, 30f);
			}
			if (checkBoxCheckedList.Count() > 6)
			{
				CreateYAxis(Chart1, Chart1.ChartAreas[0], Chart1.Series[6], 12f, 0f);
			}
			for (int i = 0; i < Chart1.Series.Count(); i++)
			{
				Chart1.Series[i].IsVisibleInLegend = false;
			}
			Chart1.ChartAreas[0].AxisY.LabelStyle.Font = new Font("Microsoft Sans Serif", 10f);
			Chart1.Titles.Clear();
			try
			{
				Chart1.Series["Graph01"].Color = Color.Red;
			}
			catch (Exception)
			{
			}
			try
			{
				Chart1.Series["Graph2"].Color = Color.Green;
			}
			catch (Exception)
			{
			}
			try
			{
				Chart1.Series["Graph3"].Color = Color.Blue;
			}
			catch (Exception)
			{
			}
			try
			{
				Chart1.Series["Graph4"].Color = Color.Orange;
			}
			catch (Exception)
			{
			}
			try
			{
				Chart1.Series["Graph5"].Color = Color.Magenta;
			}
			catch (Exception)
			{
			}
			try
			{
				Chart1.Series["Graph6"].Color = Color.SkyBlue;
			}
			catch (Exception)
			{
			}
			try
			{
				Chart1.Series["Graph7"].Color = Color.SandyBrown;
			}
			catch (Exception)
			{
			}
			try
			{
				Chart1.Series["Graph8"].Color = Color.FromArgb(85, Color.Indigo);
			}
			catch (Exception)
			{
			}
			try
			{
				Chart1.Series["Graph9"].Color = Color.Lime;
			}
			catch (Exception)
			{
			}
			try
			{
				Chart1.Series["Graph10"].Color = Color.DarkKhaki;
			}
			catch (Exception)
			{
			}
			Title title = new Title(DataExchangerClass.droneModel + " flight data statistics", Docking.Left, new Font("Microsoft Sans Serif", 14f, FontStyle.Bold), Color.Black);
			Chart1.Titles.Add(title);
			title.Position = new ElementPosition(50f, 4f, 0f, 0f);
			title = new Title("Generated by FlightData Manager", Docking.Left, new Font("Microsoft Sans Serif", 11f, FontStyle.Regular), Color.Black);
			Chart1.Titles.Add(title);
			title.Position = new ElementPosition(50f, 8f, 0f, 0f);
			title = new Title("Start date and time: " + startDateTime.ToString("dd. MMM. yyyy - HH:mm:ss"), Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Regular), Color.Black);
			Chart1.Titles.Add(title);
			title.Position = new ElementPosition(50f, 12f, 0f, 0f);
			Title title2 = new Title(checkBoxCheckedListAlias[0], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), Chart1.Series[0].Color);
			title2.Docking = Docking.Left;
			Chart1.Titles.Add(title2);
			title2.TextOrientation = TextOrientation.Rotated270;
			title2.Position = new ElementPosition(14f, 6f, 0f, 0f);
			if (checkBoxCheckedList.Count() > 1)
			{
				title2 = new Title(checkBoxCheckedListAlias[1], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), Chart1.Series[1].Color);
				title2.Docking = Docking.Left;
				Chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(89f, 6f, 0f, 0f);
			}
			if (checkBoxCheckedList.Count() > 2)
			{
				title2 = new Title(checkBoxCheckedListAlias[2], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), Chart1.Series[2].Color);
				title2.Docking = Docking.Left;
				Chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(10f, 6f, 0f, 0f);
			}
			if (checkBoxCheckedList.Count() > 3)
			{
				title2 = new Title(checkBoxCheckedListAlias[3], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), Chart1.Series[3].Color);
				title2.Docking = Docking.Left;
				Chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(93f, 6f, 0f, 0f);
			}
			if (checkBoxCheckedList.Count() > 4)
			{
				title2 = new Title(checkBoxCheckedListAlias[4], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), Chart1.Series[4].Color);
				title2.Docking = Docking.Left;
				Chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(6f, 6f, 0f, 0f);
			}
			if (checkBoxCheckedList.Count() > 5)
			{
				title2 = new Title(checkBoxCheckedListAlias[5], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), Chart1.Series[5].Color);
				title2.Docking = Docking.Left;
				Chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(96f, 6f, 0f, 0f);
			}
			if (checkBoxCheckedList.Count() > 6)
			{
				title2 = new Title(checkBoxCheckedListAlias[6], Docking.Left, new Font("Microsoft Sans Serif", 8f, FontStyle.Bold), Chart1.Series[6].Color);
				title2.Docking = Docking.Left;
				Chart1.Titles.Add(title2);
				title2.TextOrientation = TextOrientation.Rotated270;
				title2.Position = new ElementPosition(2f, 6f, 0f, 0f);
			}
			int num13 = checkBoxCheckedList.IndexOf("Graph01");
			int num14 = checkBoxCheckedList.IndexOf("Graph8");
			if ((num13 >= 0 && num14 >= 0) || checkBoxCheckedList.IndexOf("Graph9") >= 0)
			{
				Chart1.Update();
			}
			if (num13 >= 0 && num14 >= 0)
			{
				double num15 = Chart1.ChartAreas[num13 * 2].AxisY.Maximum;
				double num16 = Chart1.ChartAreas[num13 * 2].AxisY.Minimum;
				double maximum = Chart1.ChartAreas[num14 * 2].AxisY.Maximum;
				double minimum = Chart1.ChartAreas[num14 * 2].AxisY.Minimum;
				if (num15 < maximum)
				{
					num15 = maximum;
				}
				if (num16 > minimum)
				{
					num16 = minimum;
				}
				Chart1.ChartAreas[num13 * 2].AxisY.Maximum = num15;
				if (num13 >= 1)
				{
					Chart1.ChartAreas[num13 * 2 - 1].AxisY.Maximum = num15;
				}
				Chart1.ChartAreas[num14 * 2].AxisY.Maximum = num15;
				if (num14 >= 1)
				{
					Chart1.ChartAreas[num14 * 2 - 1].AxisY.Maximum = num15;
				}
				Chart1.ChartAreas[num13 * 2].AxisY.Minimum = num16;
				if (num13 >= 1)
				{
					Chart1.ChartAreas[num13 * 2 - 1].AxisY.Minimum = num16;
				}
				Chart1.ChartAreas[num14 * 2].AxisY.Minimum = num16;
				if (num14 >= 1)
				{
					Chart1.ChartAreas[num14 * 2 - 1].AxisY.Minimum = num16;
				}
			}
			int num17 = 15;
			if (chartValueList2.Count > 1 && (int)chartValueList2.Max() >= num17)
			{
				num17 = (int)chartValueList2.Max() + 1;
			}
			int num18 = checkBoxCheckedList.IndexOf("Graph2");
			if (num18 >= 0)
			{
				Chart1.ChartAreas[num18 * 2].AxisY.Maximum = num17;
				if (num18 >= 1)
				{
					Chart1.ChartAreas[num18 * 2 - 1].AxisY.Maximum = num17;
				}
			}
			num18 = checkBoxCheckedList.IndexOf("Graph2");
			int num19 = checkBoxCheckedList.IndexOf("Graph10");
			if (num18 >= 0 && num19 >= 0)
			{
				Chart1.Update();
				double num20 = Chart1.ChartAreas[num18 * 2].AxisY.Maximum;
				double maximum2 = Chart1.ChartAreas[num19 * 2].AxisY.Maximum;
				if (num20 < maximum2)
				{
					num20 = maximum2;
				}
				Chart1.ChartAreas[num18 * 2].AxisY.Maximum = num20;
				if (num18 >= 1)
				{
					Chart1.ChartAreas[num18 * 2 - 1].AxisY.Maximum = num20;
				}
				Chart1.ChartAreas[num19 * 2].AxisY.Maximum = num20;
				if (num19 >= 1)
				{
					Chart1.ChartAreas[num19 * 2 - 1].AxisY.Maximum = num20;
				}
			}
			int num21 = checkBoxCheckedList.IndexOf("Graph3");
			if (num21 >= 0)
			{
				Chart1.ChartAreas[num21 * 2].AxisY.Maximum = 100.0;
				if (num21 >= 1)
				{
					Chart1.ChartAreas[num21 * 2 - 1].AxisY.Maximum = 100.0;
				}
			}
			int num22 = checkBoxCheckedList.IndexOf("Graph6");
			if (num22 >= 0)
			{
				Chart1.ChartAreas[num22 * 2].AxisY.Maximum = -20.0;
				Chart1.ChartAreas[num22 * 2].AxisY.Minimum = -100.0;
				if (num22 >= 1)
				{
					Chart1.ChartAreas[num22 * 2 - 1].AxisY.Maximum = -20.0;
					Chart1.ChartAreas[num22 * 2 - 1].AxisY.Minimum = -100.0;
				}
			}
			int num23 = checkBoxCheckedList.IndexOf("Graph7");
			if (num23 >= 0)
			{
				Chart1.ChartAreas[num23 * 2].AxisY.Maximum = 24.0;
				Chart1.ChartAreas[num23 * 2].AxisY.Minimum = 4.0;
				if (num23 >= 1)
				{
					Chart1.ChartAreas[num23 * 2 - 1].AxisY.Maximum = 24.0;
					Chart1.ChartAreas[num23 * 2 - 1].AxisY.Minimum = 4.0;
				}
			}
			int num24 = checkBoxCheckedList.IndexOf("Graph9");
			if (num24 >= 0)
			{
				double num25 = Chart1.ChartAreas[num24 * 2].AxisY.Maximum;
				double minimum2 = Chart1.ChartAreas[num24 * 2].AxisY.Minimum;
				if (num25 - minimum2 < 20.0)
				{
					num25 = minimum2 + 20.0;
				}
				num25 = (num25 - minimum2) * 2.5 + minimum2;
				Chart1.ChartAreas[num24 * 2].AxisY.Maximum = num25;
				if (num24 >= 1)
				{
					Chart1.ChartAreas[num24 * 2 - 1].AxisY.Maximum = num25;
				}
			}
		}

		public void CreateYAxis(Chart chart, ChartArea area, Series series, float axisOffset, float labelsSize)
		{
			ChartArea chartArea = chart.ChartAreas.Add("ChartArea_" + series.Name);
			chartArea.BackColor = Color.Transparent;
			chartArea.BorderColor = Color.Transparent;
			chartArea.Position.FromRectangleF(area.Position.ToRectangleF());
			chartArea.InnerPlotPosition.FromRectangleF(area.InnerPlotPosition.ToRectangleF());
			chartArea.AxisX.MajorGrid.Enabled = false;
			chartArea.AxisX.MajorTickMark.Enabled = false;
			chartArea.AxisX.LabelStyle.Enabled = false;
			chartArea.AxisY.MajorGrid.Enabled = false;
			chartArea.AxisY.MajorTickMark.Enabled = false;
			chartArea.AxisY.LabelStyle.Enabled = false;
			chartArea.AxisY.IsStartedFromZero = area.AxisY.IsStartedFromZero;
			series.ChartArea = chartArea.Name;
			ChartArea chartArea2 = chart.ChartAreas.Add("AxisY_" + series.ChartArea);
			chartArea2.BackColor = Color.Transparent;
			chartArea2.BorderColor = Color.Transparent;
			chartArea2.Position.FromRectangleF(chart.ChartAreas[series.ChartArea].Position.ToRectangleF());
			chartArea2.InnerPlotPosition.FromRectangleF(chart.ChartAreas[series.ChartArea].InnerPlotPosition.ToRectangleF());
			Series series2 = chart.Series.Add(series.Name + "_Copy");
			series2.ChartType = series.ChartType;
			foreach (DataPoint point in series.Points)
			{
				series2.Points.AddXY(point.XValue, point.YValues[0]);
			}
			series2.IsVisibleInLegend = false;
			series2.Color = Color.Transparent;
			series2.BorderColor = Color.Transparent;
			series2.ChartArea = chartArea2.Name;
			chartArea2.AxisX.LineWidth = 0;
			chartArea2.AxisX.MajorGrid.Enabled = false;
			chartArea2.AxisX.MajorTickMark.Enabled = false;
			chartArea2.AxisX.LabelStyle.Enabled = false;
			chartArea2.AxisY.MajorGrid.Enabled = false;
			chartArea2.AxisY.IsStartedFromZero = area.AxisY.IsStartedFromZero;
			chartArea2.AxisY.LabelStyle.Font = area.AxisY.LabelStyle.Font;
			chartArea2.Position.X -= axisOffset;
			chartArea2.InnerPlotPosition.X += labelsSize;
		}

		private void checkBoxGraphs_MouseUp(object sender, MouseEventArgs e)
		{
			checkBoxCheckedList = new List<string>();
			checkBoxCheckedListAlias = new List<string>();
			if (checkBoxGraph1.Checked)
			{
				checkBoxCheckedList.Add("Graph01");
				checkBoxCheckedListAlias.Add("Altitude\nmeter");
			}
			if (checkBoxGraph2.Checked)
			{
				checkBoxCheckedList.Add("Graph2");
				checkBoxCheckedListAlias.Add("Speed\nm/s");
			}
			if (checkBoxGraph3.Checked)
			{
				checkBoxCheckedList.Add("Graph3");
				checkBoxCheckedListAlias.Add("Battery\n%");
			}
			if (checkBoxGraph4.Checked)
			{
				checkBoxCheckedList.Add("Graph4");
				checkBoxCheckedListAlias.Add("Distance\nmeter");
			}
			if (checkBoxGraph5.Checked)
			{
				checkBoxCheckedList.Add("Graph5");
				checkBoxCheckedListAlias.Add("Distance\nFlown, m");
			}
			if (checkBoxGraph6.Checked)
			{
				if (chartValueList6.Max() > -180.0)
				{
					checkBoxCheckedList.Add("Graph6");
					checkBoxCheckedListAlias.Add("Wifi level");
				}
				else
				{
					checkBoxGraph6.Checked = false;
				}
			}
			if (checkBoxGraph7.Checked)
			{
				if (chartValueList7.Max() > 1.0)
				{
					checkBoxCheckedList.Add("Graph7");
					checkBoxCheckedListAlias.Add("Nr of SVs");
				}
				else
				{
					checkBoxGraph7.Checked = false;
				}
			}
			if (checkBoxGraph8.Checked)
			{
				checkBoxCheckedList.Add("Graph8");
				checkBoxCheckedListAlias.Add("Altitude,\nrelative, m");
			}
			if (checkBoxGraph9.Checked)
			{
				checkBoxCheckedList.Add("Graph9");
				checkBoxCheckedListAlias.Add("Terrain\nlevel, m");
			}
			if (checkBoxGraph10.Checked)
			{
				checkBoxCheckedList.Add("Graph10");
				checkBoxCheckedListAlias.Add("AirSpeed\nm/s");
			}
			if (checkBoxCheckedList.Count < 1)
			{
				if (((CheckBox)sender).Name == "checkBoxGraph1")
				{
					checkBoxGraph1.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph2")
				{
					checkBoxGraph2.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph3")
				{
					checkBoxGraph3.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph4")
				{
					checkBoxGraph4.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph5")
				{
					checkBoxGraph5.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph6")
				{
					checkBoxGraph6.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph7")
				{
					checkBoxGraph7.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph8")
				{
					checkBoxGraph8.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph9")
				{
					checkBoxGraph9.Checked = true;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph10")
				{
					checkBoxGraph10.Checked = true;
				}
			}
			else if (checkBoxCheckedList.Count > 7)
			{
				MessageBox.Show(this, "Too many graphs enabled\nYou can maximum select 7 graphs at the same time");
				if (((CheckBox)sender).Name == "checkBoxGraph1")
				{
					checkBoxGraph1.Checked = false;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph2")
				{
					checkBoxGraph2.Checked = false;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph3")
				{
					checkBoxGraph3.Checked = false;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph4")
				{
					checkBoxGraph4.Checked = false;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph5")
				{
					checkBoxGraph5.Checked = false;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph6")
				{
					checkBoxGraph6.Checked = false;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph7")
				{
					checkBoxGraph7.Checked = false;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph8")
				{
					checkBoxGraph8.Checked = false;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph9")
				{
					checkBoxGraph9.Checked = false;
				}
				if (((CheckBox)sender).Name == "checkBoxGraph10")
				{
					checkBoxGraph10.Checked = false;
				}
			}
			else
			{
				UpdateGraphs();
			}
		}

		private void chart1_MouseMove(object sender, MouseEventArgs e)
		{
			try
			{
				Point location = e.Location;
				if (!prevMousePosition.HasValue || !(location == prevMousePosition.Value))
				{
					myTooltip.RemoveAll();
					prevMousePosition = location;
					HitTestResult[] array = Chart1.HitTest(location.X, location.Y, true, ChartElementType.DataPoint);
					foreach (HitTestResult hitTestResult in array)
					{
						if (hitTestResult.ChartElementType != ChartElementType.DataPoint)
						{
							continue;
						}
						DataPoint dataPoint = hitTestResult.Object as DataPoint;
						if (dataPoint == null)
						{
							continue;
						}
						hitTestResult.ChartArea.AxisX.ValueToPixelPosition(dataPoint.XValue);
						hitTestResult.ChartArea.AxisY.ValueToPixelPosition(dataPoint.YValues[0]);
						int pointIndex = hitTestResult.PointIndex;
						int num = pointIndex / 60;
						string text = string.Concat(str3: (pointIndex % 60).ToString("00"), str0: "Time = ", str1: num.ToString("00"), str2: ":");
						string text2 = "-";
						string name = hitTestResult.Series.Name;
						if ((checkBoxGraph8.Enabled && name == "Graph01") || name == "Graph8" || name == "Graph9")
						{
							double num2 = terrainLevelAbsolute[0] + chartValueList9[hitTestResult.PointIndex];
							text2 = "Altitude reported by drone = " + chartValueList1[hitTestResult.PointIndex].ToString("0.0") + " meter - " + (chartValueList1[hitTestResult.PointIndex] * 3.2808).ToString("0.0") + " feet\n -Altitude, relative to terrain level = " + chartValueList8[hitTestResult.PointIndex].ToString("0.0") + " meter - " + (chartValueList8[hitTestResult.PointIndex] * 3.2808).ToString("0.0") + " feet\n -Relative terrain level = " + chartValueList9[hitTestResult.PointIndex].ToString("0.0") + " meter - " + (chartValueList9[hitTestResult.PointIndex] * 3.2808).ToString("0.0") + " feet\n -Terrain over sea level = " + num2.ToString("0.0") + " meter - " + (num2 * 3.2808).ToString("0.0") + " feet\n - " + text;
						}
						else
						{
							if (!checkBoxGraph10.Enabled || !(name == "Graph2"))
							{
								switch (name)
								{
								case "Graph01":
									text2 = "Altitude = " + dataPoint.YValues[0].ToString("0.0") + " meter - " + (dataPoint.YValues[0] * 3.2808).ToString("0.0") + " feet \n - " + text;
									goto IL_0929;
								case "Graph2":
									text2 = "Speed = " + dataPoint.YValues[0].ToString("0.0") + " m/s - " + (dataPoint.YValues[0] * 3.6).ToString("0.0") + " km/h - " + (dataPoint.YValues[0] * 2.2369).ToString("0.0") + " mph \n - " + text;
									goto IL_0929;
								case "Graph3":
									text2 = "Battery = " + dataPoint.YValues[0].ToString("0") + " % \n - " + text;
									goto IL_0929;
								case "Graph4":
									text2 = "Distance to drone = " + dataPoint.YValues[0].ToString("0") + " meter / " + (dataPoint.YValues[0] * 3.2808).ToString("0.0") + " feet \n - " + text;
									goto IL_0929;
								case "Graph5":
									text2 = "Distance flown = " + dataPoint.YValues[0].ToString("0") + " meter / " + (dataPoint.YValues[0] * 3.2808).ToString("0.0") + " feet \n - " + text;
									goto IL_0929;
								case "Graph6":
									text2 = "Wi-Fi signal level = " + dataPoint.YValues[0].ToString("0") + "\n - " + text;
									goto IL_0929;
								case "Graph7":
									text2 = "Nr of GPS satellites visible= " + dataPoint.YValues[0].ToString("0") + "\n - " + text;
									goto IL_0929;
								case "Graph8":
									text2 = "Altitude, relative to terrain level = " + dataPoint.YValues[0].ToString("0.0") + "meter - " + (dataPoint.YValues[0] * 3.2808).ToString("0.0") + " feet\n - " + text;
									goto IL_0929;
								case "Graph9":
								{
									double num3 = terrainLevelAbsolute[0] + dataPoint.YValues[0];
									text2 = "Relative terrain level = " + dataPoint.YValues[0].ToString("0.0") + " meter - " + (dataPoint.YValues[0] * 3.2808).ToString("0.0") + " feet\n - Terrain over sea level = " + num3.ToString("0.0") + " meter - " + (num3 * 3.2808).ToString("0.0") + " feet\n - " + text;
									goto IL_0929;
								}
								case "Graph10":
									text2 = "AirSpeed = " + dataPoint.YValues[0].ToString("0.0") + " m/s - " + (dataPoint.YValues[0] * 3.6).ToString("0.0") + " km/h - " + (dataPoint.YValues[0] * 2.2369).ToString("0.0") + " mph \n - " + text;
									goto IL_0929;
								default:
									goto IL_0929;
								}
							}
							text2 = "Speed over ground = " + chartValueList2[hitTestResult.PointIndex].ToString("0.0") + " m/s - " + (chartValueList2[hitTestResult.PointIndex] * 3.6).ToString("0.0") + " km/h - " + (chartValueList2[hitTestResult.PointIndex] * 2.2369).ToString("0.0") + " mph \n - AirSpeed = " + chartValueList10[hitTestResult.PointIndex].ToString("0.0") + " m/s - " + (chartValueList10[hitTestResult.PointIndex] * 3.6).ToString("0.0") + " km/h - " + (chartValueList10[hitTestResult.PointIndex] * 2.2369).ToString("0.0") + " mph \n - " + text;
						}
						goto IL_0929;
						IL_0929:
						int index = checkBoxCheckedList.IndexOf(name);
						_ = checkBoxCheckedListAlias[index];
						myTooltip.Show(text2, Chart1, location.X, location.Y - 15);
					}
				}
			}
			catch (Exception)
			{
			}
		}

		private void buttonToolWebsite_Click(object sender, EventArgs e)
		{
			Process.Start("https://sites.google.com/site/pud2gpxkmlcsv/");
		}

		private void buttonGarminVirbWebsite_Click(object sender, EventArgs e)
		{
			Process.Start("http://www.garmin.com/en-US/shop/downloads/virb-edit");
		}

		private void buttonExportGraph_Click(object sender, EventArgs e)
		{
			int x = base.Location.X;
			int y = base.Location.Y;
			x += Chart1.Location.X;
			y += Chart1.Location.Y;
			int num = (base.Width - base.ClientSize.Width) / 2;
			int num2 = base.Height - base.ClientSize.Height - num;
			x += num;
			y += num2;
			int num3 = Chart1.Size.Width;
			int height = Chart1.Size.Height;
			if (checkBoxCheckedList.Count() <= 6)
			{
				if (checkBoxCheckedList.Count() > 5)
				{
					x += (int)((double)Chart1.Size.Width * 0.03);
					num3 -= (int)((double)Chart1.Size.Width * 0.04);
				}
				else if (checkBoxCheckedList.Count() > 4)
				{
					x += (int)((double)Chart1.Size.Width * 0.03);
					num3 -= (int)((double)Chart1.Size.Width * 0.08);
				}
				else if (checkBoxCheckedList.Count() > 3)
				{
					x += (int)((double)Chart1.Size.Width * 0.07);
					num3 -= (int)((double)Chart1.Size.Width * 0.11);
				}
				else if (checkBoxCheckedList.Count() > 2)
				{
					x += (int)((double)Chart1.Size.Width * 0.07);
					num3 -= (int)((double)Chart1.Size.Width * 0.14);
				}
				else if (checkBoxCheckedList.Count() > 1)
				{
					x += (int)((double)Chart1.Size.Width * 0.11);
					num3 -= (int)((double)Chart1.Size.Width * 0.2);
				}
				else
				{
					x += (int)((double)Chart1.Size.Width * 0.11);
					num3 -= (int)((double)Chart1.Size.Width * 0.23);
				}
			}
			Rectangle rectangle = new Rectangle(x, y, num3, height);
			Bitmap bitmap = new Bitmap(rectangle.Width, rectangle.Height, PixelFormat.Format64bppPArgb);
			Graphics.FromImage(bitmap).CopyFromScreen(rectangle.Left, rectangle.Top, 0, 0, bitmap.Size, CopyPixelOperation.SourceCopy);
			MessageBox.Show(this, "Select *.bmp for best quality or *.jpg for lowest file size");
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = "Bitmap Image|*.bmp|Jpeg Image|*.jpg";
			saveFileDialog.Title = "Select file to export flight data chart to - Select bmp for best quality or jpg for lowest file size";
			saveFileDialog.ShowDialog();
			if (!(saveFileDialog.FileName != ""))
			{
				return;
			}
			if (saveFileDialog.FileName.ToLower().EndsWith(".jpg") || saveFileDialog.FileName.ToLower().EndsWith(".bmp"))
			{
				if (saveFileDialog.FileName.ToLower().EndsWith(".bmp"))
				{
					bitmap.Save(saveFileDialog.FileName, ImageFormat.Bmp);
				}
				else
				{
					bitmap.Save(saveFileDialog.FileName, ImageFormat.Jpeg);
				}
			}
			else
			{
				MessageBox.Show(this, "Selected filename shall contain extension .bmp or .jpg");
			}
		}

		private void buttonLoadDataFromDroneAcademy_Click(object sender, EventArgs e)
		{
			buttonSwitchMapMode.Text = "Switch to Google Maps View";
			gmap.MapProvider = BingHybridMapProvider.Instance;
			buttonSwitchMapsGraphs.Text = "Switch to Maps";
			dataLoadedFromDroneAcademy = false;
			dataLoadedFromJsonFile = false;
			SetChartVisibility(visible: false);
			SetMapVisibility(visible: false);
			buttonSwitchMapsGraphs.Visible = false;
			textBoxStatus.BackColor = Color.White;
			buttonConvertFiles.Enabled = false;
			buttonConvertFiles.BackColor = SystemColors.Control;
			buttonExportFiles.BackColor = SystemColors.Control;
			textBoxStatus.Text = "";
			buttonExportFiles.Enabled = false;
			textBoxFileName.Text = "Load flight data from Parrot Cloud or select Json file by pressing one of the buttons here --->>";
			textBoxStatus.Text = "";
			dateTimePickerConvertFrom.Enabled = false;
			dateTimePickerConvertTo.Enabled = false;
			checkBoxIgnoreNegativeAltitude.Enabled = false;
			numericUpDownVideoGpsSyncOffset.Enabled = false;
			labelSyncOffset0.Enabled = false;
			labelSyncOffset1.Enabled = false;
			labelSyncOffset2.Enabled = false;
			labelSyncOffset3.Enabled = false;
			DisableStatTextBoxes();
			new FormGetDataFromDroneAcademy().ShowDialog();
			if (DataExchangerClass.droneAcademyFlightData == null || DataExchangerClass.droneAcademyFlightData.Length <= 1)
			{
				return;
			}
			if (DataExchangerClass.lengthOfFileInTotalSec < 6)
			{
				MessageBox.Show(this, "Selected flight contains too few valid GPS positions");
				return;
			}
			dataLoadedFromDroneAcademy = true;
			textBoxStatus.Text = "Total length of flight data " + DataExchangerClass.droneAcademyFlightDuration + " [mm:ss]   -   Press button 'Convert file' to convert flight data to Gpx, Kml and Csv --->>";
			buttonConvertFiles.Enabled = true;
			buttonConvertFiles.BackColor = Color.LightGreen;
			realDateTime = DataExchangerClass.droneAcademyFlightStartDateTime;
			startDateTime = realDateTime;
			TimeSpan utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);
			realDateTime -= utcOffset;
			DateTime value = myDateTimeRef.AddSeconds(DataExchangerClass.lengthOfFileInTotalSec);
			dateTimePickerConvertTo.Value = value;
			dateTimePickerConvertFrom.Value = myDateTimeRef;
			dateTimePickerConvertFrom.Enabled = true;
			dateTimePickerConvertTo.Enabled = true;
			checkBoxIgnoreNegativeAltitude.Enabled = true;
			numericUpDownVideoGpsSyncOffset.Enabled = true;
			labelSyncOffset0.Enabled = true;
			labelSyncOffset1.Enabled = true;
			labelSyncOffset2.Enabled = true;
			labelSyncOffset3.Enabled = true;
			labelStartConvertingAt.Enabled = true;
			labelStopConvertingAt.Enabled = true;
			textBoxFileName.Text = "Flight data loaded from Parrot Cloud - Drone model: " + DataExchangerClass.droneModel + " - Flight ID: " + DataExchangerClass.flightID + " - Start date and time: " + startDateTime.ToString("dd. MMM. yyyy - HH:mm:ss");
			buttonConvertFiles.PerformClick();
			EnableAndUpdateStatTextBoxes();
			if (DataExchangerClass.droneModel.Contains("Anafi") && numericUpDownVideoGpsSyncOffset.Value == -900m)
			{
				numericUpDownVideoGpsSyncOffset.Value = 0m;
			}
		}

		private void buttonExportFiles_Click(object sender, EventArgs e)
		{
			HelpClass.UnhideOverlaysWithoutGmetrixInVirb();
			HelpClass.SetDefaultSettingForLiquidVolumeInVirb();
			addAltitudeExtraInfoInKmlFile();
			if (checkBoxGraph8.Enabled)
			{
				for (int i = 0; i < fitValueAltitude.Count; i++)
				{
					fitValueAltitudeRelative[i] = fitValueAltitude[i] - (double)(int)chartValueList9[i];
				}
			}
			bool flag = false;
			buttonExportFiles.BackColor = SystemColors.Control;
			if (dataLoadedFromDroneAcademy)
			{
				fileNameWithoutExtension = DataExchangerClass.droneModel + "_" + DataExchangerClass.droneAcademyFlightStartDateTime.ToString("yyyy-MM-dd__HH-mm-ss");
				if (buttonSwitchMapsGraphs.Text == "Switch to Graphs")
				{
					SetChartVisibility(visible: true);
					SetMapVisibility(visible: false);
					buttonSwitchMapsGraphs.Text = "Switch to Maps";
				}
				FolderSelectDialog folderSelectDialog = new FolderSelectDialog();
				folderSelectDialog.Title = "Select a folder to export Fit, Kml and Csv files to";
				if (folderSelectDialog.ShowDialog())
				{
					filePath = folderSelectDialog.FileName;
				}
			}
			if (!(filePath != ""))
			{
				return;
			}
			try
			{
				using (StreamWriter streamWriter = new StreamWriter(filePath + "\\" + fileNameWithoutExtension + ".csv", append: false))
				{
					foreach (string csvFileLine in csvFileLines)
					{
						streamWriter.WriteLine(csvFileLine);
					}
				}
			}
			catch (Exception ex)
			{
				flag = true;
				MessageBox.Show(this, ex.Message);
			}
			try
			{
				using (StreamWriter streamWriter2 = new StreamWriter(filePath + "\\" + fileNameWithoutExtension + ".kml", append: false))
				{
					foreach (string kmlFileLine in kmlFileLines)
					{
						streamWriter2.WriteLine(kmlFileLine);
					}
				}
			}
			catch (Exception ex2)
			{
				flag = true;
				MessageBox.Show(this, ex2.Message);
			}
			try
			{
				using (StreamWriter streamWriter3 = new StreamWriter(filePath + "\\" + fileNameWithoutExtension + "_Dont_use_this_file_for_garmin_Virb_Use_fit_file_instead.gpx", append: false))
				{
					foreach (string gpxFileLine in gpxFileLines)
					{
						streamWriter3.WriteLine(gpxFileLine);
					}
				}
			}
			catch (Exception ex3)
			{
				flag = true;
				MessageBox.Show(this, ex3.Message);
			}
			if (fitValueAltitude.Max() > 649.0)
			{
				MessageBox.Show(this, "The highest altitude value in this flight is higher than the Garmin fit file supports.\n\nA maximum of 650 meter or feet can be shown in Garmin Virb Edit, all values above this will just be shown as 650 meter or feet, everything else will work fine");
			}
			else if (fitValueAltitude.Max() > 198.0)
			{
				MessageBox.Show(this, "The highest altitude value in this flight is higher than the Garmin fit file supports, but only if the fit file in feet is used.\n\nA maximum of 650 meter or feet can be shown in Garmin Virb Edit, all values above this will just be shown as 650 meter or feet, everything else will work fine. \n\nMinimise this problem by using the fit file in meter since the limitation in meter is much higher");
			}
			string liquidVolumeUnitInVirbSettings = HelpClass.GetLiquidVolumeUnitInVirbSettings();
			string speedUnitInVirbSettings = HelpClass.GetSpeedUnitInVirbSettings();
			if (liquidVolumeUnitInVirbSettings == "ukgallons")
			{
				if (fitValueAltitudeRelative.Max() * 4.55 * 3.28084 > 3276.0)
				{
					MessageBox.Show(this, "Some of the highest relative altitude values in this flight is higher than the fit file supports.\n\nMinimise this problem by changing the settings in Garmin Virb Edit.\n\nIn Virb Edit go to Menu -> Settings -> Units - Change Liquid Volume to Liters and export this flight data again");
				}
			}
			else if (liquidVolumeUnitInVirbSettings == "usgallons" && fitValueAltitudeRelative.Max() * 3.79 * 3.28084 > 3276.0)
			{
				MessageBox.Show(this, "Some of the highest relative altitude values in this flight is higher than the fit file supports.\n\nMinimise this problem by changing the settings in Garmin Virb Edit.\n\nIn Virb Edit go to Menu -> Settings -> Units - Change Liquid Volume to Liters and export this flight data again");
			}
			FitFileGenerator.GenerateFitFiles(filePath + "\\" + fileNameWithoutExtension, fitValueLat, fitValueLon, fitValueBattery, fitValueNrOfSvs, fitValueWifiSignal, fitValueSpeed, fitValueAirSpeed, fitValueAltitude, fitValueAltitudeRelative, fitValueDistanceToDrone, fitValueDistanceFlown, fitValueAccelerationXyzMilliSecTimeStamp, fitValueAccelerationXyzPitch, fitValueAccelerationXyzRoll, fitValueAccelerationXyzYaw, startDateTime, liquidVolumeUnitInVirbSettings, speedUnitInVirbSettings);
			if (!flag)
			{
				if (reportStatistics)
				{
					GoogleTracker.trackEvent("Files exported");
				}
				if (MessageBox.Show(this, "Files exported successfully\n\nOpen Kml file in Google Earth now?", "Files exported succesfully", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
				{
					Process.Start(filePath + "\\" + fileNameWithoutExtension + ".kml");
				}
			}
		}

		private void buttonLoadDataFromDroneAcademy_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(buttonLoadDataFromDroneAcademy, "Press here to get flight data from your account on the Parrot Cloud");
		}

		private void buttonLoadPudFile_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(buttonLoadPudFile, "Press here if you have a Json file from My.Parrot Cloud");
		}

		private void buttonConvertFiles_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(buttonConvertFiles, "Press here to convert the flight data and see a graph of the flight");
		}

		private void buttonExportFiles_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(buttonExportFiles, "Press here to export the flight data into Fit, Kml and Csv files on your PC");
		}

		private void buttonSwitchMapsGraphs_Click(object sender, EventArgs e)
		{
			myTooltip.RemoveAll();
			if (buttonSwitchMapsGraphs.Text == "Switch to Maps")
			{
				SetChartVisibility(visible: false);
				SetMapVisibility(visible: true);
				buttonSwitchMapsGraphs.Text = "Switch to Graphs";
			}
			else
			{
				SetChartVisibility(visible: true);
				SetMapVisibility(visible: false);
				buttonSwitchMapsGraphs.Text = "Switch to Maps";
			}
		}

		private void initMaps()
		{
			gmap.MapProvider = BingHybridMapProvider.Instance;
			Singleton<GMaps>.Instance.Mode = AccessMode.ServerAndCache;
			gmap.MinZoom = 7;
			gmap.MaxZoom = 19;
			gmap.ShowCenter = false;
			buttonSwitchMapMode.Text = "Switch to Google Maps View";
		}

		private void buttonZoomIn_Click(object sender, EventArgs e)
		{
			gmap.Zoom += 1.0;
		}

		private void buttonZoomOut_Click(object sender, EventArgs e)
		{
			gmap.Zoom -= 1.0;
		}

		private void button1_Click(object sender, EventArgs e)
		{
		}

		private void buttonSwitchMapMode_Click(object sender, EventArgs e)
		{
			if (buttonSwitchMapMode.Text == "Switch to Google Maps View")
			{
				buttonSwitchMapMode.Text = "Switch to Google Earth View";
				gmap.MapProvider = GoogleMapProvider.Instance;
			}
			else if (buttonSwitchMapMode.Text == "Switch to Bing Satellite View")
			{
				buttonSwitchMapMode.Text = "Switch to Google Maps View";
				gmap.MapProvider = BingHybridMapProvider.Instance;
			}
			else
			{
				buttonSwitchMapMode.Text = "Switch to Bing Satellite View";
				gmap.MapProvider = GoogleHybridMapProvider.Instance;
			}
		}

		private void buttonConfigureKmlFile_Click(object sender, EventArgs e)
		{
			new FormConfigureKmlFile().ShowDialog();
			buttonConvertFiles.PerformClick();
		}

		private void gmap_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			PointLatLng position = gmap.FromLocalToLatLng(e.X, e.Y);
			gmap.Position = position;
			if (e.Button == MouseButtons.Left)
			{
				gmap.Zoom++;
			}
			else
			{
				gmap.Zoom--;
			}
		}

		private void checkBoxIgnoreNegativeAltitude_CheckedChanged(object sender, EventArgs e)
		{
			buttonConvertFiles.PerformClick();
		}

		private void buttonShowSharedPicVideo_Click(object sender, EventArgs e)
		{
			DataExchangerClass.myOwnFlight = false;
			new FormShowSharedMedia().ShowDialog();
		}

		private string GetValueOfField(string stringToSearch, string nameOfField, string nameOfFieldFriendly, int myStartIndex = 0, bool useLastValue = false)
		{
			string text = "";
			if (!ErrorInDecodingFlightList)
			{
				try
				{
					int length = nameOfField.Length;
					int num = 0;
					num = ((!useLastValue) ? stringToSearch.IndexOf(nameOfField, myStartIndex) : stringToSearch.LastIndexOf(nameOfField));
					if (num <= -1)
					{
						throw new Exception("Exception generated by GetValueOfField method due to value of posOfFieldNameStart is less than 1");
					}
					int num2 = stringToSearch.IndexOf(",", num + 1);
					if (num2 < 1)
					{
						num2 = stringToSearch.Length;
					}
					text = stringToSearch.Substring(num + length, num2 - num - length);
					text = text.Trim();
					text = text.Replace("\"", "");
					text = text.Replace("}", "");
					text = text.Replace("]", "");
					return text;
				}
				catch (Exception ex)
				{
					ErrorInDecodingFlightList = true;
					string text2 = "Failed to decode data from a flight from Parrot Cloud, this flight will be ignored \nPress OK to continue \n\n Error details: \nAn error occurred when trying to decode field: " + nameOfFieldFriendly + " \n" + ex.Message + "\n\nData from flight= \n" + stringToSearch;
					MessageBox.Show(this, text2);
					return text;
				}
			}
			return text;
		}

		private void numericUpDownVideoGpsSyncOffset_showTooltip(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(numericUpDownVideoGpsSyncOffset, "Used for synchronizing GPS and video in Garmin Virb so video and GPS overlays can be in sync.\nIf video is ahead of 'Orientation of drone' in Garmin Virb, then set it to a negative value, otherwise set it to positive value");
		}

		private void label5_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(labelSyncOffset0, "Used for synchronizing GPS and video in Garmin Virb so video and GPS overlays can be in sync.\nIf video is ahead of 'Orientation of drone' in Garmin Virb, then set it to a negative value, otherwise set it to positive value");
		}

		private void label6_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(labelSyncOffset1, "Used for synchronizing GPS and video in Garmin Virb so video and GPS overlays can be in sync.\nIf video is ahead of 'Orientation of drone' in Garmin Virb, then set it to a negative value, otherwise set it to positive value");
		}

		private void DisableStatTextBoxes()
		{
			textBoxStatMaxSpeed.Enabled = false;
			textBoxStatMaxAlt.Enabled = false;
			textBoxStatMaxDist.Enabled = false;
			textBoxStatDistFlown.Enabled = false;
			textBoxStatComments.Enabled = false;
			textBoxStatMaxSpeed.Text = "Max Speed:";
			textBoxStatMaxAlt.Text = "Max Altitude:";
			textBoxStatMaxDist.Text = "Max Distance:";
			textBoxStatDistFlown.Text = "Distance Flown:";
			textBoxStatComments.Text = "No comments for this flight";
		}

		private void EnableAndUpdateStatTextBoxes()
		{
			textBoxStatMaxSpeed.Enabled = true;
			textBoxStatMaxAlt.Enabled = true;
			textBoxStatMaxDist.Enabled = true;
			textBoxStatDistFlown.Enabled = true;
			textBoxStatComments.Enabled = true;
			textBoxStatMaxSpeed.BackColor = Color.FromArgb(244, 244, 244);
			textBoxStatMaxSpeed.ReadOnly = true;
			textBoxStatMaxAlt.BackColor = Color.FromArgb(244, 244, 244);
			textBoxStatMaxAlt.ReadOnly = true;
			textBoxStatMaxDist.BackColor = Color.FromArgb(244, 244, 244);
			textBoxStatMaxDist.ReadOnly = true;
			textBoxStatDistFlown.BackColor = Color.FromArgb(244, 244, 244);
			textBoxStatDistFlown.ReadOnly = true;
			textBoxStatComments.BackColor = Color.FromArgb(244, 244, 244);
			textBoxStatComments.ReadOnly = true;
			if (chartValueList1.Count > 2)
			{
				textBoxStatMaxSpeed.Text = "Max Speed: " + chartValueList2.Max().ToString("0.0") + " m/s";
				textBoxStatMaxAlt.Text = "Max Altitude: " + chartValueList1.Max().ToString("0.0") + " m";
				textBoxStatMaxDist.Text = "Max Distance: " + chartValueList4.Max().ToString("0") + " m";
				textBoxStatDistFlown.Text = "Distance Flown: " + chartValueList5.Max().ToString("0") + " m";
			}
			if (dataLoadedFromDroneAcademy)
			{
				string flightCommentFromFlightCommentsTxt = GetFlightCommentFromFlightCommentsTxt(DataExchangerClass.flightID);
				if (flightCommentFromFlightCommentsTxt == "")
				{
					textBoxStatComments.Text = "No comments for this flight, doubleclick to add a comment";
				}
				else
				{
					textBoxStatComments.Text = flightCommentFromFlightCommentsTxt;
				}
			}
		}

		private void textBoxStatMaxSpeed_MouseHover(object sender, EventArgs e)
		{
			double num = chartValueList2.Max();
			new ToolTip().SetToolTip(textBoxStatMaxSpeed, "Max Speed: " + num.ToString("0.0") + " m/s - " + (num * 3.6).ToString("0.0") + " km/h - " + (num * 2.2369).ToString("0.0") + " mph");
		}

		private void pictureBox1_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(pictureBox1, "Click here if you want to make a donation to support further development of the FlightData Manager");
		}

		private void textBoxStatMaxAlt_MouseHover(object sender, EventArgs e)
		{
			double num = chartValueList1.Max();
			new ToolTip().SetToolTip(textBoxStatMaxAlt, "Max Altitude: " + num.ToString("0.0") + " m - " + (num * 3.2808).ToString("0.0") + " feet");
		}

		private void textBoxStatMaxDist_MouseHover(object sender, EventArgs e)
		{
			double num = chartValueList4.Max();
			new ToolTip().SetToolTip(textBoxStatMaxDist, "Max Distance: " + num.ToString("0.0") + " m - " + (num * 3.2808).ToString("0.0") + " feet");
		}

		private void textBoxStatDistFlown_MouseHover(object sender, EventArgs e)
		{
			double num = chartValueList5.Max();
			new ToolTip().SetToolTip(textBoxStatDistFlown, "Distance Flown: " + num.ToString("0.0") + " m - " + (num * 3.2808).ToString("0.0") + " feet");
		}

		private void textBoxStatComments_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(textBoxStatComments, "Double click to add or edit a comment for this flight");
		}

		private string GetFlightCommentFromFlightCommentsTxt(string flightId)
		{
			string result = "";
			string text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\");
			if (Directory.Exists(text))
			{
				string path = Path.Combine(text, "FlightComments.txt");
				if (File.Exists(path))
				{
					string text2 = "";
					StreamReader streamReader = new StreamReader(path);
					while ((text2 = streamReader.ReadLine()) != null)
					{
						if (text2.StartsWith(flightId + ";"))
						{
							result = text2.Substring(text2.IndexOf(";") + 1);
						}
					}
					streamReader.Close();
				}
			}
			return result;
		}

		private void textBoxStatComments_DoubleClick(object sender, EventArgs e)
		{
			if (!dataLoadedFromDroneAcademy)
			{
				return;
			}
			MessageBoxInputTextWithDefaultText messageBoxInputTextWithDefaultText = new MessageBoxInputTextWithDefaultText("Edit a comment for current flight", "Edit a comment for current flight below\nComments are only saved on your own PC", "Save comment", "Cancel");
			if (textBoxStatComments.Text == "No comments for this flight, doubleclick to add a comment")
			{
				MessageBoxInputTextWithDefaultText.defaultText = "";
			}
			else
			{
				MessageBoxInputTextWithDefaultText.defaultText = textBoxStatComments.Text;
			}
			messageBoxInputTextWithDefaultText.ShowDialog();
			if (MessageBoxInputTextWithDefaultText.dialogAdvancedResult == 1)
			{
				string text = MessageBoxInputTextWithDefaultText.textResult.Trim();
				if (text != "")
				{
					textBoxStatComments.Text = text;
					updateCommentInFlightCommentsTxtFile(DataExchangerClass.flightID, text);
				}
			}
		}

		private void updateCommentInFlightCommentsTxtFile(string flightID, string comment)
		{
			string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FlightDataManager\\FlightComments.txt");
			try
			{
				List<string> list = new List<string>();
				try
				{
					list = File.ReadAllLines(path).ToList();
					list = list.Where((string x) => x.IndexOf(flightID + ";") < 0).ToList();
				}
				catch
				{
				}
				list.Add(flightID + ";" + comment);
				File.WriteAllLines(path, list.ToArray());
			}
			catch
			{
			}
		}

		private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
		{
			new FormDonate().ShowDialog();
		}

		private void buttonDonate_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(buttonDonate, "Click here if you want to make a donation to support further development of the FlightData Manager");
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
		}

		private void buttonInstallGarminVirbTemplates_MouseHover(object sender, EventArgs e)
		{
			new ToolTip().SetToolTip(buttonInstallGarminVirbTemplates, "Click here to install Garmin Virb GPS Templates designed for Parrot Anafi, Bebop and Disco, furthermore you can see a demo video of all the available templates");
		}

		private void ChangeToHighResolution()
		{
			int num = pixelsToMoveInLowHighResolution;
			textBoxStatMaxSpeed.Visible = true;
			textBoxStatMaxAlt.Visible = true;
			textBoxStatMaxDist.Visible = true;
			textBoxStatDistFlown.Visible = true;
			textBoxStatComments.Visible = true;
			pictureBox1.Visible = true;
			buttonLoadDataFromDroneAcademy.Location = new Point(buttonLoadDataFromDroneAcademy.Location.X + num, buttonLoadDataFromDroneAcademy.Location.Y);
			buttonLoadPudFile.Location = new Point(buttonLoadPudFile.Location.X + num, buttonLoadPudFile.Location.Y);
			buttonInstallGarminVirbTemplates.Location = new Point(buttonInstallGarminVirbTemplates.Location.X + num, buttonInstallGarminVirbTemplates.Location.Y);
			buttonConfigureKmlFile.Location = new Point(buttonConfigureKmlFile.Location.X + num, buttonConfigureKmlFile.Location.Y);
			buttonToolWebsite.Location = new Point(buttonToolWebsite.Location.X + num, buttonToolWebsite.Location.Y);
			buttonConvertFiles.Location = new Point(buttonConvertFiles.Location.X + num, buttonConvertFiles.Location.Y);
			buttonExportFiles.Location = new Point(buttonExportFiles.Location.X + num, buttonExportFiles.Location.Y);
			buttonDonate.Location = new Point(buttonDonate.Location.X + num, buttonDonate.Location.Y);
			buttonGarminVirbWebsite.Location = new Point(buttonGarminVirbWebsite.Location.X + num, buttonGarminVirbWebsite.Location.Y);
			textBoxFileName.Size = new Size(textBoxFileName.Size.Width + num, textBoxFileName.Size.Height);
			textBoxStatus.Size = new Size(textBoxStatus.Size.Width + num, textBoxStatus.Size.Height);
		}

		private void ChangeToLowResolution()
		{
			pixelsToMoveInLowHighResolution = buttonLoadDataFromDroneAcademy.Location.X - textBoxStatMaxSpeed.Location.X;
			int num = -pixelsToMoveInLowHighResolution;
			textBoxStatMaxSpeed.Visible = false;
			textBoxStatMaxAlt.Visible = false;
			textBoxStatMaxDist.Visible = false;
			textBoxStatDistFlown.Visible = false;
			textBoxStatComments.Visible = false;
			pictureBox1.Visible = false;
			buttonLoadDataFromDroneAcademy.Location = new Point(buttonLoadDataFromDroneAcademy.Location.X + num, buttonLoadDataFromDroneAcademy.Location.Y);
			buttonLoadPudFile.Location = new Point(buttonLoadPudFile.Location.X + num, buttonLoadPudFile.Location.Y);
			buttonInstallGarminVirbTemplates.Location = new Point(buttonInstallGarminVirbTemplates.Location.X + num, buttonInstallGarminVirbTemplates.Location.Y);
			buttonConfigureKmlFile.Location = new Point(buttonConfigureKmlFile.Location.X + num, buttonConfigureKmlFile.Location.Y);
			buttonToolWebsite.Location = new Point(buttonToolWebsite.Location.X + num, buttonToolWebsite.Location.Y);
			buttonConvertFiles.Location = new Point(buttonConvertFiles.Location.X + num, buttonConvertFiles.Location.Y);
			buttonExportFiles.Location = new Point(buttonExportFiles.Location.X + num, buttonExportFiles.Location.Y);
			buttonDonate.Location = new Point(buttonDonate.Location.X + num, buttonDonate.Location.Y);
			buttonGarminVirbWebsite.Location = new Point(buttonGarminVirbWebsite.Location.X + num, buttonGarminVirbWebsite.Location.Y);
			textBoxFileName.Size = new Size(textBoxFileName.Size.Width + num, textBoxFileName.Size.Height);
			textBoxStatus.Size = new Size(textBoxStatus.Size.Width + num, textBoxStatus.Size.Height);
		}

		private void FormMain_SizeChanged(object sender, EventArgs e)
		{
			if (!formSizeComplete)
			{
				return;
			}
			int height = base.ClientSize.Height - (Chart1.Location.Y + (int)((double)buttonSwitchMapMode.Height * 1.7));
			Chart1.Height = height;
			gmap.Height = height;
			Chart1.Width = base.ClientSize.Width - Chart1.Location.X * 2;
			gmap.Width = base.ClientSize.Width - gmap.Location.X * 2;
			int y = base.ClientSize.Height - (int)((double)buttonSwitchMapsGraphs.Height * 1.4);
			buttonSwitchMapsGraphs.Location = new Point(buttonSwitchMapsGraphs.Location.X, y);
			buttonSwitchMapMode.Location = new Point(buttonSwitchMapMode.Location.X, y);
			buttonExportGraph.Location = new Point(buttonExportGraph.Location.X, y);
			buttonZoomIn.Location = new Point(buttonZoomIn.Location.X, y);
			buttonZoomOut.Location = new Point(buttonZoomOut.Location.X, y);
			int y2 = base.ClientSize.Height - (int)((double)buttonSwitchMapsGraphs.Height * 0.2 + (double)checkBoxGraph1.Height);
			checkBoxGraph6.Location = new Point(checkBoxGraph6.Location.X, y2);
			checkBoxGraph7.Location = new Point(checkBoxGraph7.Location.X, y2);
			checkBoxGraph8.Location = new Point(checkBoxGraph8.Location.X, y2);
			checkBoxGraph9.Location = new Point(checkBoxGraph9.Location.X, y2);
			int y3 = base.ClientSize.Height - (int)((double)buttonSwitchMapsGraphs.Height * 0.2 + (double)checkBoxGraph1.Height * 0.7);
			labelColorGraph6.Location = new Point(labelColorGraph6.Location.X, y3);
			labelColorGraph7.Location = new Point(labelColorGraph7.Location.X, y3);
			labelColorGraph8.Location = new Point(labelColorGraph8.Location.X, y3);
			labelColorGraph9.Location = new Point(labelColorGraph9.Location.X, y3);
			int y4 = base.ClientSize.Height - (int)((double)buttonSwitchMapsGraphs.Height * 1.6);
			checkBoxGraph1.Location = new Point(checkBoxGraph1.Location.X, y4);
			checkBoxGraph2.Location = new Point(checkBoxGraph2.Location.X, y4);
			checkBoxGraph3.Location = new Point(checkBoxGraph3.Location.X, y4);
			checkBoxGraph4.Location = new Point(checkBoxGraph4.Location.X, y4);
			checkBoxGraph5.Location = new Point(checkBoxGraph5.Location.X, y4);
			checkBoxGraph10.Location = new Point(checkBoxGraph10.Location.X, y4);
			int y5 = base.ClientSize.Height - (int)((double)buttonSwitchMapsGraphs.Height * 1.35);
			labelColorGraph1.Location = new Point(labelColorGraph1.Location.X, y5);
			labelColorGraph2.Location = new Point(labelColorGraph2.Location.X, y5);
			labelColorGraph3.Location = new Point(labelColorGraph3.Location.X, y5);
			labelColorGraph4.Location = new Point(labelColorGraph4.Location.X, y5);
			labelColorGraph5.Location = new Point(labelColorGraph5.Location.X, y5);
			labelColorGraph10.Location = new Point(labelColorGraph10.Location.X, y5);
			int num = base.ClientSize.Width - (labelDevelopedBy.Width + buttonZoomIn.Height / 2);
			if (num < buttonExportGraph.Location.X + buttonExportGraph.Width + buttonExportGraph.Height / 2)
			{
				num = buttonExportGraph.Location.X + buttonExportGraph.Width + buttonExportGraph.Height / 2;
			}
			labelDevelopedBy.Location = new Point(num, base.ClientSize.Height - (int)((double)buttonSwitchMapsGraphs.Height * 1.45));
			labelMailAddress.Location = new Point(num, base.ClientSize.Height - (int)((double)buttonSwitchMapsGraphs.Height * 0.9));
			labelPanMapInfo.Location = new Point(labelPanMapInfo.Location.X, base.ClientSize.Height - (int)((double)buttonSwitchMapsGraphs.Height * 1.2));
			if (limitForHighLowResolution == 0)
			{
				limitForHighLowResolution = buttonLoadPudFile.Location.X + buttonLoadPudFile.Width;
			}
			if (base.Width > limitForHighLowResolution)
			{
				if (!highScreenResolution)
				{
					ChangeToHighResolution();
					highScreenResolution = true;
				}
			}
			else if (highScreenResolution)
			{
				ChangeToLowResolution();
				highScreenResolution = false;
			}
		}

		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.FormMain));
			buttonLoadPudFile = new System.Windows.Forms.Button();
			textBoxFileName = new System.Windows.Forms.TextBox();
			buttonConvertFiles = new System.Windows.Forms.Button();
			textBoxStatus = new System.Windows.Forms.TextBox();
			dateTimePickerConvertFrom = new System.Windows.Forms.DateTimePicker();
			dateTimePickerConvertTo = new System.Windows.Forms.DateTimePicker();
			labelStartConvertingAt = new System.Windows.Forms.Label();
			labelStopConvertingAt = new System.Windows.Forms.Label();
			buttonInstallGarminVirbTemplates = new System.Windows.Forms.Button();
			buttonDonate = new System.Windows.Forms.Button();
			Chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			labelColorGraph4 = new System.Windows.Forms.Label();
			labelColorGraph3 = new System.Windows.Forms.Label();
			labelColorGraph2 = new System.Windows.Forms.Label();
			labelColorGraph1 = new System.Windows.Forms.Label();
			checkBoxGraph4 = new System.Windows.Forms.CheckBox();
			checkBoxGraph3 = new System.Windows.Forms.CheckBox();
			checkBoxGraph2 = new System.Windows.Forms.CheckBox();
			checkBoxGraph1 = new System.Windows.Forms.CheckBox();
			buttonToolWebsite = new System.Windows.Forms.Button();
			buttonGarminVirbWebsite = new System.Windows.Forms.Button();
			labelSolidLineLeft = new System.Windows.Forms.Label();
			labelDevelopedBy = new System.Windows.Forms.Label();
			buttonExportGraph = new System.Windows.Forms.Button();
			labelMailAddress = new System.Windows.Forms.Label();
			buttonLoadDataFromDroneAcademy = new System.Windows.Forms.Button();
			buttonExportFiles = new System.Windows.Forms.Button();
			gmap = new GMap.NET.WindowsForms.GMapControl();
			buttonSwitchMapsGraphs = new System.Windows.Forms.Button();
			buttonZoomIn = new System.Windows.Forms.Button();
			buttonZoomOut = new System.Windows.Forms.Button();
			labelPanMapInfo = new System.Windows.Forms.Label();
			buttonSwitchMapMode = new System.Windows.Forms.Button();
			buttonConfigureKmlFile = new System.Windows.Forms.Button();
			LabelSolidLineRight = new System.Windows.Forms.Label();
			labelColorGraph5 = new System.Windows.Forms.Label();
			checkBoxGraph5 = new System.Windows.Forms.CheckBox();
			checkBoxIgnoreNegativeAltitude = new System.Windows.Forms.CheckBox();
			numericUpDownVideoGpsSyncOffset = new System.Windows.Forms.NumericUpDown();
			labelSyncOffset0 = new System.Windows.Forms.Label();
			labelSyncOffset1 = new System.Windows.Forms.Label();
			checkBoxGraph6 = new System.Windows.Forms.CheckBox();
			checkBoxGraph7 = new System.Windows.Forms.CheckBox();
			labelColorGraph6 = new System.Windows.Forms.Label();
			labelColorGraph7 = new System.Windows.Forms.Label();
			checkBoxGraph8 = new System.Windows.Forms.CheckBox();
			checkBoxGraph9 = new System.Windows.Forms.CheckBox();
			labelColorGraph8 = new System.Windows.Forms.Label();
			labelColorGraph9 = new System.Windows.Forms.Label();
			pictureBox1 = new System.Windows.Forms.PictureBox();
			textBoxStatMaxSpeed = new System.Windows.Forms.TextBox();
			textBoxStatMaxAlt = new System.Windows.Forms.TextBox();
			textBoxStatDistFlown = new System.Windows.Forms.TextBox();
			textBoxStatMaxDist = new System.Windows.Forms.TextBox();
			textBoxStatComments = new System.Windows.Forms.TextBox();
			checkBoxGraph10 = new System.Windows.Forms.CheckBox();
			labelColorGraph10 = new System.Windows.Forms.Label();
			labelSyncOffset2 = new System.Windows.Forms.Label();
			labelSyncOffset3 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)Chart1).BeginInit();
			((System.ComponentModel.ISupportInitialize)numericUpDownVideoGpsSyncOffset).BeginInit();
			((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
			SuspendLayout();
			buttonLoadPudFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
			buttonLoadPudFile.Location = new System.Drawing.Point(1260, 14);
			buttonLoadPudFile.Margin = new System.Windows.Forms.Padding(4);
			buttonLoadPudFile.Name = "buttonLoadPudFile";
			buttonLoadPudFile.Size = new System.Drawing.Size(189, 27);
			buttonLoadPudFile.TabIndex = 0;
			buttonLoadPudFile.TabStop = false;
			buttonLoadPudFile.Text = "Import from json file";
			buttonLoadPudFile.UseVisualStyleBackColor = true;
			buttonLoadPudFile.Click += new System.EventHandler(buttonLoadPudFile_Click);
			buttonLoadPudFile.MouseHover += new System.EventHandler(buttonLoadPudFile_MouseHover);
			textBoxFileName.BackColor = System.Drawing.SystemColors.Window;
			textBoxFileName.Location = new System.Drawing.Point(31, 15);
			textBoxFileName.Margin = new System.Windows.Forms.Padding(4);
			textBoxFileName.Name = "textBoxFileName";
			textBoxFileName.ReadOnly = true;
			textBoxFileName.ShortcutsEnabled = false;
			textBoxFileName.Size = new System.Drawing.Size(947, 22);
			textBoxFileName.TabIndex = 1;
			buttonConvertFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
			buttonConvertFiles.Location = new System.Drawing.Point(1017, 47);
			buttonConvertFiles.Margin = new System.Windows.Forms.Padding(4);
			buttonConvertFiles.Name = "buttonConvertFiles";
			buttonConvertFiles.Size = new System.Drawing.Size(235, 27);
			buttonConvertFiles.TabIndex = 2;
			buttonConvertFiles.Text = "Convert flight data";
			buttonConvertFiles.UseVisualStyleBackColor = true;
			buttonConvertFiles.Click += new System.EventHandler(buttonConvertFiles_Click);
			buttonConvertFiles.MouseHover += new System.EventHandler(buttonConvertFiles_MouseHover);
			textBoxStatus.BackColor = System.Drawing.SystemColors.Window;
			textBoxStatus.Location = new System.Drawing.Point(31, 48);
			textBoxStatus.Margin = new System.Windows.Forms.Padding(4);
			textBoxStatus.Name = "textBoxStatus";
			textBoxStatus.ReadOnly = true;
			textBoxStatus.Size = new System.Drawing.Size(947, 22);
			textBoxStatus.TabIndex = 3;
			dateTimePickerConvertFrom.Location = new System.Drawing.Point(193, 89);
			dateTimePickerConvertFrom.Margin = new System.Windows.Forms.Padding(4);
			dateTimePickerConvertFrom.Name = "dateTimePickerConvertFrom";
			dateTimePickerConvertFrom.Size = new System.Drawing.Size(67, 22);
			dateTimePickerConvertFrom.TabIndex = 4;
			dateTimePickerConvertFrom.TabStop = false;
			dateTimePickerConvertTo.Location = new System.Drawing.Point(193, 117);
			dateTimePickerConvertTo.Margin = new System.Windows.Forms.Padding(4);
			dateTimePickerConvertTo.Name = "dateTimePickerConvertTo";
			dateTimePickerConvertTo.Size = new System.Drawing.Size(67, 22);
			dateTimePickerConvertTo.TabIndex = 5;
			labelStartConvertingAt.AccessibleRole = System.Windows.Forms.AccessibleRole.Equation;
			labelStartConvertingAt.AutoSize = true;
			labelStartConvertingAt.Enabled = false;
			labelStartConvertingAt.Location = new System.Drawing.Point(27, 91);
			labelStartConvertingAt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelStartConvertingAt.Name = "labelStartConvertingAt";
			labelStartConvertingAt.Size = new System.Drawing.Size(156, 17);
			labelStartConvertingAt.TabIndex = 8;
			labelStartConvertingAt.Text = "Start converting data at";
			labelStopConvertingAt.AutoSize = true;
			labelStopConvertingAt.Enabled = false;
			labelStopConvertingAt.Location = new System.Drawing.Point(27, 121);
			labelStopConvertingAt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelStopConvertingAt.Name = "labelStopConvertingAt";
			labelStopConvertingAt.Size = new System.Drawing.Size(155, 17);
			labelStopConvertingAt.TabIndex = 9;
			labelStopConvertingAt.Text = "Stop converting data at";
			buttonInstallGarminVirbTemplates.BackColor = System.Drawing.SystemColors.Control;
			buttonInstallGarminVirbTemplates.Location = new System.Drawing.Point(1017, 79);
			buttonInstallGarminVirbTemplates.Margin = new System.Windows.Forms.Padding(4);
			buttonInstallGarminVirbTemplates.Name = "buttonInstallGarminVirbTemplates";
			buttonInstallGarminVirbTemplates.Size = new System.Drawing.Size(433, 28);
			buttonInstallGarminVirbTemplates.TabIndex = 17;
			buttonInstallGarminVirbTemplates.Text = "Install Garmin Virb GPS Templates designed for Parrot drones";
			buttonInstallGarminVirbTemplates.UseVisualStyleBackColor = false;
			buttonInstallGarminVirbTemplates.Click += new System.EventHandler(buttonInstallGarminVirbTemplates_Click);
			buttonInstallGarminVirbTemplates.MouseHover += new System.EventHandler(buttonInstallGarminVirbTemplates_MouseHover);
			buttonDonate.Location = new System.Drawing.Point(1017, 143);
			buttonDonate.Margin = new System.Windows.Forms.Padding(4);
			buttonDonate.Name = "buttonDonate";
			buttonDonate.Size = new System.Drawing.Size(236, 28);
			buttonDonate.TabIndex = 18;
			buttonDonate.Text = "Make a donation to the author";
			buttonDonate.UseVisualStyleBackColor = true;
			buttonDonate.Click += new System.EventHandler(buttonDonate_Click);
			buttonDonate.MouseHover += new System.EventHandler(buttonDonate_MouseHover);
			chartArea.Name = "ChartArea1";
			Chart1.ChartAreas.Add(chartArea);
			legend.Name = "Legend1";
			Chart1.Legends.Add(legend);
			Chart1.Location = new System.Drawing.Point(16, 190);
			Chart1.Margin = new System.Windows.Forms.Padding(4);
			Chart1.Name = "Chart1";
			Chart1.Size = new System.Drawing.Size(1435, 623);
			Chart1.TabIndex = 19;
			Chart1.Text = "chart1";
			Chart1.MouseMove += new System.Windows.Forms.MouseEventHandler(chart1_MouseMove);
			labelColorGraph4.AutoSize = true;
			labelColorGraph4.BackColor = System.Drawing.Color.Orange;
			labelColorGraph4.Location = new System.Drawing.Point(808, 825);
			labelColorGraph4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph4.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph4.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph4.Name = "labelColorGraph4";
			labelColorGraph4.Size = new System.Drawing.Size(33, 7);
			labelColorGraph4.TabIndex = 27;
			labelColorGraph4.Text = " ";
			labelColorGraph3.AutoSize = true;
			labelColorGraph3.BackColor = System.Drawing.Color.Blue;
			labelColorGraph3.Location = new System.Drawing.Point(655, 825);
			labelColorGraph3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph3.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph3.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph3.Name = "labelColorGraph3";
			labelColorGraph3.Size = new System.Drawing.Size(33, 7);
			labelColorGraph3.TabIndex = 26;
			labelColorGraph3.Text = " ";
			labelColorGraph2.AutoSize = true;
			labelColorGraph2.BackColor = System.Drawing.Color.Green;
			labelColorGraph2.Location = new System.Drawing.Point(381, 825);
			labelColorGraph2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph2.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph2.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph2.Name = "labelColorGraph2";
			labelColorGraph2.Size = new System.Drawing.Size(33, 7);
			labelColorGraph2.TabIndex = 25;
			labelColorGraph2.Text = " ";
			labelColorGraph1.AutoSize = true;
			labelColorGraph1.BackColor = System.Drawing.Color.Red;
			labelColorGraph1.Location = new System.Drawing.Point(252, 825);
			labelColorGraph1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph1.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph1.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph1.Name = "labelColorGraph1";
			labelColorGraph1.Size = new System.Drawing.Size(33, 7);
			labelColorGraph1.TabIndex = 24;
			labelColorGraph1.Text = " ";
			checkBoxGraph4.AutoSize = true;
			checkBoxGraph4.Checked = true;
			checkBoxGraph4.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph4.Location = new System.Drawing.Point(723, 817);
			checkBoxGraph4.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph4.Name = "checkBoxGraph4";
			checkBoxGraph4.Size = new System.Drawing.Size(85, 21);
			checkBoxGraph4.TabIndex = 23;
			checkBoxGraph4.Text = "Distance";
			checkBoxGraph4.UseVisualStyleBackColor = true;
			checkBoxGraph4.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			checkBoxGraph3.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			checkBoxGraph3.AutoSize = true;
			checkBoxGraph3.Checked = true;
			checkBoxGraph3.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph3.Location = new System.Drawing.Point(580, 817);
			checkBoxGraph3.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph3.Name = "checkBoxGraph3";
			checkBoxGraph3.Size = new System.Drawing.Size(75, 21);
			checkBoxGraph3.TabIndex = 22;
			checkBoxGraph3.Text = "Battery";
			checkBoxGraph3.UseVisualStyleBackColor = true;
			checkBoxGraph3.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			checkBoxGraph2.AutoSize = true;
			checkBoxGraph2.Checked = true;
			checkBoxGraph2.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph2.Location = new System.Drawing.Point(311, 817);
			checkBoxGraph2.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph2.Name = "checkBoxGraph2";
			checkBoxGraph2.Size = new System.Drawing.Size(71, 21);
			checkBoxGraph2.TabIndex = 21;
			checkBoxGraph2.Text = "Speed";
			checkBoxGraph2.UseVisualStyleBackColor = true;
			checkBoxGraph2.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			checkBoxGraph1.AutoSize = true;
			checkBoxGraph1.Checked = true;
			checkBoxGraph1.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph1.Location = new System.Drawing.Point(177, 817);
			checkBoxGraph1.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph1.Name = "checkBoxGraph1";
			checkBoxGraph1.Size = new System.Drawing.Size(77, 21);
			checkBoxGraph1.TabIndex = 20;
			checkBoxGraph1.Text = "Altitude";
			checkBoxGraph1.UseVisualStyleBackColor = true;
			checkBoxGraph1.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			buttonToolWebsite.Location = new System.Drawing.Point(1260, 110);
			buttonToolWebsite.Margin = new System.Windows.Forms.Padding(4);
			buttonToolWebsite.Name = "buttonToolWebsite";
			buttonToolWebsite.Size = new System.Drawing.Size(189, 28);
			buttonToolWebsite.TabIndex = 28;
			buttonToolWebsite.Text = "FlightData Manager Web";
			buttonToolWebsite.UseVisualStyleBackColor = true;
			buttonToolWebsite.Click += new System.EventHandler(buttonToolWebsite_Click);
			buttonGarminVirbWebsite.Location = new System.Drawing.Point(1260, 143);
			buttonGarminVirbWebsite.Margin = new System.Windows.Forms.Padding(4);
			buttonGarminVirbWebsite.Name = "buttonGarminVirbWebsite";
			buttonGarminVirbWebsite.Size = new System.Drawing.Size(189, 28);
			buttonGarminVirbWebsite.TabIndex = 29;
			buttonGarminVirbWebsite.Text = "Garmin Virb Edit website";
			buttonGarminVirbWebsite.UseVisualStyleBackColor = true;
			buttonGarminVirbWebsite.Click += new System.EventHandler(buttonGarminVirbWebsite_Click);
			labelSolidLineLeft.AutoSize = true;
			labelSolidLineLeft.BackColor = System.Drawing.Color.Black;
			labelSolidLineLeft.Location = new System.Drawing.Point(28, 176);
			labelSolidLineLeft.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelSolidLineLeft.MaximumSize = new System.Drawing.Size(1327, 2);
			labelSolidLineLeft.MinimumSize = new System.Drawing.Size(1327, 2);
			labelSolidLineLeft.Name = "labelSolidLineLeft";
			labelSolidLineLeft.Size = new System.Drawing.Size(1327, 2);
			labelSolidLineLeft.TabIndex = 32;
			labelSolidLineLeft.Text = "label11";
			labelDevelopedBy.AutoSize = true;
			labelDevelopedBy.Location = new System.Drawing.Point(1152, 820);
			labelDevelopedBy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelDevelopedBy.Name = "labelDevelopedBy";
			labelDevelopedBy.Size = new System.Drawing.Size(186, 17);
			labelDevelopedBy.TabIndex = 33;
			labelDevelopedBy.Text = "Developed by Kenth Jensen";
			buttonExportGraph.Location = new System.Drawing.Point(1033, 825);
			buttonExportGraph.Margin = new System.Windows.Forms.Padding(4);
			buttonExportGraph.Name = "buttonExportGraph";
			buttonExportGraph.Size = new System.Drawing.Size(100, 28);
			buttonExportGraph.TabIndex = 34;
			buttonExportGraph.Text = "Export graph";
			buttonExportGraph.UseVisualStyleBackColor = true;
			buttonExportGraph.Click += new System.EventHandler(buttonExportGraph_Click);
			labelMailAddress.AutoSize = true;
			labelMailAddress.Location = new System.Drawing.Point(1153, 836);
			labelMailAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelMailAddress.Name = "labelMailAddress";
			labelMailAddress.Size = new System.Drawing.Size(164, 17);
			labelMailAddress.TabIndex = 35;
			labelMailAddress.Text = "Mail: kfj4100@gmail.com";
			buttonLoadDataFromDroneAcademy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
			buttonLoadDataFromDroneAcademy.Location = new System.Drawing.Point(1017, 14);
			buttonLoadDataFromDroneAcademy.Margin = new System.Windows.Forms.Padding(4);
			buttonLoadDataFromDroneAcademy.Name = "buttonLoadDataFromDroneAcademy";
			buttonLoadDataFromDroneAcademy.Size = new System.Drawing.Size(235, 28);
			buttonLoadDataFromDroneAcademy.TabIndex = 37;
			buttonLoadDataFromDroneAcademy.Text = "Load data from My.Parrot";
			buttonLoadDataFromDroneAcademy.UseVisualStyleBackColor = true;
			buttonLoadDataFromDroneAcademy.Click += new System.EventHandler(buttonLoadDataFromDroneAcademy_Click);
			buttonLoadDataFromDroneAcademy.MouseHover += new System.EventHandler(buttonLoadDataFromDroneAcademy_MouseHover);
			buttonExportFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
			buttonExportFiles.Location = new System.Drawing.Point(1260, 47);
			buttonExportFiles.Margin = new System.Windows.Forms.Padding(4);
			buttonExportFiles.Name = "buttonExportFiles";
			buttonExportFiles.Size = new System.Drawing.Size(189, 27);
			buttonExportFiles.TabIndex = 38;
			buttonExportFiles.Text = "Export data to files";
			buttonExportFiles.UseVisualStyleBackColor = true;
			buttonExportFiles.Click += new System.EventHandler(buttonExportFiles_Click);
			buttonExportFiles.MouseHover += new System.EventHandler(buttonExportFiles_MouseHover);
			gmap.Bearing = 0f;
			gmap.CanDragMap = true;
			gmap.EmptyTileColor = System.Drawing.Color.Navy;
			gmap.GrayScaleMode = false;
			gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
			gmap.LevelsKeepInMemmory = 5;
			gmap.Location = new System.Drawing.Point(16, 190);
			gmap.Margin = new System.Windows.Forms.Padding(4);
			gmap.MarkersEnabled = true;
			gmap.MaxZoom = 2;
			gmap.MinZoom = 2;
			gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
			gmap.Name = "gmap";
			gmap.NegativeMode = false;
			gmap.PolygonsEnabled = true;
			gmap.RetryLoadTile = 0;
			gmap.RoutesEnabled = true;
			gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
			gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(33, 65, 105, 225);
			gmap.ShowTileGridLines = false;
			gmap.Size = new System.Drawing.Size(1435, 623);
			gmap.TabIndex = 39;
			gmap.Zoom = 0.0;
			gmap.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(gmap_MouseDoubleClick);
			buttonSwitchMapsGraphs.Location = new System.Drawing.Point(19, 825);
			buttonSwitchMapsGraphs.Margin = new System.Windows.Forms.Padding(4);
			buttonSwitchMapsGraphs.Name = "buttonSwitchMapsGraphs";
			buttonSwitchMapsGraphs.Size = new System.Drawing.Size(137, 28);
			buttonSwitchMapsGraphs.TabIndex = 40;
			buttonSwitchMapsGraphs.Text = "Switch to Maps";
			buttonSwitchMapsGraphs.UseVisualStyleBackColor = true;
			buttonSwitchMapsGraphs.Click += new System.EventHandler(buttonSwitchMapsGraphs_Click);
			buttonZoomIn.Location = new System.Drawing.Point(193, 825);
			buttonZoomIn.Margin = new System.Windows.Forms.Padding(4);
			buttonZoomIn.Name = "buttonZoomIn";
			buttonZoomIn.Size = new System.Drawing.Size(100, 28);
			buttonZoomIn.TabIndex = 41;
			buttonZoomIn.Text = "Zoom In";
			buttonZoomIn.UseVisualStyleBackColor = true;
			buttonZoomIn.Click += new System.EventHandler(buttonZoomIn_Click);
			buttonZoomOut.Location = new System.Drawing.Point(323, 825);
			buttonZoomOut.Margin = new System.Windows.Forms.Padding(4);
			buttonZoomOut.Name = "buttonZoomOut";
			buttonZoomOut.Size = new System.Drawing.Size(100, 28);
			buttonZoomOut.TabIndex = 42;
			buttonZoomOut.Text = "Zoom Out";
			buttonZoomOut.UseVisualStyleBackColor = true;
			buttonZoomOut.Click += new System.EventHandler(buttonZoomOut_Click);
			labelPanMapInfo.AutoSize = true;
			labelPanMapInfo.Location = new System.Drawing.Point(444, 831);
			labelPanMapInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelPanMapInfo.Name = "labelPanMapInfo";
			labelPanMapInfo.Size = new System.Drawing.Size(284, 17);
			labelPanMapInfo.TabIndex = 43;
			labelPanMapInfo.Text = "Click and hold left mouse button to pan map";
			buttonSwitchMapMode.Location = new System.Drawing.Point(931, 825);
			buttonSwitchMapMode.Margin = new System.Windows.Forms.Padding(4);
			buttonSwitchMapMode.Name = "buttonSwitchMapMode";
			buttonSwitchMapMode.Size = new System.Drawing.Size(201, 28);
			buttonSwitchMapMode.TabIndex = 44;
			buttonSwitchMapMode.Text = "Switch to Google Earth View";
			buttonSwitchMapMode.UseVisualStyleBackColor = true;
			buttonSwitchMapMode.Click += new System.EventHandler(buttonSwitchMapMode_Click);
			buttonConfigureKmlFile.BackColor = System.Drawing.SystemColors.Control;
			buttonConfigureKmlFile.Location = new System.Drawing.Point(1017, 110);
			buttonConfigureKmlFile.Margin = new System.Windows.Forms.Padding(4);
			buttonConfigureKmlFile.Name = "buttonConfigureKmlFile";
			buttonConfigureKmlFile.Size = new System.Drawing.Size(236, 28);
			buttonConfigureKmlFile.TabIndex = 45;
			buttonConfigureKmlFile.Text = "Configure Kml File altitudes colors";
			buttonConfigureKmlFile.UseVisualStyleBackColor = false;
			buttonConfigureKmlFile.Click += new System.EventHandler(buttonConfigureKmlFile_Click);
			LabelSolidLineRight.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			LabelSolidLineRight.AutoSize = true;
			LabelSolidLineRight.BackColor = System.Drawing.Color.Black;
			LabelSolidLineRight.Location = new System.Drawing.Point(337, 176);
			LabelSolidLineRight.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			LabelSolidLineRight.MaximumSize = new System.Drawing.Size(1327, 2);
			LabelSolidLineRight.MinimumSize = new System.Drawing.Size(1327, 2);
			LabelSolidLineRight.Name = "LabelSolidLineRight";
			LabelSolidLineRight.Size = new System.Drawing.Size(1327, 2);
			LabelSolidLineRight.TabIndex = 46;
			LabelSolidLineRight.Text = "label11";
			labelColorGraph5.AutoSize = true;
			labelColorGraph5.BackColor = System.Drawing.Color.Magenta;
			labelColorGraph5.Location = new System.Drawing.Point(980, 825);
			labelColorGraph5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph5.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph5.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph5.Name = "labelColorGraph5";
			labelColorGraph5.Size = new System.Drawing.Size(33, 7);
			labelColorGraph5.TabIndex = 48;
			labelColorGraph5.Text = " ";
			checkBoxGraph5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grip;
			checkBoxGraph5.AutoSize = true;
			checkBoxGraph5.Checked = true;
			checkBoxGraph5.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph5.Location = new System.Drawing.Point(863, 817);
			checkBoxGraph5.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph5.Name = "checkBoxGraph5";
			checkBoxGraph5.Size = new System.Drawing.Size(121, 21);
			checkBoxGraph5.TabIndex = 47;
			checkBoxGraph5.Text = "Distance flown";
			checkBoxGraph5.UseVisualStyleBackColor = true;
			checkBoxGraph5.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			checkBoxIgnoreNegativeAltitude.AutoSize = true;
			checkBoxIgnoreNegativeAltitude.Location = new System.Drawing.Point(31, 148);
			checkBoxIgnoreNegativeAltitude.Margin = new System.Windows.Forms.Padding(4);
			checkBoxIgnoreNegativeAltitude.Name = "checkBoxIgnoreNegativeAltitude";
			checkBoxIgnoreNegativeAltitude.RightToLeft = System.Windows.Forms.RightToLeft.No;
			checkBoxIgnoreNegativeAltitude.Size = new System.Drawing.Size(246, 21);
			checkBoxIgnoreNegativeAltitude.TabIndex = 49;
			checkBoxIgnoreNegativeAltitude.Text = "Ignore negative altitudes on graph";
			checkBoxIgnoreNegativeAltitude.UseVisualStyleBackColor = true;
			numericUpDownVideoGpsSyncOffset.Location = new System.Drawing.Point(432, 143);
			numericUpDownVideoGpsSyncOffset.Margin = new System.Windows.Forms.Padding(4);
			numericUpDownVideoGpsSyncOffset.Maximum = new decimal(new int[4]
			{
				2000,
				0,
				0,
				0
			});
			numericUpDownVideoGpsSyncOffset.Minimum = new decimal(new int[4]
			{
				2000,
				0,
				0,
				-2147483648
			});
			numericUpDownVideoGpsSyncOffset.Name = "numericUpDownVideoGpsSyncOffset";
			numericUpDownVideoGpsSyncOffset.Size = new System.Drawing.Size(69, 22);
			numericUpDownVideoGpsSyncOffset.TabIndex = 51;
			numericUpDownVideoGpsSyncOffset.Value = new decimal(new int[4]
			{
				1000,
				0,
				0,
				-2147483648
			});
			numericUpDownVideoGpsSyncOffset.ValueChanged += new System.EventHandler(numericUpDownVideoGpsSyncOffset_showTooltip);
			labelSyncOffset0.AutoSize = true;
			labelSyncOffset0.Location = new System.Drawing.Point(307, 91);
			labelSyncOffset0.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelSyncOffset0.Name = "labelSyncOffset0";
			labelSyncOffset0.Size = new System.Drawing.Size(199, 17);
			labelSyncOffset0.TabIndex = 52;
			labelSyncOffset0.Text = "Offset for GPS and video sync";
			labelSyncOffset0.MouseHover += new System.EventHandler(label5_MouseHover);
			labelSyncOffset1.AutoSize = true;
			labelSyncOffset1.Location = new System.Drawing.Point(307, 110);
			labelSyncOffset1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelSyncOffset1.Name = "labelSyncOffset1";
			labelSyncOffset1.Size = new System.Drawing.Size(177, 17);
			labelSyncOffset1.TabIndex = 53;
			labelSyncOffset1.Text = "-900 ms is recomended for";
			labelSyncOffset1.MouseHover += new System.EventHandler(label6_MouseHover);
			checkBoxGraph6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grip;
			checkBoxGraph6.AutoSize = true;
			checkBoxGraph6.Checked = true;
			checkBoxGraph6.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph6.Location = new System.Drawing.Point(677, 837);
			checkBoxGraph6.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph6.Name = "checkBoxGraph6";
			checkBoxGraph6.Size = new System.Drawing.Size(95, 21);
			checkBoxGraph6.TabIndex = 55;
			checkBoxGraph6.Text = "Wi-Fi level";
			checkBoxGraph6.UseVisualStyleBackColor = true;
			checkBoxGraph6.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			checkBoxGraph7.AccessibleRole = System.Windows.Forms.AccessibleRole.Grip;
			checkBoxGraph7.AutoSize = true;
			checkBoxGraph7.Checked = true;
			checkBoxGraph7.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph7.Location = new System.Drawing.Point(832, 837);
			checkBoxGraph7.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph7.Name = "checkBoxGraph7";
			checkBoxGraph7.Size = new System.Drawing.Size(157, 21);
			checkBoxGraph7.TabIndex = 56;
			checkBoxGraph7.Text = "Nr of Satellites used";
			checkBoxGraph7.UseVisualStyleBackColor = true;
			checkBoxGraph7.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			labelColorGraph6.AutoSize = true;
			labelColorGraph6.BackColor = System.Drawing.Color.SkyBlue;
			labelColorGraph6.Location = new System.Drawing.Point(771, 844);
			labelColorGraph6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph6.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph6.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph6.Name = "labelColorGraph6";
			labelColorGraph6.Size = new System.Drawing.Size(33, 7);
			labelColorGraph6.TabIndex = 57;
			labelColorGraph6.Text = " ";
			labelColorGraph7.AutoSize = true;
			labelColorGraph7.BackColor = System.Drawing.Color.SandyBrown;
			labelColorGraph7.Location = new System.Drawing.Point(980, 844);
			labelColorGraph7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph7.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph7.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph7.Name = "labelColorGraph7";
			labelColorGraph7.Size = new System.Drawing.Size(33, 7);
			labelColorGraph7.TabIndex = 58;
			labelColorGraph7.Text = " ";
			checkBoxGraph8.AutoSize = true;
			checkBoxGraph8.Location = new System.Drawing.Point(177, 837);
			checkBoxGraph8.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph8.Name = "checkBoxGraph8";
			checkBoxGraph8.Size = new System.Drawing.Size(225, 21);
			checkBoxGraph8.TabIndex = 59;
			checkBoxGraph8.Text = "Altitude, relative to terrain level";
			checkBoxGraph8.UseVisualStyleBackColor = true;
			checkBoxGraph8.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			checkBoxGraph9.AutoSize = true;
			checkBoxGraph9.Location = new System.Drawing.Point(443, 837);
			checkBoxGraph9.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph9.Name = "checkBoxGraph9";
			checkBoxGraph9.Size = new System.Drawing.Size(159, 21);
			checkBoxGraph9.TabIndex = 60;
			checkBoxGraph9.Text = "Relative terrain level";
			checkBoxGraph9.UseVisualStyleBackColor = true;
			checkBoxGraph9.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			labelColorGraph8.AutoSize = true;
			labelColorGraph8.BackColor = System.Drawing.Color.Indigo;
			labelColorGraph8.Location = new System.Drawing.Point(389, 844);
			labelColorGraph8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph8.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph8.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph8.Name = "labelColorGraph8";
			labelColorGraph8.Size = new System.Drawing.Size(33, 7);
			labelColorGraph8.TabIndex = 61;
			labelColorGraph8.Text = " ";
			labelColorGraph9.AutoSize = true;
			labelColorGraph9.BackColor = System.Drawing.Color.Lime;
			labelColorGraph9.Location = new System.Drawing.Point(595, 844);
			labelColorGraph9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph9.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph9.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph9.Name = "labelColorGraph9";
			labelColorGraph9.Size = new System.Drawing.Size(33, 7);
			labelColorGraph9.TabIndex = 62;
			labelColorGraph9.Text = " ";
			pictureBox1.Image = (System.Drawing.Image)resources.GetObject("pictureBox1.Image");
			pictureBox1.Location = new System.Drawing.Point(867, 80);
			pictureBox1.Margin = new System.Windows.Forms.Padding(4);
			pictureBox1.Name = "pictureBox1";
			pictureBox1.Size = new System.Drawing.Size(115, 52);
			pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			pictureBox1.TabIndex = 63;
			pictureBox1.TabStop = false;
			pictureBox1.Click += new System.EventHandler(pictureBox1_Click);
			pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(pictureBox1_MouseClick);
			pictureBox1.MouseHover += new System.EventHandler(pictureBox1_MouseHover);
			textBoxStatMaxSpeed.Enabled = false;
			textBoxStatMaxSpeed.Location = new System.Drawing.Point(533, 84);
			textBoxStatMaxSpeed.Margin = new System.Windows.Forms.Padding(4);
			textBoxStatMaxSpeed.Name = "textBoxStatMaxSpeed";
			textBoxStatMaxSpeed.Size = new System.Drawing.Size(159, 22);
			textBoxStatMaxSpeed.TabIndex = 64;
			textBoxStatMaxSpeed.Text = "Max speed:";
			textBoxStatMaxSpeed.MouseHover += new System.EventHandler(textBoxStatMaxSpeed_MouseHover);
			textBoxStatMaxAlt.Enabled = false;
			textBoxStatMaxAlt.Location = new System.Drawing.Point(701, 84);
			textBoxStatMaxAlt.Margin = new System.Windows.Forms.Padding(4);
			textBoxStatMaxAlt.Name = "textBoxStatMaxAlt";
			textBoxStatMaxAlt.Size = new System.Drawing.Size(159, 22);
			textBoxStatMaxAlt.TabIndex = 65;
			textBoxStatMaxAlt.Text = "Max altitude:";
			textBoxStatMaxAlt.MouseHover += new System.EventHandler(textBoxStatMaxAlt_MouseHover);
			textBoxStatDistFlown.Enabled = false;
			textBoxStatDistFlown.Location = new System.Drawing.Point(701, 113);
			textBoxStatDistFlown.Margin = new System.Windows.Forms.Padding(4);
			textBoxStatDistFlown.Name = "textBoxStatDistFlown";
			textBoxStatDistFlown.Size = new System.Drawing.Size(159, 22);
			textBoxStatDistFlown.TabIndex = 67;
			textBoxStatDistFlown.Text = "Distance flown:";
			textBoxStatDistFlown.MouseHover += new System.EventHandler(textBoxStatDistFlown_MouseHover);
			textBoxStatMaxDist.Enabled = false;
			textBoxStatMaxDist.Location = new System.Drawing.Point(533, 113);
			textBoxStatMaxDist.Margin = new System.Windows.Forms.Padding(4);
			textBoxStatMaxDist.Name = "textBoxStatMaxDist";
			textBoxStatMaxDist.Size = new System.Drawing.Size(159, 22);
			textBoxStatMaxDist.TabIndex = 66;
			textBoxStatMaxDist.Text = "Max distance:";
			textBoxStatMaxDist.MouseHover += new System.EventHandler(textBoxStatMaxDist_MouseHover);
			textBoxStatComments.Enabled = false;
			textBoxStatComments.Location = new System.Drawing.Point(533, 143);
			textBoxStatComments.Margin = new System.Windows.Forms.Padding(4);
			textBoxStatComments.Name = "textBoxStatComments";
			textBoxStatComments.Size = new System.Drawing.Size(444, 22);
			textBoxStatComments.TabIndex = 68;
			textBoxStatComments.Text = "No comments for this flight";
			textBoxStatComments.DoubleClick += new System.EventHandler(textBoxStatComments_DoubleClick);
			textBoxStatComments.MouseHover += new System.EventHandler(textBoxStatComments_MouseHover);
			checkBoxGraph10.AutoSize = true;
			checkBoxGraph10.Checked = true;
			checkBoxGraph10.CheckState = System.Windows.Forms.CheckState.Checked;
			checkBoxGraph10.Location = new System.Drawing.Point(432, 817);
			checkBoxGraph10.Margin = new System.Windows.Forms.Padding(4);
			checkBoxGraph10.Name = "checkBoxGraph10";
			checkBoxGraph10.Size = new System.Drawing.Size(88, 21);
			checkBoxGraph10.TabIndex = 69;
			checkBoxGraph10.Text = "AirSpeed";
			checkBoxGraph10.UseVisualStyleBackColor = true;
			checkBoxGraph10.MouseUp += new System.Windows.Forms.MouseEventHandler(checkBoxGraphs_MouseUp);
			labelColorGraph10.AutoSize = true;
			labelColorGraph10.BackColor = System.Drawing.Color.DarkKhaki;
			labelColorGraph10.Location = new System.Drawing.Point(520, 825);
			labelColorGraph10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelColorGraph10.MaximumSize = new System.Drawing.Size(0, 7);
			labelColorGraph10.MinimumSize = new System.Drawing.Size(33, 7);
			labelColorGraph10.Name = "labelColorGraph10";
			labelColorGraph10.Size = new System.Drawing.Size(33, 7);
			labelColorGraph10.TabIndex = 70;
			labelColorGraph10.Text = " ";
			labelSyncOffset2.AutoSize = true;
			labelSyncOffset2.Location = new System.Drawing.Point(307, 129);
			labelSyncOffset2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelSyncOffset2.Name = "labelSyncOffset2";
			labelSyncOffset2.Size = new System.Drawing.Size(120, 17);
			labelSyncOffset2.TabIndex = 71;
			labelSyncOffset2.Text = "Bebop and Disco,";
			labelSyncOffset3.AutoSize = true;
			labelSyncOffset3.Location = new System.Drawing.Point(307, 148);
			labelSyncOffset3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			labelSyncOffset3.Name = "labelSyncOffset3";
			labelSyncOffset3.Size = new System.Drawing.Size(95, 17);
			labelSyncOffset3.TabIndex = 72;
			labelSyncOffset3.Text = "0 ms for Anafi";
			base.AutoScaleDimensions = new System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(1459, 866);
			base.Controls.Add(labelSyncOffset3);
			base.Controls.Add(labelSyncOffset2);
			base.Controls.Add(labelColorGraph10);
			base.Controls.Add(checkBoxGraph10);
			base.Controls.Add(textBoxStatComments);
			base.Controls.Add(textBoxStatDistFlown);
			base.Controls.Add(textBoxStatMaxDist);
			base.Controls.Add(textBoxStatMaxAlt);
			base.Controls.Add(textBoxStatMaxSpeed);
			base.Controls.Add(labelColorGraph9);
			base.Controls.Add(labelColorGraph8);
			base.Controls.Add(checkBoxGraph9);
			base.Controls.Add(checkBoxGraph8);
			base.Controls.Add(labelColorGraph7);
			base.Controls.Add(labelColorGraph6);
			base.Controls.Add(checkBoxGraph7);
			base.Controls.Add(checkBoxGraph6);
			base.Controls.Add(labelSyncOffset1);
			base.Controls.Add(labelSyncOffset0);
			base.Controls.Add(numericUpDownVideoGpsSyncOffset);
			base.Controls.Add(checkBoxIgnoreNegativeAltitude);
			base.Controls.Add(labelColorGraph5);
			base.Controls.Add(checkBoxGraph5);
			base.Controls.Add(LabelSolidLineRight);
			base.Controls.Add(buttonConfigureKmlFile);
			base.Controls.Add(buttonSwitchMapMode);
			base.Controls.Add(labelPanMapInfo);
			base.Controls.Add(buttonZoomOut);
			base.Controls.Add(buttonZoomIn);
			base.Controls.Add(buttonSwitchMapsGraphs);
			base.Controls.Add(gmap);
			base.Controls.Add(buttonExportFiles);
			base.Controls.Add(buttonLoadDataFromDroneAcademy);
			base.Controls.Add(labelMailAddress);
			base.Controls.Add(buttonExportGraph);
			base.Controls.Add(labelDevelopedBy);
			base.Controls.Add(labelSolidLineLeft);
			base.Controls.Add(buttonGarminVirbWebsite);
			base.Controls.Add(buttonToolWebsite);
			base.Controls.Add(labelColorGraph4);
			base.Controls.Add(labelColorGraph3);
			base.Controls.Add(labelColorGraph2);
			base.Controls.Add(labelColorGraph1);
			base.Controls.Add(checkBoxGraph4);
			base.Controls.Add(checkBoxGraph3);
			base.Controls.Add(checkBoxGraph2);
			base.Controls.Add(checkBoxGraph1);
			base.Controls.Add(Chart1);
			base.Controls.Add(buttonDonate);
			base.Controls.Add(buttonInstallGarminVirbTemplates);
			base.Controls.Add(labelStopConvertingAt);
			base.Controls.Add(labelStartConvertingAt);
			base.Controls.Add(dateTimePickerConvertTo);
			base.Controls.Add(dateTimePickerConvertFrom);
			base.Controls.Add(textBoxStatus);
			base.Controls.Add(buttonConvertFiles);
			base.Controls.Add(textBoxFileName);
			base.Controls.Add(buttonLoadPudFile);
			base.Controls.Add(pictureBox1);
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.Margin = new System.Windows.Forms.Padding(4);
			MinimumSize = new System.Drawing.Size(998, 745);
			base.Name = "FormMain";
			Text = "FlightData Manager for Parrot Bebop";
			base.FormClosing += new System.Windows.Forms.FormClosingEventHandler(Form1_FormClosing);
			base.Load += new System.EventHandler(Form1_Load);
			base.SizeChanged += new System.EventHandler(FormMain_SizeChanged);
			((System.ComponentModel.ISupportInitialize)Chart1).EndInit();
			((System.ComponentModel.ISupportInitialize)numericUpDownVideoGpsSyncOffset).EndInit();
			((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
