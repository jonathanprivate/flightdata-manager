using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class FormMissingFlights : Form
	{
		private IContainer components;

		private RichTextBox richTextBox1;

		private Button buttonClose;

		public FormMissingFlights()
		{
			InitializeComponent();
		}

		private void buttonClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FormMissingFlights_Load(object sender, EventArgs e)
		{
			base.MaximizeBox = false;
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Location = new Point(220, 100);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.FormMissingFlights));
			richTextBox1 = new System.Windows.Forms.RichTextBox();
			buttonClose = new System.Windows.Forms.Button();
			SuspendLayout();
			richTextBox1.BackColor = System.Drawing.SystemColors.Control;
			richTextBox1.Location = new System.Drawing.Point(12, 12);
			richTextBox1.Name = "richTextBox1";
			richTextBox1.Size = new System.Drawing.Size(710, 209);
			richTextBox1.TabIndex = 0;
			richTextBox1.Text = resources.GetString("richTextBox1.Text");
			buttonClose.Location = new System.Drawing.Point(331, 227);
			buttonClose.Name = "buttonClose";
			buttonClose.Size = new System.Drawing.Size(75, 23);
			buttonClose.TabIndex = 1;
			buttonClose.Text = "Close";
			buttonClose.UseVisualStyleBackColor = true;
			buttonClose.Click += new System.EventHandler(buttonClose_Click);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(734, 260);
			base.Controls.Add(buttonClose);
			base.Controls.Add(richTextBox1);
			base.MinimizeBox = false;
			base.Name = "FormMissingFlights";
			Text = "Missing flights from Drone Academy";
			base.Load += new System.EventHandler(FormMissingFlights_Load);
			ResumeLayout(false);
		}
	}
}
