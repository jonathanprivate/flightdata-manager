using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class FormShowSharedMedia : Form
	{
		private int picVideoIndex = 1;

		private int nrOfPictures;

		private int nrOfVideos;

		private string link = "";

		private double desiredPicSizeRatio;

		private IContainer components;

		private Button buttonClose;

		private Label labelStatus;

		private Button buttonNextPicVideo;

		private WebBrowser webBrowser1;

		private Button buttonPrevPicVideo;

		private Button buttonCopyLink;

		private Button buttonRemoveLink;

		public FormShowSharedMedia()
		{
			InitializeComponent();
		}

		private void FormShowSharedMedia_Load(object sender, EventArgs e)
		{
			base.Location = new Point(110, 6);
			Text = "Shared pictures and videos - FlightData Manager - Version " + DataExchangerClass.currentToolVersion;
			if (!DataExchangerClass.myOwnFlight)
			{
				buttonRemoveLink.Visible = false;
			}
			if (DataExchangerClass.reportStatistics)
			{
				GoogleTracker.trackEvent("Shared pics or video displayed");
			}
			picVideoIndex = 1;
			nrOfPictures = DataExchangerClass.pictureLinksList.Count();
			nrOfVideos = DataExchangerClass.videoLinksList.Count();
			if (nrOfPictures + nrOfVideos < 2)
			{
				buttonNextPicVideo.Enabled = false;
				buttonPrevPicVideo.Enabled = false;
			}
			updatePictureOrVideo();
		}

		private void updatePictureOrVideo(bool updateDueToResize = false)
		{
			if (!updateDueToResize)
			{
				int num = nrOfPictures + nrOfVideos;
				if (picVideoIndex > num)
				{
					picVideoIndex = 1;
				}
				else if (picVideoIndex < 1)
				{
					picVideoIndex = num;
				}
				labelStatus.Text = "Media nr " + picVideoIndex + " of " + num + " (" + nrOfPictures + " pics and " + nrOfVideos + " videos)";
				_ = DataExchangerClass.mediaIdList[picVideoIndex - 1];
				if (picVideoIndex <= nrOfPictures)
				{
					link = DataExchangerClass.pictureLinksList[picVideoIndex - 1];
					Image image = Image.FromStream(((HttpWebResponse)((HttpWebRequest)WebRequest.Create(link)).GetResponse()).GetResponseStream());
					int height = image.Height;
					int width = image.Width;
					desiredPicSizeRatio = (double)width / (double)height;
				}
			}
			if (picVideoIndex <= nrOfPictures)
			{
				link = DataExchangerClass.pictureLinksList[picVideoIndex - 1];
				webBrowser1.ScrollBarsEnabled = false;
				int num2 = webBrowser1.Width;
				int num3 = webBrowser1.Height;
				if ((double)num2 / (double)num3 > desiredPicSizeRatio)
				{
					num2 = Convert.ToInt32((double)num3 * desiredPicSizeRatio);
				}
				else
				{
					num3 = Convert.ToInt32((double)num2 / desiredPicSizeRatio);
				}
				webBrowser1.DocumentText = string.Format("<html><head><title></title></head><body style=\"margin:0;\">{0}</body></html>", "<center><img src=\"" + link + "\" height=\" " + num3 + "\" width=\"" + num2 + "\">");
			}
			else if (!updateDueToResize)
			{
				webBrowser1.ScrollBarsEnabled = false;
				link = DataExchangerClass.videoLinksList[picVideoIndex - 1 - nrOfPictures];
				string text = link.Replace("watch?v=", "embed/");
				string text2 = link.Substring(link.LastIndexOf("=") + 1);
				webBrowser1.DocumentText = string.Format("<html><head><title></title></head><body marginwidth=\"0\" marginheight=\"0\" leftmargin = \"0\" topmargin = \"0\" rightmargin = \"0\" bottommargin = \"0\" >{0}</body></html>", "<iframe width=\"100%\" height=\"100%\" src=\"" + text + "?autoplay=1&vq=hd1080&loop=1&showinfo=0&playlist= " + text2 + "\" frameborder=\"0\" allowfullscreen></iframe>");
			}
		}

		private void buttonPrevPicVideo_Click(object sender, EventArgs e)
		{
			picVideoIndex--;
			updatePictureOrVideo();
		}

		private void buttonNextPicVideo_Click(object sender, EventArgs e)
		{
			picVideoIndex++;
			updatePictureOrVideo();
		}

		private void buttonRemoveLink_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to remove the link to this video/picture from this flight?", "Confirm link removal", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
			{
				return;
			}
			DataExchangerClass.mediaIdToBeUnlinkedList.Add(DataExchangerClass.mediaIdList[picVideoIndex - 1]);
			if (nrOfPictures + nrOfVideos < 2)
			{
				MessageBox.Show("Link removed, last media for this flight");
				Close();
				return;
			}
			MessageBox.Show("Link removed");
			DataExchangerClass.mediaIdList.RemoveAt(picVideoIndex - 1);
			if (picVideoIndex <= nrOfPictures)
			{
				DataExchangerClass.pictureLinksList.RemoveAt(picVideoIndex - 1);
			}
			else
			{
				DataExchangerClass.videoLinksList.RemoveAt(picVideoIndex - 1 - nrOfPictures);
			}
			FormShowSharedMedia_Load(null, null);
		}

		private void buttonCopyLink_Click(object sender, EventArgs e)
		{
			Clipboard.SetText(link);
		}

		private void webBrowser1_Resize(object sender, EventArgs e)
		{
			updatePictureOrVideo(updateDueToResize: true);
		}

		private void buttonClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightDataManager.FormShowSharedMedia));
			buttonClose = new System.Windows.Forms.Button();
			labelStatus = new System.Windows.Forms.Label();
			buttonNextPicVideo = new System.Windows.Forms.Button();
			webBrowser1 = new System.Windows.Forms.WebBrowser();
			buttonPrevPicVideo = new System.Windows.Forms.Button();
			buttonCopyLink = new System.Windows.Forms.Button();
			buttonRemoveLink = new System.Windows.Forms.Button();
			SuspendLayout();
			buttonClose.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			buttonClose.Location = new System.Drawing.Point(1024, 634);
			buttonClose.Name = "buttonClose";
			buttonClose.Size = new System.Drawing.Size(75, 23);
			buttonClose.TabIndex = 0;
			buttonClose.Text = "Close";
			buttonClose.UseVisualStyleBackColor = true;
			buttonClose.Click += new System.EventHandler(buttonClose_Click);
			labelStatus.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			labelStatus.AutoSize = true;
			labelStatus.Location = new System.Drawing.Point(12, 639);
			labelStatus.Name = "labelStatus";
			labelStatus.Size = new System.Drawing.Size(59, 13);
			labelStatus.TabIndex = 1;
			labelStatus.Text = "labelStatus";
			buttonNextPicVideo.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			buttonNextPicVideo.Location = new System.Drawing.Point(277, 634);
			buttonNextPicVideo.Name = "buttonNextPicVideo";
			buttonNextPicVideo.Size = new System.Drawing.Size(54, 23);
			buttonNextPicVideo.TabIndex = 2;
			buttonNextPicVideo.Text = "Next ->";
			buttonNextPicVideo.UseVisualStyleBackColor = true;
			buttonNextPicVideo.Click += new System.EventHandler(buttonNextPicVideo_Click);
			webBrowser1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			webBrowser1.Location = new System.Drawing.Point(12, 12);
			webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
			webBrowser1.Name = "webBrowser1";
			webBrowser1.Size = new System.Drawing.Size(1087, 616);
			webBrowser1.TabIndex = 3;
			webBrowser1.Resize += new System.EventHandler(webBrowser1_Resize);
			buttonPrevPicVideo.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			buttonPrevPicVideo.Location = new System.Drawing.Point(201, 634);
			buttonPrevPicVideo.Name = "buttonPrevPicVideo";
			buttonPrevPicVideo.Size = new System.Drawing.Size(70, 23);
			buttonPrevPicVideo.TabIndex = 5;
			buttonPrevPicVideo.Text = "<- Previous";
			buttonPrevPicVideo.UseVisualStyleBackColor = true;
			buttonPrevPicVideo.Click += new System.EventHandler(buttonPrevPicVideo_Click);
			buttonCopyLink.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			buttonCopyLink.Location = new System.Drawing.Point(888, 634);
			buttonCopyLink.Name = "buttonCopyLink";
			buttonCopyLink.Size = new System.Drawing.Size(117, 23);
			buttonCopyLink.TabIndex = 6;
			buttonCopyLink.Text = "Copy link to clipboard";
			buttonCopyLink.UseVisualStyleBackColor = true;
			buttonCopyLink.Click += new System.EventHandler(buttonCopyLink_Click);
			buttonRemoveLink.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			buttonRemoveLink.Location = new System.Drawing.Point(725, 634);
			buttonRemoveLink.Name = "buttonRemoveLink";
			buttonRemoveLink.Size = new System.Drawing.Size(157, 23);
			buttonRemoveLink.TabIndex = 8;
			buttonRemoveLink.Text = "Remove link to media in flight";
			buttonRemoveLink.UseVisualStyleBackColor = true;
			buttonRemoveLink.Click += new System.EventHandler(buttonRemoveLink_Click);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(1111, 661);
			base.Controls.Add(buttonRemoveLink);
			base.Controls.Add(buttonCopyLink);
			base.Controls.Add(buttonPrevPicVideo);
			base.Controls.Add(webBrowser1);
			base.Controls.Add(buttonNextPicVideo);
			base.Controls.Add(labelStatus);
			base.Controls.Add(buttonClose);
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.MinimizeBox = false;
			MinimumSize = new System.Drawing.Size(745, 486);
			base.Name = "FormShowSharedMedia";
			Text = "Shared pictures and videos";
			base.Load += new System.EventHandler(FormShowSharedMedia_Load);
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
