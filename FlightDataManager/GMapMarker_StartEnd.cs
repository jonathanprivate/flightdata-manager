using GMap.NET;
using GMap.NET.WindowsForms;
using System.Drawing;

namespace FlightDataManager
{
	public class GMapMarker_StartEnd : GMapMarker
	{
		private Pen Pen = new Pen(Brushes.Blue, 3f);

		private Brush _FillBrush = new SolidBrush(Color.Blue);

		private int circleHeight;

		private int circleWidth;

		private string _MyTag;

		private string _markerTitle;

		private PointLatLng _p;

		public string MyTag => _MyTag;

		public GMapMarker_StartEnd(PointLatLng p, string MyTag, string markerTitle, Brush FillBrush)
			: base(p)
		{
			_p = p;
			_MyTag = MyTag;
			_markerTitle = markerTitle;
			_FillBrush = FillBrush;
			base.Size = new Size(5, 5);
			base.Offset = new Point(0, 0);
		}

		public override void OnRender(Graphics g)
		{
			circleHeight = 18;
			circleWidth = 18;
			g.FillEllipse(_FillBrush, base.LocalPosition.X - circleWidth / 2, base.LocalPosition.Y - circleHeight / 2, circleWidth, circleHeight);
			Font font = new Font("Arial", 10f, FontStyle.Bold);
			Pen pen = new Pen(Color.FromArgb(255, Color.White), 5f);
			g.DrawString(_markerTitle, font, pen.Brush, base.LocalPosition.X - 6, base.LocalPosition.Y - 8);
		}
	}
}
