using GMap.NET;
using GMap.NET.WindowsForms;
using System.Drawing;

namespace FlightDataManager
{
	public class GMapMarker_Text : GMapMarker
	{
		private Pen Pen = new Pen(Brushes.Blue, 3f);

		private Brush _FillBrush = new SolidBrush(Color.Blue);

		private int circleHeight;

		private int circleWidth;

		private string _MyTag;

		private string _markerTitle;

		private PointLatLng _p;

		public string MyTag => _MyTag;

		public GMapMarker_Text(PointLatLng p, string MyTag, string markerTitle, Brush FillBrush)
			: base(p)
		{
			_p = p;
			_MyTag = MyTag;
			_markerTitle = markerTitle;
			_FillBrush = FillBrush;
			base.Size = new Size(5, 5);
			base.Offset = new Point(0, 0);
		}

		public override void OnRender(Graphics g)
		{
			circleHeight = 24;
			circleWidth = 24;
			if (_markerTitle.Length > 2)
			{
				circleWidth += 6;
			}
			g.FillEllipse(_FillBrush, base.LocalPosition.X - circleWidth / 2, base.LocalPosition.Y - circleHeight / 2, circleWidth, circleHeight);
			Font font = new Font("Arial", 11f, FontStyle.Bold);
			Pen pen = new Pen(Color.FromArgb(255, Color.White), 5f);
			int num = 6;
			if (_markerTitle.Length == 2)
			{
				num += 4;
			}
			else if (_markerTitle.Length == 3)
			{
				num += 8;
			}
			else if (_markerTitle.Length > 3)
			{
				num += 12;
			}
			g.DrawString(_markerTitle, font, pen.Brush, base.LocalPosition.X - num, base.LocalPosition.Y - 8);
		}
	}
}
