using RestSharp.Contrib;
using System;
using System.Collections;
using System.Globalization;
using System.Net;

namespace FlightDataManager
{
	public static class GoogleTracker
	{
		private static string googleURL = "http://www.google-analytics.com/collect";

		private static string googleVersion = "1";

		private static string googleTrackingID = "UA-62944848-1";

		private static string googleClientID = "555";

		public static void trackEventPos(string eventString, string stringToReport)
		{
			trackEventInternal(eventString, stringToReport, "UserAndPosition", "1");
		}

		public static void trackEvent(string eventString)
		{
			string currentToolVersion = DataExchangerClass.currentToolVersion;
			string englishName = RegionInfo.CurrentRegion.EnglishName;
			int num = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours;
			if (TimeZone.CurrentTimeZone.IsDaylightSavingTime(DateTime.Now))
			{
				num--;
			}
			englishName = englishName + ", Z" + num.ToString("00");
			trackEventInternal(eventString + ", country", englishName, "Version=" + currentToolVersion, "1");
			trackEventInternal(eventString + ", version", "Version=" + currentToolVersion, englishName, "1");
		}

		private static void trackEventInternal(string category, string action, string label, string value)
		{
			try
			{
				Hashtable hashtable = new Hashtable();
				hashtable.Add("v", googleVersion);
				hashtable.Add("tid", googleTrackingID);
				hashtable.Add("cid", googleClientID);
				hashtable.Add("t", "event");
				hashtable.Add("ec", category);
				hashtable.Add("ea", action);
				hashtable.Add("el", label);
				hashtable.Add("ev", value);
				string text = "";
				foreach (object key in hashtable.Keys)
				{
					if (text != "")
					{
						text += "&";
					}
					if (hashtable[key] != null)
					{
						text = text + key.ToString() + "=" + HttpUtility.UrlEncode(hashtable[key].ToString());
					}
				}
				using (WebClient webClient = new WebClient())
				{
					webClient.UploadString(googleURL, "POST", text);
				}
			}
			catch
			{
			}
		}
	}
}
