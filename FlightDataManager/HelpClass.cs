using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

namespace FlightDataManager
{
	public static class HelpClass
	{
		private static bool ReverseGeocodingFailed;

		public static string GetSpeedUnitInVirbSettings()
		{
			string text = LocateUserConfigFile(generateWarningIfFileIsntFound: true);
			if (text == null)
			{
				return "";
			}
			string[] array = File.ReadAllLines(text);
			for (int i = 0; i < array.Count(); i++)
			{
				array[i] = array[i].Trim();
			}
			string text2 = string.Join("", array);
			if (text2.Contains("<setting name=\"Distance\" serializeAs=\"String\"><value>eMetric</value></setting>"))
			{
				return "kmh";
			}
			if (text2.Contains("<setting name=\"Distance\" serializeAs=\"String\"><value>eStatute</value></setting>"))
			{
				return "mph";
			}
			if (text2.Contains("<setting name=\"Distance\" serializeAs=\"String\"><value>eNauticalFeet</value></setting>"))
			{
				return "kph";
			}
			return "mph";
		}

		public static string GetLiquidVolumeUnitInVirbSettings()
		{
			string text = LocateUserConfigFile(generateWarningIfFileIsntFound: true);
			if (text == null)
			{
				return "";
			}
			string[] array = File.ReadAllLines(text);
			for (int i = 0; i < array.Count(); i++)
			{
				array[i] = array[i].Trim();
			}
			string text2 = string.Join("", array);
			if (text2.Contains("<setting name=\"LiquidVolumeUnit\" serializeAs=\"String\"><value>eLiters</value></setting>"))
			{
				return "liters";
			}
			if (text2.Contains("<setting name=\"LiquidVolumeUnit\" serializeAs=\"String\"><value>eGallons</value></setting>"))
			{
				return "usgallons";
			}
			if (text2.Contains("<setting name=\"LiquidVolumeUnit\" serializeAs=\"String\"><value>eImperialGallons</value></setting>"))
			{
				return "ukgallons";
			}
			return "usgallons";
		}

		public static string LocateUserConfigFile(bool generateWarningIfFileIsntFound)
		{
			string text = Environment.ExpandEnvironmentVariables("%LOCALAPPDATA%\\Garmin\\");
			if (!Directory.Exists(text))
			{
				if (generateWarningIfFileIsntFound)
				{
					MessageBox.Show("Directory " + text + " was not found\nMake sure Garmin Virb Edit is installed correctly");
				}
				return null;
			}
			string[] directories = Directory.GetDirectories(text, "Virbedit.exe_Url*");
			if (directories.Count() < 1)
			{
				if (generateWarningIfFileIsntFound)
				{
					MessageBox.Show("Could not find directory containing Virbedit.exe_Url\nMake sure Garmin Virb Edit is installed correctly");
				}
				return null;
			}
			List<string> list = new List<string>();
			string[] array = directories;
			for (int i = 0; i < array.Length; i++)
			{
				string[] directories2 = Directory.GetDirectories(array[i]);
				foreach (string item in directories2)
				{
					list.Add(item);
				}
			}
			if (list.Count() == 0)
			{
				if (generateWarningIfFileIsntFound)
				{
					MessageBox.Show("Could not find any subdirectories under " + text + "\nMake sure Garmin Virb Edit is installed correctly");
				}
				return null;
			}
			if (list.Count() > 1)
			{
				double[] array2 = new double[list.Count()];
				for (int k = 0; k < list.Count(); k++)
				{
					string[] array3 = list[k].Remove(0, list[k].LastIndexOf('\\') + 1).Split('.');
					for (int l = 0; l < array3.Count(); l++)
					{
						double num = Math.Pow(10.0, 14 - l * 3);
						array2[k] += (double)Convert.ToInt32(array3[l]) * num;
					}
				}
				int index = array2.ToList().IndexOf(array2.Max());
				string item2 = list[index];
				list.Clear();
				list.Add(item2);
			}
			if (!File.Exists(list[0] + "\\user.config"))
			{
				if (generateWarningIfFileIsntFound)
				{
					MessageBox.Show("Could not find file user.config under " + list[0] + "\nMake sure Garmin Virb Edit is installed correctly");
				}
				return null;
			}
			return list[0] + "\\user.config";
		}

		public static string ReverseGeocodingFromLatLon(string latitude, string longitude)
		{
			if (ReverseGeocodingFailed)
			{
				return "Address unknown, no resp from Google Maps";
			}
			string uriString = $"http://maps.googleapis.com/maps/api/geocode/xml?latlng={latitude},{longitude}&sensor=false";
			using (WebClient webClient = new WebClient())
			{
				try
				{
					for (int i = 0; i < 10; i++)
					{
						XElement xElement = XElement.Parse(webClient.DownloadString(new Uri(uriString)));
						if ((from elm in xElement.Descendants()
							where elm.Name == "status"
							select elm).FirstOrDefault().Value.ToLower() == "ok")
						{
							XElement xElement2 = (from elm in xElement.Descendants()
								where elm.Name == "formatted_address"
								select elm).FirstOrDefault();
							Thread.Sleep(10);
							return xElement2.Value;
						}
						Thread.Sleep(20);
					}
					return "Address unknown, no resp from Google Maps";
				}
				catch (Exception)
				{
					ReverseGeocodingFailed = true;
					return "Address unknown, no resp from Google Maps";
				}
			}
		}

		public static void UnhideOverlaysWithoutGmetrixInVirb()
		{
			bool flag = false;
			string text = LocateUserConfigFile(generateWarningIfFileIsntFound: false);
			if (text == null)
			{
				return;
			}
			string[] array = File.ReadAllLines(text);
			for (int i = 0; i < array.Count(); i++)
			{
				array[i] = array[i].Trim();
			}
			string text2 = string.Join("", array);
			if (text2.Contains("<setting name=\"HideOverlaysWithoutGmetrix\" serializeAs=\"String\"><value>False</value></setting>"))
			{
				return;
			}
			if (text2.Contains("<setting name=\"HideOverlaysWithoutGmetrix\" serializeAs=\"String\"><value>True</value></setting>"))
			{
				flag = true;
			}
			string[] array2 = File.ReadAllLines(text);
			StreamWriter streamWriter = new StreamWriter(text, append: false);
			if (flag)
			{
				for (int j = 0; j < array2.Count(); j++)
				{
					streamWriter.WriteLine(array2[j]);
					if (array2[j].Contains("<setting name=\"HideOverlaysWithoutGmetrix\"") && array2[j + 1].EndsWith("<value>True</value>"))
					{
						streamWriter.WriteLine("                <value>False</value>");
						j++;
					}
				}
			}
			else
			{
				string[] array3 = array2;
				foreach (string text3 in array3)
				{
					streamWriter.WriteLine(text3);
					if (text3.Contains("<Virb.Properties.Settings>"))
					{
						streamWriter.WriteLine("            <setting name=\"HideOverlaysWithoutGmetrix\" serializeAs=\"String\">");
						streamWriter.WriteLine("                <value>False</value>");
						streamWriter.WriteLine("            </setting>");
					}
				}
			}
			streamWriter.Close();
		}

		public static void SetDefaultSettingForLiquidVolumeInVirb()
		{
			string text = LocateUserConfigFile(generateWarningIfFileIsntFound: false);
			if (text == null)
			{
				return;
			}
			int num = 0;
			string[] array = File.ReadAllLines(text);
			string[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				if (array2[i].Contains("<setting name=\"LiquidVolumeUnit\" serializeAs=\"String\">"))
				{
					num++;
				}
			}
			if (num == 1)
			{
				string[] array3 = File.ReadAllLines(text);
				for (int j = 0; j < array3.Count(); j++)
				{
					array3[j] = array3[j].Trim();
				}
				if (string.Join("", array3).Contains("<setting name=\"LiquidVolumeUnit\" serializeAs=\"String\"><value>"))
				{
					return;
				}
			}
			StreamWriter streamWriter = new StreamWriter(text, append: false);
			if (num == 0)
			{
				array2 = array;
				foreach (string text2 in array2)
				{
					streamWriter.WriteLine(text2);
					if (text2.Contains("<Virb.Properties.Settings>"))
					{
						streamWriter.WriteLine("            <setting name=\"LiquidVolumeUnit\" serializeAs=\"String\">");
						streamWriter.WriteLine("                <value>eLiters</value>");
						streamWriter.WriteLine("            </setting>");
					}
				}
			}
			if (num == 1)
			{
				int num2 = 0;
				for (int k = 0; k < array.Length; k++)
				{
					if (num2 == 0)
					{
						streamWriter.WriteLine(array[k]);
					}
					if (array[k].Contains("<setting name=\"LiquidVolumeUnit\" serializeAs=\"String\">"))
					{
						num2 = k;
					}
					if (num2 > 0 && k > num2 && array[k].Contains("value"))
					{
						streamWriter.WriteLine("                <value>eLiters</value>");
						num2 = 0;
					}
				}
			}
			if (num == 2)
			{
				int num3 = 0;
				bool flag = false;
				for (int l = 0; l < array.Length; l++)
				{
					if (array[l].Contains("<setting name=\"LiquidVolumeUnit\" serializeAs=\"String\">"))
					{
						num3 = l;
						if (flag)
						{
							streamWriter.WriteLine(array[l]);
						}
						continue;
					}
					if (num3 == 0)
					{
						streamWriter.WriteLine(array[l]);
					}
					if (num3 > 0 && l - 1 == num3 && array[l].Contains("value") && flag)
					{
						streamWriter.WriteLine("                <value>eLiters</value>");
					}
					if (num3 > 0 && l - 2 == num3 && array[l].Contains("</setting>"))
					{
						if (flag)
						{
							streamWriter.WriteLine(array[l]);
						}
						flag = true;
						num3 = 0;
					}
				}
			}
			streamWriter.Close();
		}
	}
}
