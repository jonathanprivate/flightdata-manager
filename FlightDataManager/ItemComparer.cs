using System;
using System.Collections;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class ItemComparer : IComparer
	{
		public int Column
		{
			get;
			set;
		}

		public SortOrder Order
		{
			get;
			set;
		}

		public ItemComparer(int colIndex)
		{
			Column = colIndex;
			Order = SortOrder.None;
		}

		public int Compare(object a, object b)
		{
			if (Column == 100)
			{
				return 0;
			}
			ListViewItem listViewItem = a as ListViewItem;
			ListViewItem listViewItem2 = b as ListViewItem;
			int num;
			if (listViewItem == null && listViewItem2 == null)
			{
				num = 0;
			}
			else if (listViewItem == null)
			{
				num = -1;
			}
			else if (listViewItem2 == null)
			{
				num = 1;
			}
			if (listViewItem == listViewItem2)
			{
				num = 0;
			}
			if (Column == 0 || Column == 1)
			{
				string s = listViewItem.SubItems[0].Text + " " + listViewItem.SubItems[1].Text;
				string s2 = listViewItem2.SubItems[0].Text + " " + listViewItem2.SubItems[1].Text;
				if (!DateTime.TryParse(s, out DateTime result))
				{
					result = DateTime.MinValue;
				}
				if (!DateTime.TryParse(s2, out DateTime result2))
				{
					result2 = DateTime.MinValue;
				}
				num = DateTime.Compare(result, result2);
				if (result != DateTime.MinValue && result2 != DateTime.MinValue)
				{
					goto IL_0337;
				}
			}
			if (!decimal.TryParse(listViewItem.SubItems[Column].Text, out decimal result3))
			{
				result3 = decimal.MinValue;
			}
			if (!decimal.TryParse(listViewItem2.SubItems[Column].Text, out decimal result4))
			{
				result4 = decimal.MinValue;
			}
			num = decimal.Compare(result3, result4);
			if (!(result3 != decimal.MinValue) || !(result4 != decimal.MinValue))
			{
				try
				{
					if (Column == 3 || Column == 4 || Column == 5 || Column == 6)
					{
						double value = Convert.ToDouble(listViewItem.SubItems[Column].Text.Split(' ')[0]);
						double value2 = Convert.ToDouble(listViewItem2.SubItems[Column].Text.Split(' ')[0]);
						num = decimal.Compare((decimal)value, (decimal)value2);
						goto IL_0337;
					}
					if (Column == 7)
					{
						double num2 = Convert.ToDouble(listViewItem.SubItems[Column].Text.Split(' ')[0]);
						double num3 = Convert.ToDouble(listViewItem.SubItems[Column].Text.Split(' ')[2]);
						double value3 = num2 - num3;
						double num4 = Convert.ToDouble(listViewItem2.SubItems[Column].Text.Split(' ')[0]);
						double num5 = Convert.ToDouble(listViewItem2.SubItems[Column].Text.Split(' ')[2]);
						double value4 = num4 - num5;
						num = decimal.Compare((decimal)value3, (decimal)value4);
						goto IL_0337;
					}
				}
				catch
				{
					num = 1;
				}
				num = string.Compare(listViewItem.SubItems[Column].Text, listViewItem2.SubItems[Column].Text);
			}
			goto IL_0337;
			IL_0337:
			if (Order == SortOrder.Descending)
			{
				num *= -1;
			}
			return num;
		}
	}
}
