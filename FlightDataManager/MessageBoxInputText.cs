using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class MessageBoxInputText : Form
	{
		public static int dialogAdvancedResult = 0;

		public static string textResult = "";

		private IContainer components;

		private Label label1;

		private Button button2;

		private Button button1;

		private TextBox textBox1;

		public MessageBoxInputText()
		{
			InitializeComponent();
		}

		public MessageBoxInputText(string title, string message, string button1Text, string button2Text)
		{
			InitializeComponent();
			base.ShowIcon = false;
			base.MinimizeBox = false;
			base.MaximizeBox = false;
			Text = title;
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			label1.Text = message;
			button1.Text = button1Text;
			button2.Text = button2Text;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (textBox1.Text.Length > 2)
			{
				textResult = textBox1.Text;
				dialogAdvancedResult = 1;
				Close();
			}
			else
			{
				textBox1.Text = "Enter info here";
				textBox1.SelectAll();
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			dialogAdvancedResult = 2;
			Close();
		}

		private void MessageBoxInputText_Load(object sender, EventArgs e)
		{
			textBox1.Select();
		}

		private void textBox1_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				button1.PerformClick();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			label1 = new System.Windows.Forms.Label();
			button2 = new System.Windows.Forms.Button();
			button1 = new System.Windows.Forms.Button();
			textBox1 = new System.Windows.Forms.TextBox();
			SuspendLayout();
			label1.AutoSize = true;
			label1.BackColor = System.Drawing.SystemColors.Control;
			label1.Location = new System.Drawing.Point(2, 1);
			label1.MinimumSize = new System.Drawing.Size(415, 50);
			label1.Name = "label1";
			label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			label1.Size = new System.Drawing.Size(415, 50);
			label1.TabIndex = 7;
			label1.Text = "label1";
			label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			button2.AutoSize = true;
			button2.Location = new System.Drawing.Point(232, 93);
			button2.Name = "button2";
			button2.Size = new System.Drawing.Size(75, 23);
			button2.TabIndex = 6;
			button2.Text = "button2";
			button2.UseVisualStyleBackColor = true;
			button2.Click += new System.EventHandler(button2_Click);
			button1.AutoSize = true;
			button1.Location = new System.Drawing.Point(100, 93);
			button1.Name = "button1";
			button1.Size = new System.Drawing.Size(75, 23);
			button1.TabIndex = 5;
			button1.Text = "button1";
			button1.UseVisualStyleBackColor = true;
			button1.Click += new System.EventHandler(button1_Click);
			textBox1.Location = new System.Drawing.Point(144, 64);
			textBox1.Name = "textBox1";
			textBox1.Size = new System.Drawing.Size(125, 20);
			textBox1.TabIndex = 8;
			textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(textBox1_KeyDown);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(420, 128);
			base.Controls.Add(textBox1);
			base.Controls.Add(label1);
			base.Controls.Add(button2);
			base.Controls.Add(button1);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "MessageBoxInputText";
			Text = "MessageBoxInputText";
			base.Load += new System.EventHandler(MessageBoxInputText_Load);
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
