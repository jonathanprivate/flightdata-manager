using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace FlightDataManager
{
	public class MessageBoxInputTextWithDefaultText : Form
	{
		public static int dialogAdvancedResult = 0;

		public static string textResult = "";

		public static string defaultText = "";

		private IContainer components;

		private TextBox textBox1;

		private Label label1;

		private Button button2;

		private Button button1;

		public MessageBoxInputTextWithDefaultText()
		{
			InitializeComponent();
		}

		public MessageBoxInputTextWithDefaultText(string title, string message, string button1Text, string button2Text)
		{
			InitializeComponent();
			base.ShowIcon = false;
			base.MinimizeBox = false;
			base.MaximizeBox = false;
			Text = title;
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			label1.Text = message;
			button1.Text = button1Text;
			button2.Text = button2Text;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			textResult = textBox1.Text;
			dialogAdvancedResult = 1;
			Close();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			dialogAdvancedResult = 2;
			Close();
		}

		private void MessageBoxInputTextWithDefaultText_Load(object sender, EventArgs e)
		{
			textBox1.Text = defaultText;
			textBox1.Select();
		}

		private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
			{
				textResult = textBox1.Text;
				dialogAdvancedResult = 1;
				Close();
			}
			else if (e.KeyChar == '\u001b')
			{
				dialogAdvancedResult = 2;
				Close();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			textBox1 = new System.Windows.Forms.TextBox();
			label1 = new System.Windows.Forms.Label();
			button2 = new System.Windows.Forms.Button();
			button1 = new System.Windows.Forms.Button();
			SuspendLayout();
			textBox1.Location = new System.Drawing.Point(31, 33);
			textBox1.Name = "textBox1";
			textBox1.Size = new System.Drawing.Size(361, 20);
			textBox1.TabIndex = 12;
			textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(textBox1_KeyPress);
			label1.AutoSize = true;
			label1.BackColor = System.Drawing.SystemColors.Control;
			label1.Location = new System.Drawing.Point(3, 0);
			label1.MinimumSize = new System.Drawing.Size(415, 30);
			label1.Name = "label1";
			label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			label1.Size = new System.Drawing.Size(415, 30);
			label1.TabIndex = 11;
			label1.Text = "label1";
			label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			button2.AutoSize = true;
			button2.Location = new System.Drawing.Point(233, 62);
			button2.Name = "button2";
			button2.Size = new System.Drawing.Size(75, 23);
			button2.TabIndex = 10;
			button2.Text = "button2";
			button2.UseVisualStyleBackColor = true;
			button2.Click += new System.EventHandler(button2_Click);
			button1.AutoSize = true;
			button1.Location = new System.Drawing.Point(101, 62);
			button1.Name = "button1";
			button1.Size = new System.Drawing.Size(75, 23);
			button1.TabIndex = 9;
			button1.Text = "button1";
			button1.UseVisualStyleBackColor = true;
			button1.Click += new System.EventHandler(button1_Click);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(420, 98);
			base.Controls.Add(textBox1);
			base.Controls.Add(label1);
			base.Controls.Add(button2);
			base.Controls.Add(button1);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "MessageBoxInputTextWithDefaultText";
			Text = "MessageBoxInputTextWithDefaultText";
			base.Load += new System.EventHandler(MessageBoxInputTextWithDefaultText_Load);
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
