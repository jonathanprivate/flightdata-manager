using System;
using System.Windows.Forms;

namespace FolderSelect
{
	public class FolderSelectDialog
	{
		private OpenFileDialog ofd;

		public string InitialDirectory
		{
			get
			{
				return ofd.InitialDirectory;
			}
			set
			{
				ofd.InitialDirectory = ((value == null || value.Length == 0) ? Environment.CurrentDirectory : value);
			}
		}

		public string Title
		{
			get
			{
				return ofd.Title;
			}
			set
			{
				ofd.Title = ((value == null) ? "Select a folder" : value);
			}
		}

		public string FileName => ofd.FileName;

		public FolderSelectDialog()
		{
			ofd = new OpenFileDialog();
			ofd.Filter = "Folders|\n";
			ofd.AddExtension = false;
			ofd.CheckFileExists = false;
			ofd.DereferenceLinks = true;
			ofd.Multiselect = false;
		}

		public bool ShowDialog()
		{
			return ShowDialog(IntPtr.Zero);
		}

		public bool ShowDialog(IntPtr hWndOwner)
		{
			bool flag = false;
			if (Environment.OSVersion.Version.Major >= 6)
			{
				Reflector reflector = new Reflector("System.Windows.Forms");
				uint num = 0u;
				Type type = reflector.GetType("FileDialogNative.IFileDialog");
				object obj = reflector.Call(ofd, "CreateVistaDialog");
				reflector.Call(ofd, "OnBeforeVistaDialog", obj);
				uint num2 = (uint)reflector.CallAs(typeof(FileDialog), ofd, "GetOptions");
				num2 |= (uint)reflector.GetEnum("FileDialogNative.FOS", "FOS_PICKFOLDERS");
				reflector.CallAs(type, obj, "SetOptions", num2);
				object obj2 = reflector.New("FileDialog.VistaDialogEvents", ofd);
				object[] array = new object[2]
				{
					obj2,
					num
				};
				reflector.CallAs2(type, obj, "Advise", array);
				num = (uint)array[1];
				try
				{
					int num3 = (int)reflector.CallAs(type, obj, "Show", hWndOwner);
					return num3 == 0;
				}
				finally
				{
					reflector.CallAs(type, obj, "Unadvise", num);
					GC.KeepAlive(obj2);
				}
			}
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			folderBrowserDialog.Description = Title;
			folderBrowserDialog.SelectedPath = InitialDirectory;
			folderBrowserDialog.ShowNewFolderButton = false;
			if (folderBrowserDialog.ShowDialog(new WindowWrapper(hWndOwner)) != DialogResult.OK)
			{
				return false;
			}
			ofd.FileName = folderBrowserDialog.SelectedPath;
			return true;
		}
	}
}
